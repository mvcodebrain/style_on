import 'dart:io';



import 'dart:convert';

import 'package:yesmadam/modals/sersubcat_model.dart';


import 'package:yesmadam/services/apis.dart';

abstract class ServsubcategoryRepos {
  Future<List<Data>> getServices(catId,subId);
}

class ServsubcategoryReposImp extends ServsubcategoryRepos {
  
  @override
  Future<List<Data>> getServices(catId,subId) async {
    var dataas = {"cat_id": catId,"sub_id":subId};

    var res = await apiPostRequest(Apis.GETSERVICE_API,dataas);

    if (res != null) {
      var data = json.decode(res);
      // List<Data> sercats = ServsubcatModel.fromJson(data).response;
      // return sercats;
    }else{
      throw Exception();
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    //request.headers.set('api-key' , Apis.SIGNUP_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }
}


// abstract class ServicesRepos {
//   Future<List<Services>> getServices();
// }

// class ServiceRepoImp extends ServicesRepos {
//   @override
//   Future<List<Services>> getServices() async{
//     // TODO: implement getServices
//     var data = {
//         "cat_id":8,
//         "sub_id":4
//     };
//     var res = await apiPostRequest(Strings.subcats,data);
//     if (res != null) {
//       var data = json.decode(res);
//       List<Services> serv = ServsubcatModel.fromJson(data).services;
//       return serv;
//     }
//   }

//   Future<String> apiPostRequest(String url, data) async {
//     HttpClient httpClient = new HttpClient();
//     HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
//     request.headers.set('content-type', 'application/json');
//     //request.headers.set('api-key' , Apis.SIGNUP_API);
//     request.add(utf8.encode(json.encode(data)));
//     HttpClientResponse response = await request.close();
//     // todo - you should check the response.statusCode
//     String reply = await response.transform(utf8.decoder).join();
//     httpClient.close();
//     return reply;
//   }

// }

