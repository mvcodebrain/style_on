import 'dart:io';



import 'dart:convert';

import 'package:yesmadam/modals/subcategory_model.dart';
import 'package:yesmadam/services/apis.dart';

abstract class SubcategoryRepos {
  Future<List<SubCategory>> getSubcategory(catId);
}

class SubcategoryRepoImp extends SubcategoryRepos {
  
  @override
  Future<List<SubCategory>> getSubcategory(catId) async {
    var data = {"cat_id": catId};
    var res = await apiPostRequest(Apis.SUBCATEGORY_API,data);
    if (res != null) {
      var data = json.decode(res);
      List<SubCategory> subcats = SubcatsModel.fromJson(data).subCategory;
      return subcats;
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    //request.headers.set('api-key' , Apis.SIGNUP_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }
}
