

import 'dart:async';
import 'dart:convert';
import 'dart:io';
// import 'package:upgrader/upgrader.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter_google_places/flutter_google_places.dart';
// import 'package:geocoder/geocoder.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

// import 'package:google_maps_webservice/places.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/seatbooking/salondetail.dart';
import 'package:yesmadam/services/apis.dart';

import 'loactionset.dart';





class saloonPage extends StatefulWidget {
  final catid;
  final pincode;
  final lattitude;
  final longitude;
  saloonPage(this.catid,this.pincode,this.lattitude,this.longitude);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return saloonPageLoader();
  }
}
class saloonPageLoader extends State<saloonPage> {
  // final appcastURL =
  //     'https://play.google.com/store/apps/details?id=com.makeover.nest';
  // final cfg = AppcastConfiguration(url: "https://play.google.com/store/apps/details?id=com.makeover.nest", supportedOS: ['android']);


  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  // List addresses = [];
  var lattitude = "";
  var longitude = "";
  var pin="";
  var sexx ="";





  List saloons = [];
  List sliders=[];
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  // void getPincode() async {
  //   setState(() {
  //     loading = true;
  //     ani = false;
  //     fail = false;
  //     _color = Colors.black.withOpacity(0.3);
  //     // Navigator.pop(context);
  //   });
  //
  //   var url = "https://styleon.thedigitalkranti.com/api/customer/saloon/find";
  //   // var data = json.encode(map);
  //   var data = {
  //     'pincode':"201301",
  //     'cat_id':"40"
  //   };
  //   print(data);
  //   // var res = await apiPostRequest(url,data);
  //   http.Response res = await http.post(url,body: json.encode(data),headers: {'Content-Type':'application/json'});
  //   print("hhhhh"+res.body.toString());
  //   setState(() {
  //     // if(json.decode(res)['status'].toString() == "1"){
  //     //   saloons = json.decode(res)['data'];
  //     //   print("jhhhjhj"+saloons.length.toString());
  //     //
  //     // }else{
  //     //   saloons = [];
  //     // }
  //     loading = false;
  //   });
  //
  //
  // }

  Future getpinfind() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.PINCODE_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {

        "pincode":widget.pincode,
        "cat_id":widget.catid,
      "latitude":widget.lattitude,
      "longitude":widget.longitude

    };
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        saloons = json.decode(res)['data'];
        // orderid=json.decode(res)['data'][0]['id'];
        print(saloons);



      } else {
        saloons = [];


      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future getslider() async {

    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.GETSLIDERS_API;
      var res = await apiGetRequest(url);
      if(json.decode(res)['status']=="1"){
        setState(() {
          sliders=json.decode(res)['sliders'];
        });
      }

      print(res);


    }

    catch (e)
    {
      print("Network Fail");
    }
  }
  Future<String> apiGetRequest(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }




  @override
  void initState() {
    getpinfind();

    super.initState();





    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < 5) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }

      _pageController.animateToPage(
        _currentPage,
        duration: Duration(milliseconds: 350),
        curve: Curves.easeIn,
      );
    });
    // getPincode();


     //getLocation();

  }
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>() ;
  _setIndex(int index){
    setState((){
      _currentPage = index;
    });
  }
  int _currentPage = 0;
  PageController _pageController =
  PageController(initialPage: 0, viewportFraction: 0.85);






  @override
  Widget build(BuildContext context) {
   // Upgrader().clearSavedSettings();


    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,

        title:
        InkWell(
          onTap: (){
            // Navigator.push(context, MaterialPageRoute(builder: (context)=>LocationPage())).then((value) => getLocation());

          },
          child: Text(
             "Saloon",
            style: TextStyle(color: AppColors.white,fontSize: 18),
          ),
        ),
        iconTheme: IconThemeData(color: AppColors.white),





      ),
      body:
      // UpgradeAlert(
      //   appcastConfig: cfg,
      //   debugLogging: true,
      //   showIgnore: true,
      //   //showLater: false,
      //   canDismissDialog: false,
      //  // debugAlwaysUpgrade: false,
      //     dialogStyle: UpgradeDialogStyle.cupertino,
      //
      //
      //   minAppVersion: '1.0.0',
        //child:
        CustomScrollView(
          slivers: [
            // SliverToBoxAdapter(
            //   child: Container(
            //     padding: EdgeInsets.only(top: 0),
            //     child: corosolSlider()
            //   ),
            //
            // ),
            // SliverToBoxAdapter(
            //   child://saloons.length == 0?Container():
            //     sliders.length == 0 ?Container():filter(),
            // ),
            SliverToBoxAdapter(
              child:
            //Saloonlist()
              Container
                (
                //padding: EdgeInsets.only(top: sliders.length != 0?5:0),
                child: saloons.length == 0?
                Container(

                  child: Column(
                    children: [

                      Container(

                        child: Image(
                          image: AssetImage("assets/images/sorry.png"),
                        height: 150,
                        )
                      ),
                      Center(
                        child: Container(
                          padding: EdgeInsets.only(top: 8,bottom: 8,left: 5,right: 5),
                          decoration: BoxDecoration(
                              color: AppColors.primary,
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10)


                              )
                          ),

                          width: MediaQuery.of(context).size.width/1.5,
                          child: Center(child: Text(
                            "Ahh we are not serving here. We are working hard to serve at your location",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: AppColors.white
                            ),
                          )),
                        ),
                      ),
                      // Container(
                      //   transform: Matrix4.translationValues(0, 15, 0),
                      //   width: MediaQuery.of(context).size.width/2,
                      //   color: AppColors.black,
                      //   child:
                      //   FlatButton(
                      //     onPressed: (){
                      //       Navigator.push(context, MaterialPageRoute(builder: (context)=>LocationPage())).then((value) => getLocation());
                      //     },
                      //     child: Text("Choose Other Location",style: TextStyle(
                      //       color: AppColors.white,fontSize: 16
                      //     ),),
                      //   ),
                      // )
                    ],
                  ),
                ) :Saloonlist(),
              ),
            )
          ],
       // ),
      ),




    );
  }
  Widget Saloonlist() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      //height: 205,
      //color: Colors.grey,
       //color: AppColors.sentColor,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 7, right: 7,top:sliders.length != 0?5:0,bottom: 20),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        //scrollDirection: Axis.horizontal,
        itemCount:

       saloons.length
        ,
        itemBuilder: (context, i) {
          return Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            elevation: 2.0,
            child: Container(
              //height: 310,
              width: MediaQuery.of(context).size.width,
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>salondetailPage(saloonid: saloons[i]['id'],vendorid: saloons[i]['vendorid'],name: saloons[i]['name'],)));
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(

                      child: Container(
                        height: 200,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12)),
                          image: DecorationImage(
                              image:
                              // cats.image != null ? NetworkImage(cats.image.src):
                              NetworkImage(
                                  //"https://thumbs.dreamstime.com/b/hair-salon-situation-21768339.jpg"
                                  saloons[i]['image'][0]
                              ),
                              fit: BoxFit.fill),
                        ),

                        // height: 300,
                        // width: MediaQuery.of(context).size.width,

                        //height: 65,
                        // width: 65,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10,left: 10,right: 10),
                      child:
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(
                                  //"Raj Saloon"
                                  saloons[i]['name']
                                  ,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,fontWeight: FontWeight.bold
                                  ),textAlign: TextAlign.start,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(right: 10),

                                child: Text(
                                  saloons[i]['gender'] != null ? saloons[i]['gender']:""
                                  ,
                                  style: TextStyle(
                                      color: AppColors.splash,fontSize: 16,fontWeight: FontWeight.w400

                                  ),),
                              ),
                              // Container(
                              //   child: Row(
                              //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                              //
                              //     children: [
                              //       Container(
                              //           padding: EdgeInsets.only(right:0),
                              //           color:AppColors.black,
                              //           child:
                              //           Icon(Icons.star,size: 15,color: AppColors.white,)
                              //       ),
                              //       Container(
                              //         padding: EdgeInsets.only(left: 10),
                              //         child: RichText(
                              //           text: new TextSpan(
                              //               text: saloons[i]['raiting'],
                              //               style: TextStyle(
                              //                 color: Colors.black,
                              //                 fontWeight: FontWeight.bold,
                              //                 fontSize: 14,
                              //               ),
                              //               children: [
                              //                 new TextSpan(
                              //                     text: "/5",
                              //                     style: TextStyle(
                              //                         color: Colors.black54,
                              //                         fontWeight: FontWeight.bold,
                              //                         fontSize: 14
                              //                     )
                              //                 ),
                              //
                              //               ]
                              //           ),
                              //
                              //         ),
                              //       )
                              //     ],
                              //   ),
                              //
                              //
                              // )
                            ],
                          ),

                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   children: [
                          //     Container(
                          //       padding: EdgeInsets.only(right:0,top: 5),
                          //       child: Text("haircut"
                          //         //saloons[i]['service_name'] != null ?saloons[i]['service_name']:""
                          //         ,
                          //         style: TextStyle(
                          //           color: AppColors.grayText,
                          //           fontSize: 15,
                          //         ),textAlign: TextAlign.start,
                          //       ),
                          //     ),
                          //     Container(
                          //       child: Row(
                          //         mainAxisAlignment: MainAxisAlignment.spaceAround,
                          //
                          //         children: [
                          //           Container(
                          //               padding: EdgeInsets.only(right:0,top: 5),
                          //
                          //               child:
                          //               Text("₹ 100"
                          //               //saloons[i]['charge'] != null ?"For Starting ₹ "+saloons[i]['charge'].toString():""
                          //                 ,style: TextStyle(
                          //                   color: AppColors.grayText,fontSize: 14
                          //               ),)
                          //           ),
                          //
                          //         ],
                          //       ),
                          //
                          //
                          //     )
                          //   ],
                          // ),
                          // // Row(
                          // //   mainAxisAlignment: MainAxisAlignment.end,
                          // //   children: [
                          // //     Container(
                          // //       padding: EdgeInsets.only(right: 10),
                          // //
                          // //       child: Text(saloons[i]['saloon_type'] != null ? saloons[i]['saloon_type']:"",
                          // //         style: TextStyle(
                          // //             color: AppColors.background,fontSize: 16,fontWeight: FontWeight.w400
                          // //
                          // //         ),),
                          // //     ),
                          // //   ],
                          // // ),
                          // Divider(
                          //   thickness: 1,
                          // ),
                          // Container(
                          //   padding: EdgeInsets.only(bottom: 10),
                          //   child: Row(
                          //     children: [
                          //
                          //       Expanded
                          //         (
                          //           flex: 1,
                          //
                          //           child:
                          //           Row(
                          //             mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                          //             children: [
                          //               Container(
                          //                 child: Row(
                          //                   mainAxisAlignment: MainAxisAlignment.spaceAround,
                          //
                          //                   children: [
                          //                     Container(
                          //                         padding: EdgeInsets.only(right:0),
                          //                         color:AppColors.black,
                          //                         child:
                          //                         Icon(Icons.star,size: 15,color: AppColors.white,)
                          //                     ),
                          //                     Container(
                          //                       padding: EdgeInsets.only(left: 10),
                          //                       child: RichText(
                          //                         text: new TextSpan(
                          //                             text: "4.5"
                          //                             //saloons[i]['raiting']
                          //                             ,
                          //                             style: TextStyle(
                          //                               color: Colors.black,
                          //                               fontWeight: FontWeight.bold,
                          //                               fontSize: 14,
                          //                             ),
                          //                             children: [
                          //                               new TextSpan(
                          //                                   text: "/5",
                          //                                   style: TextStyle(
                          //                                       color: Colors.black54,
                          //                                       fontWeight: FontWeight.bold,
                          //                                       fontSize: 14
                          //                                   )
                          //                               ),
                          //
                          //                             ]
                          //                         ),
                          //
                          //                       ),
                          //                     )
                          //                   ],
                          //                 ),
                          //
                          //
                          //               )
                          //             ],
                          //           ) ),
                          //       Expanded
                          //         (
                          //           flex: 1,
                          //
                          //           child:
                          //           Row(
                          //             mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                          //             children: [
                          //               Container(
                          //
                          //                 height: 30,
                          //                 child: Image(image: AssetImage("assets/images/seat.png"), ),
                          //               ),
                          //
                          //
                          //               Container(
                          //                 child:Text("5 Seats"
                          //                     // saloons[i]['seats'] != null ?saloons[i]['seats']+
                          //                     //     " Seats":""
                          //                 )
                          //                 ,)
                          //
                          //             ],
                          //           ) ),
                          //       Expanded(
                          //           flex: 1,
                          //
                          //           child:
                          //           Row(
                          //             mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                          //             children: [
                          //               Container(
                          //
                          //                   height: 30,
                          //                   child: Icon(Icons.location_on)
                          //               ),
                          //
                          //
                          //               Container(
                          //                 child:Text("2"
                          //                     //saloons[i]['distance']+
                          //                     "KM"
                          //                 ),
                          //               )
                          //             ],
                          //           ) )
                          //     ],
                          //   ),
                          // )
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 5,bottom: 10),
                          child: Icon(Icons.location_on,color: AppColors.black,size: 20,),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(top: 5,left: 10,bottom: 20,right: 15),
                            child:  Text(
                              //"Raj Saloon"
                              saloons[i]['address'] + ","+ saloons[i]['city'] + ","+ saloons[i]['state']
                              ,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                              ),textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget corosolSlider() {
    return
      Container(
//      color: Colors.white,
        height:
        //sliders.length != 0?
        200
           // :0
        ,
        margin: EdgeInsets.only(
            top:10,left: 0,right: 0
        ),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  children: [
                    Container(
                      height: 180,
                      child: PageView.builder(
                        allowImplicitScrolling: true,
                        controller: _pageController,
                        scrollDirection: Axis.horizontal,
                        itemCount:
                        sliders.length != null ?sliders.length:"",
                        //CategoryList.length,
                        reverse: false,
                        pageSnapping: true,
                        onPageChanged: (val) => _setIndex(val),
                        itemBuilder: (context, i)

                        {
                          return Container(
                              padding: EdgeInsets.only(
                                  right: 10,
                                  left: 0
                              ),
                              transform: Matrix4.translationValues(-15, 0, 0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: AppColors.black,
                                image: DecorationImage(
                                    image:NetworkImage(
                                        //"https://thumbs.dreamstime.com/b/hair-salon-situation-21768339.jpg"
                                       sliders[i]['image']
                                    ),
                                    fit: BoxFit.cover
                                ),
                                // borderRadius:
                                //BorderRadius.all(Radius.circular(10)),
                                // border: Border.all(color: Colors.black26)
                              ),
                            ),);
                        },
                      ),
                    ),
                    Expanded(
                      child: Center(
                        child: Container(

                          padding: EdgeInsets.only(top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(
                               sliders.length != null ?sliders.length:""
                                ,
                                // CategoryList.length,
                                    (index) => Container(
                                    padding: EdgeInsets.only(right: 10),
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                          color: _currentPage == index ? Colors.blueAccent : Colors.white,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(120)),
                                          boxShadow : [
                                            BoxShadow(
                                                color: Colors.black
                                            )
                                          ]
                                      ),
                                    ))),
                          ),
                        ),
                      ),
                    )
                  ],
                ))
          ],
        ),
      );
  }



}
