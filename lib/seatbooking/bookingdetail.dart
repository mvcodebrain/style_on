import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/servicepage/resheduletime.dart';
import 'package:yesmadam/services/apis.dart';



class Bookingdetail2 extends StatefulWidget {
  final orderid;


  Bookingdetail2(this.orderid);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return BookingdetailLoader();
  }
}

class BookingdetailLoader extends State<Bookingdetail2> {
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  var selectedRadio;
  int selectotherradio=0;
  var valuefirst = "";

  var reason = TextEditingController();






  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var historydetail = "";
  var name = "";
  var starttime = "";
  var endtime = "";
  var date = "";
  var addon = "";
  // List image = [];
  var staus = "";
  List service=[];
  List reasoncan=[];
  var bookingid="";
  var totalprice="";


  Future getbookinghistorylist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.BOOKINGHISTORYdetalis_API2;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {"booking_id": widget.orderid};
    print(data);

   // print(widget.vendorid);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
         historydetail = json.decode(res)['data']['book_payment_mode'];
         staus = json.decode(res)['data']['book_status'];
         addon = json.decode(res)['data']['book_create_at'];
        service=json.decode(res)['data']['book_service'];
         bookingid=json.decode(res)['data']['id'];
         name=json.decode(res)['data']['name'];
         starttime=json.decode(res)['data']['book_time_start'];
         date=json.decode(res)['data']['book_date'];
         endtime=json.decode(res)['data']['book_time_end'];
         totalprice=json.decode(res)['data']['book_total_amount'];

        print(bookingid);

      } else {

      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
   _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   Navigator.pop(context);
    return reply;
  }


  var selectedindex = 0;
  var textreson="";







  @override
  void initState() {
     getbookinghistorylist();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Container(
          child: Text("Booking Details",style: TextStyle(color: AppColors.white),
          ),

        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: CustomScrollView(
        slivers: [

          SliverToBoxAdapter(
            child: Container(child: data()),
          ),
          SliverToBoxAdapter(
            child: servicedetail(),
          )
        ],
      ),
    );
  }

  Widget data() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5, right: 5, left: 5),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: 1,
          itemBuilder: (BuildContext context, int i) {
            return Card(
              child: Container(
                  child: Column(
                // crossAxisAlignment: CrossAxisAlignment.,
                children: [
                  Container(
                    padding:
                        EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                              "Order Date:" + addon,
                        ),
                        // Container(
                        //   child: Text("Order Id: 18 Mar 2021"),
                        // )
                        )],
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
                    color: AppColors.grayBorder,
                    height: 1,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 15, bottom: 8),
                        child: Text("Name"),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        //height: 25,
                        width: MediaQuery.of(context).size.width / 2.2,
                        // color: AppColors.sentColor,
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 25, top: 10, right: 10, bottom: 5),
                                  //color: AppColors.primary,
                                  child: RichText(
                                    text: new TextSpan(
                                      text: name,
                                      style: TextStyle(
                                        color: AppColors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Container(
                  //       padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                  //       child: Text("Address"),
                  //       alignment: Alignment.topLeft,
                  //     ),
                  //     Container(
                  //       //height: 25,
                  //       width: MediaQuery.of(context).size.width / 2.2,
                  //       // color: AppColors.sentColor,
                  //       child: Row(
                  //         children: [
                  //           Flexible(
                  //             child: Container(
                  //                 padding: EdgeInsets.only(
                  //                     left: 25, top: 5, right: 10, bottom: 5),
                  //                 //color: AppColors.primary,
                  //                 child: RichText(
                  //                   text: new TextSpan(
                  //                     text: historydetail[i]['address'] == null
                  //                         ? ""
                  //                         : historydetail[i]['address'],
                  //                     style: TextStyle(
                  //                       color: AppColors.black,
                  //                       fontSize: 14,
                  //                     ),
                  //                   ),
                  //                   textAlign: TextAlign.left,
                  //                 )),
                  //           ),
                  //         ],
                  //       ),
                  //     )
                  //   ],
                  // ),
                  Container(
                    padding:
                        EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
                    color: AppColors.grayBorder,
                    height: 1,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                        child: Text("Booking Id:".toUpperCase()),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        //height: 25,
                        width: MediaQuery.of(context).size.width / 2.2,
                        // color: AppColors.sentColor,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 25, top: 5, right: 10, bottom: 5),
                                  //color: AppColors.primary,
                                  child: RichText(
                                    text: new TextSpan(
                                      text: bookingid,
                                      style: TextStyle(
                                        color: AppColors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Container(
                  //       padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                  //       child: Text("Service".toUpperCase()),
                  //       alignment: Alignment.topLeft,
                  //     ),
                  //     Container(
                  //       //height: 25,
                  //       width: MediaQuery.of(context).size.width / 2.2,
                  //       // color: AppColors.sentColor,
                  //       child: Row(
                  //         children: [
                  //           Flexible(
                  //             child: Container(
                  //                 padding: EdgeInsets.only(
                  //                     left: 25, top: 5, right: 10, bottom: 5),
                  //                 //color: AppColors.primary,
                  //                 child: RichText(
                  //                   text: new TextSpan(
                  //                     text: historydetail[i]['service_title'],
                  //                     style: TextStyle(
                  //                       color: AppColors.grayBorder,
                  //                       fontSize: 12,
                  //                     ),
                  //                   ),
                  //                   textAlign: TextAlign.left,
                  //                 )),
                  //           ),
                  //         ],
                  //       ),
                  //     )
                  //   ],
                  // ),

                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Container(
                  //       padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                  //       child: Text("Category"),
                  //       alignment: Alignment.topLeft,
                  //     ),
                  //     Container(
                  //       //height: 25,
                  //       width: MediaQuery.of(context).size.width / 2.2,
                  //       // color: AppColors.sentColor,
                  //       child: Row(
                  //         children: [
                  //           Flexible(
                  //             child: Container(
                  //                 padding: EdgeInsets.only(
                  //                     left: 25, top: 5, right: 10, bottom: 5),
                  //                 //color: AppColors.primary,
                  //                 child: RichText(
                  //                   text: new TextSpan(
                  //                     text: historydetail[i]['categoryname'],
                  //                     style: TextStyle(
                  //                       color: AppColors.grayBorder,
                  //                       fontSize: 12,
                  //                     ),
                  //                   ),
                  //                   textAlign: TextAlign.left,
                  //                 )),
                  //           ),
                  //         ],
                  //       ),
                  //     )
                  //   ],
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Container(
                  //       padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                  //       child: Text("Service Minutes"),
                  //       alignment: Alignment.topLeft,
                  //     ),
                  //     Container(
                  //       //height: 25,
                  //       width: MediaQuery.of(context).size.width / 2.2,
                  //       // color: AppColors.sentColor,
                  //       child: Row(
                  //         children: [
                  //           Flexible(
                  //             child: Container(
                  //                 padding: EdgeInsets.only(
                  //                     left: 25, top: 5, right: 10, bottom: 5),
                  //                 //color: AppColors.primary,
                  //                 child: RichText(
                  //                   text: new TextSpan(
                  //                     text: historydetail[i]['duration'] == null
                  //                         ? ""
                  //                         : historydetail[i]['duration'] +
                  //                             " min",
                  //                     style: TextStyle(
                  //                       color: AppColors.grayBorder,
                  //                       fontSize: 12,
                  //                     ),
                  //                   ),
                  //                   textAlign: TextAlign.left,
                  //                 )),
                  //           ),
                  //         ],
                  //       ),
                  //     )
                  //   ],
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                        child: Text("Booking Date"),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        //height: 25,
                        width: MediaQuery.of(context).size.width / 2.2,
                        // color: AppColors.sentColor,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(

                                  padding: EdgeInsets.only(
                                      left: 25, top: 5, right: 10, bottom: 5),
                                  //color: AppColors.primary,
                                  child: RichText(

                                    text: new TextSpan(
                                      text: date,
                                      style: TextStyle(
                                        color: AppColors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                        child: Text("Booking Time"),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        //height: 25,
                        width: MediaQuery.of(context).size.width / 2.2,
                        // color: AppColors.sentColor,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 25, top: 5, right: 10, bottom: 5),
                                  //color: AppColors.primary,
                                  child: RichText(
                                    text: new TextSpan(
                                      text: starttime+" to" + endtime,

                                      style: TextStyle(
                                        color: AppColors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
            //       Row(
            //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //         children: [
            //           Container(
            //             padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
            //             child: Text("ADD ON".toUpperCase()),
            //             alignment: Alignment.topLeft,
            //           ),
            //           Container(
            //             //height: 25,
            //             width: MediaQuery.of(context).size.width / 2.2,
            //             // color: AppColors.sentColor,
            //             child: Row(
            //               children: List.generate(
            //                   addon.length,
            //                       (index) => Flexible(
            //                     child: Container(
            //                         padding: EdgeInsets.only(
            //                             left: 20,
            //                             top: 5,
            //                             right: 0,
            //                             bottom: 5),
            //                         //color: AppColors.primary,
            //                         child: RichText(
            //                           text: new TextSpan(
            //                             text:
            // //addon[index]['name']==null?"":addon[index]['name']
            //
            // ","
            //                             ,
            //                             style: TextStyle(
            //                               color: AppColors.black,
            //                               fontSize: 14,
            //                             ),
            //                           ),
            //                           textAlign: TextAlign.left,
            //                         )),
            //                   )),
            //             ),
            //           )
            //         ],
            //       ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Container(
                  //       padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                  //       child: Text("ADD ON Price".toUpperCase()),
                  //       alignment: Alignment.topLeft,
                  //     ),
                  //     Container(
                  //       //height: 25,
                  //       width: MediaQuery.of(context).size.width / 2.2,
                  //       // color: AppColors.sentColor,
                  //       child: Row(
                  //         children: List.generate(
                  //             addon.length==null?"": addon.length,
                  //                 (index) => Flexible(
                  //               child: Container(
                  //                   padding: EdgeInsets.only(
                  //                       left: 20,
                  //                       top: 5,
                  //                       right: 0,
                  //                       bottom: 5),
                  //                   //color: AppColors.primary,
                  //                   child: RichText(
                  //                     text: new TextSpan(
                  //                       text:
                  //                       //addon[index]['price']==null?"":
                  //                       "₹" +
                  //                      // addon[index]['price']
                  //                       //+
                  //                        ","
                  //                       ,
                  //                       style: TextStyle(
                  //                         color: AppColors.black,
                  //                         fontSize: 14,
                  //                       ),
                  //                     ),
                  //                     textAlign: TextAlign.left,
                  //                   )),
                  //             )),
                  //       ),
                  //     )
                  //   ],
                  // ),
                  Container(
                    padding:
                        EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
                    color: AppColors.grayBorder,
                    height: 1,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                        child: Text("Payment Medium"),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        //height: 25,
                        width: MediaQuery.of(context).size.width / 2.2,
                        // color: AppColors.sentColor,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 25, top: 5, right: 10, bottom: 5),
                                  //color: AppColors.primary,
                                  child: RichText(
                                    text: new TextSpan(
                                      text: historydetail,
                                      style: TextStyle(
                                        color: AppColors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                        child: Text("Total Amount"),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        //height: 25,
                        width: MediaQuery.of(context).size.width / 2.2,
                        // color: AppColors.sentColor,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 25, top: 5, right: 10, bottom: 5),
                                  //color: AppColors.primary,
                                  child: RichText(
                                    text: new TextSpan(
                                      text: "₹" +
                                          totalprice,
                                      style: TextStyle(
                                        color: AppColors.sentColor,
                                        fontSize: 14,
                                      ),
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                        child: Text("Status"),
                        alignment: Alignment.topLeft,
                      ),
                      Container(
                        //height: 25,
                        width: MediaQuery.of(context).size.width / 2.2,
                        // color: AppColors.sentColor,
                        child: Row(
                          children: [
                            Flexible(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 25, top: 05, right: 10, bottom: 5),
                                  //color: AppColors.primary,
                                  child: RichText(
                                    text: new TextSpan(
                                      text: staus,
                                      // text: historydetail[i]['status'] == "0"
                                      //     ? "Pending"
                                      //     : historydetail[i]['status'] == "1"
                                      //         ? "Vendor Accepted"
                                      //         : historydetail[i]['status'] ==
                                      //                 "2"
                                      //             ? "Service Started"
                                      //             : historydetail[i]
                                      //                         ['status'] ==
                                      //                     "3"
                                      //                 ? "Cancel"
                                      //                 : historydetail[i]
                                      //                             ['status'] ==
                                      //                         "4"
                                      //                     ? "Cancel"
                                      //                     : historydetail[i][
                                      //                                 'status'] ==
                                      //                             "5"
                                      //                         ? "Completed"
                                      //                         : "",
                                      style: TextStyle(
                                        color:staus=="pending"
                                        // historydetail[i]
                                        // ['status'] ==
                                        //     "3"
                                        //     ?AppColors.redcolor3:historydetail[i]
                                        // ['status'] ==
                                        //     "4"
                                            ?AppColors.redcolor3:AppColors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              )),
            );
          }),
    );
  }
  Widget servicedetail() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5, right: 5, left: 5),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: service.length,
          itemBuilder: (BuildContext context, int i) {
            return Container(
            //  height: 150,
              padding: EdgeInsets.only(top: 10, bottom: 15, left: 5, right: 5,),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 10,bottom: 10),
                      child: ListTile(
                        leading: Container(
                          // padding: EdgeInsets.only(top: 20),
                          width: MediaQuery.of(context).size.width / 3.9,
                          //height: 200,

                          decoration: BoxDecoration(
                            //color: AppColors.lightgreen,
                              borderRadius: BorderRadius.circular(10.0),
                              // border: Border.all(
                              //   color: AppColors.grayBorder,
                              //
                              //
                              // ),
                              image: DecorationImage(
                                  image: NetworkImage(
                                    service[i]['image']==null?"": service[i]['image'],
                                  ),
                                  fit: BoxFit.contain)),
                        ),
                        title: Container(
                          child: Text( service[i]['service_name']==null?"":service[i]['service_name']
                          ),
                        ),
                        trailing: Container(
                          child: Text( service[i]['service_amount']==null?"":"₹"+service[i]['service_amount']
                          ),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            );
          }),
    );
  }
}
