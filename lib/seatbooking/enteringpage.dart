// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/material.dart';
// // import 'package:geolocator/geolocator.dart';
//
// import 'package:yesmadam/color/AppColors.dart';
// import 'package:yesmadam/pages/Home.dart';
// import 'package:yesmadam/pages/Notification.dart';
//
// import 'Home.dart';
// import 'Homepage.dart';
//
// class EntryPage extends StatefulWidget{
//   @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//     return EntrypageView();
//   }
//
// }
//
// class EntrypageView extends State<EntryPage>{
//   final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _firebaseMessaging.configure(
//         // onBackgroundMessage: myBackgroundMessageHandler,
//         onResume: (Map<String, dynamic> message) async{
//           print(message['data'].toString());
//           if(message['data']['page'] == "notification"){
//             Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationPage()));
//           }
//         }
//     );
//   }
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Scaffold(
//       backgroundColor: AppColors.splash,
//       body: Stack(
//         children: [
//           Positioned(
//             top: 100,
//             left: 0,
//             right: 0,
//             child: Container(
//               child: Container(
//                 height: 200,
//                 child: Image(
//                   image: AssetImage("assets/images/launcher.png"),
//                   width: 100,
//                 ),
//               ),
//             ),
//           ),
//           Positioned(
//             bottom: 0,
//             left: 0,
//             right: 0,
//             child: Container(
//               padding: EdgeInsets.all(10),
//               child: Container(
//                 height: 300,
//                 decoration: BoxDecoration(
//                     color: Colors.white,
//                     borderRadius: BorderRadius.circular(10)
//                 ),
//                 child: Column(
//                   children: [
//                     Container(
//                       margin: EdgeInsets.only(
//                           top: 50,
//                           right: 40,
//                           left: 40,
//                           bottom: 10
//                       ),
//                       child: Center(
//                         child: Text(
//                           "What kind a service you are looking for",
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                               fontSize: 20
//                           ),
//                         ),
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(
//                           top: 20
//                       ),
//                       child: Material(
//                         color: Colors.amberAccent,
//
//                         child: InkWell(
//                           onTap: (){
//                             Navigator.push(context,
//                                 MaterialPageRoute(builder: (context) => Home1())
//                             );
//                           },
//                           highlightColor: Colors.amber,
//                           child: Container(
//                             padding: EdgeInsets.only(
//                                 top: 10,
//                                 bottom: 10
//                             ),
//                             decoration: BoxDecoration(
//                               // color: Colors.amberAccent
//                             ),
//                             child: Center(
//                                 child: Text(
//                                   "Book a Seat",
//                                   style: TextStyle(
//                                       fontSize: 20
//                                   ),
//                                 )
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(
//                           top: 20
//                       ),
//                       child: Material(
//                         color: Colors.redAccent,
//                         child: InkWell(
//                           onTap: (){
//                             Navigator.push(context,
//                                 MaterialPageRoute(builder: (context) => Home())
//                             );
//                           },
//                           child: Container(
//                             padding: EdgeInsets.only(
//                                 top: 10,
//                                 bottom: 10
//                             ),
//                             decoration: BoxDecoration(
//                               // color: Colors.redAccent
//                             ),
//                             child: Center(child: Text(
//                               "Home Service",
//                               style: TextStyle(
//                                   fontSize: 20,
//                                   color: Colors.white
//                               ),
//
//                             )
//                             ),
//                           ),
//                         ),
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
//
// }