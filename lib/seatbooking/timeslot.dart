import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/lastpage.dart';
import 'package:yesmadam/pages/login.dart';
import 'package:yesmadam/services/apis.dart';
import 'package:http/http.dart' as http;

class timePage1 extends StatefulWidget {
  final saloonid;
  final vendorid;
  final name;
  final price;
  final List serviceList;
  final serviceTime;
  timePage1(this.saloonid, this.vendorid, this.name, this.price,
      this.serviceList, this.serviceTime);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return timePageLoader();
  }
}

class timePageLoader extends State<timePage1> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  List<DateSlotModel> records = [];
  var dates = "";
  var dateslen = 0;

  List<TimeSlotModel> times = [];
  var selectedDate;
  var timelen = 0;

  var valuefirst = "";

  var seat = "";

  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }

  alertDailoga(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var selectedindex = 0;

  _checkLogin(contextt) async {
    var sp = await SharedPreferences.getInstance();

    if (sp.containsKey("userid")) {
      if (valuefirst == "") {
        alertDailoga("Please Select Time");
      } else {
        createBooking(contextt);
      }
    } else {
      if (valuefirst == "") {
        alertDailoga("Please Select Time");
      } else {
        Navigator.push(
                context, MaterialPageRoute(builder: (context) => LoginLoader()))
            .then((value) => {createBooking(contextt)});
      }
    }
  }

  Future gettime() async {
    // print("service list: "+widget.serviceList.toString());
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });

    var url = Apis.GETTIMESLOT_API;
    // var data = json.encode(map);
    var data = {
      "saloon_id": widget.saloonid,
      "service_id": widget.serviceList,
    };
    log('====>  $data  ');
    // const headers = {'Content-Type': 'application/json'};
    // http.Response res = await http.post(url,body: data);
    var res = await apiPostRequest(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        records = json.decode(res)['data'];
        date = records[0].date.toString();
        times = json.decode(res)['data'][selectedindex]['time'];
        log('=====>> $date  $times');
      } else {}
      loading = false;
    });
  }
  // Future getcheckseat() async {
  //
  //   try {
  //
  //     setState(() {
  //       loading = true;
  //       ani = false;
  //       fail = false;
  //       _color = Colors.black.withOpacity(0.3);
  //     });
  //
  //     var url = Apis.SEATAVAIABLE_API;
  //     // var data = json.encode(map);
  //     var data = {
  //       "saloon_id":widget.saloonid.toString(),
  //       "start_time":starttime,
  //       "end_time":endTime,
  //       "service_date":date
  //     };
  //
  //     print(data);
  //     var res = await apiPostRequest(url,data);
  //
  //     print(res);
  //     setState(() {
  //        seat = json.decode(res)['status']['seats'];
  //        print(seat);
  //
  //
  //
  //       loading = false;
  //     });
  //
  //
  //
  //
  //   }
  //
  //   catch (e)
  //   {
  //     print("Network Fail");
  //   }
  // }

  _selectTimeSlot(index, dates) async {
    setState(() {
      selectedindex = index;
      times = records[index].timeList;

      valuefirst = "";
      date = dates;
      print(date);
    });
  }

  slectTime(startTimeset, endtimeset) {
    setState(() {
      starttime = startTimeset;
      endTime = endtimeset;
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 1),
      // action: ,
    ));
  }

  Future<String> apiPostRequest(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    log('message==>>  $reply');
    // Navigator.pop(context);
    return reply;
  }

  var date = "";
  var starttime = "";
  var endTime = "";

  Future createBooking(context) async {
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      var data = {
        "customer_id": sp.getString("userid").toString(),
        "vendor_id": widget.vendorid.toString(),
        "saloon_id": widget.saloonid.toString(),
        "service_data": widget.serviceList,
        "start_time": starttime,
        "end_time": endTime,
        "service_date": date,
        "wallet_amount": "0",
        "payment_mode": "cod",
        "total_amount": widget.price,
        "payable_amount": widget.price,
        "coupon_id": "0",
        "coupon_discount": "0"
      };

      print(data);
      var url = Apis.BOOKING_API;
      var res = await apiPostRequest(url, data);
      if (json.decode(res)['status'] == 1) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LastpageLoader()),
            (Route<dynamic> route) => false);
        // Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
      } else {
        alertDailog("NO SEAT AVAILABLE");
      }
      // _showInSnackBar(context);
      print(res);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    callGetDateTimeApi();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        elevation: 0,
        title: Text(
          "",
          style: TextStyle(color: AppColors.white),
        ),
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              color: AppColors.white,
              padding: EdgeInsets.only(left: 20, right: 10, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${widget.serviceList.length} Services",
                    style: TextStyle(
                        color: AppColors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: 18),
                  ),
                  Column(
                    children: [
                      Text(
                        '${widget.serviceTime} Minute',
                        style: TextStyle(
                            color: AppColors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                      Text(
                        '${widget.serviceList.length} Slot Required',
                        style: TextStyle(
                            color: AppColors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 18),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
              SliverToBoxAdapter(
                  child: 
                  records.length>0?
                  Container(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '$selectedDate',
                          style: TextStyle(
                              color: AppColors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 18),
                        ),
                      ],
                    ),
                  ):Container(),
                ),
              
          SliverToBoxAdapter(
            child: Container(
              color: AppColors.white,
              padding:
                  EdgeInsets.only(left: 20, right: 10, top: 10, bottom: 10),
              child: Text(
                "Select the time Slot",
                style: TextStyle(
                    color: AppColors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 18),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              // padding: EdgeInsets.only(left: 20),
              child: time(),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              child: SingleChildScrollView(
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Card(
                        child: Container(
                          color: Colors.white,
                          child: Container(
                              padding: EdgeInsets.only(
                                top: 10,
                                right: 20,
                                left: 20,
                                bottom: 10,
                              ),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 10, right: 10, top: 10),
                                        child: Text(
                                          "Service Charger",
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.black45),
                                        ),
                                      ),
                                      Text(
                                        "₹" + widget.price,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black45),
                                      )
                                    ],
                                  ),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  //   children: [
                                  //     Padding(padding: EdgeInsets.only(
                                  //         left: 10,
                                  //         right: 10,
                                  //         top: 10
                                  //     ),
                                  //       child: Text("Discount",
                                  //         style: TextStyle(
                                  //             fontSize: 14,
                                  //             color: Colors.black45
                                  //         ),),
                                  //     ),
                                  //     Text("₹500",
                                  //       style: TextStyle(
                                  //           fontSize: 14,
                                  //           color: Colors.black45
                                  //       ),)
                                  //
                                  //   ],
                                  // ),
                                  Divider(),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 10, right: 10, top: 10),
                                        child: Text(
                                          "Total",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black),
                                        ),
                                      ),
                                      Text(
                                        "₹" + widget.price,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black),
                                      )
                                    ],
                                  ),
                                ],
                              )),
                        ),
                      ),
                    ),
                    // Container(
                    //
                    //   child: Container(
                    //     // height: 100,
                    //     child: Container(
                    //
                    //       child: ListTile(
                    //         title: Container(
                    //           child: TextField(
                    //             decoration: InputDecoration(
                    //                 labelText: "Type Coupon code"
                    //             ),
                    //           ),
                    //         ),
                    //         trailing: FlatButton(
                    //           onPressed: (){},
                    //           child: Text(
                    //             "APPLY",style: TextStyle(
                    //               color: AppColors.black
                    //           ),
                    //           ),
                    //         ),
                    //       ),
                    //
                    //     ),
                    //   ),
                    //
                    // ),
                    // Card(
                    //
                    //   child: Container(
                    //       padding: EdgeInsets.only(
                    //           top: 10,
                    //           right: 10,
                    //           left: 10,
                    //           bottom: 10
                    //       ),
                    //       child:Column(
                    //
                    //         children: [
                    //
                    //           Row(
                    //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //             children: [
                    //               Padding(
                    //                 padding: EdgeInsets.only(
                    //                     left: 10,
                    //                     right: 10,
                    //                     top: 5
                    //                 ),
                    //                 child: Text("Service Name", style: TextStyle(
                    //                     fontSize: 11,
                    //                     color: Colors.black45
                    //                 ),),
                    //
                    //               ),
                    //               Text("Hair Cut"
                    //                 , style: TextStyle(
                    //                     fontSize: 11,
                    //                     color: Colors.black45
                    //                 ),)
                    //
                    //
                    //             ],
                    //           ),
                    //
                    //
                    //         ],
                    //       )
                    //
                    //
                    //   ),
                    // ),
                    //Divider(),
                    // Container(
                    //     padding: EdgeInsets.only(top: 10,
                    //         bottom: 5,
                    //         left: 15),
                    //
                    //     child: Text("Payment Option",
                    //       style:TextStyle (fontSize: 15,
                    //           color: Colors.black,
                    //           fontWeight: FontWeight.bold
                    //
                    //       ),)
                    // ),
                    // Card
                    //   (
                    //   child: Container(
                    //     height: 100,
                    //       padding: EdgeInsets.only(
                    //         top: 5,
                    //         right: 10,
                    //         left: 10,
                    //         bottom: 10,
                    //       ),
                    //       child:Column(
                    //         children: [
                    //
                    //           InkWell(
                    //             onTap: ()
                    //             {
                    //
                    //             },
                    //             child: Row(
                    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //               children: [
                    //                 Padding(padding: EdgeInsets.only(
                    //                     left: 5,
                    //                     right: 10,
                    //                     top: 10
                    //                 ),
                    //
                    //                   child:
                    //                   Row(
                    //                     children: [
                    //                       Icon(Icons.payment,
                    //                         size: 20.0,
                    //                       ),
                    //                       Padding(padding: EdgeInsets.only(left: 10),),
                    //                       Text("Cash",
                    //                         style: TextStyle(
                    //                             fontSize: 15
                    //
                    //                         ),),
                    //                     ],
                    //                   ),
                    //                 ),
                    //                 Icon(Icons.radio_button_unchecked,
                    //                   size: 17.0,
                    //
                    //                 ),
                    //
                    //               ],
                    //             ),
                    //           ),
                    //           Row(
                    //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //             children: [
                    //               Padding(padding: EdgeInsets.only(
                    //                   left: 5,
                    //                   right: 10,
                    //                   top: 10
                    //               ),
                    //                 child:
                    //                 Row(
                    //                   children: [
                    //
                    //                     Icon(Icons.payment,
                    //                       size: 17.0,
                    //                     ),
                    //                     Padding(padding: EdgeInsets.only(left: 10),),
                    //
                    //                     Text("Online",style: TextStyle(
                    //                       fontSize: 15,
                    //
                    //                     ),
                    //                     ),
                    //                   ],
                    //                 ),
                    //               ),
                    //               Icon(Icons.radio_button_unchecked,
                    //                 size: 17.0,
                    //               )
                    //
                    //
                    //             ],
                    //           ),
                    //         ],
                    //       )
                    //
                    //
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: new Container(
              padding: EdgeInsets.only(top: 10),
              child: Container(
                color: Colors.white,
                child: new Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(top: 10, bottom: 5),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            //"Online/Cash",
                            "Cash",
                            style: TextStyle(
                                fontFamily: "opensan",
                                fontSize: 20,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Divider(),
                    ),
                    new ListTile(
                      // onTap: () {
                      //   setListing();
                      //   _setRadio(2);
                      // },
                      title: Text(
                        "Cash On Delivery",
                        // style: TextStyle(fontSize: 18),
                      ),
                      leading: SizedBox(
                        child: Radio(
                          value: 1,
                          groupValue: 1,
                          activeColor: AppColors.primary2,
                          onChanged: (val) {},
                        ),
                      ),
                      trailing: IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.supervised_user_circle_outlined),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: times.length == 0
          ? Container(
              height: 1,
            )
          : Container(
              color: AppColors.black,
              height: 40,
              child: SizedBox(
                child: InkWell(
                  onTap: () {
                    _checkLogin(context);
                    // Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
                  },
                  child: Container(
                    child: Center(
                      child: Text(
                        "Booked",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget time() {
    return Container(
      child: Column(
        children: [
          Container(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 90,
                  padding:
                      EdgeInsets.only(top: 2, bottom: 0, right: 0, left: 0),
                  alignment: Alignment.centerLeft,
                  color: AppColors.white,
                  child: ListView.builder(
                    padding: EdgeInsets.only(left: 15),
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: records.length,
                    itemBuilder: (context, i) {
                      // var  dateFormates = DateFormat(records[i]['time_date'].toString()).format(DateTime.parse(records[i]['time_date'].toString()));
                      // print(dateFormates);
                      return Container(
                          padding: EdgeInsets.only(
                              left: 5, right: 5, top: 5, bottom: 5),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                selectedDate = records[i].date;
                              });
                              _selectTimeSlot(i, records[i].date.toString());
                            },
                            child: Card(
                              color: selectedindex == i
                                  ? AppColors.lightblue
                                  : Colors.white,
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 10, bottom: 10),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        records[i].isOpen
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: 2, bottom: 0),
                                                child: Text(
                                                  getdays(records[i]
                                                      .date
                                                      .toString()),
                                                  //getdates(dates.toString()),
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                  ),
                                                ),
                                              )
                                            : Padding(
                                                padding: EdgeInsets.only(
                                                    top: 2, bottom: 0),
                                                child: Text(
                                                  "Shop Close",
                                                  //getdates(dates.toString()),
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color:
                                                          AppColors.redcolor3),
                                                ),
                                              )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        records[i].isOpen
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: 2, bottom: 0),
                                                child: Text(
                                                  getdates(records[i]
                                                      .date
                                                      .toString()),
                                                  style: TextStyle(
                                                    fontSize: 15,
                                                  ),
                                                ),
                                              )
                                            : Container(
                                                height: 1,
                                              )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ));
                    },
                  ),
                )
              ],
            ),
          ),
          Container(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  // height : 180,
                  padding:
                      EdgeInsets.only(top: 2, bottom: 0, right: 10, left: 10),
                  alignment: Alignment.centerLeft,
                  color: Colors.white12,
                  child: times.length == 0
                      ? Card(
                          child: Center(
                              child: Container(
                            margin: EdgeInsets.all(10),
                            // height: 50,
                            child: Text(
                              "Shop Close",
                              style: TextStyle(
                                  color: AppColors.redcolor3, fontSize: 18),
                            ),
                          )),
                        )
                      : GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 3.0,
                          ),
                          padding: EdgeInsets.only(top: 10),
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: times.length,
                          //timelen,
                          itemBuilder: (context, i) {
                            var item = times[i];
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  // for (int j = i+widget.serviceList.length; j < widget.serviceList.length; j++) {
                                  //   times[j].isSelect = !times[j].isSelect;
                                  // }

                                  for(i = i+widget.serviceList.length; i < widget.serviceList.length; i++){
                                    times[i].isSelect = !times[i].isSelect;
                                  }
                                  // item.isSelect=!item.isSelect;
                                  
                                });
                                log("message===> $i");
                              },
                              child: Card(
                                color: (times[i].isSelect)
                                    ? Colors.amber
                                    : Color(0xFFFFFFFFF),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        item.startTime + " to " + item.endTime,
                                        style: TextStyle(
                                            color: (times[i].isSelect)
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void callGetDateTimeApi() async {
    var url = Uri.parse(Apis.GETTIMESLOT_API);

    var data = jsonEncode({
      "saloon_id": widget.saloonid,
      "service_id": widget.serviceList,
    });
    try {
      final response = await http.post(
        url,
        body: data,
      );
      log('===123===>  ${response.body}');
      if (response.statusCode == 200) {
        var result = jsonDecode(response.body);
        if (result['status'] == 1) {
          records.clear();
          var _listData = result['data'] as List;
          setState(() {
            var _list =
                _listData.map((e) => DateSlotModel.fromJson(e)).toList();
            records.addAll(_list);

            if (records.length > 0) {
              times = records.elementAt(0).timeList;
              selectedDate = records.elementAt(0).date;
            }
          });
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  getdates(String text) {
    var string = text;
    print(text);
    // var now = new DateTime.now();
    var formatter = new DateFormat('dd-MM-yyyy').parse(string);
    // String formatted = formatter.format(DateTime.parse(string));
    var weekDay = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
    return weekDay[formatter.weekday - 1].toString();
  }

  getdays(String text) {
    var string = text;

//    var now = new DateTime.now();
    var formatter = new DateFormat('dd-MM-yyyy').parse(string);
    // String formatted = formatter.format(DateTime.parse(string));
    List monthList = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    var day = formatter.day.toString();
    var month = monthList[formatter.month - 1].toString();
    var fullDate = "$month $day";
    return fullDate;
  }
}

class DateSlotModel {
  String date = '';
  String day = '';
  String totalTime = '';
  bool isOpen = false;
  List<TimeSlotModel> timeList = [];

  DateSlotModel(
      {this.date, this.day, this.totalTime, this.isOpen, this.timeList});

  factory DateSlotModel.fromJson(Map<String, dynamic> json) {
    List<TimeSlotModel> _list = [];

    if (json['time'] != null) {
      var _listdata = json['time'] as List;
      _list = _listdata.map((e) => TimeSlotModel.fromJson(e)).toList();
    }

    return DateSlotModel(
      date: json['date'],
      day: json['day'],
      totalTime: json['total_time'],
      isOpen: json['shop_open'] == '0' ? true : false,
      timeList: _list,
    );
  }
}

class TimeSlotModel {
  String startTime = '';
  String endTime = '';
  bool isSelect = false;

  TimeSlotModel({this.startTime, this.endTime, this.isSelect});

  factory TimeSlotModel.fromJson(Map<String, dynamic> json) {
    return TimeSlotModel(
      startTime: json['time_open'],
      endTime: json['time_open'],
      isSelect: false,
    );
  }
}
