

import 'dart:ffi';

import "package:flutter/material.dart";
import 'package:flutter_icons/flutter_icons.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:geocoder/geocoder.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
//import 'package:flare_flutter/flare_actor.dart';
import 'dart:async';
import 'package:google_place/google_place.dart';
import 'dart:io';

import 'dart:convert';


class LocationPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return LocationLoader();
  }

}

class LocationLoader extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LocationView();
  }

}

class LocationView extends State<LocationLoader>{
  List<AutocompletePrediction> predictions = [];
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }
  GooglePlace googlePlace;
  DetailsResult detailsResult;
  var lat = "";
  var lng = "";

  List cats = [];
  var catslength = 0;

  void getDetils(String placeId) async {
    var sp = await SharedPreferences.getInstance();
    _onLoading();
    var result = await this.googlePlace.details.get(placeId);
    if (result != null && result.result != null && mounted) {
      setState(() {
        detailsResult = result.result;
        lat = detailsResult.geometry.location.lat.toString();
        lng = detailsResult.geometry.location.lng.toString();
        sp.setString("savedAddress",detailsResult.formattedAddress.toString());
        sp.setString("lat",detailsResult.geometry.location.lat.toString());
        sp.setString("lng",detailsResult.geometry.location.lng.toString());

        Navigator.pop(context);

        // address.text = detailsResult.formattedAddress.toString();

      });
    }
    Navigator.pop(context);
    print(detailsResult.formattedAddress.toString());
  }

  List places;
  var lattitude = "";
  var longitude = "";
  var pin="";
  var placecounts = 0;
  Future getLocations(String locationName) async{
    var kGoogleApiKey = "AIzaSyAyRY9zVqzDFo8Qc2puq8T6gYiTARTdnNo"
        // "AIzaSyAClVtUddVF89FASU55EjvJZ4OO6l72TGs"
    ;
    // var url = Uri.tryParse("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="+locationName+"&inputtype=textquery&fields=formatted_address,geometry&location=20.7711857,73.729974&radius=10000&key="+kGoogleApiKey);
    var url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="+locationName+"&inputtype=textquery&fields=formatted_address,geometry&location=20.7711857,73.729974&radius=10000&key="+kGoogleApiKey;
    http.Response res = await http.get(Uri.parse(url));
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    lattitude = position.latitude.toString();
    longitude = position.longitude.toString();
    var lat = position.latitude;
    var lng = position.longitude;
    final coordinates = new Coordinates(lat, lng);
    // var addresses =
    // await Geocoder.local.findAddressesFromCoordinates(coordinates);
    // print(addresses.first.postalCode);
    // var first = addresses.first;
    // print("${first.featureName} : ${first.addressLine}");
    setState(() {
      places = json.decode(res.body)['candidates'];
      placecounts = places.length;
      // pin=addresses.first.postalCode.toString();
    });
   print(res.body);
  }

  Future _setLocation(locations,lattitude,longitude,pin) async{
    var sp = await SharedPreferences.getInstance();
    sp.setString("location", locations);
    sp.setString("lat", lattitude.toString());
    sp.setString("log", longitude.toString());
    sp.setString("pin", pin.toString());
    Navigator.pop(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    googlePlace = GooglePlace("AIzaSyAyRY9zVqzDFo8Qc2puq8T6gYiTARTdnNo");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: new TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Search your location",
                hintStyle: TextStyle(
                    fontFamily: "opensan",
                    fontWeight: FontWeight.bold
                )
            ),
            onChanged: (value) {
              if (value.isNotEmpty) {
                print("vvgg");
                autoCompleteSearch(value);
              } else {
                if (predictions.length > 0 && mounted) {
                  setState(() {
                    print("ggg");
                    predictions = [];
                  });
                }
              }
            },
            autofocus: true,
          ),
          leading: IconButton(
             icon: Icon(
              Feather.arrow_left,
               color: Colors.black,
             ),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          elevation: 0,
        ),
        body: new Container(
          child: Container(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: predictions.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: CircleAvatar(
                    child: Icon(
                      Icons.pin_drop,
                      color: Colors.white,
                    ),
                  ),
                  title: Text(predictions[index].description),
                  onTap: () {
                    print(predictions[index]);
                    getDetils(predictions[index].placeId);
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) => DetailsPage(
                    //       placeId: predictions[index].placeId,
                    //       googlePlace: googlePlace,
                    //     ),
                    //   ),
                    // );
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget Categorytable() {
    return DataTable(
      columns: [
        DataColumn(label: Text("Image")),
        DataColumn(label: Text("Name")),
        DataColumn(label: Text("Edit/Delete")),
      ],
      rows: cats.map((e) {
        return DataRow(cells: [
          DataCell(Container(
            decoration: BoxDecoration(
                image:
                DecorationImage(image: NetworkImage(e['category_image']))),
          )),
          DataCell(Text(e['category_name'])),
          DataCell(IconButton(
              icon: Icon(Icons.edit),
              onPressed: () async {
              })),
        ]);
      }).toList(),
    );
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions;
      });
    }
  }

}