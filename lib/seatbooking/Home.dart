import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_icons/flutter_icons.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:share/share.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/Notification.dart';
import 'package:yesmadam/pages/chat.dart';
import 'package:yesmadam/pages/login.dart';
import 'package:yesmadam/pages/updateprofile.dart';
import 'package:yesmadam/pages/wallet.dart';
import 'package:yesmadam/servicepage/appointment.dart';
import 'package:yesmadam/servicepage/pageView.dart';
import 'package:yesmadam/services/apis.dart';

import 'Homepage.dart';
import 'booking.dart';

class Home1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeLoader();
  }
}
class HomeLoader extends State<Home1>with SingleTickerProviderStateMixin
{
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool expanded = true;
  AnimationController controller;
  getFcmToken() async{
    var sp = await SharedPreferences.getInstance();
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      // setState(() {
      //   // _homeScreenText = "Push Messaging token: $token";
      // });
      print("Push Messaging token: $token");
      sp.setString("device_token", '$token');
    });
    _firebaseMessaging.configure(
        onBackgroundMessage: myBackgroundMessageHandler
    );
  }


  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      print(message['data']);
      // Handle data message
      _onLoading();
      final dynamic data = message['data'];

    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }
  var number = "";
  var email = "";
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  bool logedin = false;
  Future getwhatapp() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.WHATAPP_API;
      var res = await apiGetRequest(url);
      // print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          number = json.decode(res)['data'][0]['whatsapp'].toString();
          print(number);
        });
      }

      // print(res);

    } catch (e) {
      print(e);
    }
  }

  Future<String> apiGetRequest(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getemail() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.EMAIL_API;
      var res = await apiGetRequesta(url);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          email = json.decode(res)['email'];
          print(email);
        });
      }

      print(res);
    } catch (e) {
      print(e);
    }
  }

  Future<String> apiGetRequesta(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }


  getUser() async {
    //_onLoading();
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      setState(() {
        logedin = true;
      });
    } else {
      setState(() {
        logedin = false;
      });
    }
  }

  logOut() async {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      sp.remove("userid");
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    });
    getUser();
  }

  var phonenumber = "";
  var profilename = "";
  var mobile = "";
  var firstname="";
  var lastname="";
  var emailed="";
  var image="";
  Future getprofile() async {
    // print("checking for login");
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.PROFILE_API;
      var data = {
        "user_id": user,
      };
      var res = await apiPostRequests(url, data);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          profilename = json.decode(res)['response'][0]['full_name'];
          mobile = json.decode(res)['response'][0]['mobile'];
          firstname=json.decode(res)['response'][0]['first_name'];
          lastname=json.decode(res)['response'][0]['last_name'];
          image=json.decode(res)['response'][0]['image'];
          emailed=json.decode(res)['response'][0]['email'];
          print(profilename);
          logedin = true;
        });
        // print();
      } else {
        setState(() {
          logedin = false;
        });
      }
    } else {
      print("Please login");
    }
  }

  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getcall() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.CALL_API;
      var res = await apiGetRequestae(url);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          phonenumber = json.decode(res)['data'][0]['mobile'];
          print(phonenumber);
        });
      }

      print(res);
    } catch (e) {
      print(e);
    }
  }
  List page=[];
  Future getpage() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.PAAGEVIEW_API;
      var res = await apiGetRequestae(url);
      print(res);
      if (json.decode(res)['status'] == "1") {
        setState(() {
          page = json.decode(res)['contentlist'];
          print(page.length);
        });
      }

      print(res);
    } catch (e) {
      print(e);
    }
  }



  Future<String> apiGetRequestae(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);

    return reply;
  }

  @override
  void initState() {
    getprofile();
    getUser();
    getFcmToken();
    getwhatapp();
    getemail();
    getcall();
    getpage();

    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
      reverseDuration: Duration(milliseconds: 400),
    );
  }




  int _currentIndex = 0;



  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Order',
      style: optionStyle,
    ),
    Text(
      'Index 1: Go Out',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
    Text(
      'Index 3: School',
      style: optionStyle,
    ),
    Text(
      'Index 4: School',
      style: optionStyle,
    ),
  ];

  Widget pages = HomePage();
  changePage(value) async{
    if(value == 0){
      setState(() {
        pages = HomePage();
      });
    }if(value == 1){
      setState(() {
        //pages = NotificationPage();
      });
    }
    if(value == 2){
      setState(() {
        //pages = bookPage();
      });
    }
    if(value == 3){
      setState(() {
        //pages = ProfilePage();
      });
    }

  }

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold(
      drawer: Drawer(


        child: Container(
          color: AppColors.sentColor,
          child: ListView(

            children: [
              // Container(
              //   //height: 100,
              //   decoration: BoxDecoration(
              //    // color: AppColors.background,
              //   ),
              //   child: Container(
              //     padding: EdgeInsets.only(top: 30),
              //
              //     child: Column(
              //       crossAxisAlignment: CrossAxisAlignment.start,
              //       children: [
              //          Center(
              //            child: Container(
              //             width: 80,
              //             height: 80,
              //             decoration: BoxDecoration(
              //                 color: AppColors.white,
              //                 border: Border.all(color: AppColors.grayBorder),
              //                 boxShadow: [
              //                   BoxShadow(
              //                       color: Colors.black, spreadRadius: 1, blurRadius: 10)
              //                 ],
              //                 borderRadius: BorderRadius.all(Radius.circular(50))),
              //             child: Image(
              //
              //
              //               image: AssetImage("assets/images/profile.png"),
              //             ),
              //         ),
              //          ),
              //
              //
              //         Center(
              //           child:
              //           Container(
              //             padding: EdgeInsets.only(right: 10,top: 25,left: 10,bottom: 10),
              //
              //             // color: Colors.white,
              //
              //             child:logedin == false
              //           ?
              //
              //            Container(
              //
              //               padding: EdgeInsets.only(top: 0, bottom: 0, left: 10),
              //             child: InkWell(
              //               onTap: () {
              //                 // Navigator.push(
              //                 //     context,
              //                 //     MaterialPageRoute(
              //                 //         builder: (context) => LoginLoader(logintype: "home")))
              //                 //     .then((value) => getUser());
              //                 // logOut();
              //               },
              //               child: ListTile(
              //                 title:
              //                 Container(
              //                   //color: AppColors.black,
              //                   height: 50,
              //                   width: 50,
              //                   padding: EdgeInsets.only(top: 0,left: 10),
              //                   child:
              //                   Center(
              //                     child: Text(
              //                       "Log in",style: TextStyle(fontSize: 25,color: AppColors.white),
              //
              //                     ),
              //                   ),
              //                 ),
              //               ),
              //             ))
              //             : Text(profilename,style: TextStyle(
              //                 color: AppColors.white,
              //                 fontSize: 15,
              //                 fontWeight: FontWeight.bold
              //             ),),
              //
              //
              //
              //           ),
              //         ),
              //
              //
              //       ],
              //     ),
              //
              //   ),
              // ),
              logedin == false
                  ? Container(
                //padding: EdgeInsets.only(top: 30),

                child: Container(
                    padding: EdgeInsets.only(
                        top: 20, bottom: 20, left: 10, right: 10),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginLoader()))
                             .then((value) => getprofile());
                        // logOut();
                      },
                      child: ListTile(
                          title: Card(
                            color: AppColors.splash,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0)),
                            child: Container(
                              //color: AppColors.black,
                              height: 50,
                              width: 50,
                              padding: EdgeInsets.only(top: 0, left: 10),
                              child: Center(
                                child: Text(
                                  "Log in",
                                  style: TextStyle(
                                      fontSize: 25, color: AppColors.white),
                                ),
                              ),
                            ),
                          )),
                    )),
              )
                  : Container(
                height: 100,
                decoration: BoxDecoration(
                  color: AppColors.sentColor,
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 50, left: 14),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(right: 10, top: 5),

                            // color: Colors.white,

                            child: Text(
                              profilename,
                              style: TextStyle(
                                  color: AppColors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            child: Text(
                              "+91"
                                  + mobile
                              ,
                              style: TextStyle(
                                  color: AppColors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => updated(firstname,lastname,emailed,image)))
                              .then((value) => getprofile());
                        },
                        child: Container(
                          padding: EdgeInsets.only(right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(120),
                                border: Border.all(
                                    color: AppColors.white)),
                            child: Container(
                                padding: EdgeInsets.all(2),
                                child: Icon(
                                  Icons.edit,
                                  color: AppColors.white,
                                )),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Divider(
                thickness: 5,
              ),
              Material(
                child: Container(

                  color : AppColors.splash,
                  child: InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text("Home",style: TextStyle(
                          fontSize: 15,fontWeight: FontWeight.w500,color: AppColors.white),),

                      leading:
                      Icon(Icons.home,size: 20,color: AppColors.white,),
                    ),
                  ),
                ),

              ),
              Material(
                child: Container(

                   color : AppColors.sentColor,
                  child: InkWell(
                    onTap: (){
                       Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text("Notification",style: TextStyle(
                          fontSize: 15,fontWeight: FontWeight.w500,color: AppColors.white),),

                      leading:
                      Icon(Icons.notifications,size: 20,color: AppColors.white,),
                    ),
                  ),
                ),

              ),
              Material(
                child: Container(

                    color : AppColors.sentColor,
                  child: InkWell(
                    onTap: (){
                       Navigator.push(context, MaterialPageRoute(builder: (context)=>bookPagesaloon()));
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text("Booking",style: TextStyle(
                          fontSize: 15,fontWeight: FontWeight.w500,color: AppColors.white),),

                      leading:
                      Icon(Icons.book,size: 20,color: AppColors.white,),
                    ),
                  ),
                ),

              ),
              Material(
                child: Container(
                  color : AppColors.sentColor,
                  //color : AppColors.whiteTransparent2,
                  child: InkWell(
                    onTap: () {
                      // launch("https://play.google.com/store/apps/details?id=" +
                      //    Apis.registerpackage);
                      Share.share(
                          "https://play.google.com/store/apps/details?id=" +
                              Apis.registerpackage);
                      //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text(
                        "Register As Partner",
                        style: TextStyle(fontSize: 15,color: AppColors.white),
                      ),

                      leading: Icon(
                        Icons.apps,
                        size: 22,
                        color: AppColors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Material(
                child: Container(
                  color : AppColors.sentColor,
                  //color : AppColors.whiteTransparent2,
                  child: InkWell(
                    onTap: () {
                      Share.share(
                          "https://play.google.com/store/apps/details?id=" +
                              Apis.package);
                      //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text(
                        "Refer a Friend",
                        style: TextStyle(fontSize: 15,color: AppColors.white),
                      ),

                      leading: Icon(
                        Feather.share,
                        size: 22,color: AppColors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Material(
                child: Container(
                  color : AppColors.sentColor,
                  //color : AppColors.whiteTransparent2,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Chat()));

                    },
                    // onTap: () async {
                    //   var url =
                    //       'https://api.whatsapp.com/send?l=en&phone=+91' + number;
                    //   if (await canLaunch(url)) {
                    //     await launch(url);
                    //   } else {
                    //     throw 'Could not launch $url';
                    //   }
                    //  },
                    child: ListTile(
                      // tileColor: AppColors.white,
                        title: Text(
                          "Chat Us",
                          style: TextStyle(fontSize: 15,color: AppColors.white),
                        ),
                        leading: Icon(Icons.chat,color: AppColors.white,)
                      // Image(
                      //   image: AssetImage("assets/images/whatap.png"),
                      //   height: 25,
                      //   width: 25,
                      // )
                    ),
                  ),
                ),
              ),
              // Material(
              //   child: Container(
              //
              //     color : AppColors.sentColor,
              //     child: InkWell(
              //       onTap: (){
              //         // Navigator.push(context, MaterialPageRoute(builder: (context)=>ProfilePage()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text("Profile",style: TextStyle(
              //             fontSize: 18,fontWeight: FontWeight.w500,color: AppColors.white),),
              //
              //         leading:
              //         Icon(Feather.user,size: 20,color: AppColors.white,),
              //       ),
              //     ),
              //   ),
              //
              // ),
              Material(
                child: Container(

                  color : AppColors.sentColor,
                  child: InkWell(
                    onTap: ()
                      async{
                        var url = 'https://api.whatsapp.com/send?l=en&phone=+91'+number;
                        if (await canLaunch(url)) {
                      await launch(url);
                      } else {
                      throw 'Could not launch $url';
                      }
                    },

                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text("Chat With Us",style: TextStyle(
                          fontSize: 18,fontWeight: FontWeight.w500,color: AppColors.white),),

                      leading:

                      Image(image: AssetImage("assets/images/whatap.png"
                      ),
                          height: 20,
                        width: 20,)
                    ),
                  ),
                ),

              ),
              Material(
                child: Container(
                  color : AppColors.sentColor,
                  child: InkWell(
                    onTap: () async {
                      var url = "tel:" + phonenumber;
                      if (await canLaunch(url)) {
                        await launch(url);
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                    //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));

                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text(
                        "Call Us",
                        style: TextStyle(fontSize: 15,color: AppColors.white),
                      ),

                      leading: Icon(
                        Icons.call,color: AppColors.white,
                        size: 22,
                      ),
                    ),
                  ),
                ),
              ),
               //Divider(thickness: 5,),
              // Material(
              //   child: Container(
              //
              //     color : AppColors.sentColor,
              //     child: InkWell(
              //       onTap: (){
              //         Share.share("https://play.google.com/store/apps/details?id=" +
              //            "com.makeover.nest" );
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text("Share",style: TextStyle(
              //             fontSize: 18,fontWeight: FontWeight.w500,color: AppColors.white),),
              //
              //         leading:
              //         Icon(Icons.share,size: 20,color: AppColors.white,),
              //       ),
              //     ),
              //   ),
              //
              // ),
              Material(
                child: Container(

                  color : AppColors.sentColor,
                  child: InkWell(
                    onTap: ()
                      async{
                        //var url = 'https://api.whatsapp.com/send?l=en&phone=+91'+number;
                        if (await canLaunch("mailto:$email")) {
                          await launch("mailto:$email");
                        } else {
                          throw 'Could not launch';
                        }
                    },

                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text("Email Us",style: TextStyle(
                          fontSize: 18,color: AppColors.white),),

                      leading:
                      Icon(Icons.email_outlined,size: 22,color: AppColors.white),
                    ),
                  ),
                ),

              ),
              Material(
                child: Container(
                  color : AppColors.sentColor,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Booking()));
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text(
                        "My Booking At Home",
                        style: TextStyle(fontSize: 15,color: AppColors.white),
                      ),

                      leading: Icon(
                        Icons.supervised_user_circle_outlined,color: AppColors.white,
                        size: 22,
                      ),
                    ),
                  ),
                ),
              ),
              // Material(
              //   child: Container(
              //
              //     // color : AppColors.whiteTransparent2,
              //     child: InkWell(
              //       onTap: (){
              //         //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text("Profile",style: TextStyle(
              //             fontSize: 15),),
              //
              //         leading:
              //         Icon(Feather.user,size: 22,),
              //       ),
              //     ),
              //   ),
              //
              // ),
              // Material(
              //   child: Container(
              //     //color : AppColors.whiteTransparent2,
              //     child: InkWell(
              //       onTap: () {
              //         Navigator.push(context,
              //             MaterialPageRoute(builder: (context) => Order()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text(
              //           "Order",
              //           style: TextStyle(fontSize: 15),
              //         ),
              //
              //         leading: Icon(
              //           Icons.favorite_border,
              //           size: 22,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              Material(
                child: Container(
                  color : AppColors.sentColor,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Wallet()));
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text(
                        "Wallet",
                        style: TextStyle(fontSize: 15,color: AppColors.white),
                      ),

                      leading: Icon(
                        Icons.account_balance_wallet_outlined,color: AppColors.white,
                        size: 22,
                      ),
                    ),
                  ),
                ),
              ),
              Material(
                child: Container(

                  color : AppColors.sentColor,
                  child: InkWell(
                    onTap: (){
                      launch("https://play.google.com/store/apps/details?id=" +
                          Apis.package);
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text("Review Us",style: TextStyle(
                          fontSize: 18,color: AppColors.white),),

                      leading:
                      Icon(Icons.star_border,size: 22,color: AppColors.white),
                    ),
                  ),
                ),

              ),
              Divider(
                thickness: 1,
              ),
              Container(
                color: AppColors.sentColor,
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount:page.length,
                    //pages.length,
                    itemBuilder: (context, i) {
                      return ListTile(
                        onTap: () {
                          // // // launch(insta);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Pageview(
                                    pagename: page[i]['content_title'],
                                    pagedetail: page[i]['content_description'],
                                  )));
                        },
                        title: Text(page[i]['content_title'],style: TextStyle(color: AppColors.white),),
                        leading: Icon(
                          EvilIcons.eye,
                          color: AppColors.white,
                          size: 18,
                        ),
                      );
                    },
                  )),
              // Divider(
              //   thickness: 1,
              // ),
              // Container(
              //   padding: EdgeInsets.only(left: 10, bottom: 10),
              //   child: Text(
              //     "Sign out",
              //     style: TextStyle(color: AppColors.white, fontSize: 16),
              //   ),
              // ),
              // logedin == false
              //     ? Container(
              //   //padding: EdgeInsets.only(top: 30),
              //
              //   child: Container(
              //       padding: EdgeInsets.only(
              //           top: 20, bottom: 20, left: 10, right: 10),
              //       child: InkWell(
              //         onTap: () {
              //           Navigator.push(
              //               context,
              //               MaterialPageRoute(
              //                   builder: (context) => LoginLoader()))
              //               .then((value) => getprofile());
              //           // logOut();
              //         },
              //         child: ListTile(
              //             title: Card(
              //               color: AppColors.sentColor,
              //               shape: RoundedRectangleBorder(
              //                   borderRadius: BorderRadius.circular(15.0)),
              //               child: Container(
              //                 //color: AppColors.black,
              //                 height: 50,
              //                 width: 50,
              //                 padding: EdgeInsets.only(top: 0, left: 10),
              //                 child: Center(
              //                   child: Text(
              //                     "Log in",
              //                     style: TextStyle(
              //                         fontSize: 25, color: AppColors.white),
              //                   ),
              //                 ),
              //               ),
              //             )),
              //       )),
              // )
              //     : Container(
              //   height: 100,
              //   decoration: BoxDecoration(
              //     color: AppColors.white,
              //   ),
              //   child: Container(
              //     padding: EdgeInsets.only(top: 50, left: 14),
              //     child: Row(
              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //       children: [
              //         Column(
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           children: [
              //             Container(
              //               padding: EdgeInsets.only(right: 10, top: 5),
              //
              //               // color: Colors.white,
              //
              //               child: Text(
              //                 profilename,
              //                 style: TextStyle(
              //                     color: AppColors.grayBorder,
              //                     fontSize: 15,
              //                     fontWeight: FontWeight.bold),
              //               ),
              //             ),
              //             Container(
              //               child: Text(
              //                 "+91" + mobile,
              //                 style: TextStyle(
              //                     color: AppColors.grayBorder,
              //                     fontSize: 15,
              //                     fontWeight: FontWeight.bold),
              //               ),
              //             )
              //           ],
              //         ),
              //         InkWell(
              //           onTap: () {
              //             Navigator.push(
              //                 context,
              //                 MaterialPageRoute(
              //                     builder: (context) => updated()))
              //                 .then((value) => getprofile());
              //           },
              //           child: Container(
              //             padding: EdgeInsets.only(right: 10),
              //             child: Container(
              //               decoration: BoxDecoration(
              //                   borderRadius: BorderRadius.circular(120),
              //                   border: Border.all(
              //                       color: AppColors.grayBorder)),
              //               child: Container(
              //                   padding: EdgeInsets.all(2),
              //                   child: Icon(
              //                     Icons.edit,
              //                     color: AppColors.grayBorder,
              //                   )),
              //             ),
              //           ),
              //         )
              //       ],
              //     ),
              //   ),
              // ),
              // Divider(
              //   thickness: 5,
              // ),
              // Material(
              //   child: Container(
              //     color: AppColors.toggele,
              //     child: InkWell(
              //       onTap: () {
              //         //getcall();
              //
              //         Navigator.pop(context);
              //         //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text(
              //           "Home",
              //           style: TextStyle(
              //               color: AppColors.sentColor,
              //               fontSize: 15,
              //               fontWeight: FontWeight.w500),
              //         ),
              //
              //         leading: Icon(
              //           Icons.home_outlined,
              //           color: AppColors.sentColor,
              //           size: 20,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),

              // Material(
              //   child: Container(
              //     //color : AppColors.whiteTransparent2,
              //     child: InkWell(
              //       onTap: () {
              //         Navigator.push(context,
              //             MaterialPageRoute(builder: (context) => FavoriteLoader()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text(
              //           "Wishlist",
              //           style: TextStyle(fontSize: 15),
              //         ),
              //
              //         leading: Icon(
              //           Icons.favorite,
              //           size: 22,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              Divider(
                thickness: 1,
              ),
              // Container(
              //   padding: EdgeInsets.only(left: 10),
              //   child: Text(
              //     "Communication",
              //     style: TextStyle(color: AppColors.grayText, fontSize: 16),
              //   ),
              // ),
              //
              // Material(
              //   child: Container(
              //     //color : AppColors.whiteTransparent2,
              //     child: InkWell(
              //       onTap: () async {
              //         if (await canLaunch("mailto:$email")) {
              //           await launch("mailto:$email");
              //         } else {
              //           throw 'Could not launch';
              //         }
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text(
              //           "Email Us",
              //           style: TextStyle(fontSize: 15),
              //         ),
              //
              //         leading: Icon(
              //           Icons.email_outlined,
              //           size: 22,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              // Material(
              //   child: Container(
              //     //color : AppColors.whiteTransparent2,
              //     child: InkWell(
              //       onTap: () {
              //         launch("https://play.google.com/store/apps/details?id=" +
              //             "com.kirana.mart");
              //         //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text(
              //           "Review Us",
              //           style: TextStyle(fontSize: 15),
              //         ),
              //
              //         leading: Icon(
              //           Icons.star_border,
              //           size: 22,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              // Material(
              //   child: Container(
              //
              //     //color : AppColors.whiteTransparent2,
              //     child: InkWell(
              //       onTap: (){
              //         //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text("FAQ",style: TextStyle(
              //             fontSize: 15),),
              //
              //         leading:
              //         Icon(Icons.help_center_outlined,size: 22,),
              //       ),
              //     ),
              //   ),
              //
              // ),


              // Container(
              //   padding: EdgeInsets.only(left: 10, bottom: 10),
              //   child: Text(
              //     "Sign out",
              //     style: TextStyle(color: AppColors.grayText, fontSize: 16),
              //   ),
              // ),
              // Material(
              //   child: Container(
              //     padding: EdgeInsets.only(bottom: 10),
              //
              //     //color : AppColors.whiteTransparent2,
              //     child: InkWell(
              //       onTap: () {
              //         logOut();
              //         //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
              //       },
              //       child: ListTile(
              //         // tileColor: AppColors.white,
              //         title: Text(
              //           logedin == false ? "Login" : "Logout",
              //           style: TextStyle(fontSize: 15),
              //         ),
              //
              //         leading: Icon(
              //           Icons.logout,
              //           size: 22,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              Material(
                child: Container(
                  padding: EdgeInsets.only(bottom: 10),

                  color : AppColors.sentColor,
                  child: InkWell(
                    onTap: () {
                      logOut();
                      //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
                    },
                    child: ListTile(
                      // tileColor: AppColors.white,
                      title: Text(
                        logedin == false ? "Login" : "Logout",
                        style: TextStyle(fontSize: 18,color: AppColors.white),
                      ),

                      leading: Icon(
                        Icons.logout,color: AppColors.white,
                        size: 22,
                      ),
                    ),
                  ),
                ),
              ),












            ],
          ),
        ),
      ),
      body:pages,
      // bottomNavigationBar: BottomNavigationBar(
      //   type: BottomNavigationBarType.fixed,
      //   currentIndex: _currentIndex,
      //   backgroundColor: AppColors.splash,
      //   selectedItemColor: AppColors.white,
      //   unselectedItemColor: AppColors.white.withOpacity(0.5),
      //   // selectedLabelStyle: textTheme.caption,
      //   // unselectedLabelStyle: textTheme.caption,
      //   onTap: (value) {
      //     // Respond to item press.
      //     changePage(value);
      //     controller.forward();
      //     setState(() => _currentIndex = value);
      //     //onTap: _onItemTapped,
      //   },
      //   items: [
      //
      //     BottomNavigationBarItem(
      //       title: Text('Home'),
      //       icon: Icon(Icons.home),
      //     ),
      //
      //     BottomNavigationBarItem(
      //       title: Text('Notification'),
      //       icon: Icon(Icons.notifications),
      //       // icon: AnimatedIcon(
      //       //   progress: controller,
      //       //   icon: AnimatedIcons.menu_home,
      //       // )
      //     ),
      //     BottomNavigationBarItem(
      //       title: Text('Booking'),
      //       icon: Icon(Icons.book),
      //     ),
      //     BottomNavigationBarItem(
      //       title: Text('Profile'),
      //       icon: Icon(Feather.user),
      //     ),
      //
      //   ],
      //
      //
      //
      //
      // ),




    );

  }


}