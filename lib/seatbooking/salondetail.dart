import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/timedate.dart';
import 'package:yesmadam/seatbooking/timeslot.dart';
import 'package:yesmadam/services/apis.dart';

class salondetailPage extends StatefulWidget {
  final saloonid;
  final vendorid;
  final name;
  salondetailPage({this.saloonid, this.vendorid, this.name});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return salondetailPageLoader();
  }
}

class salondetailPageLoader extends State<salondetailPage> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  var valuefirst = "0";
  var price = 0;
  var time = 0;
  var service_dets = [];

  List saloons_details = [];
  List gallery_image = [];
  List saloon_facility = [];
  List saloon_services = [];
  var saloon_name = "";
  var saloon_id = "";
  var vendor_id = "";
  var description = "";
  var address = "";
  var lat = "";
  var lot = "";
  var rating = "0";
  var totalrating = "5";
  var seat = "0";
  var dou = 0.0;

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future getdetais() async {
    try {
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.GETDETAIL_API;
      // var data = json.encode(map);
      var data = {'saloonid': widget.saloonid};

      print(data);

      var res = await apiPostRequest(url, data);
      print(res);

      // print(json.decode(res)['saloons_details']['saloon_name']);
      setState(() {
        // gallery_image = json.decode(res)['gallery_image'];
        // saloons_details = json.decode(res)['saloons_details'];
        if (json.decode(res)['status'].toString() == "1") {
          description = json.decode(res)['data']['description'];
          gallery_image = json.decode(res)['data']['image'];
          // saloon_facility = json.decode(res)['saloon_facility'];
          saloon_id = json.decode(res)['data']['id'];
          vendor_id = json.decode(res)['data']['vendorid'];
          saloon_services = json.decode(res)['data']['service'];
          address = json.decode(res)['data']['address'] +
              "," +
              json.decode(res)['data']['city'] +
              "," +
              json.decode(res)['data']['state'];
          lat = json.decode(res)['data']['latitude'];
          lot = json.decode(res)['data']['longitude'];
          seat = json.decode(res)['data']['sheet'];
          rating = json.decode(res)['data']['rateingavg'];
          // totalrating = json.decode(res)['raiting']['total_review'];
          // print(rating);
          // print(saloon_facility.length);
          // print(totalrating);
        } else {}
      });

      for (var i = 0; i <= saloon_services.length; i++) {
        var appendData = {
          "service_id": saloon_services[i]['service_id'],
          "service_name": saloon_services[i]['service_name'],
          "service_amount": saloon_services[i]['service_amount'],
          "selected": false,
        };
        service_dets.add(appendData);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    HttpClient httpClient = new HttpClient();

    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    _onLoading();
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  _setIndex(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  int _currentPage = 0;
  PageController _pageController =
      PageController(initialPage: 0, viewportFraction: 1.0);
  ScrollController scollBarController = ScrollController();
  @override
  void initState() {
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < 5) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }

      _pageController.animateToPage(
        _currentPage,
        duration: Duration(milliseconds: 200),
        curve: Curves.easeIn,
      );
    });
    // TODO: implement initState
    super.initState();

    getdetais();
  }

  List selectedServiceList = [];

  Future setPrice(int setprice, bool val, serviceid, int addTime) async {
    print(setprice);
    if (val == true) {
      setState(() {
        price = price + setprice;
        time = time + addTime;
        var data =
            // "service_id":
            serviceid.toString();
        // "service_charge": setprice.toString()

        selectedServiceList.add(data);
      });
    } else {
      setState(() {
        price = price - setprice;
        time = time - addTime;
        var data =
            // "service_id":
            serviceid.toString();
        // "service_charge": setprice.toString()

        selectedServiceList.remove(data);
      });
    }

    print(price);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Text(
          widget.name,
          style: TextStyle(color: AppColors.white),
        ),
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
      ),
      body: CustomScrollView(
        controller: scollBarController,
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              child: detail(),
            ),
          )
        ],
      ),
      bottomNavigationBar: price.toString() == "0"
          ? Container(
              height: 0,
            )
          : Container(
              padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
              child: Material(
                color: Color.fromRGBO(0, 0, 0, 1),
                borderRadius: BorderRadius.circular(5),
                child: InkWell(
                  borderRadius: BorderRadius.circular(5),
                  splashColor: Colors.blueAccent,
                  onTap: () {
                    log('321==>  $time');

                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => timePage1(
                                  '$saloon_id',
                                  vendor_id,
                                  widget.name,
                                  '$price',
                                  selectedServiceList,
                                  time.toString(),
                                )));
                    // Navigator.push(context, MaterialPageRoute(builder: (context)=>OtpLoader()));
                  },
                  child: Container(
                      height: 55,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 10, left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: Text(
                                    " ITEM",
                                    style: TextStyle(color: AppColors.white),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 2),
                                  child: Text(
                                    "₹" + price.toString() + " plus taxes",
                                    style: TextStyle(color: AppColors.white),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              children: [
                                Container(
                                  child: Text(
                                    "View Cart",
                                    style: TextStyle(color: AppColors.white),
                                  ),
                                ),
                                Container(
                                  child: Icon(
                                    Icons.arrow_right,
                                    color: AppColors.white,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )),
                ),
              ),
            ),
    );
  }

  Widget detail() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Container(
            child: corosolSlider(),
          ),
          Container(
            padding: EdgeInsets.only(top: 10, left: 20, right: 10, bottom: 0),
            child: Text(
              widget.name,
              style: TextStyle(
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      address,
                      style: TextStyle(color: AppColors.grayText, fontSize: 15),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    var url =
                        "https://www.google.com/maps/search/?api=1&query=$lat,$lot";
                    if (await canLaunch(url)) {
                      await launch(url);
                    } else {
                      throw 'Could not launch $url';
                    }
                  },
                  child: Container(
                    padding: EdgeInsets.only(right: 10),
                    child: Column(
                      children: [
                        Container(
                            padding: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.location_on,
                              color: AppColors.sentColor,
                              size: 35,
                            )),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          child: Text(
                            "Map",
                            style: TextStyle(color: AppColors.sentColor),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 10),
                  child: Container(
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 10, bottom: 10),
                      height: 40,
                      width: MediaQuery.of(context).size.width / 3.8,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(0.0),
                          color: AppColors.priceControl),
                      child: Container(
                        child: Row(
                          children: [
                            Container(
                              child: Text(
                                rating == null ? "0" : rating,
                                style: TextStyle(
                                    color: AppColors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              child: Icon(
                                Icons.star,
                                size: 12,
                                color: AppColors.white,
                              ),
                            ),
                            Container(
                              child: Text(
                                "|  " + totalrating,
                                style: TextStyle(color: AppColors.white),
                              ),
                            )
                          ],
                        ),
                      )),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10, left: 20, bottom: 5),
            child: Text(
              "Description",
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 5),
            child: Text(
              description,
              style: TextStyle(height: 1.5),
            ),
          ),
          // Container(
          //   child: saloon_facility.length!=null? Container():Divider(
          //     thickness: 2,
          //   ),
          // ),
          // Container(
          //   padding: EdgeInsets.only(top: saloon_facility.length!=null?0:10, left: saloon_facility.length!=null?0:20,),
          //   child: Text(
          //     saloon_facility.length!=null? "":"Amenities",
          //     style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
          //   ),
          // ),
          // Container(
          //   child:  saloon_facility.length!=null? Container():facitlty(),
          // ),

          // Container(
          //   padding: EdgeInsets.only(top: 10,left: 20,bottom: 5),
          //   child: Row(
          //     children: [
          //       Expanded
          //         (
          //         // flex: 1,
          //
          //           child:
          //           Row(
          //             // mainAxisAlignment:MainAxisAlignment.spaceAround,
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.only(right: 0),
          //
          //                 height: 30,
          //                 child:Icon(Icons.wifi)
          //                 //Image(image: AssetImage("assets/images/cc.png"), ),
          //               ),
          //
          //
          //               Container(
          //                 padding: EdgeInsets.only(left: 10),
          //                 child:Text("Free Wifi"),
          //               )
          //             ],
          //           ) ),
          //
          //       Expanded
          //         (
          //         // flex: 1,
          //
          //           child:
          //           Row(
          //             // mainAxisAlignment:MainAxisAlignment.spaceAround,
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.only(right: 0),
          //
          //                 height: 30,
          //                 child: Image(image: AssetImage("assets/images/cc.png"), ),
          //               ),
          //
          //
          //               Container(
          //                 padding: EdgeInsets.only(left: 10),
          //                 child:Text("AC/NON AC"),
          //               )
          //             ],
          //           ) ),
          //     ],
          //   ),
          // ),
          // Container(
          //   padding: EdgeInsets.only(top: 10,left: 20,bottom: 5),
          //   child: Row(
          //     children: [
          //       Expanded
          //         (
          //         // flex: 1,
          //
          //           child:
          //           Row(
          //             // mainAxisAlignment:MainAxisAlignment.spaceAround,
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.only(left: 0),
          //
          //                 height: 30,
          //                 child: Image(image: AssetImage("assets/images/cc.png"), ),
          //               ),
          //
          //
          //               Container(
          //                 padding: EdgeInsets.only(left: 10),
          //                 child:Text("AC/NON AC"),
          //               )
          //             ],
          //           ) ),
          //
          //       Expanded
          //         (
          //         // flex: 1,
          //
          //           child:
          //           Row(
          //             // mainAxisAlignment:MainAxisAlignment.spaceAround,
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.only(right: 0),
          //
          //                 height: 30,
          //                 child: Image(image: AssetImage("assets/images/cc.png"), ),
          //               ),
          //
          //
          //               Container(
          //                 padding: EdgeInsets.only(left: 10),
          //                 child:Text("AC/NON AC"),
          //               )
          //             ],
          //           ) ),
          //     ],
          //   ),
          // ),
          Container(
            child: Divider(
              thickness: 2,
            ),
          ),

          Container(
            child: Servicelist(),
          ),
          //  Container(
          //   height: 65,
          //   // color: Colors.grey,
          //   color: AppColors.white,
          //   child: ListView.builder(
          //     padding: EdgeInsets.only(left: 10, right: 7, top: 5, bottom: 8),
          //     shrinkWrap: true,
          //     // physics: NeverScrollableScrollPhysics(),
          //     scrollDirection: Axis.horizontal,
          //     itemCount: 3,
          //     itemBuilder: (context, i) {
          //       return Card(
          //         shape: RoundedRectangleBorder(
          //             borderRadius: BorderRadius.all(Radius.circular(0))),
          //         elevation: 2.0,
          //         child: InkWell(
          //           onTap: () {},
          //           child: Container(
          //             color: AppColors.priceControl,
          //             height: 50,
          //             width: MediaQuery.of(context).size.width / 3.4,
          //             child: InkWell(
          //               onTap: () {
          //                 //Navigator.push(context, MaterialPageRoute(builder: (context)=>OfferdetaiPage()));
          //               },
          //               child: Column(
          //                 children: [
          //                   Expanded(
          //                     child: Container(
          //                       padding: EdgeInsets.only(
          //                           top: 5, bottom: 0, right: 5, left: 5),
          //                       child: Container(
          //                         decoration: BoxDecoration(
          //                           borderRadius: BorderRadius.circular(10.0),
          //                         ),
          //                         child: Column(
          //                           crossAxisAlignment: CrossAxisAlignment.start,
          //                           children: [
          //                             Container(
          //                               child: Text(
          //                                 "Free Delivery",
          //                                 style: TextStyle(color: AppColors.white),
          //                               ),
          //                             ),
          //                             Container(
          //                               padding: EdgeInsets.only(top: 2),
          //                               child: Text(
          //                                 "on your first order",
          //                                 style: TextStyle(
          //                                     color: AppColors.whiteTransparent2,
          //                                     fontSize: 11),
          //                               ),
          //                             ),
          //                           ],
          //                         ),
          //
          //                         // height: 65,
          //                         // width: 65,
          //                       ),
          //                     ),
          //                   ),
          //                   // Expanded(
          //                   //   child: Center(
          //                   //     child: Text(
          //                   //       i == 0
          //                   //           ? "Max Safety"
          //                   //           : i == 1
          //                   //           ? "Pure Veg"
          //                   //           : i == 2
          //                   //           ? "Market"
          //                   //           : i == 3
          //                   //           ? "Trending"
          //                   //           : "",
          //                   //       style: TextStyle(
          //                   //         color: Colors.black,
          //                   //         fontSize: 15,
          //                   //       ),
          //                   //     ),
          //                   //   ),
          //                   // )
          //                 ],
          //               ),
          //             ),
          //           ),
          //         ),
          //       );
          //     },
          //   ),
          // ),
          //
          //     Container(
          //       child: Divider(
          //         thickness: 2,
          //       ),
          //     ),
          // Container(
          //   child: ListView.builder(
          //     padding: EdgeInsets.only(left: 10, right: 7, top: 0),
          //     shrinkWrap: true,
          //     physics: NeverScrollableScrollPhysics(),
          //     //scrollDirection: Axis.horizontal,
          //     itemCount: saloon_services.length,
          //     itemBuilder: (context, i) {
          //       return
          //           // Card(
          //           // shape: RoundedRectangleBorder(
          //           //     borderRadius: BorderRadius.all(Radius.circular(10))),
          //           // elevation: 2.0,
          //           Container(
          //         //height: 40,
          //         width: MediaQuery.of(context).size.width,
          //
          //         child: CheckboxListTile(
          //           title: Row(
          //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //             children: [
          //               Text(service_dets[i]['service_name']),
          //               Container(
          //                 child: Row(
          //                   children: [
          //                     Container(
          //                         padding: EdgeInsets.only(right: 15),
          //                         child: Text(
          //                           "₹ " + saloon_services[i]['mrp'],
          //                           style: TextStyle(
          //                               decoration: TextDecoration.lineThrough,
          //                               fontSize: 18),
          //                         )),
          //                     Container(
          //                         child:
          //                             Text("₹ " + service_dets[i]['charge'])),
          //                   ],
          //                 ),
          //               )
          //             ],
          //           ),
          //           // trailing:   Container(
          //           //   padding: EdgeInsets.only(left: 70,top: 5),
          //           //   child: Text(service_dets[i]['charge']),
          //           // ),
          //
          //           checkColor: AppColors.black,
          //           activeColor: Colors.white,
          //           value: service_dets[i]['selected'],
          //           onChanged: (value) async {
          //             setState(() {
          //               service_dets[i]['selected'] = value;
          //               valuefirst = service_dets[i]['service_id'].toString();
          //               // var int.parse(service_dets[i]['charge']);
          //               // price = int.parse(saloon_services[i]['charge']);
          //               // if(value == true){
          //               //   valuefirst = service_dets[i]['service_id'].toString();
          //               //   price=(price+int.parse(service_dets[i]['charge']));
          //               //   print(price);
          //               // }else{
          //               //
          //               //   price=(price-int.parse(service_dets[i]['charge']));
          //               //   print(price);
          //               // }
          //             });
          //
          //             setPrice(int.parse(service_dets[i]['charge']), value,
          //                 service_dets[i]['service_id']);
          //           },
          //         ),
          //       );
          //     },
          //   ),
          // ),
          Container(
            child: Divider(
              thickness: 2,
            ),
          ),
        ],
      ),
    );
  }

  Widget corosolSlider() {
    return Container(
//      color: Colors.white,
      height: 250,
      margin: EdgeInsets.only(
        top: 0,
      ),
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          Align(
              alignment: Alignment.centerLeft,
              child: Column(
                children: [
                  Container(
                    height: 230,
                    child: PageView.builder(
                      allowImplicitScrolling: true,
                      controller: _pageController,
                      scrollDirection: Axis.horizontal,
                      itemCount: gallery_image.length,
                      //CategoryList.length,
                      reverse: false,
                      pageSnapping: true,
                      onPageChanged: (val) => _setIndex(val),
                      itemBuilder: (context, i) {
                        return Container(
                            // padding: EdgeInsets.only(
                            //   right: 20,
                            // ),
                            child: InkWell(
                          // onTap: (){}
                          // onTap: () {
                          //   Navigator.push(
                          //       context,
                          //       MaterialPageRoute(
                          //           builder: (context) => Products(
                          //               CategoryList[i]['subcat_id'],
                          //               CategoryList[i]
                          //               ['sub_category_name'])));
                          // },
                          child: Container(
                            decoration: BoxDecoration(
                              color: AppColors.black,
                              image: DecorationImage(
                                  image:
                                      NetworkImage(gallery_image[i].toString()),
                                  fit: BoxFit.cover),
                              // borderRadius:
                              // BorderRadius.all(Radius.circular(10)),
                              // border: Border.all(color: Colors.black26)
                            ),
                          ),
                        ));
                      },
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.only(top: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                              gallery_image.length,
                              // CategoryList.length,
                              (index) => Container(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Container(
                                    width: 10,
                                    height: 10,
                                    decoration: BoxDecoration(
                                        color: _currentPage == index
                                            ? Colors.blueAccent
                                            : Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(120)),
                                        boxShadow: [
                                          BoxShadow(color: Colors.black)
                                        ]),
                                  ))),
                        ),
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }

  Widget facitlty() {
    return Column(
      children: [
        Container(
            child: GridView.builder(
          padding: EdgeInsets.only(left: 5, top: 5),
          itemCount: saloon_facility.length,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 2.6,
              crossAxisSpacing: 1.0,
              mainAxisSpacing: 1.0),
          //scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              padding: EdgeInsets.only(right: 4, bottom: 4),
              child: saloon_facility[i]['facility_icon'] != null
                  ? Container(
                      padding: EdgeInsets.only(top: 0),
                      child: Column(
                        children: [
                          Container(
                            padding:
                                EdgeInsets.only(top: 10, left: 20, bottom: 5),
                            child: Row(
                              children: [
                                Expanded(
                                    // flex: 1,

                                    child: Row(
                                  // mainAxisAlignment:MainAxisAlignment.spaceAround,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(right: 0),
                                      height: 25,
                                      child: Image(
                                        image: NetworkImage(saloon_facility[i]
                                                    ['facility_icon'] !=
                                                null
                                            ? saloon_facility[i]
                                                ['facility_icon']
                                            : ""),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                          saloon_facility[i]['name'] != null
                                              ? saloon_facility[i]['name']
                                              : ""),
                                    )
                                  ],
                                )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
            );
          },
        )),
      ],
    );
  }

  Widget Servicelist() {
    var device = MediaQuery.of(context).size;
    return Container(
      //height: 00,
      padding: EdgeInsets.all(10),
      child: Container(
        // shape: RoundedRectangleBorder(
        //   borderRadius: BorderRadius.circular(15.0),
        //),
        child: Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
          child: GridView.builder(
              itemCount: saloon_services.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: device.width > 600.0 ? 4 : 2,
                  childAspectRatio: 0.7,
                  crossAxisSpacing: 1.0,
                  mainAxisSpacing: 1.0),
              itemBuilder: (Context, i) {
                return Container(
                  //height: 300,
                  child: InkWell(
                    onTap: () {
                      log('1234===>  ${saloon_services[i]['service_time']}');
                      var val = service_dets[i]['selected'];
                      var value;
                      setState(() {
                        if (val == true) {
                          value = false;
                        } else {
                          value = true;
                        }
                        service_dets[i]['selected'] = value;
                        valuefirst = service_dets[i]['service_id'].toString();
                      });
                      dou = double.parse(
                          service_dets[i]['service_amount'].toString());
                      var num = dou.toStringAsFixed(0);
                      setPrice(
                        int.parse(num.toString()),
                        value,
                        service_dets[i]['service_id'],
                        int.parse(
                            saloon_services[i]['service_time'].toString()),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: AppColors.grayBorder,
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(120.0),
                              border: Border.all(
                                color: AppColors.grayBorder,
                              ),
                              image: DecorationImage(
                                  image: NetworkImage(
                                      //"",
                                      saloon_services[i]['image']),
                                  fit: BoxFit.fill),
                            ),
                            padding: EdgeInsets.only(
                                top: 5, bottom: 0, right: 0, left: 10),
                            height: 95,
                            width: 95,
                          ),
                          Flexible(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                child: Center(
                                  child: Text(
                                    // saloon_services[i]['service_name'].toString().toUpperCase(),
                                    service_dets[i]['service_name']
                                        .toString()
                                        .toUpperCase(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                    ),
                                    textAlign: TextAlign.center,
                                    maxLines: 2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                // Container(
                                //     padding: EdgeInsets.only(right: 15, left: 28),
                                //     child: Text(
                                //       "₹ " + saloon_services[i]['service_amount'],
                                //       style: TextStyle(
                                //           decoration: TextDecoration.lineThrough,
                                //           fontSize: 16),
                                //     )),
                                Container(
                                    child: Text(
                                  service_dets[i]['service_amount'] == null
                                      ? ""
                                      : "₹ " +
                                          service_dets[i]['service_amount'],
                                  style: TextStyle(fontSize: 16),
                                )),
                              ],
                            ),
                          ),
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(right: 10, left: 10),
                                child: Text(
                                  saloon_services[i]['service_time'] == null
                                      ? ""
                                      : "Service Min " +
                                          saloon_services[i]['service_time'],
                                  style: TextStyle(fontSize: 12),
                                  maxLines: 2,
                                )),
                          ),
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(right: 10, left: 10),
                                child: Text(
                                  "Saloon Seat  " + seat,
                                  style: TextStyle(fontSize: 12),
                                  maxLines: 2,
                                )),
                          ),
                          Container(
                              child: Flexible(
                            child: Container(
                              // height: 50,

                              // color: AppColors.sentColor,
                              padding: EdgeInsets.only(bottom: 10),
                              child: CheckboxListTile(
                                checkColor: AppColors.black,
                                activeColor: AppColors.sentColor,
                                value: service_dets[i]['selected'],
                                onChanged: (value) async {
                                  setState(() {
                                    service_dets[i]['selected'] = value;
                                    valuefirst = service_dets[i]['service_id']
                                        .toString();
                                  });
                                  dou = double.parse(service_dets[i]
                                          ['service_amount']
                                      .toString());
                                  var num = dou.toStringAsFixed(0);
                                  setPrice(
                                      int.parse(num.toString()),
                                      value,
                                      service_dets[i]['service_id'],
                                      int.parse(saloon_services[i]
                                              ['service_time']
                                          .toString()));
                                },
                                title: Container(
                                  // height: 50,
                                  //color: AppColors.sentColor,
                                  child: Text(
                                    "Book",
                                    style:
                                        TextStyle(color: AppColors.sentColor),
                                  ),
                                ),
                              ),
                            ),
                          ))
                        ],
                      ),
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }
}
