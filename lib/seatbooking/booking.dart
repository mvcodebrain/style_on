import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

import 'bookingdetail.dart';

class bookPagesaloon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return bookPageLoader();
  }
}

class bookPageLoader extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return bookPageView();
  }
}

class bookPageView extends State<bookPageLoader> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }
  List records = [];
  bool datafound = false;

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future getbooking() async {
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      try {
        setState(() {
          loading = true;
          ani = false;
          fail = false;
          _color = Colors.black.withOpacity(0.3);
        });

        var url = Apis.BOOKINGHISTORY_API2;
        var map = Map<String, dynamic>();

        var data = {"customer_id":
        sp.getString("userid")
        };
        print(data.toString());
        var res = await apiPostRequest(url, data);
        print(res);
        setState(() {
          if (json.decode(res)['status'].toString() == "1") {
            records = json.decode(res)['data'];
            print(records);
            loading = false;
            datafound = true;
          } else {
            datafound = false;
          }
        });
      } catch (e) {
        print("Network Fail");
      }
    } else {
      datafound = false;
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future cancel(bookingid) async{
    var sp = await SharedPreferences.getInstance();
    try {
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.CANCEL_API2;
      var map = Map<String, dynamic>();

      var data = {

        "booking_id":bookingid,

      };
      print(data.toString());
      var res = await apiPostRequest(url, data);
      print(res);
      // setState(() {
      //   if (json.decode(res)['status'].toString() == "1") {
      //     records = json.decode(res)['records'];
      //     print(records);
      //     loading = false;
      //     datafound = true;
      //   } else {
      //     datafound = false;
      //   }
      // });
      getbooking();
    } catch (e) {
      print("Network Fail");
    }

  }




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getbooking();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(
        title: Text("Booking",style: TextStyle(color: AppColors.white),),
        elevation: 0,
        backgroundColor: AppColors.sentColor,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: CustomScrollView(
        slivers: [
          datafound == false
              ? SliverToBoxAdapter(
                  child: imge(),
                )
              : SliverToBoxAdapter(
                  child: Container(
                    child: booking(),
                  ),
                ),
        ],
      ),
    );
  }

  Widget booking() {
    return Container(
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: records.length,
          itemBuilder: (context, i) {
            return Container(
              child: Card(
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              padding: EdgeInsets.only(top: 5),
                              child:
                              Text("Booking ID  " + records[i]['id'],style: TextStyle(
                                fontSize: 18
                              ),),
                          ),

                          Container(

                            padding: EdgeInsets.only(top: 0,bottom: 0),
                            child:records[i]['book_status'].toString()=="pending"?InkWell(
                              onTap: (){
                                cancel(records[i]['id']);
                              },
                              child: Container(

                                height: 35,
                                width: MediaQuery.of(context).size.width/3.5,


                                decoration: BoxDecoration(
                                  color: AppColors.black,

                                  border: Border.all(color: AppColors.grayBorder),
                                  borderRadius: BorderRadius.all(Radius.circular(10),

                                  ),

                                ),
                                child:
                                Center(
                                  child: Text("CANCEL",style: TextStyle(color: AppColors.white,
                                      fontSize: 18 ),),
                                ),
                              ),
                            ):Container(),
                          ),
                        ],
                      ),


                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Text("Visting Date:-" + records[i]['book_date'])),
                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Text("Slot:-" +
                              records[i]['book_time_start'] +
                              " to" +
                              records[i]['book_time_end'])),

                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Text(
                              "Saloon Name:-" + records[i]['name'])),
                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Row(
                           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Container(
                                    child: Text("Address:-"
                                       + records[i]['address']
                                        + ","+ records[i]['city']+ ","+ records[i]['state']
                                    )
                                ),
                              ),

                            ],
                          )),

                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: services(i)
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Text("Total:-" + records[i]['book_total_amount'])),
                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Text("Booking Date:-" + records[i]['book_create_at'])),
                      Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text(
                            records[i]['book_status'].toString().toUpperCase(),
                            style: TextStyle(
                              fontSize: 20,
                                color: records[i]['book_status'].toString()=="pending"?AppColors.redcolor3:AppColors.black,
                                fontWeight: FontWeight.bold,

                            ),
                          )),
                      Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  child: Text("Take me to locations"
                                     ,style: TextStyle(
                                          fontSize: 16,
                                        color: AppColors.lightblue
                                      ) )),
                              InkWell(
                                  onTap: ()
                                  async{
                                    var url= "https://www.google.com/maps/search/?api=1&query=${records[i]['latitude']},${records[i]['longitude']}";
                                    if (await canLaunch(url)) {
                                      await launch(url);
                                    } else {
                                      throw 'Could not launch $url';
                                    }
                                  },
                                  child: Container(
                                      padding: EdgeInsets.only(right: 10),
                                      child: Icon(
                                        Icons.send,
                                        color: AppColors.primary,
                                        size: 30,
                                      )))
                            ],
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [

                          InkWell(
                            onTap: (){
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Bookingdetail2(
                                          records[i]['id']

                                      ))
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 10),

                              height: 40,
                              width: 120,
                              decoration: BoxDecoration(
                                color: AppColors.sentColor,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child:
                              Center(child: Text(loading == true ? "Loading.." :"VIEW DETAILS",style: TextStyle(color: AppColors.white,
                                fontSize: 16,),)),
                            ),
                          ),
                        ],
                      ),




                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }

  Widget imge() {

    return
      Container(
      color: AppColors.white,


      child: Image(

        image: AssetImage("assets/images/datanotfound.jpg"),
        height: 600,
      ),
    );
  }

  Widget services(ind){
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: records[ind]['book_service'].length,
      itemBuilder: (context, i){
        return Container(
            padding: EdgeInsets.only(top: 5),
            child: Text(records[ind]['book_service'][i]['service_name'] == null ?"":"Service:-"+records[ind]['book_service'][i]['service_name'])
        );
      },
    );
  }
}
