import 'dart:ui';

import 'package:flutter/material.dart';


class AppColors {
  static const Color secondary = const Color(0xFF440F09);
  static const Color bottomUnslected = const Color(0xFF999999);
  static const Color primary = const Color(0xFF00e687);
  static const Color splash = const Color(0xFF1b2443);



  // e94c3c
  static const Color accent = const Color(0xFFffffff);
  static const Color assentDark = const Color(0xFF1b2443);
  // static const Color assentDark = const Color(0xFFfac926);
  // static const Color assentDark = const Color(0xFF811B17);
  static const Color textColor = const Color(0xFFffffff);
  static const Color backColor = const Color(0xFFffffff);
  static const Color hintColor = const Color(0xFF878787);
  // static const Color buttonTopGradient = const Color(0xFF811B17);
  static const Color buttonTopGradient = const Color(0xFF811B17);
  // static const Color sentColor = const Color(0xFFC90000);
  static const Color sentColor = const Color(0xFFfac926);
  static const Color sentColortransparent = const Color(0x34b0eefc);
  static const Color receiveColor = const Color(0xFF008409);
  static const Color buttonBottomGradient = const Color(0xFF440F09 );
  static const Color lightgreen = const Color(0xFF9AC166);
  static const Color whitetransperent = const Color(0x80FFFFFF);
  static const Color lightblue = const Color(0xFFc1d8ff);
  static const Color listLightTextColor = const Color(0xFF707070);
  static const Color dividerColor = const Color(0xFFE3E9EC);
  static const Color secondLayer = const Color(0xff9D8481);
  static const Color firstLayer = const Color(0xff79524E);
  static const Color spinbutton = const Color(0xffDDBD67);
  static const Color segcolor1 = const Color(0xff93144b);
  static const Color segcolor2 = const Color(0xff87a624);
  static const Color segcolor3 = const Color(0xff443049);
  static const Color redcolor3 = const Color(0xFFff3f6c);

  static const Color segcolor4 = const Color(0xffe7882a);
  static const Color priceControl = const Color(0xff39A70D);
  static const Color blackTransparent = const Color(0x60000000);
  static const Color black75 = const Color(0xB3000000);
  static const Color black12 = const Color(0x1F000000);
  static const Color grayBorder = const Color(0xB39A9999);
  static const Color grayText = const Color(0xB36A6A6A);
  static const Color whiteTransparent = const Color(0xBFFFFFFF);
  static const Color colorBack = const Color(0xBFFCFCFC);
  static const Color bgColor = const Color(0xBFf9f9f9);
  static const Color formborder = const Color(0xFFCBCACA);
  static const Color toggele = const Color(0xFFe8e8e8);
  static const Color selectedItemColor = const Color(0xFF0095ff);

  static const Color appbar = const Color(0xFF6739b7);

//  06BD90

  static const Color accent2 = const Color(0xFF305697);
  static const Color primary2 = const Color(0xFF305697);
  static const Color tabcolor = const Color(0xFF305697);
  static const Color buttons = const Color(0xFF0c68c5);
  static const Color grey2 = const Color(0xFF585858);
  static const Color grayText2 = const Color(0xB36A6A6A);
  static const Color formborder2 = const Color(0xFFCBCACA);
  static const Color black = const Color(0xFF000000);
  static const Color white = const Color(0xFFffffff);
  static const Color lightblue2 = const Color(0xFFE8F3FF);
  static const Color secondary2 = const Color(0xFF440F09);
  static const Color bgColor2 = const Color(0xBFf9f9f9);
  static const Color whiteTransparent2 = const Color(0xBFFFFFFF);
  static const Color priceControl2 = const Color(0xff39A70D);
  static const Color blackTransparent2 = const Color(0x60000000);
  static const Color textColor2 = const Color(0xFFffffff);
  static const Color bottomUnslected2 = const Color(0xFF999999);
  static const Color dividerColor2 = const Color(0xFFE3E9EC);
  static const Color receiveColor2 = const Color(0xFF008409);






  static const LinearGradient buttonGradient = const LinearGradient(
      colors: [secondary, primary, accent],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);

  static const LinearGradient buttonGradient1 = const LinearGradient(
      colors: [primary,accent],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);
  static const LinearGradient buttonGradient2 = const LinearGradient(
      colors: [accent,primary],
      end: Alignment.topLeft,
      begin: Alignment.topRight);

  static const LinearGradient navItemsGradiant = const LinearGradient(
      colors: [whiteTransparent,accent],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);

  static const LinearGradient trasparantGradiant = const LinearGradient(
      colors: [Colors.transparent,Colors.transparent],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);

  static const LinearGradient bgGradient = const LinearGradient(
      colors: [secondary, primary, accent],
      end: Alignment.bottomRight,
      begin: Alignment.topLeft);
  static const LinearGradient profileGradiant = const LinearGradient(
      colors: [assentDark, secondary],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);
  static const LinearGradient buttonBg = const LinearGradient(
      colors: [AppColors.buttonTopGradient, AppColors.buttonBottomGradient],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);
  static const LinearGradient spinGradient = const LinearGradient(
      colors: [lightgreen, lightblue],
      end: Alignment.bottomCenter,
      begin: Alignment.topCenter);





  static const LinearGradient buttonGradient_ui2 = const LinearGradient(
      colors: [ primary2, accent2],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);
  static const LinearGradient buttonGradient21 = const LinearGradient(
      colors: [accent2,primary2],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);

  static const LinearGradient trasparantGradiant2 = const LinearGradient(
      colors: [Colors.transparent,Colors.transparent],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);

  static const LinearGradient navItemsGradiant2 = const LinearGradient(
      colors: [primary2,accent2],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);

  static const LinearGradient appGrediant2 = const LinearGradient(
      colors: [primary2,accent2],
      end: Alignment.topLeft,
      begin: Alignment.bottomRight);

}
