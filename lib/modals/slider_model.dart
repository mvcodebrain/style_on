class SliderModel {
  String status;
  String message;
  List<Sliders> sliders;

  SliderModel({this.status, this.message, this.sliders});

  SliderModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['sliders'] != null) {
      sliders = new List<Sliders>();
      json['sliders'].forEach((v) {
        sliders.add(new Sliders.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.sliders != null) {
      data['sliders'] = this.sliders.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Sliders {
  String id;
  String sliderName;
  String sliderImage;
  String createdAt;
  String image;

  Sliders(
      {this.id, this.sliderName, this.sliderImage, this.createdAt, this.image});

  Sliders.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sliderName = json['slider_name'];
    sliderImage = json['slider_image'];
    createdAt = json['created_at'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['slider_name'] = this.sliderName;
    data['slider_image'] = this.sliderImage;
    data['created_at'] = this.createdAt;
    data['image'] = this.image;
    return data;
  }
}