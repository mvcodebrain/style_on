class ProductModel {
  String status;
  String message;
  List<Productlist> productlist;

  ProductModel({this.status, this.message, this.productlist});

  ProductModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['productlist'] != null) {
      productlist = new List<Productlist>();
      json['productlist'].forEach((v) {
        productlist.add(new Productlist.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.productlist != null) {
      data['productlist'] = this.productlist.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Productlist {
  String productId;
  String name;
  String category;
  String subcategory;
  String description;
  String costPrice;
  String mrp;
  String sellPrice;
  String image;
  String status;
  String createDate;
  String updateDate;

  Productlist(
      {this.productId,
        this.name,
        this.category,
        this.subcategory,
        this.description,
        this.costPrice,
        this.mrp,
        this.sellPrice,
        this.image,
        this.status,
        this.createDate,
        this.updateDate});

  Productlist.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    name = json['name'];
    category = json['category'];
    subcategory = json['subcategory'];
    description = json['description'];
    costPrice = json['cost_price'];
    mrp = json['mrp'];
    sellPrice = json['sellPrice'];
    image = json['image'];
    status = json['status'];
    createDate = json['create_date'];
    updateDate = json['update_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.productId;
    data['name'] = this.name;
    data['category'] = this.category;
    data['subcategory'] = this.subcategory;
    data['description'] = this.description;
    data['cost_price'] = this.costPrice;
    data['mrp'] = this.mrp;
    data['sellPrice'] = this.sellPrice;
    data['image'] = this.image;
    data['status'] = this.status;
    data['create_date'] = this.createDate;
    data['update_date'] = this.updateDate;
    return data;
  }
}