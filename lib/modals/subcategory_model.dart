class SubcatsModel {
  String status;
  String message;
  List<SubCategory> subCategory;

  SubcatsModel({this.status, this.message, this.subCategory});

  SubcatsModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['sub_category'] != null) {
      subCategory = new List<SubCategory>();
      json['sub_category'].forEach((v) {
        subCategory.add(new SubCategory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.subCategory != null) {
      data['sub_category'] = this.subCategory.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SubCategory {
  String id;
  String catId;
  String categoryname;
  String categoryimage;
  String status;
  String createdAt;
  String image;

  SubCategory(
      {this.id,
      this.catId,
      this.categoryname,
      this.categoryimage,
      this.status,
      this.createdAt,
      this.image});

  SubCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    catId = json['cat_id'];
    categoryname = json['categoryname'];
    categoryimage = json['categoryimage'];
    status = json['status'];
    createdAt = json['created_at'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cat_id'] = this.catId;
    data['categoryname'] = this.categoryname;
    data['categoryimage'] = this.categoryimage;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['image'] = this.image;
    return data;
  }
}