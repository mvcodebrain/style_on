
class CategoryModel {
  String status;
  String message;
  List<Categories> categories;

  CategoryModel({this.status, this.message, this.categories});

  CategoryModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Categories {
  String id;
  String categoryname;
  String categoryimage;
  String categoryslider;
  String status;
  String added;
  String image;

  Categories(
      {this.id,
      this.categoryname,
      this.categoryimage,
      this.categoryslider,
      this.status,
      this.added,
      this.image});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryname = json['categoryname'];
    categoryimage = json['categoryimage'];
    categoryslider = json['categoryslider'];
    status = json['status'];
    added = json['added'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['categoryname'] = this.categoryname;
    data['categoryimage'] = this.categoryimage;
    data['categoryslider'] = this.categoryslider;
    data['status'] = this.status;
    data['added'] = this.added;
    data['image'] = this.image;
    return data;
  }
}