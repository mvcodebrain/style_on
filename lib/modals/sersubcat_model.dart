class ServsubcatModel {
  String status;
  String message;
  List<Response> response;

  ServsubcatModel({this.status, this.message, this.response});

  ServsubcatModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['response'] != null) {
      response = new List<Response>();
      json['response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.response != null) {
      data['response'] = this.response.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Response {
  Data data;

  Response({this.data});

  Response.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String subcategoryname;
  List<Service> service;

  Data({this.subcategoryname, this.service});

  Data.fromJson(Map<String, dynamic> json) {
    subcategoryname = json['subcategoryname'];
    if (json['service'] != null) {
      service = new List<Service>();
      json['service'].forEach((v) {
        service.add(new Service.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subcategoryname'] = this.subcategoryname;
    if (this.service != null) {
      data['service'] = this.service.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Service {
  String id;
  String catId;
  String subId;
  String tittle;
  String oldPrice;
  String newPrice;
  String duration;
  String description;
  Details details;
  String status;
  String productImages;
  String images;

  Service(
      {this.id,
        this.catId,
        this.subId,
        this.tittle,
        this.oldPrice,
        this.newPrice,
        this.duration,
        this.description,
        this.details,
        this.status,
        this.productImages,
        this.images});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    catId = json['cat_id'];
    subId = json['sub_id'];
    tittle = json['tittle'];
    oldPrice = json['old_price'];
    newPrice = json['new_price'];
    duration = json['duration'];
    description = json['description'];
    details =
    json['details'] != null ? new Details.fromJson(json['details']) : null;
    status = json['status'];
    productImages = json['product_images'];
    images = json['images'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cat_id'] = this.catId;
    data['sub_id'] = this.subId;
    data['tittle'] = this.tittle;
    data['old_price'] = this.oldPrice;
    data['new_price'] = this.newPrice;
    data['duration'] = this.duration;
    data['description'] = this.description;
    if (this.details != null) {
      data['details'] = this.details.toJson();
    }
    data['status'] = this.status;
    data['product_images'] = this.productImages;
    data['images'] = this.images;
    return data;
  }
}

class Details {
  List<String> heading;
  List<String> description;

  Details({this.heading, this.description});

  Details.fromJson(Map<String, dynamic> json) {
    heading = json['heading'].cast<String>();
    description = json['description'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['heading'] = this.heading;
    data['description'] = this.description;
    return data;
  }
}