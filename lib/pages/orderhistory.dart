import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

class Orderhistory extends StatefulWidget {
  final orderid;
  Orderhistory(this.orderid);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OrderhistoryLoader();
  }
}

class OrderhistoryLoader extends State<Orderhistory> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  List historydetail = [];
  List producthis = [];
  List image = [];
  var totalprice="";

  Future getOrderhistorylist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.ORDERHISTORYDETAIL_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {"order_id": widget.orderid};
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        historydetail = json.decode(res)['data'];
        producthis = json.decode(res)['data'][0]['productData'];
        totalprice= json.decode(res)['data'][0]['total_price'];
        print(historydetail.length);
      } else {
        historydetail = [];
      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  @override
  void initState() {
    getOrderhistorylist();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Container(
          child: Text("Order Details"),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(child: data()),
          ),
          SliverToBoxAdapter(
            child: productdetail(),
          ),
          SliverToBoxAdapter(
            child: pricedetail(),
          )
        ],
      ),
    );
  }

  Widget data() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5, right: 5, left: 5),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: historydetail.length,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 5, right: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                                "Order Date: " + historydetail[i]['placed_at']),
                          ),
                          // Container(
                          //   child: Text("Order Id: 18 Mar 2021"),
                          // )
                        ],
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
                      color: AppColors.grayBorder,
                      height: 1,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                          child: Text("Order Id:"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width: MediaQuery.of(context).size.width / 2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text: historydetail[i]['order_key'],
                                        style: TextStyle(
                                          color: AppColors.black,
                                          fontSize: 12,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                          child: Text("Delivery Time"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width: MediaQuery.of(context).size.width / 2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text: "Within " +
                                            historydetail[i]
                                                ['delivery_hour'] +
                                            " Hours",
                                        style: TextStyle(
                                          color: AppColors.black,
                                          fontSize: 14,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                          child: Text("Payment Mode"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width: MediaQuery.of(context).size.width / 2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text: historydetail[i]['payment_mode'],
                                        style: TextStyle(
                                          color: AppColors.black,
                                          fontSize: 14,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                          child: Text(
                            "₹" + historydetail[i]['total_price'],
                            style: TextStyle(color: AppColors.redcolor3),
                          ),
                          alignment: Alignment.topLeft,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10, top: 5, bottom: 8),
                          child: Text("Status"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width: MediaQuery.of(context).size.width / 2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text: historydetail[i]['status'] == "0"
                                            ? "Pending"
                                            : historydetail[i]['status'] == "1"
                                                ? "Deliverd"
                                                : historydetail[i]['status'] ==
                                                        "2"
                                                    ? "Cancel"
                                                    : historydetail[i]
                                                                ['status'] ==
                                                            "3"
                                                        ? "Cancel"
                                                        : "",
                                        style: TextStyle(
                                          color: historydetail[i]['status'] == "2" ? AppColors.redcolor3:historydetail[i]['status'] == "2" ? AppColors.redcolor3:
                                          AppColors.black,
                                          fontSize: 14,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }

  Widget productdetail() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5, right: 5, left: 5),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: producthis.length,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              height: 120,
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 20),
                      child: ListTile(
                        leading: Container(
                          // padding: EdgeInsets.only(top: 20),
                          width: MediaQuery.of(context).size.width / 3.9,
                          //height: 200,

                          decoration: BoxDecoration(
                              //color: AppColors.lightgreen,
                              borderRadius: BorderRadius.circular(10.0),
                              // border: Border.all(
                              //   color: AppColors.grayBorder,
                              //
                              //
                              // ),
                              image: DecorationImage(
                                  image: NetworkImage(
                                    producthis[i]['image'],),
                                  fit: BoxFit.contain)),
                        ),
                        title: Container(
                          child: Text( producthis[i]['name']),
                        ),
                        trailing: Container(
                          child: Text("₹"+  producthis[i]['sell_price']),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }

  Widget pricedetail() {
    return Container(
      padding: EdgeInsets.only(right: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Text("Product Amount:  "+totalprice +".00"),
          ),
          // Container(
          //   padding: EdgeInsets.only(top: 10, bottom: 10),
          //   child: Text("Delivery Charge: ₹199.00"),
          // ),
          // Container(
          //   padding: EdgeInsets.only(top: 10, bottom: 10),
          //   child: Text("Wallet Amount: ₹199.00"),
          // ),
          // Container(
          //   padding: EdgeInsets.only(top: 10, bottom: 10),
          //   child: Text("Grand Total: ₹199.00"),
          // ),
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 50),
            child: Text("Total Paid Amount:  "+totalprice +".00"),
          ),
        ],
      ),
    );
  }
}
