import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/Notification.dart';
import 'package:yesmadam/pages/productdetali.dart';
import 'package:yesmadam/pages/productlist.dart';
import 'package:yesmadam/pages/rating.dart';
import 'package:yesmadam/pages/wallet.dart';
import 'package:yesmadam/seatbooking/Home.dart';
import 'package:yesmadam/servicepage/servicedetail.dart';
import 'package:yesmadam/services/apis.dart';
import 'locations.dart';
import 'login.dart';

 import 'package:google_maps_webservice/places.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomepageLoader();
  }
}

class HomepageLoader extends State<HomePage>
    with SingleTickerProviderStateMixin {
  List homeSlider=[];
  List category=[];

  List productcategory = [];

  List product = [];
  List productslider = [];
  List testimonial=[];

  var productids ="";

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  // var lat="";
  // var lot="";
  var location = TextEditingController();
  var lattitude = "";
  var longitude = "";



  Future getCurrentLocation() async {
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    lattitude = position.latitude.toString();
    longitude = position.longitude.toString();
    var lat = position.latitude;
    var lng = position.longitude;
    final coordinates = new Coordinates(lat, lng);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    print(addresses.first.postalCode);
    var first = addresses.first;
    print("${first.featureName} : ${first.addressLine}");
    setState(() {
      locations = first.addressLine.trimRight();
      // pin=addresses.first.postalCode.toString();
      print(locations);
 });

  }


  var kGoogleApiKey = "AIzaSyAyRY9zVqzDFo8Qc2puq8T6gYiTARTdnNo";

  var locations = "What is your location";
  Future _getLocations() async {
    Prediction p = await PlacesAutocomplete.show(
        context: context, apiKey: kGoogleApiKey, mode: Mode.overlay);
    setState(() {
      locations = p.description.toString();
      print(locations);
    });
    Navigator.pop(context);
  }

  void _handleTap() async {
    Prediction p = await _getLocations();

    if (p == null) return;

    print(p.description);
  }

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _setIndex(int index) {
    setState(() {
      _currentPage = index;
    });
  }
  _setIndexx(int index) {
    setState(() {
      _currentPages = index;
    });
  }

  Future getLocation() async {
    var sp = await SharedPreferences.getInstance();
    var loc = sp.getString("location");
    print(loc);
    setState(() {
      locations = loc;
    });
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future gethomeslider() async {
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.SLIDER_API;

    var res = await apiPostRequestww(url);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1")
      {
        homeSlider = json.decode(res)['sliders'];
        print(homeSlider.length);
      }
      else {

      }

    });
  }
  Future<String> apiPostRequestww(String url) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   //  Navigator.pop(context);
    return reply;
  }

  // Future getcategory() async {
  //   setState(() {
  //     loading = true;
  //     ani = false;
  //     fail = false;
  //     _color = Colors.black.withOpacity(0.3);
  //     // Navigator.pop(context);
  //   });
  //
  //   var url = Apis.CATEGORY_API;
  //
  //   var res = await apiPostRequestw(url);
  //
  //   print(res);
  //   setState(()
  //   {
  //     if (json.decode(res)['status'].toString() == "1")
  //     {
  //       category = json.decode(res)['data'];
  //       print(category.length);
  //     } else
  //       {
  //       //category = [];
  //     }
  //
  //   });
  // }
  var terms="";
  Future getcategory() async {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.CATEGORY_API;

    var res = await apiPostRequestw(url);

    print(res);
    setState(()
    {
      if (json.decode(res)['status'].toString() == "1")
      {
        category = json.decode(res)['data'];
        terms=json.decode(res)['data'][0]['terms'];
        print(category.length);
        sp.remove("term");
        // sp.setString("term",terms);
        // print(sp.get("term"));
      } else
      {
        //category = [];
      }

    });
  }
  Future<String> apiPostRequestw(String url) async {
   // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }
  Future getcategoryproduct() async {
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.CATEGORYPRODUCT_API;

    var res = await apiPostRequest(url);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        productcategory = json.decode(res)['categoryList'];
        print(productcategory.length);
      } else {
        productcategory = [];
      }
      loading = false;
    });
  }

  Future<String> apiPostRequest(String url) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
     // Navigator.pop(context);
    return reply;
  }

  var selectedtab = "";

  var sellPrice ="";
  var productid="";
  var cartstatus="";

  Future getProduct() async {
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.PRODUCT_API;
      var data = {
        "user_id":user,
        'cat_id': selectedtab,
      };

      var res = await apiPostRequestss(url, data);

      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {
          product = json.decode(res)['data'];
        });
        print(product);
      }
      else {
        setState(() {
          product = [];
        });
      }}
    else{
      try {
        //var sp = await SharedPreferences.getInstance();
        setState(() {
          loading = true;
          ani = false;
          fail = false;
          _color = Colors.black.withOpacity(0.3);
          // Navigator.pop(context);
        });

        var url = Apis.PRODUCT_API;

        var data = {
          'cat_id': selectedtab,
        };

        var res = await apiPostRequests(url, data);

        print(res);
        setState(() {
          if (json.decode(res)['status'].toString() == "1")
          {
            product = json.decode(res)['data'];


          }
          else{
            product = [];

          }
          loading = false;
        });
      }
      catch (e) {
        print("Network Fail");
      }

    }

  }

  selectTab(tabid) async {
    setState(() {
      selectedtab = tabid;
    });
  }

  Future<String> apiPostRequests(String url, data) async {
   //  _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   // Navigator.pop(context);
    return reply;
  }

  Future getProductAddtocart(productid,sellPrice) async {
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.PRODUCTADDTOCART_API;

      var data = {
        "user_id": sp.getString("userid"),
        "product_id":productid,
        "quantity":"1",
        "unit_price":sellPrice
      };
      print(data);

      var res = await apiPostRequestss(url, data);
     // print(res);


      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
     getProduct();
    // getProductAddtocart();
  }

  Future<String> apiPostRequestss(String url, data) async {
     _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getProductRemoveAddtocart(productid) async {
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.REMOVEPRODUCTADDTOCART_API;

      var data = {
        "user_id": sp.getString("userid"),
        "product_id":productid



      };
      print(data);

      var res = await apiPostRequestsse(url, data);
      // print(res);


      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
    getProduct();
    // getProductAddtocart();
  }

  Future<String> apiPostRequestsse(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }

  Future getproductslider() async {
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.PRODUCTSLIDER_API;

    var res = await apiPostRequested(url);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        productslider = json.decode(res)['data'];
        print(productslider.length);
      } else {

      }
      loading = false;
    });
  }

  Future<String> apiPostRequested(String url) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    // Navigator.pop(context);
    return reply;
  }

  Future gettestimonal() async {
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.TESTIMONIAL_API;

    var res = await apiPostRequesttes(url);

    print(res);
    setState(()
    {
      if (json.decode(res)['status'].toString() == "1")
      {
        testimonial = json.decode(res)['data'];
        print(testimonial.length);
      } else
      {
        //category = [];
      }

    });
  }
  Future<String> apiPostRequesttes(String url) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }
  int _currentPage = 0;
  int _currentPages=0;
  PageController _pageController =
      PageController(initialPage: 0, viewportFraction: 1.0);
  PageController _pageControllerr =
  PageController(initialPage: 0, viewportFraction: 1.0);
  @override
  void initState() {
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < 5) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }

      _pageController.animateToPage(
        _currentPage,
        duration: Duration(milliseconds: 350),
        curve: Curves.easeIn,
      );
    });
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPages < 5) {
        _currentPages++;
      } else {
        _currentPages = 0;
      }

      _pageControllerr.animateToPage(
        _currentPages,
        duration: Duration(milliseconds: 350),
        curve: Curves.easeIn,
      );
    });


    // TODO: implement initState
    super.initState();
    getcategory();
    gethomeslider();
    getcategoryproduct();
    getProduct();
    getproductslider();
    getCurrentLocation();
    getLocation();
    gettestimonal();

  }
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));

    setState(() {
      HomePage();
      // new HomePage();
    });

    return null;
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: AppColors.lightblue,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: AppColors.white,
          ),
          onPressed: () {
           Navigator.pop(context);
          },
        ),
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            Container(
              child: InkWell(
                onTap: () {
                 // gethomeslider();
                 // getcategory();
                  getCurrentLocation();
                },
                child: Container(
                  padding: EdgeInsets.only(right: 1),
                  child: Icon(
                    Icons.location_on,
                    color: AppColors.white,
                    size: 30,
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.push(context, FadeRoute(page: LocationPage()))
                        .then((val) {
                      getLocation();
                    });
                  },
                  child: Container(
                    //padding: EdgeInsets.only(left: -20),
                    height: 20,
                    width: MediaQuery.of(context).size.width / 5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(120.0),
                        color: AppColors.assentDark),
                    child: Row(
                      children: [
                        Container(
                          padding:EdgeInsets.only(left: 10),
                          child: Text(
                            "Address",
                            style:
                                TextStyle(fontSize: 15, color: AppColors.white),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2.2,
                  padding: EdgeInsets.only(top: 2),
                  child: Text(locations.toString(),
                      style: GoogleFonts.workSans(
                        fontSize: 14,
                        color: AppColors.white,
                      )
    ),
                )
              ],
            ),
          ],
        ),

        actions: <Widget>[
          InkWell(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Wallet()));
            },
            child: Container(
              width: 22,
              //height: 0,
              child: Image(
                  image: AssetImage(
                "assets/images/wallets.png",
              )),
            ),
          ),

          // IconButton(
          //   onPressed: (){
          //     //Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchLoader()));
          //   },
          //   icon: Icon(
          //     Feather.wa,
          //     color: AppColors.white,
          //   ),
          // ),
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => NotificationPage()));
            },
            icon: Icon(
              Icons.notifications_active_outlined,
              color: AppColors.white,
            ),
          ),
        ],
      ),
      body: RefreshIndicator(
        key: refreshKey,
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
                child: Container(
                    child:corosolSlider()
                  //     BlocBuilder<SliderBloc, SliderState>(builder: (context, state) {
                  //   if (state is SliderInitialState) {
                  //     // setState(() {
                  //       loading = true;
                  //       return Container(height: 0.0,width: 0.0);
                  //     // });
                  //     // return buildLoading();
                  //   } else if (state is SliderLoadingState) {
                  //     // setState(() {
                  //       loading = true;
                  //       return Container(height: 0.0,width: 0.0,);
                  //     // });
                  //   } else if (state is SliderLoadedState) {
                  //     return corosolSlider(state.slider);
                  //   } else if (state is SliderErrorState) {
                  //     return Text("No data found");
                  //   }
                  // }),
                )),
            SliverToBoxAdapter(
                child:womanlist()

            ),
            SliverToBoxAdapter(
              child: Container(
                // child: banner(),
              ),
            ),

            // SliverToBoxAdapter(
            //   child: Container(
            //     height: 20,
            //     color: AppColors.dividerColor,
            //   ),
            // ),

            SliverToBoxAdapter(
              child: Container(
                // child: Tab(),
              ),
            ),

            SliverToBoxAdapter(
              child: Container(
                // child: products(),

                // child:  BlocBuilder<ProductBloc, ProductState>(builder: (context, state) {
                //   if (state is ProductInitialState) {
                //     return buildLoading();
                //   } else if (state is ProductLoadingState) {
                //     return buildLoading();
                //   } else if (state is ProductLoadedState) {
                //     return products(state.productlist);
                //   } else if (state is ProductErrorState) {
                //     return Text(state.msg.toString());
                //   }
                // }),
              ),
            ),
            // SliverToBoxAdapter(
            //   child: Container(
            //     height: 40,
            //     // width: MediaQuery.of(context).size.width/3,
            //     //  padding: EdgeInsets.only(),
            //     padding: EdgeInsets.only(left: 15, right: 10, bottom: 5),
            //     color: AppColors.white,
            //     child: InkWell(
            //       onTap: () {
            //         Navigator.push(
            //             context,
            //             MaterialPageRoute(
            //                 builder: (context) =>
            //                     ProductLoader(selected: selectedtab))).then((value) => getProduct());
            //       },
            //       child: Container(
            //         decoration: BoxDecoration(
            //           // color: AppColors.sentColor,
            //             borderRadius: BorderRadius.circular(5.0),
            //             border:
            //             Border.all(color: Colors.black26.withOpacity(0.1))),
            //         // shape: RoundedRectangleBorder(
            //         //   borderRadius: BorderRadius.circular(15.0),
            //         // ),
            //
            //         child: Container(
            //             padding: EdgeInsets.all(0),
            //             child: Center(
            //               child: Text(
            //                 "VIEW ALL",
            //                 style: TextStyle(color: AppColors.black),
            //               ),
            //             )),
            //       ),
            //     ),
            //   ),
            // ),
            SliverToBoxAdapter(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Text(
                  "Customer Testimonials",
                  style: TextStyle(color: AppColors.sentColor, fontSize: 16),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                child: testimonials(),
              ),
            ),
            // SliverToBoxAdapter(
            //   child: Container(
            //     child: banner2(),
            //   ),
            // ),
          ],
        ),
        onRefresh: refreshList,
      )
    );
  }

  Widget buildLoading() {
    return 
    Container(
      color: AppColors.white,
      height: 600,
      child: Center(
        child:
            // FadeInImage.memoryNetwork(
            //   placeholder: kTransparentImage,
            //   image: 'https://picsum.photos/250?image=9',
            // )
            CircularProgressIndicator(),
            
      ),
    );
  }
Widget datanotfound()
{
  return Container(

    child: Column(
      children: [

        Container(

            child: Image(
              image: AssetImage("assets/images/sorry.png"),
              height: 150,
            )
        ),
        Center(
          child: Container(
            padding: EdgeInsets.only(top: 8,bottom: 8,left: 5,right: 5),
            decoration: BoxDecoration(
                color: AppColors.primary,
                borderRadius: BorderRadius.all(
                    Radius.circular(10)


                )
            ),

            width: MediaQuery.of(context).size.width/1.5,
            child: Center(child: Text("Data Not Found "
              "Please Check Internet",
             // "Ahh we are not serving here. We are working hard to serve at your location",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: AppColors.white
              ),
            )),
          ),
        ),
        // Container(
        //   transform: Matrix4.translationValues(0, 15, 0),
        //   width: MediaQuery.of(context).size.width/2,
        //   color: AppColors.black,
        //   child:
        //   FlatButton(
        //     onPressed: (){
        //       Navigator.push(context, MaterialPageRoute(builder: (context)=>LocationPage())).then((value) => getLocation());
        //     },
        //     child: Text("Choose Other Location",style: TextStyle(
        //         color: AppColors.white,fontSize: 16
        //     ),),
        //   ),
        // )
      ],
    ),
  );

}
  // Widget womanlist() {
  //   var device=MediaQuery.of(context).size;
  //   return Container(
  //     //height: 00,
  //     padding: EdgeInsets.all(10),
  //     child: Card(
  //       shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.circular(15.0),
  //       ),
  //       child: Container(
  //         padding: EdgeInsets.only(top: 30, bottom: 0, left: 10, right: 10),
  //         child: GridView.builder(
  //             itemCount: category.length != null ?category.length:"",
  //             //category.length==null?category.length:"",
  //             shrinkWrap: true,
  //             physics: NeverScrollableScrollPhysics(),
  //             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
  //                 crossAxisCount: device.width>600.0?5:3,
  //                 childAspectRatio: .7,
  //                 crossAxisSpacing: 1.0,
  //                 mainAxisSpacing: 1.0),
  //             itemBuilder: (Context, i) {
  //               return Container(
  //                 height: 240,
  //                 child: InkWell(
  //                   onTap: () {
  //                     Navigator.push(
  //                         context,
  //                         MaterialPageRoute(
  //                             builder: (context) =>
  //                                 ServiceLoader(
  //                                     category[i]['main_name'],
  //                                     category[i]['id'],category[i]['terms'],category[i]['term_description']
  //                                 )
  //                         )
  //                     ).then((value) => getcategory());
  //                   },
  //                   child: Column(
  //                     children: [
  //                       Container(
  //                         decoration: BoxDecoration(
  //                           borderRadius: BorderRadius.circular(10.0),
  //                           border: Border.all(
  //                             color: AppColors.grayBorder,
  //                           ),
  //                           image: DecorationImage(
  //                               image: NetworkImage(category[i]['image'] != null
  //                                   ? category[i]['image']
  //                                   : ""),
  //                               fit: BoxFit.cover),
  //                         ),
  //                         padding: EdgeInsets.only(
  //                             top: 0, bottom: 0, right: 0, left: 10),
  //                         height: 95,
  //                         width: 95,
  //                       ),
  //                       Expanded(
  //                         child: Padding(
  //                           padding: const EdgeInsets.all(2.0),
  //                           child: Center(
  //                             child: Text(
  //                               category[i]['main_name'] != null
  //                                   ? category[i]['main_name']
  //                                   : "",
  //                               style: TextStyle(
  //                                 color: Colors.black,
  //                                 fontSize: 15,
  //                               ),
  //                               textAlign: TextAlign.center,
  //                               maxLines: 2,
  //                             ),
  //                           ),
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //               );
  //             }),
  //       ),
  //     ),
  //   );
  // }
  Widget womanlist() {
    return Container(
      //height: 00,
      padding: EdgeInsets.all(10),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Container(
          padding: EdgeInsets.only(top: 30, bottom: 0, left: 10, right: 10),
          child: GridView.builder(
              itemCount: category.length != null ?category.length:"",
              //category.length==null?category.length:"",
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: .7,
                  crossAxisSpacing: 1.0,
                  mainAxisSpacing: 1.0),
              itemBuilder: (Context, i) {
                return Container(
                  height: 240,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ServiceLoader(
                                      category[i]['main_name'],
                                      category[i]['id'],category[i]['terms'],category[i]['term_description']
                                  )
                          )
                      ).then((value) => getcategory());
                    },
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                              color: AppColors.grayBorder,
                            ),
                            image: DecorationImage(
                                image: NetworkImage(category[i]['image'] != null
                                    ? category[i]['image']
                                    : ""),
                                fit: BoxFit.cover),
                          ),
                          padding: EdgeInsets.only(
                              top: 0, bottom: 0, right: 0, left: 10),
                          height: 95,
                          width: 95,
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Center(
                              child: Text(
                                category[i]['main_name'] != null
                                    ? category[i]['main_name']
                                    : "",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                ),
                                textAlign: TextAlign.center,
                                maxLines: 2,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }

  Widget banner() {
    return Container(
//      color: Colors.white,
      height: 220,
      margin: EdgeInsets.only(top: 10, left: 0, right: 0),
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          Align(
              alignment: Alignment.centerLeft,
              child: Column(
                children: [
                  Container(
                    height: 180,
                    child: PageView.builder(
                      allowImplicitScrolling: true,
                      controller: _pageControllerr,
                      scrollDirection: Axis.horizontal,
                      itemCount: productslider.length == null?0:productslider.length,
                      //CategoryList.length,
                      reverse: false,
                      pageSnapping: true,
                      onPageChanged: (val) => _setIndexx(val),
                      itemBuilder: (context, i) {
                        return Container(
                            padding: EdgeInsets.only(right: 10, left: 10),
                            child: InkWell(
                              onTap: () {
                                 Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductdetailLoader(productid:productslider[i]['product_id']))).then((value) => getProduct());
                              },

                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: AppColors.black,
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          productslider[i]['image'] != null
                                              ? productslider[i]['image']
                                              : ""),
                                      fit: BoxFit.cover),
                                  // borderRadius:
                                  //BorderRadius.all(Radius.circular(10)),
                                  // border: Border.all(color: Colors.black26)
                                ),
                              ),
                            ));
                      },
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                              productslider.length,
                              // CategoryList.length,
                                  (index) => Container(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Container(
                                    width: 10,
                                    height: 10,
                                    decoration: BoxDecoration(
                                        color: _currentPages == index
                                            ? Colors.blueAccent
                                            : Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(120)),
                                        boxShadow: [
                                          BoxShadow(color: Colors.black)
                                        ]),
                                  ))),
                        ),
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
    // return Container(
    //   padding: EdgeInsets.all(10),
    //   child: Card(
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(10.0),
    //     ),
    //     child: Container(
    //         child: Container(
    //       height: 200,
    //       decoration: BoxDecoration(
    //           borderRadius: BorderRadius.circular(10.0),
    //           // border: Border.all(
    //           //   color: AppColors.grayBorder,
    //           //
    //           //
    //           // ),
    //           image: DecorationImage(
    //               image: NetworkImage(
    //                   "https://cdn.yesmadam.com/images/live/app/banner/161015891508.jpg"),
    //               fit: BoxFit.cover)),
    //     )),
    //   ),
    // );
  }

  Widget booksalonathome() {
    return Container(
      color: AppColors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              padding: EdgeInsets.only(top: 25),
              child: RichText(
                text: new TextSpan(
                    text: "WHY BOOK ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                    children: [
                      new TextSpan(
                          text: "SALON AT HOME",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 14))
                    ]),
                //textAlign: TextAlign.left,
              )),
          Container(
            //height: 100,
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 3,
                itemBuilder: (BuildContext context, int i) {
                  return Container(
                    padding: EdgeInsets.all(20),
                    child: Center(
                      child: Container(
                        height: 75,
                        width: MediaQuery.of(context).size.width / 1.5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            // border: Border.all(
                            //   color: AppColors.grayBorder,
                            //
                            //
                            // ),
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://cdn.yesmadam.com/images/live/app/banner/1610175891508.jpg"),
                                fit: BoxFit.cover)),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget vocalforLocal() {
    return Container(
      color: AppColors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              padding: EdgeInsets.only(top: 25),
              child: RichText(
                text: new TextSpan(
                    text: "VOCAL ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                    children: [
                      new TextSpan(
                          text: "FOR LOCAL",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 14))
                    ]),
                //textAlign: TextAlign.left,
              )),
          Container(
            //height: 100,
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 2,
                itemBuilder: (BuildContext context, int i) {
                  return Container(
                    padding: EdgeInsets.all(20),
                    child: Center(
                      child: Container(
                        height: 75,
                        width: MediaQuery.of(context).size.width / 1.5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            // border: Border.all(
                            //   color: AppColors.grayBorder,
                            //
                            //
                            // ),
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://cdn.yesmadam.com/images/live/app/banner/1610175891508.jpg"),
                                fit: BoxFit.cover)),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget banner2() {
    return Container(
      //height: 100,

      child: Container(
        padding: EdgeInsets.only(bottom: 10),
        child: Image(
          image: NetworkImage(
              "https://image.shutterstock.com/z/stock-vector-refer-a-friend-vector-illustration-concept-group-of-young-man-and-woman-attracts-customers-for-1377037922.jpg"),
        ),
      ),
    );
  }

  // Widget product() {
  //   return Container(
  //     padding: EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
  //     child:
  //     Column(
  //       children: [
  //         Container(
  //           padding: EdgeInsets.only(top: 10,bottom: 10),
  //
  //           child: Text("Services",style: TextStyle(
  //             fontWeight: FontWeight.bold,fontSize: 16
  //           ),),
  //         ),
  //         Container(
  //           child: GridView.builder(
  //               itemCount: 5,
  //               //categories.length,
  //
  //               shrinkWrap: true,
  //               physics: NeverScrollableScrollPhysics(),
  //               gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
  //                   crossAxisCount: 3,
  //                   childAspectRatio: 0.7,
  //                   crossAxisSpacing: 1.0,
  //                   mainAxisSpacing: 1.0),
  //               itemBuilder: (Context, i)
  //               {
  //                 //final cats = categories[i];
  //
  //                 return Container(
  //                   height: 200,
  //                   child: Column(
  //                     children: [
  //                       Container(
  //                         // child: ClipOval(
  //                         //
  //                         //   child: Image(image: AssetImage("assets/images/aa.png"),),
  //                         // ),
  //                         decoration: BoxDecoration(
  //                           borderRadius: BorderRadius.circular(120.0),
  //                           image: DecorationImage(
  //                               image:
  //                               //cats.image != null ? NetworkImage(cats.image.src):
  //                               NetworkImage("https://cdn.yesmadam.com/images/live/category/1606829571819.jpg"),
  //                               fit: BoxFit.fill),
  //                         ),
  //                         padding: EdgeInsets.only(
  //                             top: 0, bottom: 10, right: 10, left: 10),
  //                         height: 100,
  //                         width: 100,
  //                       ),
  //                       Expanded(
  //                         child: Center(
  //                           child: Text(
  //                             "Service",//cats.name,
  //                             style: TextStyle(
  //                               color: Colors.black,
  //                               fontSize: 15,
  //                             ),
  //                             textAlign: TextAlign.center,
  //                           ),
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                 );
  //               }),
  //         ),
  //       ],
  //     )
  //   );
  // }
  Widget testimonials() {
    return Container(
      height: MediaQuery.of(context).size.longestSide/2.5,
      //height/2.5,

      child:
      ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: testimonial.length,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              width: MediaQuery.of(context).size.width,
              //height: 350,
              padding: EdgeInsets.all(5),
              // color: AppColors.white,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 20, top: 20),
                      child: Text(
                        testimonial[i]['first_name']==null?"": testimonial[i]['first_name'],
                        style: TextStyle(
                            color: AppColors.sentColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 10, left: 20),
                            child: Icon(
                              Icons.rate_review_outlined,
                              size: 16,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(
                              "Reviewed DOS",
                              style: TextStyle(
                                  color: AppColors.grayText,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                    ),
                    Flexible(
                      child: Center(
                        child: Container(
                          padding:
                          EdgeInsets.only(top: 5, bottom: 5, left: 20, right: 20),
                          child: Text(
                            testimonial[i]['review'],
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                top: 10, left: 10, right: 10, bottom: 10),
                            child: Divider(
                              color: AppColors.sentColor,
                              thickness: 2,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 10, right: 10, bottom: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                // Container(
                                //   child: Text(
                                //     "Manisha",
                                //     style: TextStyle(
                                //         fontSize: 16,
                                //         fontWeight: FontWeight.bold,
                                //         color: AppColors.sentColor),
                                //   ),
                                // ),
                                Container(
                                  child: Row(
                                    children: [
                                      Row(

                                        children:List.generate(int.parse(testimonial[i]['rating_point']), (index) =>
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.star,
                                                  size: 18,
                                                  color: AppColors.sentColor,
                                                ),
                                              ],
                                            ),
                                         )
                                      ),
                                      Row(

                                          children:List.generate(5-int.parse(testimonial[i]['rating_point']), (index) =>
                                              Row(
                                                children: [
                                                  Icon(
                                                    Icons.star_border,
                                                    size: 18,
                                                  ),
                                                ],
                                              ),
                                              ),
                                          )


                                    ],


                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          }),

    );
  }

  Widget Tab() {
    return Container(
        child: Container(
            padding: EdgeInsets.only(left: 5, right: 10),
            height: 50,
            //width: 250,
            child: ListView.builder(
              padding: EdgeInsets.only(left: 5, top: 0),
              itemCount: productcategory.length, //productlist.length,
              scrollDirection: Axis.horizontal,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int i) {
                return InkWell(
                  onTap: () {
                    selectTab(productcategory[i]['foodcategory_id'].toString());
                    getProduct();
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 3.1,
                    child: Card(
                      color: selectedtab ==
                              productcategory[i]['foodcategory_id'].toString()
                          ? AppColors.lightblue
                          : AppColors.white,
                      child: Center(
                          child: Text(productcategory[i]['categoryname'])),
                    ),
                  ),
                );
              },
            )
            //color: AppColors.white,
            // child: Row(
            //   children: [
            //     Expanded(
            //       flex: 1,
            //         child:
            //     Container(
            //       child: Card(
            //         child:
            //         Center(child: Text("MEN")),
            //       ),
            //
            //
            //     )),
            //     Expanded(
            //         flex: 1,
            //         child:
            //         Container(
            //           child: Card(
            //             child:
            //             Center(child: Text("WOMEN")),
            //           ),
            //
            //
            //         )),
            //     Expanded
            //       (
            //         flex: 1,
            //         child:
            //         Container(
            //           child: Card(
            //             child:
            //             Center(child: Text("OTHERS")),
            //           ),
            //
            //
            //         ))
            //   ],
            //
            // ),
            ));
  }

  Widget products() {
    return Container(
        color: AppColors.white,
        padding: EdgeInsets.only(top: 10, bottom: 15, left: 10, right: 5),
        height: 330,

        //height:MediaQuery.of(context).size.height/2.4,
        // width: MediaQuery.of(context).size.height/2.35,
        child: ListView.builder(
          padding: EdgeInsets.only(left: 5, top: 0),
          itemCount: product.length > 10 ? 10 : product.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              padding: EdgeInsets.only(right: 10),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ProductdetailLoader(
                              productid: product[i]['product_id']))).then((value) => getProduct());
                },
                child: Container(
                  //height: 300,
                  width: MediaQuery.of(context).size.width / 2.4,
                  padding: EdgeInsets.only(top: 0),

                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: Colors.white,
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    children: [
                      Container(
                        height: 170,
                        //width: MediaQuery.of(context).size.width/2.2,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.all(Radius.circular(0)),
                            image: DecorationImage(
                                image: NetworkImage(product[i]['image'] != null
                                    ? product[i]['image']
                                    : ""),
                                fit: BoxFit.contain)),
                      ),
                      Flexible(
                        flex: 4,
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 10, top: 5),
                                child: Container(
                                  child: Text(product[i]['name'] != null
                                      ? product[i]['name']
                                      : ""),
                                ),
                              ),
                              Container(
                                padding:
                                    EdgeInsets.only(top: 4, left: 10, right: 3),
                                child: Container(
                                  child: Text(
                                    product[i]['description'] != null
                                        ? product[i]['description']
                                        : "",
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.black38),
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                              Container(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 10,
                                          bottom: 10,
                                          left: 3,
                                          right: 3),
                                    ),
                                    Container(
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 5,
                                            right: 5,
                                            bottom: 2,
                                            top: 2),
                                        child: Text(
                                          "₹" + product[i]['sellPrice'] != null
                                              ? "₹" + product[i]['sellPrice']
                                              : "",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 12),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      //padding: EdgeInsets.only(left: 2),
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 5,
                                            right: 5,
                                            bottom: 2,
                                            top: 2),
                                        child: Text(
                                          "₹" + product[i]['mrp'] != null
                                              ? "₹" + product[i]['mrp']
                                              : "",
                                          style: TextStyle(
                                            color: Colors.black38,
                                            fontSize: 12,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 0),
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 0,
                                            right: 0,
                                            bottom: 2,
                                            top: 2),
                                        child: Text(
                                          getPercent(product[i]['mrp'],
                                                      product[i]['sellPrice'])
                                                  .toString() +
                                              "%OFF",
                                          style: TextStyle(
                                              color: Color(0xFFFff3f6c),
                                              fontSize: 12),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        child:
                        Container(
                          child: InkWell(
                              onTap: () {
                                if(product[i]['in_cart_status']=="0"){
                                  productid = product[i]['product_id'];
                                  sellPrice = product[i]['sellPrice'];
                                  getProductAddtocart(productid,sellPrice);
                                  getProduct();


                                }
                                else{
                                  productid = product[i]['product_id'];
                                  sellPrice = product[i]['sellPrice'];

                                  getProductRemoveAddtocart(productid);
                                  getProduct();
                                  // getCartlist();

                                }
                              },
                              child:product[i]['in_cart_status']=="0"? Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          top: BorderSide(
                                              color: Colors.black12))),
                                  width: MediaQuery.of(context).size.width,
                                  // color:Colors.redAccent,
                                  child: Center(
                                      child: Text(
                                    "ADD TO CART",
                                    style: TextStyle(
                                        color: Color(0xFFFff3f6c),
                                        fontWeight: FontWeight.w600),
                                  ))
                              ):Container(

                                  child: Container(
                                      decoration: BoxDecoration(
                                          border: Border(
                                              top: BorderSide(
                                                  color: Colors.black12))),
                                      width: MediaQuery.of(context).size.width,
                                      // color:Colors.redAccent,
                                      child: Center(
                                          child: Text(
                                            "GO TO CART",
                                            style: TextStyle(
                                                color: Color(0xFFFff3f6c),
                                                fontWeight: FontWeight.w600),
                                          )))


                              ),
                          )
                        )
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ));
  }

  getPercent(mrp, sell_price) {
    var percent = (double.parse(mrp) - double.parse(sell_price)) /
        double.parse(mrp) *
        100;
    percent = double.parse(num.parse(percent.toString()).toString());
    return percent.round().toString();
  }

  Widget corosolSlider() {
    return Container(
//      color: Colors.white,
      height: 200,
      margin: EdgeInsets.only(top: 10, left: 0, right: 0),
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          Align(
              alignment: Alignment.centerLeft,
              child: Column(
                children: [
                  Container(
                    height: 180,
                    child: PageView.builder(
                      allowImplicitScrolling: true,
                      controller: _pageController,
                      scrollDirection: Axis.horizontal,
                      itemCount: homeSlider.length != null ?homeSlider.length:"",
                      //CategoryList.length,
                      reverse: false,
                      pageSnapping: true,
                      onPageChanged: (val) => _setIndex(val),
                      itemBuilder: (context, i) {
                        return Container(
                            padding: EdgeInsets.only(right: 10, left: 10),
                            child:  Container(
                        decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: AppColors.black,
                        image: DecorationImage(
                        image: NetworkImage(
                            homeSlider[i]['image'] != null
                        ? homeSlider[i]['image']
                            : ""),
                        fit: BoxFit.cover),
                        // borderRadius:
                        //BorderRadius.all(Radius.circular(10)),
                        // border: Border.all(color: Colors.black26)
                        ),
                        ));
                      },
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                              homeSlider.length != null ?homeSlider.length:"",
                              // CategoryList.length,
                              (index) => Container(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Container(
                                    width: 10,
                                    height: 10,
                                    decoration: BoxDecoration(
                                        color: _currentPage == index
                                            ? Colors.blueAccent
                                            : Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(120)),
                                        boxShadow: [
                                          BoxShadow(color: Colors.black)
                                        ]),
                                  ))),
                        ),
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
