import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:yesmadam/pages/productcart.dart';
import 'package:yesmadam/pages/productzoom.dart';
import 'package:yesmadam/services/apis.dart';

import 'login.dart';

class ProductdetailLoader extends StatefulWidget {
  final productid;
  ProductdetailLoader({this.productid});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProductdetailView();
  }
}

class ProductdetailView extends State<ProductdetailLoader> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  var sellPrice = "";
  var productid = "";
  var cartstatus = "";
  List productlist = [];
  List imagesdddd = [];
  var name = "";
  var mrp = "";
  List cartlist = [];
  var sell_price = "";
  var description = "";
  var cartstaus = "";
  var off = "";
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future getProductdetail() async
  {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");

      var url = Apis.CATPRODUCTDETAIL_API;
      var data = {
        'product_id': widget.productid,
        "user_id": user
      };

      var res = await apiPostRequestss(url, data);

      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {
          if (json.decode(res)['status'].toString() == "1")
          {
            productlist = json.decode(res)['data']['detail'];
            imagesdddd = json.decode(res)['data']['images'];
            name = json.decode(res)['data']['detail'][0]['name'];
            mrp = json.decode(res)['data']['detail'][0]['mrp'];
            sell_price = json.decode(res)['data']['detail'][0]['sellPrice'];
            description = json.decode(res)['data']['detail'][0]['description'];
            cartstaus = json.decode(res)['data']['detail'][0]['in_cart_status'];

            //save = ((json.decode(res)['data']['detail'][0]['sellPrice'])-(json.decode(res)['data']['detail'][0]['mrp']).toString());
            off = getPercent(json.decode(res)['data']['detail'][0]['mrp'],
                json.decode(res)['data']['detail'][0]['sellPrice'])
                .toString();

            print(off);
        }
        });
        //print(product);
      }
      else {
        setState(() {
          productlist = [];
        });
      }}
    else{
      try {
        //var sp = await SharedPreferences.getInstance();
        setState(() {
          loading = true;
          ani = false;
          fail = false;
          _color = Colors.black.withOpacity(0.3);
          // Navigator.pop(context);
        });

        var url = Apis.CATPRODUCTDETAIL_API;

        var data = {
          'product_id': widget.productid,
        };

        var res = await apiPostRequests(url, data);

        print(res);
        setState(() {
          if (json.decode(res)['status'].toString() == "1")
          {
            productlist = json.decode(res)['data']['detail'];
            imagesdddd = json.decode(res)['data']['images'];
            name = json.decode(res)['data']['detail'][0]['name'];
            mrp = json.decode(res)['data']['detail'][0]['mrp'];
            sell_price = json.decode(res)['data']['detail'][0]['sellPrice'];
            description = json.decode(res)['data']['detail'][0]['description'];
            cartstaus = json.decode(res)['data']['detail'][0]['in_cart_status'];

            //save = ((json.decode(res)['data']['detail'][0]['sellPrice'])-(json.decode(res)['data']['detail'][0]['mrp']).toString());
            off = getPercent(json.decode(res)['data']['detail'][0]['mrp'],
                json.decode(res)['data']['detail'][0]['sellPrice'])
                .toString();

            print(off);
          }
          else{
            productlist = [];

          }
          loading = false;
        });
      }
      catch (e) {
        print("Network Fail");
      }

    }

  }

  Future<String> apiPostRequests(String url, data) async {
    //_onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   // Navigator.pop(context);
    return reply;
  }

  Future getProductAddtocart(productid, sellPrice) async {
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.PRODUCTADDTOCART_API;

      var data = {
        "user_id": sp.getString("userid"),
        "product_id": productid,
        "quantity": "1",
        "unit_price": sellPrice
      };
      print(data);

      var res = await apiPostRequestss(url, data);
      // print(res);

      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
    getProductdetail();
    getProductCartlist();
    // getProductAddtocart();
  }

  Future<String> apiPostRequestss(String url, data) async {
     _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getProductRemoveAddtocart(productid) async {
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.REMOVEPRODUCTADDTOCART_API;

      var data = {"user_id": sp.getString("userid"), "product_id": productid};
      print(data);

      var res = await apiPostRequestsse(url, data);
      // print(res);

      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
    getProductdetail();
    getProductCartlist();
    // getProductAddtocart();
  }

  Future<String> apiPostRequestsse(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getProductCartlist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.PRODUCTCARTLIST_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "user_id": sp.getString("userid")
    };
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        cartlist = json.decode(res)['data']['records'];
      } else {
        cartlist = [];
        // totalprice="0";
        // itemcost= "";
        // oldcost="";
        // saveprice="";
        // addonprice="0";
        // totalpricedetail="0";
        //
        // payprice="";
        // payaddonprice="";

      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
  //  _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   // Navigator.pop(context);
    return reply;
  }

  @override
  _setIndex(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  int _currentPage = 0;
  PageController _pageController =
      PageController(initialPage: 0, viewportFraction: 1.0);
  @override
  void initState() {
    getProductdetail();
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < 5) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }

      _pageController.animateToPage(
        _currentPage,
        duration: Duration(milliseconds: 350),
        curve: Curves.easeIn,
      );
    });
    // TODO: implement initState
    getProductdetail();
    getProductCartlist();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // appBar:,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: [
                Container(
                  child: corosolSlider(),
                ),
                Container(
                  child: price(),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  child: Container(
                    padding: EdgeInsets.all(15),
                    color: AppColors.white,
                    child: Text(description),
                  ),
                ),
                Container(
                  child: text(),
                ),
                // Container(
                //   //color: Colors.white,
                //   padding: EdgeInsets.only(top: 15,bottom: 15),
                //   child: Column(
                //     children: [
                //       Container(
                //         padding: EdgeInsets.only(top: 10,bottom: 2,left: 10),
                //         color: AppColors.white,
                //         child: Text(
                //           "Easy 15days returns and exchanges",
                //           style: TextStyle(color: AppColors.black,fontSize: 12,
                //               fontWeight: FontWeight.w700),),alignment: Alignment.topLeft,
                //       ),
                //       Container(
                //         color: Colors.white,
                //         padding: EdgeInsets.only(top: 5,left: 10,bottom: 5,right: 5),
                //         child: Text("Choose to return or exchange for a different size(if available) "
                //             "within 15 days. "),
                //       ),
                //
                //     ],
                //   ),
                // ),
                Container(
                  child: sizechart(),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                ),
                // Container(
                //   child: new Column(
                //     crossAxisAlignment: CrossAxisAlignment.start,
                //     children: [
                //       // Align(
                //       //   alignment : Alignment.centerLeft,
                //       //   child: Container(
                //       //     padding: EdgeInsets.all(15),
                //       //     color: AppColors.white,
                //       //     child: Row(
                //       //       mainAxisAlignment: MainAxisAlignment.start,
                //       //       children: [
                //       //         RichText(
                //       //           text: new TextSpan(
                //       //               text: "You will earn",
                //       //               style: TextStyle(
                //       //                 color: AppColors.black,
                //       //                 fontSize: 12,
                //       //               ),
                //       //               children: [
                //       //                 new TextSpan(
                //       //                     text: "105",
                //       //                     style: TextStyle(
                //       //                         color: AppColors.primary,
                //       //                         fontWeight: FontWeight.w600,
                //       //                         fontSize: 12
                //       //                     )
                //       //                 ),
                //       //                 new TextSpan(
                //       //                     text: " insider points on this purchase",
                //       //                     style: TextStyle(
                //       //                         color: AppColors.black,
                //       //                         fontWeight: FontWeight.w600,
                //       //                         fontSize: 12
                //       //                     )
                //       //                 )
                //       //               ]
                //       //           ),
                //       //           textAlign: TextAlign.left,
                //       //         )
                //       //       ],
                //       //     ),
                //       //   ),
                //       // ),
                //
                //     ],
                //   ),
                // ),
                // Container(
                //   padding: EdgeInsets.only(top: 20),
                // ),

                // Container(
                //   padding: EdgeInsets.only(top: 15),
                //   color: AppColors.black12,
                // )
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              // height: 50,
              child: AppBar(
                automaticallyImplyLeading: false,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        padding: EdgeInsets.all(6),
                        decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(120))),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Feather.arrow_left,
                            color: AppColors.black,
                          ),
                        )),
                    Container(
                      // color: Colors.black,
                      width: MediaQuery.of(context).size.width / 9,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              padding: EdgeInsets.all(6),
                              decoration: BoxDecoration(
                                  color: AppColors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(120))),
                              child: Badge(
                                badgeColor: AppColors.white,
                                position: BadgePosition.topEnd(),
                                badgeContent: Text(cartlist.length.toString()),
                                child:
                                InkWell(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductCartitems()));
                                  },
                                  child: Icon(
                                    Feather.shopping_cart,
                                    color: Color(0xFF000000),
                                  ),
                                ),
                              )),
                          // Container(
                          //     width: 40,
                          //     height :40,
                          //     // padding: EdgeInsets.all(6),
                          //     decoration: BoxDecoration(
                          //         color : AppColors.white,
                          //         borderRadius: BorderRadius.all(
                          //             Radius.circular(120)
                          //         )
                          //     ),
                          //
                          //     child : InkWell(
                          //         onTap: (){
                          //           Navigator.pop(context);
                          //         },
                          //         child: Icon(
                          //           Feather.share_2,
                          //           color: Color(0xFFF000000),
                          //         )
                          //     )
                          // ),
                          // Container(
                          //     padding: EdgeInsets.all(6),
                          //     decoration: BoxDecoration(
                          //         color : Colors.white,
                          //         borderRadius: BorderRadius.all(
                          //             Radius.circular(120)
                          //         )
                          //     ),
                          //
                          //     child : InkWell(
                          //         onTap: ()async {
                          //           //_BottomSheetMenu();
                          //         },
                          //         child: Icon(
                          //           Feather.heart,
                          //           color: Color(0xFF000000),
                          //         )
                          //     )
                          // ),
                        ],
                      ),
                    )
                  ],
                ),

                elevation: 0,
                backgroundColor: Colors.transparent,
                iconTheme: IconThemeData(color: Colors.black),
                // actions: [
                //
                // ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget corosolSlider() {
    return Container(
//      color: Colors.white,
      height: 400,
      margin: EdgeInsets.only(
        top: 0,
      ),
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          Align(
              alignment: Alignment.centerLeft,
              child: Column(
                children: [
                  Container(
                    height: 370,
                    child: PageView.builder(
                      allowImplicitScrolling: true,
                      controller: _pageController,
                      scrollDirection: Axis.horizontal,
                      itemCount: imagesdddd.length,
                      //CategoryList.length,
                      reverse: false,
                      pageSnapping: true,
                      onPageChanged: (val) => _setIndex(val),
                      itemBuilder: (context, i) {
                        return Container(
                            // padding: EdgeInsets.only(
                            //   right: 20,
                            // ),
                            child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        Productzoom(image:imagesdddd)));
                          },
                          // onTap: () {
                          //   Navigator.push(
                          //       context,
                          //       MaterialPageRoute(
                          //           builder: (context) => Products(
                          //               CategoryList[i]['subcat_id'],
                          //               CategoryList[i]
                          //               ['sub_category_name'])));
                          // },
                          child: Container(
                            decoration: BoxDecoration(
                              color: AppColors.black,
                              image: DecorationImage(
                                  image: NetworkImage(imagesdddd[i]['image']),
                                  fit: BoxFit.contain),
                              // borderRadius:
                              // BorderRadius.all(Radius.circular(10)),
                              // border: Border.all(color: Colors.black26)
                            ),
                          ),
                        ));
                      },
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.only(top: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                              imagesdddd.length,
                              // CategoryList.length,
                              (index) => Container(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Container(
                                    width: 10,
                                    height: 10,
                                    decoration: BoxDecoration(
                                        color: _currentPage == index
                                            ? Colors.blueAccent
                                            : Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(120)),
                                        boxShadow: [
                                          BoxShadow(color: Colors.black)
                                        ]),
                                  ))),
                        ),
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }

  Widget price() {
    return Container(
      color: AppColors.white,
      child: Column(
        children: [
          Container(
            // width: 320,
            // height: 50,
            padding: EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 10),

            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(name,
                    style: TextStyle(
                      color: AppColors.black,
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                    ))
                // RichTex
                //t(
                //   text: new TextSpan(
                //       text: "Perfume ",
                //       style: TextStyle(
                //         color: AppColors.black,
                //         fontWeight: FontWeight.bold,
                //         fontSize: 14,
                //       ),
                //       children: [
                //         new TextSpan(
                //             text: name,
                //             style: TextStyle(
                //                 color: AppColors.black75,
                //
                //                 fontSize: 12
                //             )
                //         )
                //       ]
                //   ),
                //   textAlign: TextAlign.left,
                // )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 2, left: 10),
            child: Row(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(top: 15, bottom: 10, left: 0, right: 3),
                ),
                Container(
                  padding: EdgeInsets.only(left: 0),
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 2),
                    child: Text(
                      "₹" + sell_price,
                      style: TextStyle(
                          color: AppColors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 12),
                    ),
                    alignment: Alignment.topLeft,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 0),
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 2),
                    child: Text(
                      "₹" + mrp,
                      style: TextStyle(
                        color: AppColors.black,
                        fontSize: 12,
                        decoration: TextDecoration.lineThrough,
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 2),
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 2, right: 3, bottom: 2, top: 2),
                    child: Text(
                      off +
                          // getPercent(
                          //     mrp,
                          //     sell_price).toString()+
                          "%Off",
                      style:
                          TextStyle(color: AppColors.redcolor3, fontSize: 12),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 10, left: 15),
            child: Text(
              "inclusive of all taxes",
              style: TextStyle(
                  color: AppColors.redcolor3,
                  fontSize: 10,
                  fontWeight: FontWeight.w600),
            ),
            alignment: Alignment.topLeft,
          )
        ],
      ),
    );
  }

  Widget text() {
    return Container(
      color: AppColors.white,
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    RichText(
                      text: new TextSpan(
                          text: "You Pay Only: ",
                          style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                          children: [
                            new TextSpan(
                                text: "₹" + sell_price,
                                style: TextStyle(
                                    color: AppColors.sentColor, fontSize: 12))
                          ]),
                      textAlign: TextAlign.left,
                    )
                  ],
                ),
              ),
              // Container(
              //     child: Text("Save:₹"
              //         + ( (int.tryParse(mrp )) - (int.tryParse(sell_price))).toString(),
              //       style: TextStyle(color: AppColors.receiveColor),)
              // )
            ],
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text(
                    "Apply the coupon during checkout",
                    style: TextStyle(color: AppColors.black75, fontSize: 10),
                  ),
                ),
                // Container(
                //   child: Row(
                //     children: [
                //       Row(
                //         children: [
                //           Icon(
                //             Icons.star,
                //             size: 16,
                //             color: AppColors.redcolor3,
                //           ),
                //         ],
                //       ),
                //       Row(
                //         children: [
                //           Icon(
                //             Icons.star,
                //             size: 16,
                //             color: AppColors.redcolor3,
                //           ),
                //         ],
                //       ),
                //       Row(
                //         children: [
                //           Icon(
                //             Icons.star,
                //             size: 16,
                //             color: AppColors.redcolor3,
                //           ),
                //         ],
                //       ),
                //       Row(
                //         children: [
                //           Icon(
                //             Icons.star_border,
                //             size: 16,
                //           ),
                //         ],
                //       ),
                //       Row(
                //         children: [
                //           Icon(
                //             Icons.star_border,
                //             size: 16,
                //           ),
                //         ],
                //       ),
                //     ],
                //   ),
                // )
              ],
            ),
            alignment: Alignment.topLeft,
          ),
          // Container(
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: [
          //       Container(
          //         child: Text("Purchase of 2 or more items.T&C apply."),
          //
          //       ),
          //       InkWell(
          //         onTap: (){
          //
          //
          //         },
          //         child: Container(
          //           child: Text("View Products",
          //             style: TextStyle(color: AppColors.redcolor3,fontWeight: FontWeight.w600,
          //                 fontSize: 12),),
          //         ),
          //       )
          //     ],
          //   ),
          // )
        ],
      ),
    );
  }

  Widget sizechart() {
    return Container(
        //padding: EdgeInsets.only(bottom: 1),
        color: Colors.white,
        child: Container(
            padding: EdgeInsets.only(left: 10, bottom: 20, right: 10),
            child: InkWell(
              onTap: () {
                if (cartstaus == "0") {
                  productid = widget.productid;
                  sellPrice = sell_price;
                  getProductAddtocart(productid, sellPrice);
                  getProductdetail();
                } else {
                  productid = widget.productid;
                  sellPrice = sell_price;

                  getProductRemoveAddtocart(productid);
                  getProductdetail();
                  // getCartlist();

                }
              },
              child:cartstaus==null?Container(
                height: 45,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border.all(color: Colors.black12),
                    color: AppColors.sentColor),
                child: Container(
                  child: Center(
                    child: Text(
                      "ADD TO CART",
                      style: TextStyle(
                          color: AppColors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ): cartstaus == "0"
                  ? Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          border: Border.all(color: Colors.black12),
                          color: AppColors.sentColor),
                      child: Container(
                        child: Center(
                          child: Text(
                            "ADD TO CART",
                            style: TextStyle(
                                color: AppColors.white,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    )
                  : Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          border: Border.all(color: Colors.black12),
                          color: AppColors.sentColor),
                      child: Container(
                        child: Center(
                          child: Text(
                            "GO TO CART",
                            style: TextStyle(
                                color: AppColors.white,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    ),
              // {},
              // child: Container(
              //
              //   height: 45,
              //   width: MediaQuery.of(context).size.width,
              //   decoration: BoxDecoration(
              //       borderRadius: BorderRadius.all(Radius.circular(5)),
              //       border: Border.all(
              //           color: Colors.black12
              //       ),
              //       color: AppColors.sentColor
              //   ),
              //   child: Container(
              //
              //
              //     child:
              //
              //
              //     Center(
              //       child: Text(
              //         "ADD TO CART",style: TextStyle(
              //           color : AppColors.white,
              //           fontSize: 16.0,
              //           fontWeight: FontWeight.w500
              //       ),
              //
              //       ),
              //     ),
              //
              //   ),
              // ),
            ))

        // child: Column(
        //   children: [
        //     // Row(
        //     //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //     //   children: [
        //     //     Container(
        //     //       padding: EdgeInsets.all(10),
        //     //
        //     //       child: Text("SIZE:S",
        //     //         style: TextStyle(color: Colors.black26),),
        //     //     ),
        //     //     InkWell(
        //     //       onTap: (){},
        //     //       child: Container(
        //     //         padding: EdgeInsets.all(10),
        //     //         child: Text("SIZE CHART",style: TextStyle(color: Colors.red),),
        //     //       ),
        //     //     ),
        //     //
        //     //   ],
        //     // ),
        //     // Container(
        //     //   padding: EdgeInsets.only(left: 10),
        //     //   child: Text("Garment Measurement:Chest 38.0in",
        //     //     style: TextStyle(color: Colors.black26),),
        //     //   alignment: Alignment.topLeft,
        //     // ),
        //     // Container(
        //     //   height: 70,
        //     //   padding: EdgeInsets.only(top: 15,bottom: 15,left: 15,right: 15),
        //     //
        //     //   child: Align(
        //     //     alignment: Alignment.centerLeft,
        //     //     child: ListView(
        //     //       shrinkWrap: true,
        //     //       scrollDirection: Axis.horizontal,
        //     //
        //     //       children: [
        //     //
        //     //         Container(
        //     //           padding: EdgeInsets.only(right: 10),
        //     //           child: Container(
        //     //             height: 40,
        //     //             width: 40,
        //     //             decoration: BoxDecoration(
        //     //
        //     //                 border: Border.all(color: Colors.black),
        //     //                 borderRadius:
        //     //                 BorderRadius.all(Radius.circular(130))),
        //     //             child: Center(child: Text("S",style: TextStyle(fontSize: 20,
        //     //                 fontWeight: FontWeight.w600),)),
        //     //           ),
        //     //         ),
        //     //         Container(
        //     //           padding: EdgeInsets.only(right: 10),
        //     //           child: Container(
        //     //             height: 40,
        //     //             width: 40,
        //     //             // padding: EdgeInsets.only(left: 10),
        //     //             decoration: BoxDecoration(
        //     //
        //     //                 border: Border.all(color: Colors.black),
        //     //                 borderRadius:
        //     //                 BorderRadius.all(Radius.circular(130))),
        //     //             child: Center(child: Text("M",style: TextStyle(fontSize: 20,
        //     //                 fontWeight: FontWeight.w600),)),
        //     //           ),
        //     //         ),
        //     //         Container(
        //     //           padding: EdgeInsets.only(right: 10),
        //     //           child: Container(
        //     //             height: 40,
        //     //             width: 40,
        //     //             // padding: EdgeInsets.only(left: 10),
        //     //             decoration: BoxDecoration(
        //     //
        //     //                 border: Border.all(color: Colors.black),
        //     //                 borderRadius:
        //     //                 BorderRadius.all(Radius.circular(130))),
        //     //             child: Center(child: Text("L",style: TextStyle(fontSize: 20,
        //     //                 fontWeight: FontWeight.w600),)),
        //     //           ),
        //     //         ),
        //     //         Container(
        //     //           height: 40,
        //     //           width: 40,
        //     //           // padding: EdgeInsets.only(left: 10),
        //     //           decoration: BoxDecoration(
        //     //
        //     //               border: Border.all(color: Colors.black),
        //     //               borderRadius:
        //     //               BorderRadius.all(Radius.circular(130))),
        //     //           child: Center(child: Text("XL",style: TextStyle(fontSize: 20,
        //     //               fontWeight: FontWeight.w600),)),
        //     //         ),
        //     //       ],
        //     //     ),
        //     //   ),
        //     // ),
        //
        //
        //   ],
        // ),
        );
  }

  // Widget productdeatail()
  // {
  //   return Container(
  //     color: Colors.white,
  //     padding: EdgeInsets.only(top: 5,bottom: 10,left: 15,right: 15),
  //     child: Column(
  //       children: [
  //         Container(
  //           child: Text("Product Details",
  //             style: TextStyle(
  //                 fontSize: 12,color: Colors.black87,
  //                 fontWeight: FontWeight.w700
  //             ),),
  //           alignment: Alignment.topLeft,
  //         ),
  //         Container(
  //           padding: EdgeInsets.only(top: 5,bottom: 5),
  //           child: Text("Brown solid biker jacket,has a mock collar,two pockets,zip"
  //               " closure,long sleeves,straight hem,polyester lining"
  //             ,style: TextStyle(color: Colors.black45,fontSize: 12),),
  //         ),
  //         Container(
  //           child: Text("Size & Fit",
  //             style: TextStyle(
  //                 fontSize: 12,color: Colors.black87,
  //                 fontWeight: FontWeight.w700
  //             ),),
  //           alignment: Alignment.topLeft,
  //         ),
  //         Container(
  //           padding: EdgeInsets.only(top: 5,bottom: 5),
  //           child: Text("The model(height 6') is wearing a size M "
  //
  //             ,style: TextStyle(color: Colors.black45,fontSize: 12),),
  //           alignment: Alignment.topLeft,
  //         ),
  //         Container(
  //           child: Text("Material & Care",
  //             style: TextStyle(
  //                 fontSize: 12,color: Colors.black87,
  //                 fontWeight: FontWeight.w700
  //             ),),
  //           alignment: Alignment.topLeft,
  //         ),
  //         Container(
  //           padding: EdgeInsets.only(top: 5,bottom: 5),
  //           child: Text("Nylon"
  //
  //             ,style: TextStyle(color: Colors.black45,fontSize: 12),),
  //           alignment: Alignment.topLeft,
  //         ),
  //         Container(
  //           child: Text("Style Note",
  //             style: TextStyle(
  //                 fontSize: 12,color: Colors.black87,
  //                 fontWeight: FontWeight.w700
  //             ),),
  //           alignment: Alignment.topLeft,
  //         ),
  //         Container(
  //           padding: EdgeInsets.only(top: 5),
  //           child: Text("Elevate your fashion quotient with these top notch track pants from ADIDAS."
  //
  //             ,style: TextStyle(color: Colors.black45,fontSize: 12),),
  //           alignment: Alignment.topLeft,
  //         ),
  //
  //       ],
  //     ),
  //
  //   );
  // }
  getPercent(mrp, sell_price) {
    var percent = (double.tryParse(mrp) - double.tryParse(sell_price)) /
        double.parse(mrp) *
        100;
    percent = double.parse(num.parse(percent.toString()).toString());
    return percent.round().toString();
  }
}
