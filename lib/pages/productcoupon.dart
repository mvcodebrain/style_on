import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/productpayment.dart';
import 'package:yesmadam/pages/timedate.dart';
import 'package:yesmadam/services/apis.dart';

class CuponLoaderproduct extends StatefulWidget{
  final totalcost;

  CuponLoaderproduct(this.totalcost);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CuponViews();
  }

}

class CuponViews extends State<CuponLoaderproduct>{



  var couponcode = TextEditingController();
  List data =[];
  var discountprice = "0";
  var couponid="";


  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }


  getprice() async{
    setState(() {
      discountprice = widget.totalcost.toString();
    });

  }
  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }
  alertaDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }


  Future getCoupon() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.COUPON_API;
      var data = { "user_id":  sp.getString("userid"),
        "amount":widget.totalcost,
        "code":couponcode.text};
     // print(data.toString());
      var res = await apiPostRequest(url, data);

      //print(res);
      setState(() {


        loading = false;
      });
      var chekExist = json.decode(res)['status'];
      // print(chekExist);
      if (chekExist == "1") {
        setState(() {
          discountprice = json.decode(res)['response']['afterDescount'].toString();

          sp.setString("couponid", json.decode(res)['response']['coupon_id'].toString());

          couponcode.text = "";
          alertDailog("Coupon Applied Successfully ");

        });

      }

      else{
        alertaDailog("Coupon Applied Invalid ");

      }
    } catch (e) {
      print("Network Fail");
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);


    return reply;
  }
  @override
  void initState() {
    getprice();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Color(0xFFFf6f6f6),
        appBar: AppBar(
          title: Text("COUPONS",style: TextStyle(
              color: Colors.black87
          ),),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
          elevation: 1,


        ),
        body:SingleChildScrollView(
          child: Column(
            children: [
              Container(

                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 10),
                  height: 65,
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(5)
                          ),
                          border: Border.all(
                              color: Colors.black38
                          )
                      ),


                      child:Row(
                        children: [
                          Container(
                            width: 270,
                            //height: 50,

                            child: TextField(
                              controller: couponcode,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Enter coupon code",
                                  hintStyle: TextStyle(color: Colors.grey[400]
                                  ),
                                  contentPadding: EdgeInsets.only(
                                    left: 10,
                                  ),
                                  counterText: ""

                              ),

                              maxLength: 10,


                            ),
                          ),
                          InkWell
                            (
                            onTap: (){
                              getCoupon();

                            },
                            child: Container(
                              child: Text("APPLY",style: TextStyle(
                                color: Color(0xFFFff3f6c),
                              ),),
                            ),
                          )
                        ],
                      )

                  ),
                ),

              ),
              // Container(
              //   padding: EdgeInsets.only(top: 10,right: 270,bottom: 10),
              //   child: Text("Coupons",
              //     style: TextStyle(
              //         color: Colors.black,
              //         fontSize: 16
              //     ),),
              //
              // ),
              //
              // Card(
              //
              //
              //   child: Container(
              //
              //     // height: 100,
              //     child: Container(
              //
              //
              //       child: ListTile(
              //
              //         title: Container(
              //           height: 30,
              //
              //
              //           child:Text("Enter the code"
              //           ),
              //
              //         ),
              //         trailing: FlatButton(
              //           onPressed: (){},
              //           child: Text(
              //             "APPLY",style: TextStyle(
              //               color: Colors.deepPurpleAccent
              //           ),
              //           ),
              //         ),
              //       ),
              //
              //     ),
              //   ),
              //
              // ),
              // Card(
              //
              //   child: Container(
              //     // height: 100,
              //     child: Container(
              //
              //       child: ListTile(
              //
              //         title: Container(
              //           height: 30,
              //
              //
              //           child:Text("Enter the code"
              //           ),
              //
              //         ),
              //         trailing: FlatButton(
              //           onPressed: (){},
              //           child: Text(
              //             "APPLY",style: TextStyle(
              //               color: Colors.deepPurpleAccent
              //           ),
              //           ),
              //         ),
              //       ),
              //
              //     ),
              //   ),
              //
              // ),
              // Card(
              //
              //   child: Container(
              //     // height: 100,
              //     child: Container(
              //
              //       child: ListTile(
              //
              //         title: Container(
              //           height: 30,
              //
              //
              //           child:Text("Enter the code"
              //           ),
              //
              //         ),
              //         trailing: FlatButton(
              //           onPressed: (){},
              //           child: Text(
              //             "APPLY",style: TextStyle(
              //               color: Colors.deepPurpleAccent
              //           ),
              //           ),
              //         ),
              //       ),
              //
              //     ),
              //   ),
              //
              // ),
              // Card(
              //
              //   child: Container(
              //     // height: 100,
              //     child: Container(
              //
              //       child: ListTile(
              //
              //         title: Container(
              //           height: 30,
              //
              //
              //           child:Text("Enter the code"
              //           ),
              //
              //         ),
              //         trailing: FlatButton(
              //           onPressed: (){},
              //           child: Text(
              //             "APPLY",style: TextStyle(
              //               color: Colors.deepPurpleAccent
              //           ),
              //           ),
              //         ),
              //       ),
              //
              //     ),
              //   ),
              //
              // ),

            ],
          ),



        ),
        //   bottomNavigationBar:
        //   Container(
        //      height: 60,
        //     child: Column(
        //     children: [

        //       Container(
        //         height: 60,
        //         padding: EdgeInsets.only(left: 15),
        //         decoration: BoxDecoration(
        //           borderRadius: BorderRadius.all(Radius.circular(2)),
        //           color: Colors.white,
        //         ),
        //         child: Row(
        //           children: [
        //             Expanded(
        //               child: Column(
        //                 crossAxisAlignment: CrossAxisAlignment.start,
        //                 children: [
        //                   Container(

        //                     padding: EdgeInsets.only(left: 0,top: 20),
        //                     child: Row(
        //                       children: [
        //                         Text("Maximum savings:",style: TextStyle(

        //                         ),
        //                         ),
        //                       ],
        //                     ),
        //                   ),
        //                   Container(

        //                     padding: EdgeInsets.only(left: 0,top: 5),
        //                     child: Text("₹0",style: TextStyle(
        //                         fontWeight: FontWeight.bold,color: Color(0xFFFff3f6c)
        //                     ),),
        //                   )
        //                 ],
        //               ),
        //             ),
        //             Expanded(
        //               child: Container(
        //                 padding: EdgeInsets.only(right: 10),


        //                 child: SizedBox(
        //                   child: InkWell(
        //                     onTap: (){
        //                       //Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginLoader()));
        //                     },

        //                     child: Container(
        //                       height: 45,
        //                       width: 45,
        //                       decoration: BoxDecoration(
        //                         borderRadius: BorderRadius.all(Radius.circular(10)),
        //                         color: Color(0xFFFff3f6c),
        //                       ),


        //                       child: Center(
        //                         child: Text(
        //                           "APPLY",style: TextStyle(
        //                             color : Colors.white,
        //                             fontSize: 15.0,
        //                             fontWeight: FontWeight.w400
        //                         ),

        //                         ),
        //                       ),
        //                     ),
        //                   ),
        //                 ),
        //               ),
        //             )


        //           ],
        //         ),


        //       )
        //     ],




        //   ),
        // )

        bottomNavigationBar:
        // cartlist.length.toString()=="0"?Container(
        //   height: 0,
        // ):
        Container(
          height: 75,
          child: Column(
            children: [

              Container(
                height: 65,
                padding: EdgeInsets.only(left: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                  color: AppColors.redcolor3,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 0, top: 20),
                            child: Row(
                              children: [
                                Text(
                                  "₹"+ discountprice,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(right: 10),
                        child: SizedBox(
                          child: InkWell(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductPaymentPage(discountprice)));
                            },
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                color: AppColors.sentColor,
                              ),
                              child: Center(
                                child: Text(
                                  "Continue",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )

    );
  }

}