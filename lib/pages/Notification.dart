// import 'package:flutter/material.dart';
// import 'package:flutter_icons/flutter_icons.dart';
// import 'package:yesmadam/color/AppColors.dart';
//
// class NotificationPage extends StatefulWidget{
//   @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//     return NotificationPageLoader();
//   }
//
// }
//
// class NotificationPageLoader extends State<NotificationPage>{
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Scaffold(
//         appBar: AppBar(
//
//          //  backgroundColor: AppColors.sentColor,
//           title:Text("Notification",style: TextStyle(
//               fontSize: 25,fontWeight: FontWeight.bold,
//               color: AppColors.white
//           )),
//
//
//
//
//           iconTheme: IconThemeData(color: AppColors.white),
//            //pinned: true,
//           // floating: true,
//           elevation: 1,
//           automaticallyImplyLeading:true,
//
//
//
//         ),
//         body: SingleChildScrollView(
//           child:Notificationlist() ,
//         )
//     );
//   }
//   Widget Notificationlist() {
//     return Container(
//       //color: AppColors.grayText,
//       child: ListView.builder(
//         physics: NeverScrollableScrollPhysics(),
//         shrinkWrap: true,
//         itemCount: 1,
//         itemBuilder: (context, i) {
//           return Container(
//             padding: EdgeInsets.only(top: 20,bottom: 10,left: 10,right: 10),
//             child: InkWell(
//                 onTap: () {},
//                 child:
//                 Card(
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                   ),
//                   child: Column(
//                     children: [
//                       Container(
//                         height: 200,
//                           padding: EdgeInsets.only(top: 20),
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(12.0),
//                             // border: Border.all(
//                             //   color: AppColors.grayBorder,
//                             //
//                             // ),
//                           image: DecorationImage(
//                             image: NetworkImage("https://cdn.yesmadam.com/images/live/app/banner/1610193154507.jpg")
//                          ,fit: BoxFit.cover )),
//
//                       ),
//                       Container(
//                         padding: EdgeInsets.all(10),
//                           child: Text("30% off cleaning upto date 31jan",style: TextStyle(fontSize: 16,color: AppColors.black),)
//                       )
//                     ],
//                   ),
//                 )
//             ),
//
//           );
//         },
//       ),
//     );
//   }
//
// }
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/rating.dart';
import 'package:yesmadam/services/apis.dart';

class NotificationPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NotificationPageLoader();
  }

}

class NotificationPageLoader extends State<NotificationPage>{
  List data =[];
  var booking_id="";
  var notification="";

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  getnotification() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.NOTIFICATION_API;
      var datas = {
        "user_id": sp.getString("userid"),
        "page":"0",
        "limit":"10"


      };
      print(datas.toString());
      var res = await apiPostRequest(url,datas);
      // print(json.decode(res)['data'][0]['message']);
      setState(() {
        if(json.decode(res)['status'].toString() == "1"){
          data = json.decode(res)['data'];
          //notification=json.decode(res)['data']['notification_type'];
        }
      });
      print(data.toString());
    }catch (e)
    {
      print("Network Fail");
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);

    return reply;
  }
  @override
  void initState() {
    // TODO: implement initState
    getnotification();
    super.initState();


  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(

          backgroundColor: AppColors.sentColor,
          title:
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Text("Notification",style: TextStyle(
                fontSize: 25,fontWeight: FontWeight.bold,
                color: AppColors.white
            )),
          ),




          iconTheme: IconThemeData(color: Colors.white),
          // pinned: true,
          //  floating: true,
          elevation: 1,
          //automaticallyImplyLeading: false,



        ),
        body: SingleChildScrollView(
          child:Notificationlist() ,
        )
    );
  }
  Widget Notificationlist() {
    return Container(

      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (context, i) {
          return Container(

            child: InkWell(
                onTap: () {
                  if(data[i]['type']=="complate"){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => RatingPage( data[i]['booking_id'])));
                  }else{}
                },
                child: Container(
                    padding: EdgeInsets.all(25),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(1.0),
                      border: Border.all(
                        color: AppColors.grayBorder,

                      ),
                    ),
                    child:
                    Row(
                      children: [
                        Flexible(child: Text(data[i]['body'],style: TextStyle(fontSize: 16,color: AppColors.black),)),
                      ],
                    )
                )
            ),

          );
        },
      ),
    );
  }

}