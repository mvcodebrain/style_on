import 'dart:ffi';

import "package:flutter/material.dart";





import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:yesmadam/animators/navianimator.dart';
//import 'package:flare_flutter/flare_actor.dart';
import 'dart:async';
import 'dart:io';

import 'dart:convert';

import 'package:yesmadam/services/apis.dart';

import 'locations.dart';

class AddressPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AddressLoader();
  }
}

class AddressLoader extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddressView();
  }
}

class AddressView extends State<AddressLoader> {
  var location = TextEditingController();
  var pincode = TextEditingController();
  var name = TextEditingController();
  var phone = TextEditingController();
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  var pin="";
  alertDailog(text) async{
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: (){
                  Navigator.pop(context);
                  // Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }
  Future getCurrentLocation(lat, lang) async {

    setState(() {
      lt = lat;
      lo = lang;
    });

   //  print("map test : "+lat.toString());
   //
   // // final coordinates = new Coordinates(lat, lang);
   //
   //  var addresses =
   //     // await Geocoder.local.findAddressesFromCoordinates(coordinates);
   //  var first = addresses.first;
   //  print("${first.featureName} : ${first.addressLine}");
   //  setState(() {
   //    location.text = first.addressLine;
   //    pin=addresses.first.postalCode.toString();
   //    pincode.text = pin;
   //  });
    // Navigator.pop(context);
  }
  var lt = 0.0;
  var lo = 0.0;
  // Future savedLocation() async {
  //   var sp = await SharedPreferences.getInstance();
  //   setState(() {
  //     // lt = double.tryParse(sp.getString("lat"));
  //     // lo = double.tryParse(sp.getString("log"));
  //     location.text = sp.getString("location");
  //     allMarks.add(Marker(
  //       markerId: MarkerId('mymarker'),
  //       draggable: true,
  //       onTap: () {
  //         print("Tapped");
  //       },
  //      // icon: BitmapDescriptor.fromAsset("assets/icons/location.png"),
  //       position: LatLng(lt, lo),
  //       visible: true,
  //       infoWindow: InfoWindow(title: "Marker")));
  //   });
  // }
  //
  // Completer<GoogleMapController> _controller = Completer();
  //
  // static final CameraPosition _kGooglePlex = CameraPosition(
  //   target: LatLng(37.42796133580664, -122.085749655962),
  //   zoom: 14.4746,
  // );
  //
  // static final CameraPosition _kLake = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(37.43296265331129, -122.08832357078792),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414
  // );
  //
  // List<Marker> allMarks = [];
  // // var locations =
  Future getLocation() async {
    var sp = await SharedPreferences.getInstance();
    var loc = sp.getString("location");
    print(loc);
    setState(() {
      location.text = loc;
    });
  }

  @override
  void initState() {
    super.initState();
    getLocation();
   // savedLocation();

  }

  // Set<Circle> circles = Set.from([
  //   Circle(
  //       circleId: CircleId('cir'),
  //       center: LatLng(0.0, 0.0),
  //       radius: 2000,
  //       fillColor: Colors.blueAccent)
  // ]);
  //
  // Set<Polyline> poly = Set.from([]);
  //
  // final Set<Marker> _markers = {};
  // final Set<Circle> _circle = {};
  // double _radius = 100.0;
  // double _zoom = 18.0;
  // bool _showFixedGpsIcon = false;
  // bool _isRadiusFixed = false;
  // String error;
  // static const LatLng _center = const LatLng(-25.4157807, -54.6166762);
  // MapType _currentMapType = MapType.normal;
  // LatLng _lastMapPosition;

  var addr_type = 0;
  Color selcol = Colors.blueAccent;

  Future changeAddrType() async{
    setState(() {
      if(addr_type == 0){
        addr_type = 1;
      }else{
        addr_type = 0;
      }
    });
  }

  var initialView = 0;



  Future saveAddress() async{
    var url = Uri.tryParse(Apis.ADDADDRESSLIST_API);
    var sp = await SharedPreferences.getInstance();
    var userid = sp.getString("userid");
    var map = new Map<String,dynamic>();
    map['user_id'] = userid;
    map['name'] = name.text;
    map['phone'] = phone.text;
    map['address'] = location.text;
    map['pincode'] = pincode.text;
    map['lng']= sp.getString("log");
    map['lat']= sp.getString("lat");
    // var data ={
    //   "user_id":userid,
    //   "name":name.text,
    //   "phone":phone.text,
    //   "address":location.text,
    //   "pincode":pincode.text
    //
    // };
    print(map);
    // var res = await apiPostRequest(url,map);
    http.Response res = await http.post(url,body: map);
    Navigator.pop(context);
    print(res.body);
  }

  Future<String> apiPostRequest(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    // request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   // Navigator.pop(context);
    return reply;
  }


  @override
  Widget build(BuildContext context) {
    double mapWidth = MediaQuery.of(context).size.width;
    double mapHeight = MediaQuery.of(context).size.height - 215;
    double iconSize = 60.0;

    // TODO: implement build
    return new Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Colors.white,
      //   title: Text(
      //     "Set address",
      //     style: TextStyle(
      //       color: Colors.black,
      //       fontFamily: "opensan"
      //     ),
      //   ),
      //   leading: ,
      // ),
      body:CustomScrollView(
          //controller: myscrollController,
          slivers: <Widget>[
            SliverAppBar(
              centerTitle: true,
              title: SizedBox(
                width: 20,
                child: Icon(
                  Icons.remove,
                  color: Colors.black38,
                ),
              ),
              backgroundColor: Colors.white,
              leading: IconButton(
                icon: Icon(Icons.chevron_left),
                color: Colors.black38,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              pinned: true,
              elevation: 0,
            ),
            SliverToBoxAdapter(
              child: new Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    new Container(
                      color: Colors.white,
                      child: new TextField(
                        controller: location,
                        decoration: InputDecoration(
                            labelText: "ADDRESS",
                            labelStyle: TextStyle(
                                fontFamily: "opensan",
                                fontSize: 16)),
                        maxLines: 2,
                        style: TextStyle(
                          fontFamily: "opensan",
                        ),
                        onTap: (){
                          Navigator.push(context, FadeRoute(page: LocationPage())).then((val) {
                            getLocation();
                          });
                        },
                        readOnly: true,
                      ),
                    ),
                    new Container(
                      color: Colors.white,
                      child: new TextField(
                        controller: pincode,
                        decoration: InputDecoration(
                            labelText: "PIN CODE",
                            labelStyle: TextStyle(
                                fontFamily: "opensan",
                                fontSize: 16)),
                        maxLines: 2,
                      ),
                    ),
                    new Container(
                      color: Colors.white,
                      child: new TextField(
                        controller: name,
                        decoration: InputDecoration(
                            labelText: "Name",
                            labelStyle: TextStyle(
                                fontFamily: "opensan",
                                fontSize: 16)),
                        maxLines: 2,
                      ),
                    ),
                    new Container(
                      color: Colors.white,

                      child: new TextField(
                        controller: phone,

                        decoration:
                        InputDecoration(
                          labelText: "Phone",
                          counterText: "",
                        ),


                        maxLength: 10,
                        keyboardType: TextInputType.number,
                      ),

                    ),
                    // new Container(
                    //   padding:
                    //       EdgeInsets.only(top: 20, bottom: 20),
                    //   color: Colors.white,
                    //   child: new Column(
                    //     children: <Widget>[
                    //       new Container(
                    //         padding: EdgeInsets.only(
                    //             left: 20,
                    //             right: 20,
                    //             bottom: 10,
                    //             top: 15),
                    //         child: Align(
                    //           alignment: Alignment.center,
                    //           child: Text("Save as"),
                    //         ),
                    //       ),
                    //       new Row(
                    //         mainAxisAlignment:
                    //             MainAxisAlignment.center,
                    //         children: <Widget>[
                    //           new Material(
                    //             color: addr_type == 0 ? Colors.blueAccent : Colors.white,
                    //             child: InkWell(
                    //               onTap: () {
                    //                 changeAddrType();
                    //               },
                    //               child: Container(
                    //                 width: 50,
                    //                 height: 50,
                    //                 child: Icon(
                    //                       Icons.home,
                    //                       color: addr_type == 0 ? Colors.white : Colors.blueAccent,
                    //                     ),
                    //               ),
                    //             ),
                    //           ),
                    //           new Material(
                    //             color: addr_type == 1 ? Colors.blueAccent : Colors.white,
                    //             child: InkWell(
                    //               onTap: () {
                    //                 changeAddrType();
                    //               },
                    //               child: Container(
                    //                 width: 50,
                    //                 height: 50,
                    //                 child: Icon(
                    //                   Icons
                    //                     .house,
                    //                     color: addr_type == 1 ? Colors.white : Colors.blueAccent,
                    //                 ),
                    //               ),
                    //             ),
                    //           )
                    //         ],
                    //       )
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
              ),
            )
          ]),
      // SafeArea(
      //   child: Stack(
      //     children: <Widget>[
      //       // Container(
      //       //   height: MediaQuery.of(context).size.height / 1.4,
      //       //   child: new Stack(
      //       //     children: <Widget>[
      //       //       new Container(
      //       //         child: GoogleMap(
      //       //           // mapType: MapType.normal,
      //       //           polylines: poly,
      //       //           initialCameraPosition: CameraPosition(
      //       //               target: LatLng(lt,lo), zoom: 12.0),
      //       //           onMapCreated: (GoogleMapController controller) {
      //       //             _controller.complete(controller);
      //       //           },
      //       //           compassEnabled: true,
      //       //           mapToolbarEnabled: true,
      //       //           zoomGesturesEnabled: true,
      //       //           myLocationButtonEnabled: true,
      //       //           // circles: circles,
      //       //           myLocationEnabled: true,
      //       //            markers: Set.from(allMarks),
      //       //           onCameraIdle: () {
      //       //             if(initialView == 0){
      //       //
      //       //             }else{
      //       //               getCurrentLocation(
      //       //                   _lastMapPosition.latitude,
      //       //                   _lastMapPosition.longitude);
      //       //             }
      //       //
      //       //           },
      //       //           onCameraMove: (CameraPosition position) {
      //       //             // print(position.target);
      //       //             setState(() {
      //       //               _lastMapPosition = position.target;
      //       //               initialView = 1;
      //       //             });
      //       //             // getCurrentLocation(
      //       //             //     _lastMapPosition.latitude, _lastMapPosition.longitude);
      //       //           },
      //       //         ),
      //       //       ),
      //       //       new Positioned(
      //       //         top: (mapHeight - 100) / 2.5,
      //       //         right: (mapWidth - 100) / 2,
      //       //         child: Image(
      //       //           image: AssetImage(
      //       //             "assets/icons/location.png",
      //       //           ),
      //       //           width: 100,
      //       //           height: 100,
      //       //         ),
      //       //       )
      //       //     ],
      //       //   ),
      //       // ),
      //       Container(
      //         child: DraggableScrollableSheet(
      //           initialChildSize: 0.26,
      //           minChildSize: 0.26,
      //           maxChildSize: 0.54,
      //           builder: (BuildContext context, myscrollController) {
      //             return SafeArea(
      //               child: CustomScrollView(
      //                   controller: myscrollController,
      //                   slivers: <Widget>[
      //                     SliverAppBar(
      //                       centerTitle: true,
      //                       title: SizedBox(
      //                         width: 20,
      //                         child: Icon(
      //                           Icons.remove,
      //                           color: Colors.black38,
      //                         ),
      //                       ),
      //                       backgroundColor: Colors.white,
      //                       leading: IconButton(
      //                         icon: Icon(Icons.chevron_left),
      //                         color: Colors.black38,
      //                         onPressed: () {
      //                           Navigator.pop(context);
      //                         },
      //                       ),
      //                       pinned: true,
      //                       elevation: 0,
      //                     ),
      //                     SliverToBoxAdapter(
      //                       child: new Container(
      //                         padding: EdgeInsets.only(left: 20, right: 20),
      //                         color: Colors.white,
      //                         child: Column(
      //                           children: <Widget>[
      //                             new Container(
      //                               color: Colors.white,
      //                               child: new TextField(
      //                                 controller: location,
      //                                 decoration: InputDecoration(
      //                                     labelText: "ADDRESS",
      //                                     labelStyle: TextStyle(
      //                                         fontFamily: "opensan",
      //                                         fontSize: 16)),
      //                                 maxLines: 2,
      //                                 style: TextStyle(
      //                                   fontFamily: "opensan",
      //                                 ),
      //                                 onTap: (){
      //                                   Navigator.push(context, FadeRoute(page: LocationPage())).then((val) {
      //                                     getLocation();
      //                                   });
      //                                 },
      //                                 readOnly: true,
      //                               ),
      //                             ),
      //                             new Container(
      //                               color: Colors.white,
      //                               child: new TextField(
      //                                 controller: pincode,
      //                                 decoration: InputDecoration(
      //                                     labelText: "PIN CODE",
      //                                     labelStyle: TextStyle(
      //                                         fontFamily: "opensan",
      //                                         fontSize: 16)),
      //                                 maxLines: 2,
      //                               ),
      //                             ),
      //                             new Container(
      //                               color: Colors.white,
      //                               child: new TextField(
      //                                 controller: name,
      //                                 decoration: InputDecoration(
      //                                     labelText: "Name",
      //                                     labelStyle: TextStyle(
      //                                         fontFamily: "opensan",
      //                                         fontSize: 16)),
      //                                 maxLines: 2,
      //                               ),
      //                             ),
      //                             new Container(
      //                               color: Colors.white,
      //
      //                               child: new TextField(
      //                                 controller: phone,
      //
      //                                 decoration:
      //                                     InputDecoration(
      //                                       labelText: "Phone",
      //                                       counterText: "",
      //                                     ),
      //
      //
      //                                 maxLength: 10,
      //                                 keyboardType: TextInputType.number,
      //                               ),
      //
      //                             ),
      //                             // new Container(
      //                             //   padding:
      //                             //       EdgeInsets.only(top: 20, bottom: 20),
      //                             //   color: Colors.white,
      //                             //   child: new Column(
      //                             //     children: <Widget>[
      //                             //       new Container(
      //                             //         padding: EdgeInsets.only(
      //                             //             left: 20,
      //                             //             right: 20,
      //                             //             bottom: 10,
      //                             //             top: 15),
      //                             //         child: Align(
      //                             //           alignment: Alignment.center,
      //                             //           child: Text("Save as"),
      //                             //         ),
      //                             //       ),
      //                             //       new Row(
      //                             //         mainAxisAlignment:
      //                             //             MainAxisAlignment.center,
      //                             //         children: <Widget>[
      //                             //           new Material(
      //                             //             color: addr_type == 0 ? Colors.blueAccent : Colors.white,
      //                             //             child: InkWell(
      //                             //               onTap: () {
      //                             //                 changeAddrType();
      //                             //               },
      //                             //               child: Container(
      //                             //                 width: 50,
      //                             //                 height: 50,
      //                             //                 child: Icon(
      //                             //                       Icons.home,
      //                             //                       color: addr_type == 0 ? Colors.white : Colors.blueAccent,
      //                             //                     ),
      //                             //               ),
      //                             //             ),
      //                             //           ),
      //                             //           new Material(
      //                             //             color: addr_type == 1 ? Colors.blueAccent : Colors.white,
      //                             //             child: InkWell(
      //                             //               onTap: () {
      //                             //                 changeAddrType();
      //                             //               },
      //                             //               child: Container(
      //                             //                 width: 50,
      //                             //                 height: 50,
      //                             //                 child: Icon(
      //                             //                   Icons
      //                             //                     .house,
      //                             //                     color: addr_type == 1 ? Colors.white : Colors.blueAccent,
      //                             //                 ),
      //                             //               ),
      //                             //             ),
      //                             //           )
      //                             //         ],
      //                             //       )
      //                             //     ],
      //                             //   ),
      //                             // ),
      //                           ],
      //                         ),
      //                       ),
      //                     )
      //                   ]),
      //             );
      //           },
      //         ),
      //       )
      //     ],
      //   ),
      // ),
      bottomNavigationBar: BottomAppBar(
        elevation: 1,
        child: new Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
          child: new FlatButton(
            color: Colors.black,
            child: Padding(
              padding: EdgeInsets.only(top: 15, bottom: 15),
              child: Text(
                "Save",
                style: TextStyle(color: Colors.white, fontFamily: "opensan"),
              ),
              // child : Container(
              //   height: 50,
              //   child: FlareActor(
              //               "assets/animations/Loader.flr",
              //               animation: "Untitled",
              //               color: Colors.blueAccent.withOpacity(0.5),
              //             ),
              // )
            ),
            onPressed: () {
              if(location.text.length==0){
                alertDailog("Please Enter Adrress");

              }
              else{
                if(pincode.text.length==0){
                  alertDailog("Please Enter Pincode");

                }
                else{
                  if(name.text.length==0){
                    alertDailog("Please Enter Name");

                  }
                  else{
                    if(phone.text.length == 0){
                      alertDailog("Please Enter Number");

                    }
                    else{
                      //print("kkkk");
                      saveAddress();

                    }
                  }
                }

              }
              // Navigator.push(context, EnterExitRoute(enterPage: SettimePage()));
              // saveAddress();
            },
            // onPressed: () {
            //   // Navigator.push(context, EnterExitRoute(enterPage: SettimePage()));
            //   saveAddress();
            // },
          ),
        ),
      ),
    );
  }
}
