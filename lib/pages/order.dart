
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

import 'orderhistory.dart';

class Order extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OrderLoader();
  }
}
class OrderLoader extends State<Order>{
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }
  
  
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  List history=[];
  var orderid="";


  Future getOrderhistorylist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.ORDERHISTORY_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "user_id": sp.getString("userid")
    };
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        history = json.decode(res)['data'];
       // orderid=json.decode(res)['data'][0]['id'];
        print(orderid);



      } else {
        history = [];


      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
 
 
 
  Future getOrderCancel(orderid) async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.CANCELPRODUCTORDER_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
    "order_id":orderid
    };
    print(data);
    var res = await apiPostRequests(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        getOrderhistorylist();
        // history = json.decode(res)['data'];
        // print(history);



      } else {
       // history = [];


      }

      loading = false;
    });
  }

  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  @override
  void initState() {
    getOrderhistorylist();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title:Container(
          child: Text("Order"),
        ),



      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child:
            Container(

                child:history.length==0?Container(
                    child:
                    Container(
                      color: AppColors.white,


                      child: Image(

                        image: AssetImage("assets/images/datanotfound.jpg"),
                        height: 600,
                      ),
                    )

                ): data()
            ),

          )
        ],
      ),


    );
  }
  Widget data(){

    return Container(

      padding: EdgeInsets.only(top: 5,bottom: 5,right: 5,left: 5),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: history.length,
        itemBuilder: (BuildContext context, int i) {
          return Container(
            padding: EdgeInsets.only(
                top: 5, bottom: 5, left: 5, right: 5),
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Orderhistory(history[i]['id'])));
              },
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 5, right: 5),
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child:
                            Text("Order Date:"+history[i]['placed_at']),
                          ),
                          // Container(
                          //   child: Text("Order Id:"+history[i]['order_key']),
                          // )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 5, bottom: 5, left: 5, right: 5),
                      color: AppColors.grayBorder,
                      height: 1,
                    ),

                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              left: 10, top: 5, bottom: 8),
                          child: Text("Order Id:"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width:
                          MediaQuery.of(context).size.width /
                              2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text: history[i]['order_key'],
                                        style: TextStyle(
                                          color: AppColors
                                              .black,
                                          fontSize: 14,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              left: 10, top: 5, bottom: 8),
                          child: Text("Delivery Time"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width:
                          MediaQuery.of(context).size.width /
                              2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text:  history[i]['max_delivery_hour']==null?"":"Within "+ history[i]['max_delivery_hour']+" Hours",
                                        style: TextStyle(
                                          color: AppColors
                                              .black,
                                          fontSize: 14,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              left: 10, top: 5, bottom: 8),
                          child: Text("Payment Mode"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width:
                          MediaQuery.of(context).size.width /
                              2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text: history[i]['payment_mode'],
                                        style: TextStyle(
                                          color: AppColors
                                              .black,
                                          fontSize: 14,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              left: 10, top: 5, bottom: 8),
                          child: Text(
                            "₹"+history[i]['total_price'],
                            style: TextStyle(
                                color: AppColors.redcolor3),
                          ),
                          alignment: Alignment.topLeft,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              left: 10, top: 5, bottom: 8),
                          child: Text("Status"),
                          alignment: Alignment.topLeft,
                        ),
                        Container(
                          //height: 25,
                          width:
                          MediaQuery.of(context).size.width /
                              2.2,
                          // color: AppColors.sentColor,
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 25,
                                        top: 05,
                                        right: 10,
                                        bottom: 5),
                                    //color: AppColors.primary,
                                    child: RichText(
                                      text: new TextSpan(
                                        text: history[i]['status'] == "0" ? "Pending"
                                            : history[i]['status'] == "1"?"Deliverd"
                                            :history[i]['status'] == "2" ? "Cancel"
                                        :history[i]['status'] == "3"?"Cancel":"",
                                        style: TextStyle(
                                          color: history[i]['status'] == "2" ? AppColors.redcolor3:history[i]['status'] == "2" ? AppColors.redcolor3:
                                          AppColors.black,

                                          fontSize: 16,
                                        ),
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10,bottom: 20,right: 20),
                      child: InkWell(onTap: (){
                        orderid=history[i]['id'];
                        getOrderCancel(orderid);
                      },
                        child:
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            history[i]['status'] == "2" ?Container(
                              height: 1,
                            ): history[i]['status'] == "1"?Container(
                              height: 1,
                            ):
                            Container(

                              height: 40,
                              width: 100,
                              decoration: BoxDecoration(
                                color: AppColors.lightgreen,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child:
                              Center(child: Text("CANCEL")),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),

          );
        }),
    );
  }
}
