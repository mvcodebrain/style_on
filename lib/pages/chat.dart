import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/servicepage/bookingdetail.dart';
import 'package:yesmadam/services/apis.dart';

class Chat extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ChatView();
  }
}

class ChatView extends State<Chat> {

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  List message = [];
  var chattx = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;



  Future getchat() async {
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.CHAT_API;
      var data = {
        "user_id":user,
        "message":chattx.text
      };
      print(data);

      var res = await apiPostRequestse(url, data);

      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          message = json.decode(res)['data'];
          chattx.text="";
        });
       // print(services);
      }
      else {
        setState(() {
          message = [];
        });
      }
    }
    else{
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
        // Navigator.pop(context);
      });

      var url = Apis.CHAT_API;
      // var data = json.encode(map);
      var data = {
        "user_id":"",
        "booking_id": "",
        "service_id": "",
        "vendor_id":"",
        "message":"so what  "
      };
      print(data);

      var res = await apiPostRequestse(url, data);

      print(res);
      setState(() {
        if (json.decode(res)['status'].toString() == "1")
        {
          setState(() {
            message = json.decode(res)['data'];
            chattx.text="";

          });

          // print(services);
        }
        else{
          setState(() {
            message=[];
            chattx.text="";


          });


        }
        loading = false;
      });

    }

  }

  Future<String> apiPostRequestse(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  @override
  void initState() {

    // TODO: implement initState
    super.initState();
    getchat();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(

          backgroundColor: AppColors.sentColor,
          title: Text("Normal Chat",style: TextStyle(fontSize: 18),),

        ),
        body: Stack(
          children: <Widget>[
            ListView.builder(
              itemCount: message.length,
              controller: _scrollController,
              // shrinkWrap: true,
              padding: EdgeInsets.only(top: 10,bottom: 70),
              // physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index){
                return Container(
                  padding: EdgeInsets.only(left: 14,right: 14,top: 10,bottom: 5),
                  child: Align(
                    alignment: message[index]['sent_by'] == "you" ?Alignment.topRight:Alignment.topLeft,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: message[index]['sent_by'] == "you" ?Colors.blue[100]:Colors.grey[300],
                      ),
                      padding: EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(message[index]['message'], style: TextStyle(fontSize: 15),textAlign: TextAlign.right,),
                          // chats[index]['type'].toString() == "image"?Image.memory(_getImage(chats[index]['msg']))
                          //     :chats[index]['type'].toString() == "text"?Text(chats[index]['msg'], style: TextStyle(fontSize: 15),textAlign: TextAlign.right,)
                          //     :chats[index]['type'].toString() == "video"
                          //     ?getVideo(
                          //     chats[index]['msg']
                          // )
                          //     :Container()
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                padding: EdgeInsets.only(left: 10,bottom: 10,top: 10),
                height: 60,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black26
                      )
                    ]
                ),
                child: Row(
                  children: <Widget>[
                    // GestureDetector(
                    //   onTap: (){
                    //     getImage();
                    //   },
                    //   child: Container(
                    //     height: 30,
                    //     width: 30,
                    //     decoration: BoxDecoration(
                    //       color: Colors.lightBlue,
                    //       borderRadius: BorderRadius.circular(30),
                    //     ),
                    //     child: Icon(Icons.add, color: Colors.white, size: 20, ),
                    //   ),
                    // ),
                    SizedBox(width: 15,),
                    Expanded(
                      child: TextField(
                        controller: chattx,
                        decoration: InputDecoration(
                            hintText: "Write message...",
                            hintStyle: TextStyle(color: Colors.black54),
                            border: InputBorder.none
                        ),
                      ),
                    ),
                    SizedBox(width: 15,),
                    FloatingActionButton(
                      onPressed: (){
                        if(chattx.text.length==0){

                        }
                        else{
                          getchat();
                        }

                       // _sendChatMessage();
                      },
                      child: Icon(Icons.send,color: Colors.blue,size: 25,),
                      backgroundColor: Colors.white,
                      elevation: 0,
                    ),
                  ],

                ),
              ),
            ),
          ],
        )
    );


  }
}
