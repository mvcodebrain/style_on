
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

import 'alladdress.dart';
import 'lastpage.dart';

class ProductPaymentPage extends StatefulWidget{
   final totalprice;
  // final date;
  // final time;
  // final catid;
   ProductPaymentPage(this.totalprice);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProductPaymentPageLoader();
  }
}

class ProductPaymentPageLoader extends State<ProductPaymentPage>{
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }
  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var walletprice="0";
  var timetext="";
  var timehour="";
  var deliveryid="";

  Future getproductorderplace() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.PRODUCTORDERPLACE_API;
    var sp = await SharedPreferences.getInstance();

    // var data = json.encode(map);
    var data = {


        "user_id":sp.getString("userid"),
        "live_lng": sp.getString("log"),
        "live_lat":sp.getString("lat"),
        "total_price":widget.totalprice.toString(),
        "delivery_charge":"0",
        "payment_mode":"COD",
        "address":sp.getString("addressid"),
        "coupon_code":sp.getString("couponid"),
        "applied_coupon_discount":(int.parse(sp.getString("finalprice")) - int.parse(widget.totalprice)).toString(),
        "delivery_hour":deliveryid.toString()



    };
    print(data);
    var res = await apiPostRequests(url,data);

    print(res);
    setState(()
    {
      if(json.decode(res)['status'].toString() == "1"){
        if(addressname==""){
          alertDailog("Please Selected Address");
        }
        else{
          Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
        }



      }
      else
        {


        }
      loading = false;
    });
  }
  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future getproducttime() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.TIMEPRODUCT_API;
      var res = await apiGetRequestss(url);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          timetext = json.decode(res)['data'][0]['delivery_note'];
          timehour=  json.decode(res)['data'][0]['max_hour'];
          deliveryid=json.decode(res)['data'][0]['id'];
          // print(timetext);
        });

      }
      else{}

      print(res);
    } catch (e) {
      print(e);
    }
  }
  Future<String> apiGetRequestss(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
   // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  getwallet()async{
    var sp = await SharedPreferences.getInstance();
    walletprice=sp.getString("balance");
    print(walletprice);
  }
  var addressname = "";
  var addressid = "";
  getAddress() async{
    var sp = await SharedPreferences.getInstance();
    if(sp.containsKey("addressid")){
      setState((){
        addressname = sp.getString("addressname");
        addressid = sp.getString("addressid");
      });

    }else{

    }

  }
  @override
  void initState() {
    getwallet();
   // getAddress();
    getproducttime();
    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Text("Payment options"),
        // elevation: loading == true ? 0 : 1,
      ),
      backgroundColor: AppColors.dividerColor2,
      body: new SafeArea(
        child: new Container(
          child: new Stack(
            children: <Widget>[

              new Container(
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                      child: Container(
                        child: Column(

                          children: [
                            Container(
                              color : Colors.white,



                              child:
                              ListTile(title: Text("Delivery Time")),

                            ),
                            Container(
                                color : Colors.white,
                              padding: EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),

                              child:
                              Text(timetext +  timehour)

                            ),
                            Container(
                              height: 10,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(236, 239, 241, 1),
                                  boxShadow: [BoxShadow(blurRadius: 1, color: Colors.grey)]),
                              height: 75,
                              width: MediaQuery.of(context).size.width,
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.only(
                                        left: 20, right: 20, bottom: 5, top: 10),
                                    child: Text("Dilevered to this address"),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(
                                        left: 20, bottom: 5, top: 5, right: 20),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        new Flexible(
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                padding: EdgeInsets.only(right: 10),
                                                child: Icon(
                                                  Icons.home,
                                                  size: 20,
                                                  color: Colors.indigo,
                                                ),
                                              ),
                                              Flexible(
                                                flex: 1,
                                                child: Text(
                                                  " $addressname",
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.indigo),
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Material(
                                          color: Color.fromRGBO(216, 67, 21, 1),
                                          borderRadius: BorderRadius.all(Radius.circular(3)),
                                          child: new InkWell(
                                            onTap: ()
                                            async {
                                              await
                                              Navigator.push(context,
                                                  EnterExitRoute(enterPage: AllAddress()))
                                                  .then((value) {
                                                getAddress();
                                              });
                                            },
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  left: 5, right: 5, bottom: 5, top: 5),
                                              child: Text(
                                                "Change",
                                                style: TextStyle(color: Colors.white),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                    ),
                    SliverToBoxAdapter(
                      child:  new Container(
                          child : Container(
                              color : Colors.white,
                              child : ListTile(
//                                 leading: Checkbox(
// //                                title: Text("Wallet"),
//
//                                  // value: radioset != 2  && radioset != 0 ? true: int.parse(balance) > int.parse(widget.price) ? false : false,
//                                   onChanged: (newValue) {
//                                     setState(() {
// //                                      checkedValue = newValue;
// //                                       if(int.parse(balance) > int.parse(widget.price)){
// //                                         setListing();
// //                                         _setRadio(6);
// //                                       }else{
// //
// //                                       }
//                                     });
//                                   },
// //                                controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
//                                 ),
                                title: Text("Wallet"),
                                trailing: Text("\u20B9 ")//+walletprice),
                              )
                          )
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: new Container(

                        padding: EdgeInsets.only(top: 10),
                        child: Container(
                          color: Colors.white,
                          child: new Column(
                            children: <Widget>[



                              new Container(
                                padding: EdgeInsets.only(top: 10, bottom: 5),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      //"Online/Cash",
                                      "Cash",
                                      style: TextStyle(
                                          fontFamily: "opensan",
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                              ),
                              new Container(
                                padding: EdgeInsets.only(left: 20, right: 20),
                                child: Divider(),
                              ),
                              new ListTile(

                                // onTap: () {
                                //   setListing();
                                //   _setRadio(2);
                                // },
                                title: Text(
                                  "Cash On Delivery",
                                  // style: TextStyle(fontSize: 18),
                                ),
                                leading: SizedBox(
                                  child: Radio(
                                    value: 1,
                                    groupValue: 1,
                                    activeColor: AppColors.primary2,
                                    onChanged: (val) {


                                    },
                                  ),
                                ),
                                trailing: IconButton(
                                  onPressed: () {},
                                  icon: Icon(Icons.supervised_user_circle_outlined),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    // SliverToBoxAdapter(
                    //   child: new Container(
                    //     padding: EdgeInsets.only(top: 10),
                    //     child: Container(
                    //       color: Colors.white,
                    //       child: new Column(
                    //         children: <Widget>[
                    //           new Container(
                    //             padding: EdgeInsets.only(top: 10, bottom: 5),
                    //             child: new Row(
                    //               mainAxisAlignment: MainAxisAlignment.center,
                    //               children: <Widget>[
                    //                 Text(
                    //                   "Other methods",
                    //                   style: TextStyle(
                    //                       fontFamily: "opensan",
                    //                       fontSize: 20,
                    //                       fontWeight: FontWeight.w500),
                    //                 )
                    //               ],
                    //             ),
                    //           ),
                    //           new ListTile(
                    //
                    //             onTap: () {
                    //               // setListing();
                    //               // _setRadio(3);
                    //             },
                    //             title: Text(
                    //               "Debit/Credit card",
                    //               // style: TextStyle(fontSize: 18),
                    //             ),
                    //             leading: Radio(
                    //               value: 3,
                    //              // groupValue: radioset,
                    //               activeColor: AppColors.primary2,
                    //               onChanged: (val) {
                    //                 print(val);
                    //
                    //               },
                    //             ),
                    //           ),
                    //           new ListTile(
                    //
                    //             onTap: () {
                    //               // setListing();
                    //               // _setRadio(4);
                    //             },
                    //             title: Text(
                    //               "UPI",
                    //               // style: TextStyle(fontSize: 18),
                    //             ),
                    //             leading: Radio(
                    //               value: 4,
                    //               //groupValue: radioset,
                    //               activeColor: AppColors.primary2,
                    //               onChanged: (val) {
                    //                 print(val);
                    //
                    //
                    //               },
                    //             ),
                    //           ),
                    //           new ListTile(
                    //
                    //             title: Text(
                    //               "Net Banking",
                    //               // style: TextStyle(fontSize: 18),
                    //             ),
                    //             leading: Radio(
                    //               value: 5,
                    //             //  groupValue: radioset,
                    //               activeColor: AppColors.primary2,
                    //               onChanged: (val) {
                    //                 print(val);
                    //
                    //               },
                    //             ),
                    //           ),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    // )
                  ],
                ),
              ),
              // new Positioned(
              //   top: 0,
              //   left: 0,
              //   right : 0,
              //   bottom: 0,
              //   // child: loading == true ? new Container(
              //   //   decoration: BoxDecoration(
              //   //       color: AppColors.primary2
              //   //   ),
              //     child: new Center(
              //       child: Column(
              //         mainAxisAlignment: MainAxisAlignment.center,
              //         children: <Widget>[
              //           new SizedBox(
              //               width: 30,
              //               height: 30,
              //               child :CircularProgressIndicator(
              //                   valueColor: new AlwaysStoppedAnimation<Color>(AppColors.white)
              //               )
              //           ),
              //           Padding(
              //             padding : EdgeInsets.only(
              //                 top: 10
              //             ),
              //             child: Text(
              //               "Please wait,",
              //               style: TextStyle(
              //                   color: Colors.white
              //               ),
              //             ),
              //           ),
              //           Padding(
              //             padding : EdgeInsets.only(
              //                 top: 0
              //             ),
              //             child: Text(
              //               "msg",
              //               style: TextStyle(
              //                   color: Colors.white
              //               ),
              //             ),
              //           )
              //         ],
              //       ),
              //     ),
              //   // ) : new Container(),
              // )
            ],
          ),
        ),
      ),
      bottomNavigationBar:
      // cartlist.length.toString()=="0"?Container(
      //   height: 0,
      // ):
      Container(
        height: 75,
        child: Column(
          children: [

            Container(
              height: 65,
              padding: EdgeInsets.only(left: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(2)),
                color: AppColors.white,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 0, top: 20),
                          child: Row(
                            children: [
                              Text(
                                "₹  "+widget.totalprice,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: InkWell(
                          onTap: () {
                            getproductorderplace();

                          },
                          child: Container(
                            height: 45,
                            width: 45,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              color: AppColors.sentColor,
                            ),
                            child: Center(
                              child: Text(
                                "Place Order",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),

    );
  }

}