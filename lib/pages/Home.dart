import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:share/share.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/bloc/category_bloc.dart';
import 'package:yesmadam/bloc/product/product_bloc.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:yesmadam/pages/Homepage.dart';
import 'package:yesmadam/fonts/my_icon_icons.dart';
import 'package:yesmadam/pages/productcart.dart';
import 'package:yesmadam/pages/chat.dart';
import 'package:yesmadam/pages/refer.dart';
import 'package:yesmadam/pages/updateprofile.dart';
import 'package:yesmadam/pages/wallet.dart';

import 'package:yesmadam/respos/categories.repos.dart';
import 'package:yesmadam/bloc/slider/slider_bloc.dart';
import 'package:yesmadam/bloc/slider/slider_event.dart';
import 'package:yesmadam/respos/product_repos.dart';
import 'package:yesmadam/respos/slider_repo.dart';
import 'package:yesmadam/services/apis.dart';
import 'package:yesmadam/servicepage/appointment.dart';
import 'package:yesmadam/servicepage/pageView.dart';

import 'login.dart';
import 'order.dart';



class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeLoader();
  }
}

class HomeLoader extends State<Home> with SingleTickerProviderStateMixin {
  Razorpay _razorpay;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  bool expanded = true;
  AnimationController controller;
  getFcmToken() async {
    var sp = await SharedPreferences.getInstance();
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      // setState(() {
      //   // _homeScreenText = "Push Messaging token: $token";
      // });
      print("Push Messaging token: $token");
      sp.setString("device_token", '$token');
      print(token);
    });
    _firebaseMessaging.configure(
        onBackgroundMessage: myBackgroundMessageHandler);
  }

  Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      print(message['data']);
      // Handle data message
      _onLoading();
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  var number = "";
  var email = "";
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future getwhatapp() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.WHATAPP_API;
      var res = await apiGetRequest(url);
      // print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          number = json.decode(res)['data'][0]['whatsapp'].toString();
          print(number);
        });
      }

      // print(res);

    } catch (e) {
      print(e);
    }
  }

  Future<String> apiGetRequest(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getemail() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.EMAIL_API;
      var res = await apiGetRequesta(url);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          email = json.decode(res)['email'];
          print(email);
        });
      }

      print(res);
    } catch (e) {
      print(e);
    }
  }

  Future<String> apiGetRequesta(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  bool logedin = false;
  getUser() async {
    //_onLoading();
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      setState(() {
        logedin = true;
      });
    } else {
      setState(() {
        logedin = false;
      });
    }
  }

  logOut() async {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      sp.remove("userid");
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    });
    getUser();
  }

  var phonenumber = "";
  var profilename = "";
  var mobile = "";
  List page=[];
  Future getprofile() async {
    // print("checking for login");
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.PROFILE_API;
      var data = {
        "user_id": user,
      };
      var res = await apiPostRequests(url, data);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          profilename = json.decode(res)['response'][0]['first_name'];
          mobile = json.decode(res)['response'][0]['mobile'];
          print(profilename);
          logedin = true;
        });
        // print();
      } else {
        setState(() {
          logedin = false;
        });
      }
    } else {
      print("Please login");
    }
  }

  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getcall() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.CALL_API;
      var res = await apiGetRequestae(url);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          phonenumber = json.decode(res)['data'][0]['mobile'];
          print(phonenumber);
        });
      }

      print(res);
    } catch (e) {
      print(e);
    }
  }
  Future getpage() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.PAAGEVIEW_API;
      var res = await apiGetRequestae(url);
      print(res);
      if (json.decode(res)['status'] == "1") {
        setState(() {
          page = json.decode(res)['contentlist'];
          print(page.length);
        });
      }

      print(res);
    } catch (e) {
      print(e);
    }
  }

  Future<String> apiGetRequestae(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);

    return reply;
  }
  var reason = TextEditingController();
  Future getpay(payment) async {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });

    var url = Apis.Payafter;
    // var data = json.encode(map);
    var data = {

      "booking_id":booking,
      "payment_id":payment
    };

    print(data.toString());
    var res = await apiPostRequest(url,data);
    print(res);
    setState(() {

      loading = false;
    });
    chekExist = json.decode(res)['status'].toString();
    print(chekExist);
    afterservice="0";
    getpayafter();


  }
  var chekExist="0";
  var ratingValue = "";
  Future getrating() async {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });

    var url = Apis.RATING_API;
    // var data = json.encode(map);
    var data = {

      "user_id":sp.getString("userid").toString(),
      "booking_id":booking,
      "rating_point":"$ratingValue",
      "review":reason.text

    };

    //    "rating":"$_rating",
    //  "customer_id":sp.getString("userid").toString(),
    // // "saloon_id":widget.saloon_id,
    //  "service_id":"",
    //  "title":"",
    //  "comments":""};
    //

    print(data.toString());
    var res = await apiPostRequest(url,data);
    // sp.containsKey("userid");

    print(res);
    setState(() {

      loading = false;
    });
    var rate = json.decode(res)['status'].toString();
    chekExist="0";
    print(chekExist);
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);

    return reply;
  }

  var booking;
  var totalprice;
  var afterservice="0";
  var rating="0";
  Future getpayafter() async {

    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);

    });
    var sp = await SharedPreferences.getInstance();
    var userid=sp.getString("userid").toString();
    var url = Apis.Payafternow + userid;
    print(url);
    var res = await apiPostRequestse(url);

    print(res);
    setState(() {

      if(json.decode(res)['status'] == 1){

        setState(() {
          rating=json.decode(res)['review'].toString();
          print("hhh"+afterservice);
          booking=json.decode(res)['data']['booking_id'];
          totalprice=json.decode(res)['data']['total_amount'];
          afterservice=json.decode(res)['data']['after_service'].toString();




        });

        //
        //
        //
        //
      }
      loading = false;
    });
  }
  Future<String> apiPostRequestse(String url) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.set ("X-Api-Key","7c4a8d09ca3762af61e59520943dc26494f8941b");
    // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }
  int selectedRadio = 0;

  @override
  void initState() {
    // getprofile();
    // getUser();
    // getFcmToken();
    // getwhatapp();
    // getemail();
    // getcall();
    // getpage();
    // getpage();
    //
    // getpayafter();
    // _razorpay = Razorpay();
    // _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    // _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    // _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    //
    // super.initState();
    // controller = AnimationController(
    //   vsync: this,
    //   duration: Duration(milliseconds: 400),
    //   reverseDuration: Duration(milliseconds: 400),
    getprofile();
    getUser();
    getFcmToken();
    getwhatapp();
    getemail();
    getcall();
    getpage();

    getpayafter();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);

    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
      reverseDuration: Duration(milliseconds: 400),
    );
  }

  int _currentIndex = 0;
  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    var options = {
      'key': Apis.payment_key,
      'amount': "$totalprice"+"00",
      'name': Apis.app_name,
      'description': '',
      'prefill': {'contact': '', 'email': ''},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint('Error: e');
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    getpay(response.paymentId);
    // getupdated(response.paymentId);
    // Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, toastLength: Toast.LENGTH_SHORT);

  }

  void _handlePaymentError(PaymentFailureResponse response) {

    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        toastLength: Toast.LENGTH_SHORT);

    // print("jhhhj"+response.code.toString());
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, toastLength: Toast.LENGTH_SHORT);
  }



  var paymentmethod;
  var payment="";
  _selectpaymentmethod(int selectedRadio) async{
    setState(() {

      paymentmethod=selectedRadio ;
      if(paymentmethod==1){
        payment="wallet";

      }
      else{
        if(paymentmethod==2){
          payment="cod";

        }
        else{
          if(paymentmethod==3){
            payment="online";

          }
          else{
            if(paymentmethod==4){
              payment="after_service";

            }


          }

        }

      }

      print(payment);

    });


  }


  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Order',
      style: optionStyle,
    ),
    Text(
      'Index 1: Go Out',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
    Text(
      'Index 3: School',
      style: optionStyle,
    ),
    Text(
      'Index 4: School',
      style: optionStyle,
    ),
  ];

  Widget pages = HomePage();
  changePage(value) async {
    if (value == 0) {
      setState(() {
        pages = HomePage();
      });
    }
    if (value == 1) {
      setState(() {
        pages = Booking();
      });
    }
    if (value == 2) {
      setState(() {
        pages = ProductCartitems();
      });
    }
    if (value == 3) {
      setState(() {
        pages = Refer();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // drawer: Drawer(
      //   child: ListView(
      //     children: [
      //       logedin == false
      //           ? Container(
      //         //padding: EdgeInsets.only(top: 30),
      //
      //         child: Container(
      //             padding: EdgeInsets.only(
      //                 top: 20, bottom: 20, left: 10, right: 10),
      //             child: InkWell(
      //               onTap: () {
      //                 Navigator.push(
      //                     context,
      //                     MaterialPageRoute(
      //                         builder: (context) => LoginLoader()))
      //                     .then((value) => getprofile());
      //                 // logOut();
      //               },
      //               child: ListTile(
      //                   title: Card(
      //                     color: AppColors.sentColor,
      //                     shape: RoundedRectangleBorder(
      //                         borderRadius: BorderRadius.circular(15.0)),
      //                     child: Container(
      //                       //color: AppColors.black,
      //                       height: 50,
      //                       width: 50,
      //                       padding: EdgeInsets.only(top: 0, left: 10),
      //                       child: Center(
      //                         child: Text(
      //                           "Log in",
      //                           style: TextStyle(
      //                               fontSize: 25, color: AppColors.white),
      //                         ),
      //                       ),
      //                     ),
      //                   )),
      //             )),
      //       )
      //           : Container(
      //         height: 100,
      //         decoration: BoxDecoration(
      //           color: AppColors.white,
      //         ),
      //         child: Container(
      //           padding: EdgeInsets.only(top: 50, left: 14),
      //           child: Row(
      //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //             children: [
      //               Column(
      //                 crossAxisAlignment: CrossAxisAlignment.start,
      //                 children: [
      //                   Container(
      //                     padding: EdgeInsets.only(right: 10, top: 5),
      //
      //                     // color: Colors.white,
      //
      //                     child: Text(
      //                       profilename,
      //                       style: TextStyle(
      //                           color: AppColors.grayBorder,
      //                           fontSize: 15,
      //                           fontWeight: FontWeight.bold),
      //                     ),
      //                   ),
      //                   Container(
      //                     child: Text(
      //                       "+91" + mobile,
      //                       style: TextStyle(
      //                           color: AppColors.grayBorder,
      //                           fontSize: 15,
      //                           fontWeight: FontWeight.bold),
      //                     ),
      //                   )
      //                 ],
      //               ),
      //               InkWell(
      //                 onTap: () {
      //                   Navigator.push(
      //                       context,
      //                       MaterialPageRoute(
      //                           builder: (context) => updated()))
      //                       .then((value) => getprofile());
      //                 },
      //                 child: Container(
      //                   padding: EdgeInsets.only(right: 10),
      //                   child: Container(
      //                     decoration: BoxDecoration(
      //                         borderRadius: BorderRadius.circular(120),
      //                         border: Border.all(
      //                             color: AppColors.grayBorder)),
      //                     child: Container(
      //                         padding: EdgeInsets.all(2),
      //                         child: Icon(
      //                           Icons.edit,
      //                           color: AppColors.grayBorder,
      //                         )),
      //                   ),
      //                 ),
      //               )
      //             ],
      //           ),
      //         ),
      //       ),
      //       Divider(
      //         thickness: 5,
      //       ),
      //       Material(
      //         child: Container(
      //           color: AppColors.toggele,
      //           child: InkWell(
      //             onTap: () {
      //               //getcall();
      //
      //               Navigator.pop(context);
      //               //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "Home",
      //                 style: TextStyle(
      //                     color: AppColors.sentColor,
      //                     fontSize: 15,
      //                     fontWeight: FontWeight.w500),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.home_outlined,
      //                 color: AppColors.sentColor,
      //                 size: 20,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () {
      //               Navigator.push(context,
      //                   MaterialPageRoute(builder: (context) => Booking()));
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "My Booking",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.supervised_user_circle_outlined,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       // Material(
      //       //   child: Container(
      //       //
      //       //     // color : AppColors.whiteTransparent2,
      //       //     child: InkWell(
      //       //       onTap: (){
      //       //         //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //       //       },
      //       //       child: ListTile(
      //       //         // tileColor: AppColors.white,
      //       //         title: Text("Profile",style: TextStyle(
      //       //             fontSize: 15),),
      //       //
      //       //         leading:
      //       //         Icon(Feather.user,size: 22,),
      //       //       ),
      //       //     ),
      //       //   ),
      //       //
      //       // ),
      //       // Material(
      //       //   child: Container(
      //       //     //color : AppColors.whiteTransparent2,
      //       //     child: InkWell(
      //       //       onTap: () {
      //       //         Navigator.push(context,
      //       //             MaterialPageRoute(builder: (context) => Order()));
      //       //       },
      //       //       child: ListTile(
      //       //         // tileColor: AppColors.white,
      //       //         title: Text(
      //       //           "Order",
      //       //           style: TextStyle(fontSize: 15),
      //       //         ),
      //       //
      //       //         leading: Icon(
      //       //           Icons.favorite_border,
      //       //           size: 22,
      //       //         ),
      //       //       ),
      //       //     ),
      //       //   ),
      //       // ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () {
      //               Navigator.push(context,
      //                   MaterialPageRoute(builder: (context) => Wallet()));
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "Wallet",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.account_balance_wallet_outlined,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       // Material(
      //       //   child: Container(
      //       //     //color : AppColors.whiteTransparent2,
      //       //     child: InkWell(
      //       //       onTap: () {
      //       //         Navigator.push(context,
      //       //             MaterialPageRoute(builder: (context) => FavoriteLoader()));
      //       //       },
      //       //       child: ListTile(
      //       //         // tileColor: AppColors.white,
      //       //         title: Text(
      //       //           "Wishlist",
      //       //           style: TextStyle(fontSize: 15),
      //       //         ),
      //       //
      //       //         leading: Icon(
      //       //           Icons.favorite,
      //       //           size: 22,
      //       //         ),
      //       //       ),
      //       //     ),
      //       //   ),
      //       // ),
      //       Divider(
      //         thickness: 1,
      //       ),
      //       Container(
      //         padding: EdgeInsets.only(left: 10),
      //         child: Text(
      //           "Communication",
      //           style: TextStyle(color: AppColors.grayText, fontSize: 16),
      //         ),
      //       ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () {
      //               launch("https://play.google.com/store/apps/details?id=" +
      //                   "com.kiranamart.multivendor");
      //               //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "Register As Partner",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.apps,
      //                 size: 22,
      //                 color: AppColors.lightgreen,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () {
      //               Share.share(
      //                   "https://play.google.com/store/apps/details?id=" +
      //                       "com.kirana.mart");
      //               //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "Refer a Friend",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Feather.share,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () {
      //               Navigator.push(context,
      //                   MaterialPageRoute(builder: (context) => Chat()));
      //
      //             },
      //             // onTap: () async {
      //             //   var url =
      //             //       'https://api.whatsapp.com/send?l=en&phone=+91' + number;
      //             //   if (await canLaunch(url)) {
      //             //     await launch(url);
      //             //   } else {
      //             //     throw 'Could not launch $url';
      //             //   }
      //             //  },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //                 title: Text(
      //                   "Chat Us",
      //                   style: TextStyle(fontSize: 15),
      //                 ),
      //                 leading: Icon(Icons.chat)
      //               // Image(
      //               //   image: AssetImage("assets/images/whatap.png"),
      //               //   height: 25,
      //               //   width: 25,
      //               // )
      //             ),
      //           ),
      //         ),
      //       ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () async {
      //               if (await canLaunch("mailto:$email")) {
      //                 await launch("mailto:$email");
      //               } else {
      //                 throw 'Could not launch';
      //               }
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "Email Us",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.email_outlined,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () {
      //               launch("https://play.google.com/store/apps/details?id=" +
      //                   "com.kirana.mart");
      //               //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "Review Us",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.star_border,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       // Material(
      //       //   child: Container(
      //       //
      //       //     //color : AppColors.whiteTransparent2,
      //       //     child: InkWell(
      //       //       onTap: (){
      //       //         //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //       //       },
      //       //       child: ListTile(
      //       //         // tileColor: AppColors.white,
      //       //         title: Text("FAQ",style: TextStyle(
      //       //             fontSize: 15),),
      //       //
      //       //         leading:
      //       //         Icon(Icons.help_center_outlined,size: 22,),
      //       //       ),
      //       //     ),
      //       //   ),
      //       //
      //       // ),
      //       Material(
      //         child: Container(
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () async {
      //               var url = "tel:" + phonenumber;
      //               if (await canLaunch(url)) {
      //                 await launch(url);
      //               } else {
      //                 throw 'Could not launch $url';
      //               }
      //             },
      //             //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 "Call Us",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.call,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //       Container(
      //           child: ListView.builder(
      //             physics: NeverScrollableScrollPhysics(),
      //             shrinkWrap: true,
      //             itemCount:page.length,
      //             //pages.length,
      //             itemBuilder: (context, i) {
      //               return ListTile(
      //                 onTap: () {
      //                   // // // launch(insta);
      //                   Navigator.push(
      //                       context,
      //                       MaterialPageRoute(
      //                           builder: (context) => Pageview(
      //                             pagename: page[i]['content_title'],
      //                             pagedetail: page[i]['content_description'],
      //                           )));
      //                 },
      //                 title: Text(page[i]['content_title']),
      //                 leading: Icon(
      //                   EvilIcons.eye,
      //                   color: AppColors.black75,
      //                   size: 18,
      //                 ),
      //               );
      //             },
      //           )),
      //       Divider(
      //         thickness: 1,
      //       ),
      //       Container(
      //         padding: EdgeInsets.only(left: 10, bottom: 10),
      //         child: Text(
      //           "Sign out",
      //           style: TextStyle(color: AppColors.grayText, fontSize: 16),
      //         ),
      //       ),
      //       Material(
      //         child: Container(
      //           padding: EdgeInsets.only(bottom: 10),
      //
      //           //color : AppColors.whiteTransparent2,
      //           child: InkWell(
      //             onTap: () {
      //               logOut();
      //               //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
      //             },
      //             child: ListTile(
      //               // tileColor: AppColors.white,
      //               title: Text(
      //                 logedin == false ? "Login" : "Logout",
      //                 style: TextStyle(fontSize: 15),
      //               ),
      //
      //               leading: Icon(
      //                 Icons.logout,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
      // appBar: AppBar(
      //   backgroundColor: AppColors.black,
      //   title:
      //   Container(
      //     child: Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       children: [
      //         Container(
      //           child: Text("UBER",style: TextStyle(
      //               fontSize: 20,fontWeight: FontWeight.bold,
      //               color: AppColors.white
      //           )),
      //         ),
      //         Container(
      //           width: 105,
      //           height: 40,
      //           decoration: BoxDecoration(
      //             border: Border.all(
      //               color: AppColors.white,
      //               style: BorderStyle.solid,
      //               width: 1.0,
      //             ),
      //             borderRadius: BorderRadius.all(Radius.circular(120)),
      //             color: AppColors.white,
      //
      //             //color: Color(0xFFFff3f6c),
      //           ),
      //           child: Row(
      //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //             children: [
      //               Container(
      //                 padding:EdgeInsets.only(left: 15),
      //                 child: Text("Help",style: TextStyle(color: AppColors.black,fontSize: 16),),
      //               ),
      //               // Container(
      //               //   padding:EdgeInsets.only(right: 10),
      //               //   child: Icon(Icons.keyboard_arrow_down),
      //               // )
      //               Container(
      //                 padding: EdgeInsets.fromLTRB(0, 0, 10, 10),
      //                 child:
      //                 IconButton(
      //                     icon: AnimatedIcon(
      //                       icon: AnimatedIcons.menu_close,
      //                       progress: controller,
      //                       semanticLabel: 'Show menu',
      //                     ),
      //                     onPressed: () {
      //                       setState(() {
      //                         expanded ? controller.forward() : controller.reverse();
      //                         expanded = !expanded;
      //                       });
      //                     }),
      //               )],
      //           ),
      //
      //         )
      //       ],
      //     ),
      //   ),
      //
      //   centerTitle: false,
      //
      //   // Row(
      //   //   children: [
      //   //     Container(
      //   //       child: CircleAvatar(
      //   //         backgroundImage:AssetImage("assets/images/aa.png") ,
      //   //
      //   //       ),
      //   //     ),
      //   //     Container(
      //   //       padding: EdgeInsets.only(),
      //   //       decoration: BoxDecoration(
      //   //
      //   //           border: Border(
      //   //               left: BorderSide(
      //   //                 color: Colors.white,
      //   //
      //   //               )
      //   //           )
      //   //         // borderRadius:
      //   //         // BorderRadius.all(Radius.circular(4)),
      //   //       ),
      //   //       child: Text("MEN",style: TextStyle(
      //   //           fontWeight: FontWeight.w600,
      //   //           color: Colors.black87
      //   //       ),
      //   //
      //   //
      //   //       ),
      //   //     )
      //   //
      //   //   ],
      //   // ),
      //
      //
      //
      //
      //   iconTheme: IconThemeData(color: Colors.black),
      //   // pinned: true,
      //   //  floating: true,
      //   elevation: 1,
      //   automaticallyImplyLeading: false,
      //
      //   actions: <Widget>[
      //
      //
      //
      //     // IconButton(
      //     //   onPressed: () {
      //     //     // getCurrentLocation();
      //     //   },
      //     //   icon: Icon(
      //     //     Icons.my_location,
      //     //     color: Colors.white,
      //     //   ),
      //     // ),
      //     // IconButton(
      //     //   onPressed: () {
      //     //     // getCurrentLocation();
      //     //   },
      //     //   icon: Icon(
      //     //     Icons.notifications,
      //     //     color: Colors.black,
      //     //   ),
      //     // )
      //   ],
      //   // IconButton(
      //   //   onPressed: (){},
      //   //   icon: Icon(
      //   //     Icons.qr_code_outlined,
      //   //     color: Colors.white,
      //   //   ),
      //   // ),
      //
      //   // IconButton(
      //   //   onPressed: ()async {
      //   //     //_BottomSheetMenu();
      //   //   },
      //   //
      //   //   icon: Icon(
      //   //       Icons.notifications,
      //   //       color: Colors.white
      //   //   ),
      //   // ),
      //   // IconButton(
      //   //   onPressed: (){
      //   //
      //   //
      //   //   },
      //   //   icon: Icon(
      //   //     Feather.help_circle,
      //   //     color: AppColors.white,
      //   //   ),
      //   // ),
      //
      // ),
      body: Stack(
        children: [
          pages,
          Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child:chekExist=="1" ?Center(
                child: Container(
                  height: 400,
                  width: MediaQuery.of(context).size.width / 1.2,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(top: 30),

                            child: Column(
                              children: [
                                // Container(
                                //   margin: EdgeInsets.only(top: 20),
                                //   child: Text(
                                //     "Total Charger",
                                //     style: TextStyle(
                                //       fontSize: 16,
                                //       fontWeight: FontWeight.bold,
                                //     ),
                                //   ),
                                // ),
                                // Container(
                                //   margin: EdgeInsets.only(top: 5),
                                //   child: Text(
                                //     "₹ totalprice ",
                                //     style: TextStyle(
                                //         fontSize: 25,
                                //         fontWeight: FontWeight.bold,
                                //         color: AppColors.sentColor),
                                //   ),
                                // ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        // Navigator.push(context, MaterialPageRoute(builder: (context)=>CompletePage(bookinger)));
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: AppColors.sentColor,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        padding: EdgeInsets.only(
                                            top: 5,
                                            right: 10,
                                            bottom: 5,
                                            left: 10),
                                        margin:
                                        EdgeInsets.only(right: 0, top: 10),
                                        child: Text(
                                          "Rate Us",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.white),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 15),
                                  child: Container(
                                    // height: 100,
                                    // width: 100,
                                    // decoration: BoxDecoration(
                                    //     borderRadius: BorderRadius.circular(120),
                                    //     color: Colors.blueAccent),
                                    //   child:Stack(
                                    //     children: [
                                    //       Positioned(
                                    //         left: 0,
                                    //         right: 0,
                                    //         top: 0,
                                    //         bottom: 0,
                                    //         child: SizedBox(
                                    //           width: 40,
                                    //           height: 40,
                                    //           child: CircularProgressIndicator(
                                    //             valueColor:
                                    //             AlwaysStoppedAnimation<Color>(
                                    //                 Colors.orangeAccent),
                                    //             strokeWidth: 2,
                                    //           ),
                                    //         ),
                                    //       )
                                    //     ],
                                    //   )
                                  ),
                                ),
                                Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Center(
                                      child: Text(
                                        "Rate The Partner"
                                        // "${widget.driverName}"
                                        ,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Center(
                                    child: RatingBar.builder(
                                      initialRating: 0,
                                      minRating: 0,
                                      direction: Axis.horizontal,
                                      allowHalfRating: true,
                                      itemCount: 5,
                                      itemPadding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                      itemBuilder: (context, _) => Icon(
                                        Icons.star,
                                        color: Colors.black,
                                      ),
                                      onRatingUpdate: (rating) {
                                        print(rating);
                                        setState(() {
                                          ratingValue =
                                              double.parse(rating.toString())
                                                  .round()
                                                  .toString();
                                        });
                                      },
                                      itemSize: 30,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              left: 10, right: 10, bottom: 10, top: 10),
                          // height: 120,

                          decoration: BoxDecoration(
                            color: AppColors.black.withOpacity(0.01),
                            border: Border.all(
                              color: AppColors.black.withOpacity(0.1),
                              // style: BorderStyle.solid,
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextField(
                            controller: reason,
                            //controller: mobile,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Feedback",
                                hintStyle: TextStyle(color: Colors.grey[400]),
                                contentPadding:
                                EdgeInsets.only(left: 10, top: 10),
                                counterText: ""),
                            maxLines: 7,
                            //  maxLength: 10,
                          ),
                        ),
                        Container(
                          height: 40,
                          // decoration: BoxDecoration(
                          //   border : Border(
                          //     top: BorderSide(
                          //       color: Colors.orange
                          //     )
                          //   )
                          // ),
                          child: Row(
                            children: [
                              Flexible(
                                child: Container(
                                  child: InkWell(
                                    onTap: () async {
                                      var sp =
                                      await SharedPreferences.getInstance();
                                      // sp.remove("bookingided");

                                      // Navigator.pop(context);
                                      // Provider.of<ViewRatingCard>(context,
                                      //     listen: false)
                                      //     .handleViewCard();
                                      // Provider.of<ViewRatingCard>(context).handleViewCard();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.orange.withOpacity(0.1),
                                          borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(20))),
                                      child: Center(
                                        child: Text(
                                          "Later",
                                          style: TextStyle(
                                              color: Colors.black, fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Flexible(
                                child: Container(
                                  child: InkWell(
                                    onTap: () {
                                      getrating();
                                      // rateDriver();
                                      // selectratingedcancel();

                                    },
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(20)),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.sentColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(20))),
                                      child: Center(
                                        child: Text(
                                          "Submit",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ):Container(height: 1,)
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: afterservice=="0"?Container(height: 1,):Center(
              child: Container(
                height: 300,
                width: MediaQuery.of(context).size.width / 1.2,
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 20),
                                child: Text(
                                  "Total Charger",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Text(
                                  "₹ $totalprice ",
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.sentColor),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>CompletePage(bookinger)));
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.sentColor,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                      padding: EdgeInsets.only(
                                          top: 5,
                                          right: 10,
                                          bottom: 5,
                                          left: 10),
                                      margin:
                                      EdgeInsets.only(right: 0, top: 10),
                                      child: Text(
                                        "Pay Now",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: AppColors.white),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 15),
                                child: Container(
                                  // height: 100,
                                  // width: 100,
                                  // decoration: BoxDecoration(
                                  //     borderRadius: BorderRadius.circular(120),
                                  //     color: Colors.blueAccent),
                                  //   child:Stack(
                                  //     children: [
                                  //       Positioned(
                                  //         left: 0,
                                  //         right: 0,
                                  //         top: 0,
                                  //         bottom: 0,
                                  //         child: SizedBox(
                                  //           width: 40,
                                  //           height: 40,
                                  //           child: CircularProgressIndicator(
                                  //             valueColor:
                                  //             AlwaysStoppedAnimation<Color>(
                                  //                 Colors.orangeAccent),
                                  //             strokeWidth: 2,
                                  //           ),
                                  //         ),
                                  //       )
                                  //     ],
                                  //   )
                                ),
                              ),
                              Container(
                                child: new ListTile(

                                  onTap: () {
                                    selectedRadio = 3;
                                    _selectpaymentmethod(selectedRadio);
                                    // openCheckout();
                                    // setListing();
                                    // _setRadio(3);
                                  },
                                  title: Text(
                                    "Pay Online",
                                    // style: TextStyle(fontSize: 18),
                                  ),
                                  leading:SizedBox(
                                    child: Radio(
                                      value: 3,
                                      groupValue: selectedRadio,
                                      activeColor: AppColors.primary2,
                                      onChanged: (val) {
                                        setState(() {
                                          selectedRadio = 3;
                                          _selectpaymentmethod(selectedRadio);
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                child: new ListTile(

                                  onTap: () {
                                    selectedRadio = 2;
                                    _selectpaymentmethod(selectedRadio);


                                  },
                                  title: Text(
                                    "Cash On Delivery",
                                    // style: TextStyle(fontSize: 18),
                                  ),
                                  leading: SizedBox(
                                    child: Radio(
                                      value: 2,
                                      groupValue: selectedRadio,
                                      activeColor: AppColors.primary2,
                                      onChanged: (val) {
                                        setState(() {

                                          selectedRadio = 2;
                                          _selectpaymentmethod(selectedRadio);
                                        });
                                      },
                                    ),
                                  ),
                                  // trailing: IconButton(
                                  //   onPressed: () {},
                                  //   icon: Icon(Icons.supervised_user_circle_outlined),
                                  // ),
                                ),
                              )

                            ],
                          ),
                        ),
                      ),

                      Container(
                        height: 40,
                        // decoration: BoxDecoration(
                        //   border : Border(
                        //     top: BorderSide(
                        //       color: Colors.orange
                        //     )
                        //   )
                        // ),
                        child: Row(
                          children: [
                            // Flexible(
                            //   child: Container(
                            //     child: InkWell(
                            //       onTap: () async {
                            //         var sp =
                            //         await SharedPreferences.getInstance();
                            //
                            //
                            //         // Navigator.pop(context);
                            //         // Provider.of<ViewRatingCard>(context,
                            //         //     listen: false)
                            //         //     .handleViewCard();
                            //         // Provider.of<ViewRatingCard>(context).handleViewCard();
                            //       },
                            //       child: Container(
                            //         decoration: BoxDecoration(
                            //             color: Colors.orange.withOpacity(0.1),
                            //             borderRadius: BorderRadius.only(
                            //                 bottomLeft: Radius.circular(20))),
                            //         child: Center(
                            //           child: Text(
                            //             "Later",
                            //             style: TextStyle(
                            //                 color: Colors.black, fontSize: 18),
                            //           ),
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                            Flexible(
                              child: Container(

                                child: InkWell(
                                  onTap: () {

                                    if(payment=="online"){
                                      openCheckout();


                                    }else{
                                      getpay(0);

                                    }
                                    //getrating();
                                    // rateDriver();
                                    // selectratingedcancel();

                                  },
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(20),
                                      bottomLeft: Radius.circular(20)),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.sentColor,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(20),
                                            bottomLeft: Radius.circular(20)
                                        )),
                                    child: Center(
                                      child: Text(
                                        "Submit",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   type: BottomNavigationBarType.fixed,
      //   currentIndex: _currentIndex,
      //   backgroundColor: AppColors.white,
      //   selectedItemColor: AppColors.sentColor,
      //   unselectedItemColor: AppColors.grayText,
      //   // selectedLabelStyle: textTheme.caption,
      //   // unselectedLabelStyle: textTheme.caption,
      //   onTap: (value) {
      //     // Respond to item press.
      //     changePage(value);
      //     controller.forward();
      //     setState(() => _currentIndex = value);
      //     //onTap: _onItemTapped,
      //   },
      //   items: [
      //     BottomNavigationBarItem(
      //       title: Text('Home'),
      //       icon: Icon(Icons.home_outlined),
      //     ),
      //     BottomNavigationBarItem(
      //       title: Text('My Booking'),
      //       icon: Icon(Icons.supervised_user_circle_outlined),
      //       //   // Image(image: AssetImage("assets/images/salonAt.png"),
      //       //   // height: 30)
      //       //   // icon: AnimatedIcon(
      //       //   //   progress: controller,
      //       //   //   icon: AnimatedIcons.menu_home,
      //       //   // )
      //     ),
      //     BottomNavigationBarItem(
      //       title: Text('Product Cart'),
      //       icon: Icon(Icons.shopping_cart),
      //     ),
      //     BottomNavigationBarItem(
      //       title: Text('Refer'),
      //       icon: Icon(Feather.share_2),
      //     ),
      //   ],
      //
      //   showSelectedLabels: true,
      //   showUnselectedLabels: true,
      // ),
    );
  }
}
