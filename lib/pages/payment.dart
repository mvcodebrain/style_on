//
// import 'dart:convert';
// import 'dart:io';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:razorpay_flutter/razorpay_flutter.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:yesmadam/color/AppColors.dart';
// import 'package:yesmadam/services/apis.dart';
//
// import 'lastpage.dart';
//
// class PaymentPage extends StatefulWidget{
//   final totalprice;
//   final date;
//   final time;
//   final catid;
//   final endtime;
//   final timeid;
//   final vendorid;
//   final coupondiscountprices;
//   final couponid;
//   PaymentPage(this.totalprice,this.date,this.time,this.catid,this.endtime,this.timeid,this.vendorid,this.coupondiscountprices,this.couponid);
//
//   @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//     return PaymentPageLoader();
//   }
// }
//
// class PaymentPageLoader extends State<PaymentPage>{
//   Razorpay _razorpay;
//   void _onLoading() {
//     showDialog(
//       context: context,
//       barrierDismissible: false,
//       builder: (BuildContext context) {
//         return Dialog(
//           child: Padding(
//               padding: EdgeInsets.only(
//                   left:20,
//                   right:20,
//                   top : 20,
//                   bottom: 20
//               ),
//               child : new Row(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   new CircularProgressIndicator(
//                   ),
//                   new Container(
//                     padding : EdgeInsets.only(
//                         left:30
//                     ),
//                     child: new Text("Please wait..."),
//                   )
//                 ],
//               )
//           ),
//         );
//       },
//     );
//   }
//   bool loading = false;
//   bool ani = false;
//   Color _color = Colors.black.withOpacity(0.3);
//   bool fail;
//   var walletprice="0";
//   int selectedRadio = 0;
//
//   Future getbookingplace() async {
//
//     // var sp = await SharedPreferences.getInstance();
//     setState(() {
//       loading = true;
//       ani = false;
//       fail = false;
//       _color = Colors.black.withOpacity(0.3);
//       // Navigator.pop(context);
//     });
//
//     var url = Apis.BOOKINGLIST_API;
//     var sp = await SharedPreferences.getInstance();
//     var addons = sp.getStringList("adonid");
//
//
//     // var data = json.encode(map);
//     var data = {
//       "user_id":sp.getString("userid"),
//       "cat_id":widget.catid,
//       "price":widget.totalprice,
//       "addon_id":addons,
//       "amount_before_des":(int.parse(widget.totalprice) - int.parse(sp.getString("payaddonprice"))).toString(),
//       "addon_amount":sp.getString("payaddonprice"),
//       "pay_mode":payment,
//       "address_id":sp.getString("addressid"),
//       "start_time": widget. time  +   widget. endtime,
//       "time_schedule_id":widget.timeid,
//       "date":widget.date,
//       "coupon_id":widget.couponid,
//       "coupon_discount":widget.coupondiscountprices,
//       "vendor_id":widget.vendorid,
//
//
//
//
//       // "user_id":sp.getString("userid"),
//       // "cat_id":widget.catid,
//       // "price":widget.totalprice,
//       // "addon_id":addons,
//       // "amount_before_des":(int.parse(widget.totalprice) - int.parse(sp.getString("payaddonprice"))).toString(),
//       // "addon_amount":sp.getString("payaddonprice"),
//       // "pay_mode":"COD",
//       // "coupon_id":coupon,//sp.getString("couponid"),
//       // "address_id":sp.getString("addressid"),
//       // "lat":sp.getString("lat"),
//       //   "lon":sp.getString("log"),
//       // "time":widget.time,
//       // "date":widget.date
//
//     };
//     print(data);
//     var res = await apiPostRequests(url,data);
//
//     print(res);
//     setState(() {
//       if(json.decode(res)['status'].toString() == "1"){
//
//         if(payment=="online"){
//           openCheckout();
//
//         }
//         else{
//           getwallet();
//
//           Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
//         }
//
//
//
//
//
//       }
//       loading = false;
//     });
//   }
//   Future<String> apiPostRequests(String url, data) async {
//     _onLoading();
//     HttpClient httpClient = new HttpClient();
//     HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
//     request.headers.set('content-type', 'application/json');
//     // request.headers.set('api-key' , Apis.LOGIN_API);
//     request.add(utf8.encode(json.encode(data)));
//     HttpClientResponse response = await request.close();
//     // todo - you should check the response.statusCode
//     String reply = await response.transform(utf8.decoder).join();
//     httpClient.close();
//     Navigator.pop(context);
//     return reply;
//   }
//   List cartlist =[];
//   var totalprice="0";
//   var itemcost= "";
//   var oldcost="";
//   var saveprice="";
//   var addonprice="0";
//   var couponid="0";
//   var coupondiscountprices="0";
//   String totalpricedetail="0";
//   var paymentmethod;
//   var payment="";
//
//   _selectpaymentmethod(int selectedRadio) async{
//     setState(() {
//
//       paymentmethod=selectedRadio ;
//       if(paymentmethod==1){
//         payment="wallet";
//
//       }
//       else{
//         if(paymentmethod==2){
//           payment="cod";
//
//         }
//         else{
//           if(paymentmethod==3){
//             payment="online";
//
//           }
//           else{
//             if(paymentmethod==4){
//               payment="after_service";
//
//             }
//
//
//           }
//
//         }
//
//       }
//
//       print(payment);
//
//     });
//
//
//   }
//   Future getCartlist() async {
//
//     // var sp = await SharedPreferences.getInstance();
//     setState(() {
//       loading = true;
//       ani = false;
//       fail = false;
//       _color = Colors.black.withOpacity(0.3);
//       // Navigator.pop(context);
//     });
//
//     var url = Apis.CARTLIST_API;
//     var sp = await SharedPreferences.getInstance();
//     // var data = json.encode(map);
//     var data = {
//       "cat_id":widget.catid,
//       "user_id":  sp.getString("userid")  };
//     print(data);
//     var res = await apiPostRequests(url,data);
//
//     print(res);
//     setState(() {
//       if(json.decode(res)['status'].toString() == "1"){
//         cartlist = json.decode(res)['response']['cartList'];
//         itemcost =json.decode(res)['response']['cartList'][0]['service_price'];
//         oldcost =json.decode(res)['response']['cartList'][0]['old_price'];
//         totalprice=json.decode(res)['response']['total_amount'].toString();
//         addonprice=json.decode(res)['response']['addOnTotal'].toString();
//
//         // saveprice=(int.parse(
//         //     json.decode(res)['response']['cartList'][0]['old_price']) -
//         //     int.parse( json.decode(res)['response']['cartList'][0]['service_price']))
//         //     .toString();
//
//         totalpricedetail= (int.parse(
//             json.decode(res)['response']['total_amount'].toString()) +
//             int.parse( json.decode(res)['response']['addOnTotal'].toString()) - int.parse(widget.coupondiscountprices)
//         )
//             .toString();
//
//         sp.setString("payprice", (int.parse(
//             json.decode(res)['response']['total_amount'].toString()) +
//             int.parse( json.decode(res)['response']['addOnTotal'].toString()))
//             .toString());
//         sp.setString("payaddonprice", json.decode(res)['response']['addOnTotal'].toString());
//
//
//         print(sp.getString("payaddonprice"));
//
//       }
//       else{
//         cartlist = [];
//         totalprice="0";
//         itemcost= "";
//         oldcost="";
//         saveprice="";
//         addonprice="0";
//         totalpricedetail="0";
//
//
//
//
//       }
//
//       loading = false;
//     });
//   }
//   List wallet =[];
//
//   var balance ="";
//   List Data =[];
//   var amu="0";
//   Future getwallet() async {
//
//     // var sp = await SharedPreferences.getInstance();
//     setState(() {
//       loading = true;
//       ani = false;
//       fail = false;
//       _color = Colors.black.withOpacity(0.3);
//       // Navigator.pop(context);
//     });
//     var sp = await SharedPreferences.getInstance();
//     var url = Apis.WALLET_API;
//     // var data = json.encode(map);
//     var data = {
//
//       "user_id":
//       //1,
//       sp.getString("userid"),
//       "page":"0",
//       "limit":"100"
//
//
//
//       //"user_id":sp.getString("userid")
//
//     };
//
//     var res = await apiPostRequests(url,data);
//
//     print(res);
//     setState(() {
//
//       if(json.decode(res)['status'].toString() == "1"){
//
//         setState(() {
//           // price=json.decode(res)['data'];
//           if(json.decode(res)['data'].length==0){
//             balance="0";
//             sp.setString("balance", "0");
//
//           }else{
//             amu = json.decode(res)['data']['balance'];
//             wallet = json.decode(res)['data']['transactions'];
//             balance = json.decode(res)['data']['transactions'][0]['amount'].toString();
//             sp.setString("balance", json.decode(res)['data']['transactions'][0]['amount'].toString());
//             print(balance);
//           }
//           print(json.decode(res)['data'].length);
//
//
//
//         });
//
//       }
//       else{
//         setState(() {
//           balance="0";
//           sp.setString("balance", "0");
//           print(balance);
//         });
//
//       }
//       loading = false;
//     });
//   }
//
//
//
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//   _showInSnackBar(String value) {
//     _scaffoldKey.currentState.showSnackBar(new SnackBar(
//       content: new Text(
//         value,
//         style: TextStyle(),
//         textAlign: TextAlign.center,
//       ),
//       backgroundColor: Colors.redAccent,
//       duration: Duration(seconds: 1),
//       // action: ,
//     ));
//   }
//   @override
//   void initState() {
//     getwallet();
//     getCartlist();
//     // TODO: implement initState
//     super.initState();
//     _razorpay = Razorpay();
//     _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
//     _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
//     _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
//   }
//
//
//
//   @override
//   void dispose() {
//     super.dispose();
//     _razorpay.clear();
//   }
//
//   void openCheckout() async {
//     var options = {
//       'key': 'rzp_test_1DP5mmOlF5G5ag',
//       'amount': totalpricedetail+"00",
//       'name': 'DOS',
//       'description': '',
//       'prefill': {'contact': '', 'email': ''},
//       'external': {
//         'wallets': ['paytm']
//       }
//     };
//
//     try {
//       _razorpay.open(options);
//     } catch (e) {
//       debugPrint('Error: e');
//     }
//   }
//
//   void _handlePaymentSuccess(PaymentSuccessResponse response) {
//     Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
//     Fluttertoast.showToast(
//         msg: "SUCCESS: " + response.paymentId, toastLength: Toast.LENGTH_SHORT);
//   }
//
//   void _handlePaymentError(PaymentFailureResponse response) {
//     Fluttertoast.showToast(
//         msg: "ERROR: " + response.code.toString() + " - " + response.message,
//         toastLength: Toast.LENGTH_SHORT);
//   }
//
//   void _handleExternalWallet(ExternalWalletResponse response) {
//     Fluttertoast.showToast(
//         msg: "EXTERNAL_WALLET: " + response.walletName, toastLength: Toast.LENGTH_SHORT);
//   }
//
//
//
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Scaffold(
//       key: _scaffoldKey,
//       appBar: AppBar(
//         backgroundColor: AppColors.sentColor,
//         title: Text("Payment options",style: TextStyle(color: AppColors.white),),
//         iconTheme: IconThemeData(color: Colors.white),
//         // elevation: loading == true ? 0 : 1,
//       ),
//       backgroundColor: AppColors.dividerColor2,
//       body: new SafeArea(
//         child: new Container(
//           child: new Stack(
//             children: <Widget>[
//
//               new Container(
//                 child: CustomScrollView(
//                   slivers: <Widget>[
//                     SliverToBoxAdapter(
//                       child: cart(),
//                     ),
//                     SliverToBoxAdapter(
//                       child: itemlist(),
//                     ),
//                     SliverToBoxAdapter(
//                       child: Container(
//                         height: 5,
//                       ),
//                     ),
//                     SliverToBoxAdapter(
//                       child:  new Container(
//                           child : Container(
//                               padding: EdgeInsets.only(right: 10),
//                               color : Colors.white,
//                               child :
//                               ListTile(
//                                 onTap: (){
//                                   if(int.parse(amu)>=int.parse(totalpricedetail))
//                                   {
//                                     selectedRadio = 1;
//                                     _selectpaymentmethod(selectedRadio);
//                                   }
//                                   else{
//
//                                     _showInSnackBar("Low Balance IN Wallet ,Recharge The Wallet");
//
//                                   }
//
//
//
//
//                                 },
//                                 leading: SizedBox(
//                                   child: Radio(
//                                     value: 1,
//                                     groupValue: selectedRadio,
//                                     activeColor: AppColors.primary2,
//                                     onChanged: (val) {
//                                       setState(() {
//                                         selectedRadio = 1;
//                                         _selectpaymentmethod(selectedRadio);
//                                       });
//                                     },
//                                   ),
//                                 ),
// //
//                                 title: Text("Wallet"),
//                                 trailing: Text("\u20B9 "
//                                     +amu==null?"0":"\u20B9 "+amu
//                                 ),
//                               )
//                           )
//                       ),
//                     ),
//                     SliverToBoxAdapter(
//                       child: new Container(
//
//                         padding: EdgeInsets.only(top: 10),
//                         child: Container(
//                           color: Colors.white,
//                           child: new Column(
//                             children: <Widget>[
//
//
//
//                               new Container(
//                                 padding: EdgeInsets.only(top: 10, bottom: 5),
//                                 child: new Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: <Widget>[
//                                     Text(
//                                       //"Online/Cash",
//                                       "Cash",
//                                       style: TextStyle(
//                                           fontFamily: "opensan",
//                                           fontSize: 20,
//                                           fontWeight: FontWeight.w500),
//                                     )
//                                   ],
//                                 ),
//                               ),
//                               new Container(
//                                 padding: EdgeInsets.only(left: 20, right: 20),
//                                 child: Divider(),
//                               ),
//                               new ListTile(
//
//                                 onTap: () {
//                                   selectedRadio = 2;
//                                   _selectpaymentmethod(selectedRadio);
//
//
//                                 },
//                                 title: Text(
//                                   "Cash On Delivery",
//                                   // style: TextStyle(fontSize: 18),
//                                 ),
//                                 leading: SizedBox(
//                                   child: Radio(
//                                     value: 2,
//                                     groupValue: selectedRadio,
//                                     activeColor: AppColors.primary2,
//                                     onChanged: (val) {
//                                       setState(() {
//
//                                         selectedRadio = 2;
//                                         _selectpaymentmethod(selectedRadio);
//                                       });
//                                     },
//                                   ),
//                                 ),
//                                 // trailing: IconButton(
//                                 //   onPressed: () {},
//                                 //   icon: Icon(Icons.supervised_user_circle_outlined),
//                                 // ),
//                               )
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                     SliverToBoxAdapter(
//                       child: new Container(
//                         padding: EdgeInsets.only(top: 10),
//                         child: Container(
//                           color: Colors.white,
//                           child: new Column(
//                             children: <Widget>[
//                               new Container(
//                                 padding: EdgeInsets.only(top: 10, bottom: 5),
//                                 child: new Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: <Widget>[
//                                     Text(
//                                       "Other methods",
//                                       style: TextStyle(
//                                           fontFamily: "opensan",
//                                           fontSize: 20,
//                                           fontWeight: FontWeight.w500),
//                                     )
//                                   ],
//                                 ),
//                               ),
//                               new ListTile(
//
//                                 onTap: () {
//                                   selectedRadio = 3;
//                                   _selectpaymentmethod(selectedRadio);
//                                   // openCheckout();
//                                   // setListing();
//                                   // _setRadio(3);
//                                 },
//                                 title: Text(
//                                   "Pay Online",
//                                   // style: TextStyle(fontSize: 18),
//                                 ),
//                                 leading:SizedBox(
//                                   child: Radio(
//                                     value: 3,
//                                     groupValue: selectedRadio,
//                                     activeColor: AppColors.primary2,
//                                     onChanged: (val) {
//                                       setState(() {
//                                         selectedRadio = 3;
//                                         _selectpaymentmethod(selectedRadio);
//                                       });
//                                     },
//                                   ),
//                                 ),
//                               ),
//                               InkWell(
//                                 onTap: (){
//                                   selectedRadio = 4;
//                                   _selectpaymentmethod(selectedRadio);
//
//                                 },
//                                 child: new ListTile(
//
//                                   // onTap: () {
//                                   //
//                                   //   // setListing();
//                                   //   // _setRadio(4);
//                                   // },
//                                   title: Text(
//                                     "Pay After Service",
//                                     // style: TextStyle(fontSize: 18),
//                                   ),
//                                   leading:SizedBox(
//                                     child: Radio(
//                                       value: 4,
//                                       groupValue: selectedRadio,
//                                       activeColor: AppColors.primary2,
//                                       onChanged: (val) {
//                                         setState(() {
//                                           selectedRadio = 4;
//                                           _selectpaymentmethod(selectedRadio);
//                                         });
//                                       },
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               // new ListTile(
//
//                               //   title: Text(
//                               //     "Net Banking",
//                               //     // style: TextStyle(fontSize: 18),
//                               //   ),
//                               //   leading: Radio(
//                               //     value: 5,
//                               //   //  groupValue: radioset,
//                               //     activeColor: AppColors.primary2,
//                               //     onChanged: (val) {
//                               //       print(val);
//
//                               //     },
//                               //   ),
//                               // ),
//
//                             ],
//                           ),
//                         ),
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//               // new Positioned(
//               //   top: 0,
//               //   left: 0,
//               //   right : 0,
//               //   bottom: 0,
//               //   // child: loading == true ? new Container(
//               //   //   decoration: BoxDecoration(
//               //   //       color: AppColors.primary2
//               //   //   ),
//               //     child: new Center(
//               //       child: Column(
//               //         mainAxisAlignment: MainAxisAlignment.center,
//               //         children: <Widget>[
//               //           new SizedBox(
//               //               width: 30,
//               //               height: 30,
//               //               child :CircularProgressIndicator(
//               //                   valueColor: new AlwaysStoppedAnimation<Color>(AppColors.white)
//               //               )
//               //           ),
//               //           Padding(
//               //             padding : EdgeInsets.only(
//               //                 top: 10
//               //             ),
//               //             child: Text(
//               //               "Please wait,",
//               //               style: TextStyle(
//               //                   color: Colors.white
//               //               ),
//               //             ),
//               //           ),
//               //           Padding(
//               //             padding : EdgeInsets.only(
//               //                 top: 0
//               //             ),
//               //             child: Text(
//               //               "msg",
//               //               style: TextStyle(
//               //                   color: Colors.white
//               //               ),
//               //             ),
//               //           )
//               //         ],
//               //       ),
//               //     ),
//               //   // ) : new Container(),
//               // )
//             ],
//           ),
//         ),
//       ),
//       bottomNavigationBar:
//       // cartlist.length.toString()=="0"?Container(
//       //   height: 0,
//       // ):
//       Container(
//         height: 75,
//         child: Column(
//           children: [
//
//             Container(
//               height: 65,
//               padding: EdgeInsets.only(left: 15),
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.all(Radius.circular(2)),
//                 color: AppColors.white,
//               ),
//               child: Row(
//                 children: [
//                   Expanded(
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Container(
//                           padding: EdgeInsets.only(left: 0, top: 20),
//                           child: Row(
//                             children: [
//                               Text(
//                                 "₹  "+widget.totalprice+".00",
//                                 style: TextStyle(
//                                     fontWeight: FontWeight.bold),
//                               ),
//                             ],
//                           ),
//                         ),
//
//                       ],
//                     ),
//                   ),
//                   Expanded(
//                     child: Container(
//                       padding: EdgeInsets.only(right: 10),
//                       child: SizedBox(
//                         child: InkWell(
//                           onTap: () {
//                             if(selectedRadio==0){
//                               _showInSnackBar("Select the Payment Method");
//
//                             }
//                             else{
//                               getbookingplace();
//                             }
//
//
//                           },
//                           child: Container(
//                             height: 45,
//                             width: 45,
//                             decoration: BoxDecoration(
//                               borderRadius:
//                               BorderRadius.all(Radius.circular(10)),
//                               color: AppColors.sentColor,
//                             ),
//                             child: Center(
//                               child: Text(
//                                 "Place Order",
//                                 style: TextStyle(
//                                     color: Colors.white,
//                                     fontSize: 16.0,
//                                     fontWeight: FontWeight.w400),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             )
//           ],
//         ),
//       ),
//
//     );
//   }
//   Widget cart() {
//     return Container(
//       color: AppColors.white,
//       child: ListView(
//         shrinkWrap: true,
//         physics: NeverScrollableScrollPhysics(),
//         children: [
//           Container(
//               padding:
//               EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Container(
//                       child: Text(
//                         "item total",
//                         style: TextStyle(color: AppColors.grayText, fontSize: 14),
//                       )),
//                   Container(
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceAround,
//                         children: [
//                           Container(
//                               padding: EdgeInsets.only(right: 10),
//                               child: Text(
//                                 "₹"+totalprice,
//                                 style: TextStyle(color: AppColors.grayText),
//                               )),
//                           // Container(
//                           //   child: Text(
//                           //     "₹"+oldcost,
//                           //     style: TextStyle(
//                           //         color: AppColors.grayText,
//                           //         decoration: TextDecoration.lineThrough,
//                           //         fontSize: 14),
//                           //   ),
//                           // )
//                         ],
//                       ))
//                 ],
//               )),
//           Container(
//               padding:
//               EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Container(
//                       child: Text(
//                         "add_on[+]",
//                         style: TextStyle(color: AppColors.grayText, fontSize: 14),
//                       )),
//                   Container(
//                       padding: EdgeInsets.only(right: 10),
//                       child: Text(
//                         "₹"+ addonprice,
//                         style: TextStyle(color: AppColors.receiveColor),
//                       )),
//                 ],
//               )),
//           // Container(
//           //     padding:
//           //         EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
//           //     child: Row(
//           //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           //       children: [
//           //         Container(
//           //             child: Text(
//           //           "Safety and support fee",
//           //           style: TextStyle(color: AppColors.grayText, fontSize: 14),
//           //         )),
//           //         Container(
//           //             padding: EdgeInsets.only(right: 10),
//           //             child: Text(
//           //               "₹45",
//           //               style: TextStyle(color: AppColors.grayText),
//           //             )),
//           //       ],
//           //     )),
//           // Container(
//           //     padding:
//           //         EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
//           //     child: Row(
//           //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           //       children: [
//           //         Container(
//           //             child: Text(
//           //           "UC PLUS Membership",
//           //           style: TextStyle(color: AppColors.grayText, fontSize: 14),
//           //         )),
//           //         Container(
//           //             child: Row(
//           //           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           //           children: [
//           //             Container(
//           //                 padding: EdgeInsets.only(right: 10),
//           //                 child: Text(
//           //                   "₹255",
//           //                   style: TextStyle(color: AppColors.grayText),
//           //                 )),
//           //             Container(
//           //               child: Text(
//           //                 "₹55",
//           //                 style: TextStyle(
//           //                     color: AppColors.grayText,
//           //                     decoration: TextDecoration.lineThrough,
//           //                     fontSize: 14),
//           //               ),
//           //             )
//           //           ],
//           //         ))
//           //       ],
//           //     )),
//           Container(
//               padding:
//               EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Container(
//                       child: Text(
//                         "Couple Discount",
//                         style: TextStyle(color: AppColors.grayText, fontSize: 14),
//                       )),
//                   Container(
//                       padding: EdgeInsets.only(right: 10),
//                       child: Text(
//                         "₹"+ widget.coupondiscountprices,
//                         style: TextStyle(color: AppColors.receiveColor),
//                       )),
//                 ],
//               )),
//           Container(
//             height: 1,
//             color: AppColors.grayText,
//             padding: EdgeInsets.only(right: 10, left: 10),
//           ),
//           Container(
//               padding:
//               EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Container(
//                       child: Text(
//                         "Total",
//                         style: TextStyle(
//                             color: AppColors.black,
//                             fontSize: 14,
//                             fontWeight: FontWeight.bold),
//                       )),
//                   Container(
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.end,
//                       children: [
//                         Container(
//                             padding: EdgeInsets.only(right: 10),
//                             child: Text(
//                               "₹"+totalpricedetail,
//                               style: TextStyle(
//                                   color: AppColors.black,
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 14),
//                             )),
//                         // Container(
//                         //     padding: EdgeInsets.only(right: 10),
//                         //     child: Text(
//                         //       "You're saving ₹"+saveprice,
//                         //       style: TextStyle(
//                         //           color: AppColors.receiveColor, fontSize: 12),
//                         //     ))
//                       ],
//                     ),
//                   ),
//                 ],
//               )),
//           Container(
//             height: 20,
//             color: AppColors.dividerColor,
//           ),
//           Container(
//             padding: EdgeInsets.only(top: 0, left: 0, bottom: 0),
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.circular(2)),
//               color: Colors.white,
//             ),
//             //color: Colors.greenAccent,
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget itemlist() {
//     return Container(
//       child: Column(
//           children: [
//             Container(
//               color: AppColors.white,
//               padding: EdgeInsets.only(top:10,left:10,bottom: 10),
//               child:  ListView.builder(
//                 shrinkWrap: true,
//                 physics: NeverScrollableScrollPhysics(),
//                 itemCount:
//                 //cartlist.length,
//                 cartlist.length != null ?cartlist.length:"",
//                 //services.length,
//                 itemBuilder: (BuildContext context, int i) {
//                   return Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Flexible(
//                         child: Container(
//                           padding:EdgeInsets.only(top: 10),
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//
//                             children: [
//                               Container(
//
//                                   child:
//                                   Text(
//                                     cartlist[i]['tittle'] != null
//                                         ? cartlist[i]['tittle']
//                                         : "",
//                                     //  cartlist[i]['tittle'],
//                                     style: TextStyle(
//                                         fontSize: 14,
//                                         fontWeight: FontWeight.w400
//                                     ),
//                                   )),
//                               Container(
//                                 // height: 30,
//                                 width: MediaQuery.of(context).size.width/5,
//                                 decoration: BoxDecoration(
//                                   color: AppColors.white,
//                                 ),
//                                 child: Center(
//                                   child: Text(
//                                     // snapshot.data[0].toString() == "Instance of 'Cart'"
//                                     //    ? snapshot.data[0].qty.toString()
//                                     // :
//                                     "Quantity -"+ cartlist[i]['qty'],
//                                     style: TextStyle(color: Colors.black),
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//
//                         ),
//                       ),
//
//                       Container(
//                           padding: EdgeInsets.only(right: 10,top: 20),
//
//                           child:Row(
//                             children: [
//
//
//                               Container(
//                                 child: Row(
//                                   children: [
//
//                                     Container(
//                                       padding: EdgeInsets.only(right: 5),
//                                       child:  Text(
//                                         cartlist[i]['old_price'] != null
//                                             ? "₹" +  cartlist[i]['old_price']
//                                             : "",
//                                         //cartlist[i]['old_price'], //+ services[i]['data']['service'][index]['old_price'],
//                                         style: TextStyle(
//                                           color:
//                                           AppColors.black,
//                                           fontSize: 12,
//                                           decoration:
//                                           TextDecoration
//                                               .lineThrough,
//                                         ),
//                                         textAlign:
//                                         TextAlign.start,
//                                       ),
//                                     ),
//                                     Container(
//                                       padding:EdgeInsets.only(right: 10),
//                                       child:  Text(
//
//                                         cartlist[i]['service_price'] != null
//                                             ? "₹" + cartlist[i]['service_price']
//                                             : "",
//                                         //cartlist[i]['service_price'], //+ //services[i]['data']['service'][index]['new_price'],
//                                         style: TextStyle(
//                                             color: AppColors
//                                                 .receiveColor2,
//                                             fontWeight:
//                                             FontWeight.bold,
//                                             fontSize: 14),
//                                         textAlign:
//                                         TextAlign.start,
//                                       ),
//                                     ),
//
//
//
//                                   ],
//                                 ),
//                               )
//
//
//
//
//                             ],)
//
//                       )
//
//
//                     ],
//                   );
//
//                 },
//               ),),
//
//             // Container(
//             //   child:Text("jgjkgkj,gjhgjgjhvbhv hmhvh hkvvvvvvvvvvvhhjjvContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of")
//             // )
//
//
//
//
//
//
//           ]
//       ),
//     );
//   }
// }
//

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

import 'Home.dart';
import 'lastpage.dart';

class PaymentPage extends StatefulWidget{
  final totalprice;
  final date;
  final time;
  final catid;
  final endtime;
  final timeid;
  final vendorid;
  final coupondiscountprices;
  final couponid;
  PaymentPage(this.totalprice,this.date,this.time,this.catid,this.endtime,this.timeid,this.vendorid,this.coupondiscountprices,this.couponid);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PaymentPageLoader();
  }
}

class PaymentPageLoader extends State<PaymentPage>{
  Razorpay _razorpay;
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  alertDailog(text) async{
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: (){
                  Navigator.pop(context);
                  // Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var walletprice="0";
  int selectedRadio = 0;
  var bookingid;
  var message="";
  Future getbookingplace() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.BOOKINGLIST_API;
    var sp = await SharedPreferences.getInstance();
    var addons = sp.getStringList("adonid");


    // var data = json.encode(map);
    var data = {
      "user_id":sp.getString("userid"),
      "cat_id":widget.catid,
      "price":widget.totalprice,
      "addon_id":addons,
      "amount_before_des":(int.parse(widget.totalprice) - int.parse(sp.getString("payaddonprice"))).toString(),
      "addon_amount":sp.getString("payaddonprice"),
      "pay_mode":payment,
      "address_id":sp.getString("addressid"),
      "start_time": widget. time  +   widget. endtime,
      "time_schedule_id":widget.timeid,
      "date":widget.date,
      "coupon_id":widget.couponid,
      "coupon_discount":widget.coupondiscountprices,
      "vendor_id":widget.vendorid,




      // "user_id":sp.getString("userid"),
      // "cat_id":widget.catid,
      // "price":widget.totalprice,
      // "addon_id":addons,
      // "amount_before_des":(int.parse(widget.totalprice) - int.parse(sp.getString("payaddonprice"))).toString(),
      // "addon_amount":sp.getString("payaddonprice"),
      // "pay_mode":"COD",
      // "coupon_id":coupon,//sp.getString("couponid"),
      // "address_id":sp.getString("addressid"),
      // "lat":sp.getString("lat"),
      //   "lon":sp.getString("log"),
      // "time":widget.time,
      // "date":widget.date

    };
    print(data);
    var res = await apiPostRequests(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        bookingid=json.decode(res)['booking_id'].toString();
        print(bookingid);

        if(payment=="online"){
          openCheckout();


        }
        else{
          getwallet();

          Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
        }





      }
      else{
        message=json.decode(res)['message'].toString();
        alertDailog(message);
        // Fluttertoast.showToast(
        //     msg:  message, toastLength: Toast.LENGTH_SHORT);
      }
      loading = false;
    });
  }
  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  List cartlist =[];
  var totalprice="0";
  var itemcost= "";
  var oldcost="";
  var saveprice="";
  var addonprice="0";
  var couponid="0";
  var coupondiscountprices="0";
  String totalpricedetail="0";
  var paymentmethod;
  var payment="";

  _selectpaymentmethod(int selectedRadio) async{
    setState(() {

      paymentmethod=selectedRadio ;
      if(paymentmethod==1){
        payment="wallet";

      }
      else{
        if(paymentmethod==2){
          payment="cod";

        }
        else{
          if(paymentmethod==3){
            payment="online";

          }
          else{
            if(paymentmethod==4){
              payment="after_service";

            }


          }

        }

      }

      print(payment);

    });


  }
  Future getCartlist() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.CARTLIST_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "cat_id":widget.catid,
      "user_id":  sp.getString("userid")  };
    print(data);
    var res = await apiPostRequests(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        cartlist = json.decode(res)['response']['cartList'];
        itemcost =json.decode(res)['response']['cartList'][0]['service_price'];
        oldcost =json.decode(res)['response']['cartList'][0]['old_price'];
        totalprice=json.decode(res)['response']['total_amount'].toString();
        addonprice=json.decode(res)['response']['addOnTotal'].toString();

        // saveprice=(int.parse(
        //     json.decode(res)['response']['cartList'][0]['old_price']) -
        //     int.parse( json.decode(res)['response']['cartList'][0]['service_price']))
        //     .toString();

        totalpricedetail= (int.parse(
            json.decode(res)['response']['total_amount'].toString()) +
            int.parse( json.decode(res)['response']['addOnTotal'].toString()) - int.parse(widget.coupondiscountprices)
        )
            .toString();

        sp.setString("payprice", (int.parse(
            json.decode(res)['response']['total_amount'].toString()) +
            int.parse( json.decode(res)['response']['addOnTotal'].toString()))
            .toString());
        sp.setString("payaddonprice", json.decode(res)['response']['addOnTotal'].toString());


        print(sp.getString("payaddonprice"));

      }
      else{
        cartlist = [];
        totalprice="0";
        itemcost= "";
        oldcost="";
        saveprice="";
        addonprice="0";
        totalpricedetail="0";




      }

      loading = false;
    });
  }
  List wallet =[];

  var balance ="";
  List Data =[];
  var amu="0";
  Future getwallet() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });
    var sp = await SharedPreferences.getInstance();
    var url = Apis.WALLET_API;
    // var data = json.encode(map);
    var data = {

      "user_id":
      //1,
      sp.getString("userid"),
      "page":"0",
      "limit":"100"



      //"user_id":sp.getString("userid")

    };

    var res = await apiPostRequests(url,data);

    print(res);
    setState(() {

      if(json.decode(res)['status'].toString() == "1"){

        setState(() {
          // price=json.decode(res)['data'];
          if(json.decode(res)['data'].length==0){
            balance="0";
            sp.setString("balance", "0");

          }else{
            amu = json.decode(res)['data']['balance'];
            wallet = json.decode(res)['data']['transactions'];
            balance = json.decode(res)['data']['transactions'][0]['amount'].toString();
            sp.setString("balance", json.decode(res)['data']['transactions'][0]['amount'].toString());
            print(balance);
          }
          print(json.decode(res)['data'].length);



        });

      }
      else{
        setState(() {
          balance="0";
          sp.setString("balance", "0");
          print(balance);
        });

      }
      loading = false;
    });
  }

  Future getbookingCancel() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.CANCELBOOKINGDETAIL_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "booking_id": bookingid,
      "reason":"Payment Back"
      //reason.text
    };
    print(data);
    var res = await apiPostRequests(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {


        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Home() ),(Route<dynamic> route) => false);

      } else {
        // history = [];

      }

      loading = false;
    });
  }

  Future getupdated(paymentid) async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.Servicepaymentid;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {

      "booking_id":bookingid,
      "payment_id":paymentid

    };
    print(data);
    var res = await apiPostRequests(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {


        // history = json.decode(res)['data'];
        // print(history);

      } else {
        // history = [];

      }

      loading = false;
    });
  }



  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 1),
      // action: ,
    ));
  }
  @override
  void initState() {
    getwallet();
    getCartlist();
    // TODO: implement initState
    super.initState();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }



  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    var options = {
      'key': Apis.payment_key,
      'amount': totalpricedetail+"00",
      'name': Apis.app_name,
      'description': '',
      'prefill': {'contact': '', 'email': ''},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint('Error: e');
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    getupdated(response.paymentId);
    Navigator.push(context, MaterialPageRoute(builder: (context)=>LastpageLoader()));
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, toastLength: Toast.LENGTH_SHORT);

  }

  void _handlePaymentError(PaymentFailureResponse response) {
    getbookingCancel();
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        toastLength: Toast.LENGTH_SHORT);

    // print("jhhhj"+response.code.toString());
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, toastLength: Toast.LENGTH_SHORT);
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Text("Payment options",style: TextStyle(color: AppColors.white),),
        iconTheme: IconThemeData(color: Colors.white),
        // elevation: loading == true ? 0 : 1,
      ),
      backgroundColor: AppColors.dividerColor2,
      body: new SafeArea(
        child: new Container(
          child: new Stack(
            children: <Widget>[

              new Container(
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                      child: cart(),
                    ),
                    SliverToBoxAdapter(
                      child: itemlist(),
                    ),
                    SliverToBoxAdapter(
                      child: Container(
                        height: 5,
                      ),
                    ),
                    SliverToBoxAdapter(
                      child:  new Container(
                          child : Container(
                              padding: EdgeInsets.only(right: 10),
                              color : Colors.white,
                              child :
                              ListTile(
                                onTap: (){
                                  if(int.parse(amu)>=int.parse(totalpricedetail))
                                  {
                                    selectedRadio = 1;
                                    _selectpaymentmethod(selectedRadio);
                                  }
                                  else{

                                    _showInSnackBar("Low Balance IN Wallet ,Recharge The Wallet");

                                  }




                                },
                                leading: SizedBox(
                                  child: Radio(
                                    value: 1,
                                    groupValue: selectedRadio,
                                    activeColor: AppColors.primary2,
                                    onChanged: (val) {
                                      setState(() {
                                        selectedRadio = 1;
                                        _selectpaymentmethod(selectedRadio);
                                      });
                                    },
                                  ),
                                ),
//
                                title: Text("Wallet"),
                                trailing: Text("\u20B9 "
                                    +amu==null?"0":"\u20B9 "+amu
                                ),
                              )
                          )
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: new Container(

                        padding: EdgeInsets.only(top: 10),
                        child: Container(
                          color: Colors.white,
                          child: new Column(
                            children: <Widget>[



                              new Container(
                                padding: EdgeInsets.only(top: 10, bottom: 5),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      //"Online/Cash",
                                      "Cash",
                                      style: TextStyle(
                                          fontFamily: "opensan",
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                              ),
                              new Container(
                                padding: EdgeInsets.only(left: 20, right: 20),
                                child: Divider(),
                              ),
                              new ListTile(

                                onTap: () {
                                  selectedRadio = 2;
                                  _selectpaymentmethod(selectedRadio);


                                },
                                title: Text(
                                  "Cash On Delivery",
                                  // style: TextStyle(fontSize: 18),
                                ),
                                leading: SizedBox(
                                  child: Radio(
                                    value: 2,
                                    groupValue: selectedRadio,
                                    activeColor: AppColors.primary2,
                                    onChanged: (val) {
                                      setState(() {

                                        selectedRadio = 2;
                                        _selectpaymentmethod(selectedRadio);
                                      });
                                    },
                                  ),
                                ),
                                // trailing: IconButton(
                                //   onPressed: () {},
                                //   icon: Icon(Icons.supervised_user_circle_outlined),
                                // ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: new Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Container(
                          color: Colors.white,
                          child: new Column(
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.only(top: 10, bottom: 5),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "Other methods",
                                      style: TextStyle(
                                          fontFamily: "opensan",
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                              ),
                              new ListTile(

                                onTap: () {
                                  selectedRadio = 3;
                                  _selectpaymentmethod(selectedRadio);
                                  // openCheckout();
                                  // setListing();
                                  // _setRadio(3);
                                },
                                title: Text(
                                  "Pay Online",
                                  // style: TextStyle(fontSize: 18),
                                ),
                                leading:SizedBox(
                                  child: Radio(
                                    value: 3,
                                    groupValue: selectedRadio,
                                    activeColor: AppColors.primary2,
                                    onChanged: (val) {
                                      setState(() {
                                        selectedRadio = 3;
                                        _selectpaymentmethod(selectedRadio);
                                      });
                                    },
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: (){
                                  selectedRadio = 4;
                                  _selectpaymentmethod(selectedRadio);

                                },
                                child: new ListTile(

                                  // onTap: () {
                                  //
                                  //   // setListing();
                                  //   // _setRadio(4);
                                  // },
                                  title: Text(
                                    "Pay After Service",
                                    // style: TextStyle(fontSize: 18),
                                  ),
                                  leading:SizedBox(
                                    child: Radio(
                                      value: 4,
                                      groupValue: selectedRadio,
                                      activeColor: AppColors.primary2,
                                      onChanged: (val) {
                                        setState(() {
                                          selectedRadio = 4;
                                          _selectpaymentmethod(selectedRadio);
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              // new ListTile(

                              //   title: Text(
                              //     "Net Banking",
                              //     // style: TextStyle(fontSize: 18),
                              //   ),
                              //   leading: Radio(
                              //     value: 5,
                              //   //  groupValue: radioset,
                              //     activeColor: AppColors.primary2,
                              //     onChanged: (val) {
                              //       print(val);

                              //     },
                              //   ),
                              // ),

                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              // new Positioned(
              //   top: 0,
              //   left: 0,
              //   right : 0,
              //   bottom: 0,
              //   // child: loading == true ? new Container(
              //   //   decoration: BoxDecoration(
              //   //       color: AppColors.primary2
              //   //   ),
              //     child: new Center(
              //       child: Column(
              //         mainAxisAlignment: MainAxisAlignment.center,
              //         children: <Widget>[
              //           new SizedBox(
              //               width: 30,
              //               height: 30,
              //               child :CircularProgressIndicator(
              //                   valueColor: new AlwaysStoppedAnimation<Color>(AppColors.white)
              //               )
              //           ),
              //           Padding(
              //             padding : EdgeInsets.only(
              //                 top: 10
              //             ),
              //             child: Text(
              //               "Please wait,",
              //               style: TextStyle(
              //                   color: Colors.white
              //               ),
              //             ),
              //           ),
              //           Padding(
              //             padding : EdgeInsets.only(
              //                 top: 0
              //             ),
              //             child: Text(
              //               "msg",
              //               style: TextStyle(
              //                   color: Colors.white
              //               ),
              //             ),
              //           )
              //         ],
              //       ),
              //     ),
              //   // ) : new Container(),
              // )
            ],
          ),
        ),
      ),
      bottomNavigationBar:
      // cartlist.length.toString()=="0"?Container(
      //   height: 0,
      // ):
      Container(
        height: 75,
        child: Column(
          children: [

            Container(
              height: 65,
              padding: EdgeInsets.only(left: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(2)),
                color: AppColors.white,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 0, top: 20),
                          child: Row(
                            children: [
                              Text(
                                "₹  "+widget.totalprice+".00",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(right: 10),
                      child: SizedBox(
                        child: InkWell(
                          onTap: () {
                            if(selectedRadio==0){
                              _showInSnackBar("Select the Payment Method");

                            }
                            else{

                              getbookingplace();
                            }


                          },
                          child: Container(
                            height: 45,
                            width: 45,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              color: AppColors.sentColor,
                            ),
                            child: Center(
                              child: Text(
                                "Place Order",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),

    );
  }
  Widget cart() {
    return Container(
      color: AppColors.white,
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Container(
              padding:
              EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                        "item total",
                        style: TextStyle(color: AppColors.grayText, fontSize: 14),
                      )),
                  Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                              padding: EdgeInsets.only(right: 10),
                              child: Text(
                                "₹"+totalprice,
                                style: TextStyle(color: AppColors.grayText),
                              )),
                          // Container(
                          //   child: Text(
                          //     "₹"+oldcost,
                          //     style: TextStyle(
                          //         color: AppColors.grayText,
                          //         decoration: TextDecoration.lineThrough,
                          //         fontSize: 14),
                          //   ),
                          // )
                        ],
                      ))
                ],
              )),
          Container(
              padding:
              EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                        "add_on[+]",
                        style: TextStyle(color: AppColors.grayText, fontSize: 14),
                      )),
                  Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Text(
                        "₹"+ addonprice,
                        style: TextStyle(color: AppColors.receiveColor),
                      )),
                ],
              )),
          // Container(
          //     padding:
          //         EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //       children: [
          //         Container(
          //             child: Text(
          //           "Safety and support fee",
          //           style: TextStyle(color: AppColors.grayText, fontSize: 14),
          //         )),
          //         Container(
          //             padding: EdgeInsets.only(right: 10),
          //             child: Text(
          //               "₹45",
          //               style: TextStyle(color: AppColors.grayText),
          //             )),
          //       ],
          //     )),
          // Container(
          //     padding:
          //         EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //       children: [
          //         Container(
          //             child: Text(
          //           "UC PLUS Membership",
          //           style: TextStyle(color: AppColors.grayText, fontSize: 14),
          //         )),
          //         Container(
          //             child: Row(
          //           mainAxisAlignment: MainAxisAlignment.spaceAround,
          //           children: [
          //             Container(
          //                 padding: EdgeInsets.only(right: 10),
          //                 child: Text(
          //                   "₹255",
          //                   style: TextStyle(color: AppColors.grayText),
          //                 )),
          //             Container(
          //               child: Text(
          //                 "₹55",
          //                 style: TextStyle(
          //                     color: AppColors.grayText,
          //                     decoration: TextDecoration.lineThrough,
          //                     fontSize: 14),
          //               ),
          //             )
          //           ],
          //         ))
          //       ],
          //     )),
          Container(
              padding:
              EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                        "Couple Discount",
                        style: TextStyle(color: AppColors.grayText, fontSize: 14),
                      )),
                  Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Text(
                        "₹"+ widget.coupondiscountprices,
                        style: TextStyle(color: AppColors.receiveColor),
                      )),
                ],
              )),
          Container(
            height: 1,
            color: AppColors.grayText,
            padding: EdgeInsets.only(right: 10, left: 10),
          ),
          Container(
              padding:
              EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                        "Total",
                        style: TextStyle(
                            color: AppColors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                            padding: EdgeInsets.only(right: 10),
                            child: Text(
                              "₹"+totalpricedetail,
                              style: TextStyle(
                                  color: AppColors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                            )),
                        // Container(
                        //     padding: EdgeInsets.only(right: 10),
                        //     child: Text(
                        //       "You're saving ₹"+saveprice,
                        //       style: TextStyle(
                        //           color: AppColors.receiveColor, fontSize: 12),
                        //     ))
                      ],
                    ),
                  ),
                ],
              )),
          Container(
            height: 20,
            color: AppColors.dividerColor,
          ),
          Container(
            padding: EdgeInsets.only(top: 0, left: 0, bottom: 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(2)),
              color: Colors.white,
            ),
            //color: Colors.greenAccent,
          ),
        ],
      ),
    );
  }

  Widget itemlist() {
    return Container(
      child: Column(
          children: [
            Container(
              color: AppColors.white,
              padding: EdgeInsets.only(top:10,left:10,bottom: 10),
              child:  ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount:
                //cartlist.length,
                cartlist.length != null ?cartlist.length:"",
                //services.length,
                itemBuilder: (BuildContext context, int i) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Container(
                          padding:EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,

                            children: [
                              Container(

                                  child:
                                  Text(
                                    cartlist[i]['tittle'] != null
                                        ? cartlist[i]['tittle']
                                        : "",
                                    //  cartlist[i]['tittle'],
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400
                                    ),
                                  )),
                              Container(
                                // height: 30,
                                width: MediaQuery.of(context).size.width/5,
                                decoration: BoxDecoration(
                                  color: AppColors.white,
                                ),
                                child: Center(
                                  child: Text(
                                    // snapshot.data[0].toString() == "Instance of 'Cart'"
                                    //    ? snapshot.data[0].qty.toString()
                                    // :
                                    "Quantity -"+ cartlist[i]['qty'],
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                              ),
                            ],
                          ),

                        ),
                      ),

                      Container(
                          padding: EdgeInsets.only(right: 10,top: 20),

                          child:Row(
                            children: [


                              Container(
                                child: Row(
                                  children: [

                                    Container(
                                      padding: EdgeInsets.only(right: 5),
                                      child:  Text(
                                        cartlist[i]['old_price'] != null
                                            ? "₹" +  cartlist[i]['old_price']
                                            : "",
                                        //cartlist[i]['old_price'], //+ services[i]['data']['service'][index]['old_price'],
                                        style: TextStyle(
                                          color:
                                          AppColors.black,
                                          fontSize: 12,
                                          decoration:
                                          TextDecoration
                                              .lineThrough,
                                        ),
                                        textAlign:
                                        TextAlign.start,
                                      ),
                                    ),
                                    Container(
                                      padding:EdgeInsets.only(right: 10),
                                      child:  Text(

                                        cartlist[i]['service_price'] != null
                                            ? "₹" + cartlist[i]['service_price']
                                            : "",
                                        //cartlist[i]['service_price'], //+ //services[i]['data']['service'][index]['new_price'],
                                        style: TextStyle(
                                            color: AppColors
                                                .receiveColor2,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontSize: 14),
                                        textAlign:
                                        TextAlign.start,
                                      ),
                                    ),



                                  ],
                                ),
                              )




                            ],)

                      )


                    ],
                  );

                },
              ),),

            // Container(
            //   child:Text("jgjkgkj,gjhgjgjhvbhv hmhvh hkvvvvvvvvvvvhhjjvContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of")
            // )






          ]
      ),
    );
  }
}

