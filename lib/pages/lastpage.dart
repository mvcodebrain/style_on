import 'package:flutter/material.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/Home.dart';
import 'package:yesmadam/seatbooking/Home.dart';
import 'package:yesmadam/seatbooking/enteringpage.dart';


class LastpageLoader extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LastpageView();
  }

}

class LastpageView extends State<LastpageLoader>{

  redirectAfter() async{

    new Future.delayed(Duration(seconds: 5),(){


    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Home1() ),(Route<dynamic> route) => false);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    redirectAfter();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: (){
        Navigator.of(context).pushAndRemoveUntil(
            SlideTopRoute(page: Home1()), (Route<dynamic> route) => false);
      },
      child: Scaffold(
        body:
        Container(
          // color: AppColors.white,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Image(image: AssetImage("assets/images/abc.gif"),),
                  ),
                  Container(
                    child: Text(
                        "Order Successful",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                        ),
                    ),
                  ),
                 FlatButton(
                   color: AppColors.sentColor,
                   onPressed: (){


                     Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Home1() ),(Route<dynamic> route) => false);
                   },
                     padding: EdgeInsets.only(
                         top: 10,
                         bottom: 10,
                     left: 10,
                     right: 10),
                     child: Text("Go To Home",style: TextStyle(
                         color: Colors.white
                     ),)

                  ),
                ],
              ),
            )
        ),

      ),
    );
  }

}