import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/productcoupon.dart';
import 'package:yesmadam/pages/productpayment.dart';
import 'package:yesmadam/pages/timedate.dart';
import 'package:yesmadam/servicepage/coupon.dart';
import 'package:yesmadam/services/apis.dart';

class ProductCartitems extends StatefulWidget {
  // final catid;
  // Cartitems(this.catid);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProductCartitemsview();
  }
}

class ProductCartitemsview extends State<ProductCartitems> {
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  List cartlist =[];
  var totalprice="0";
  var itemcart= "";
  var oldcost="0";
  var saveprice="";
  var addonprice="0";
  var totalpricedetail="0";

  var payprice="";
  var payaddonprice="";
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

var finalprice="";
  Future getProductCartlist() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.PRODUCTCARTLIST_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "user_id":  sp.getString("userid")  };
    print(data);
    var res = await apiPostRequests(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        cartlist = json.decode(res)['data']['records'];

        itemcart =json.decode(res)['data']['records'][0]['cart_quantity'];
        print(itemcart);
         oldcost =json.decode(res)['data']['prices'][0]['mrp_total'];
         totalprice=json.decode(res)['data']['prices'][0]['sell_price_total'].toString();
        sp.setString("finalprice",totalprice );
        // addonprice=json.decode(res)['response']['addOnTotal'].toString();
        //
        // saveprice=(int.parse(
        //     json.decode(res)['response']['cartList'][0]['old_price']) -
        //     int.parse( json.decode(res)['response']['cartList'][0]['service_price']))
        //     .toString();
        //
        // totalpricedetail= (int.parse(
        //     json.decode(res)['response']['total_amount'].toString()) +
        //     int.parse( json.decode(res)['response']['addOnTotal'].toString()))
        //     .toString();
        //
        // sp.setString("payprice", (int.parse(
        //     json.decode(res)['response']['total_amount'].toString()) +
        //     int.parse( json.decode(res)['response']['addOnTotal'].toString()))
        //     .toString());
        // sp.setString("payaddonprice", json.decode(res)['response']['addOnTotal'].toString());
        //
        //
        // print(sp.getString("payaddonprice"));

      }
      else{
        cartlist = [];
        // totalprice="0";
        // itemcost= "";
         oldcost="0";
        // saveprice="";
        // addonprice="0";
        // totalpricedetail="0";
        //
        // payprice="";
        // payaddonprice="";


      }

      loading = false;
    });
  }
  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future getremoverproduct(serviceid,price) async {

    var sp = await SharedPreferences.getInstance();

    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.REMOVEPRODUCTADDTOCART_API;
      var data = {
        "user_id": sp.getString("userid"),
        "product_id":serviceid
       // "cat_id":widget.catid
      };

      var res = await apiPostRequestsss(url,data);
      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {
          loading = false;
        });
      }
      else
      {
        setState(() {
          loading = false;
        });
      }
    } else {

    }

    getProductCartlist();
  }
  Future<String> apiPostRequestsss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getPRODUCTUpdateCartlist(qty,String prodid,int price) async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.UPDATEDCARTPRODUCT_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "user_id":  sp.getString("userid"),
      "product_id":prodid,
      "quantity":qty,
      "unit_price":price};
    print(data);
    var res = await apiPostRequestss(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        print(cartlist);
      }
      else{

      }
      getProductCartlist();
      loading = false;
    });
  }
  Future<String> apiPostRequestss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  updateCart(int qty, String prodid,int price,type){

    if(qty  > 0){

      // print(qty+1);
      var qtys;
      var prices;
      if(type == "add"){
        qtys = qty+1;
        // prices = price*qtys;
      }else{
        qtys = qty-1;
        // prices = price*qtys;
        print(qtys);
      }
      if(qty  == 1 && type == "sub"){
        getremoverproduct(prodid,price);

      }else{
        getPRODUCTUpdateCartlist(qtys,prodid,price);


      }

    }
  }



  @override
  void initState() {
    getProductCartlist();
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Container(child: Text("My Cart")),
          elevation: 1,
        ),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: cart(),
            ),
            // SliverToBoxAdapter(
            //     child:itemlist()
            // ),
            SliverToBoxAdapter(
                child:
                Container(child:cartlist.length==0?
                Container(
                  child:
                    Container(
                      color: AppColors.white,


                      child: Image(

                        image: AssetImage("assets/images/datanotfound.jpg"),
                        height: 600,
                      ),
                    )

                ):
                productdetail())
            )
          ],
        ),
        bottomNavigationBar:
        totalprice == "0" ?Container(
          height: 1,
        ):
        Container(
          height: 60,
          child: Column(
            children: [

              Container(
                height: 55,
                padding: EdgeInsets.only(left: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                  color: AppColors.white,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 0, top: 20),
                            child: Row(
                              children: [
                                Text(
                                  "₹  "+totalprice,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                    Expanded(
                      child:
                      Container(
                        padding: EdgeInsets.only(right: 10),
                        child: SizedBox(
                          child: InkWell(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductPaymentPage(totalprice)));
                            },
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                color: AppColors.sentColor,
                              ),
                              child: Center(
                                child: Text(
                                  "Continue",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
    );
  }

  Widget cart() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Container(
              padding:
              EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                        "item total",
                        style: TextStyle(color: AppColors.grayText, fontSize: 14),
                      )),
                  Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                              padding: EdgeInsets.only(right: 10),
                              child: Text(
                                "₹"+oldcost,
                                style: TextStyle(color: AppColors.grayText),
                              )),
                          // Container(
                          //   child: Text(
                          //     "₹"+oldcost,
                          //     style: TextStyle(
                          //         color: AppColors.grayText,
                          //         decoration: TextDecoration.lineThrough,
                          //         fontSize: 14),
                          //   ),
                          // )
                        ],
                      ))
                ],
              )),
          Container(
              padding:
              EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                        "Sub Total",
                        style: TextStyle(color: AppColors.grayText, fontSize: 14),
                      )),
                  Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Text(
                        "₹"+ totalprice,
                        style: TextStyle(color: AppColors.receiveColor),
                      )),
                ],
              )),
          // Container(
          //     padding:
          //         EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //       children: [
          //         Container(
          //             child: Text(
          //           "Safety and support fee",
          //           style: TextStyle(color: AppColors.grayText, fontSize: 14),
          //         )),
          //         Container(
          //             padding: EdgeInsets.only(right: 10),
          //             child: Text(
          //               "₹45",
          //               style: TextStyle(color: AppColors.grayText),
          //             )),
          //       ],
          //     )),
          // Container(
          //     padding:
          //         EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //       children: [
          //         Container(
          //             child: Text(
          //           "UC PLUS Membership",
          //           style: TextStyle(color: AppColors.grayText, fontSize: 14),
          //         )),
          //         Container(
          //             child: Row(
          //           mainAxisAlignment: MainAxisAlignment.spaceAround,
          //           children: [
          //             Container(
          //                 padding: EdgeInsets.only(right: 10),
          //                 child: Text(
          //                   "₹255",
          //                   style: TextStyle(color: AppColors.grayText),
          //                 )),
          //             Container(
          //               child: Text(
          //                 "₹55",
          //                 style: TextStyle(
          //                     color: AppColors.grayText,
          //                     decoration: TextDecoration.lineThrough,
          //                     fontSize: 14),
          //               ),
          //             )
          //           ],
          //         ))
          //       ],
          //     )),
          // Container(
          //     padding:
          //         EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //       children: [
          //         Container(
          //             child: Text(
          //           "Membership Discount",
          //           style: TextStyle(color: AppColors.grayText, fontSize: 14),
          //         )),
          //         Container(
          //             padding: EdgeInsets.only(right: 10),
          //             child: Text(
          //               "-₹45",
          //               style: TextStyle(color: AppColors.receiveColor),
          //             )),
          //       ],
          //     )),
          Container(
            height: 1,
            color: AppColors.grayText,
            padding: EdgeInsets.only(right: 10, left: 10),
          ),
          Container(
              padding:
              EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                        "Total",
                        style: TextStyle(
                            color: AppColors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                            padding: EdgeInsets.only(right: 10),
                            child: Text(
                              "₹"+totalprice,
                              style: TextStyle(
                                  color: AppColors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                            )),
                        // Container(
                        //     padding: EdgeInsets.only(right: 10),
                        //     child: Text(
                        //       "You're saving ₹"+saveprice,
                        //       style: TextStyle(
                        //           color: AppColors.receiveColor, fontSize: 12),
                        //     ))
                      ],
                    ),
                  ),
                ],
              )),
          Container(
            height: 20,
            color: AppColors.dividerColor,
          ),
          Container(
            padding: EdgeInsets.only(top: 0, left: 0, bottom: 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(2)),
              color: Colors.white,
            ),
            child: ListTile(
                contentPadding: EdgeInsets.only(left: 10, right: 10),
                onTap: () {
                   Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CuponLoaderproduct(totalprice)));
                },
                title: Text(
                  "Coupon",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                ),
                trailing: Icon(Icons.chevron_right),
                leading: Icon(Feather.tag)),

            //color: Colors.greenAccent,
          ),
        ],
      ),
    );
  }
  Widget productdetail(){
    return Container(

      padding: EdgeInsets.only(top: 5,bottom: 5,right: 5,left: 5),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: cartlist.length,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              height: 150,

              padding: EdgeInsets.only(
                  top: 5, bottom: 5, left: 5, right: 5),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 20),
                      child: ListTile(
                        leading:
                        Container(

                          // padding: EdgeInsets.only(top: 20),
                          width: MediaQuery.of(context).size.width/3.9,
                          //height: 100,

                          decoration: BoxDecoration(
                            //color: AppColors.lightgreen,
                              borderRadius: BorderRadius.circular(10.0),
                              // border: Border.all(
                              //   color: AppColors.grayBorder,
                              //
                              //
                              // ),
                              image: DecorationImage(
                                  image: NetworkImage(cartlist[i]['image_url']
                                      ),
                                  fit: BoxFit.contain)),
                        ),
                        title: Container(
                          child: Text(cartlist[i]['name']),
                        ),


                        trailing: Container(
                          child: Text("₹"+cartlist[i]['sell_price']),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                            padding: EdgeInsets.only(right: 10,top: 15,left: 10),

                            child:Row(
                              children: [
                                Container(
                                  child: Material(
                                    child: InkWell(
                                      onTap: () {
                                        // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                        updateCart(int.parse(cartlist[i]['cart_quantity']), cartlist[i]['product_id'],int.parse(cartlist[i]['sell_price']),"sub");
                                      },
                                      child: Container(
                                        height: 25,
                                        width: MediaQuery.of(context).size.width/9,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: const Radius.circular(5),
                                              bottomLeft: const Radius.circular(5)),
                                          color: AppColors.selectedItemColor,
                                        ),
                                        child: Center(
                                          child: Text(
                                            "-",
                                            style: TextStyle(
                                                color: Colors.white, fontSize: 20),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                ),
                                Container(
                                  height: 25,
                                  width: MediaQuery.of(context).size.width/8,
                                  decoration: BoxDecoration(
                                    color: AppColors.white,
                                  ),
                                  child: Center(
                                    child: Text(
                                      // snapshot.data[0].toString() == "Instance of 'Cart'"
                                      //    ? snapshot.data[0].qty.toString()
                                      // :
                                      cartlist[i]['cart_quantity'],
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Material(
                                    child: InkWell(
                                      onTap: () {
                                        // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                         updateCart(int.parse(cartlist[i]['cart_quantity']), cartlist[i]['product_id'],int.parse(cartlist[i]['sell_price']),"add");
                                      },
                                      child: Container(
                                        height: 25,
                                        width: MediaQuery.of(context).size.width/9,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topRight: const Radius.circular(5),
                                              bottomRight: const Radius.circular(5)),
                                          color:AppColors.selectedItemColor
                                        ),
                                        child: Center(
                                          child: Text(
                                            "+",
                                            style: TextStyle(
                                                color: Colors.white, fontSize: 20),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                )




                              ],)

                        ),
                      ],
                    ),


                  ],
                ),
              ),

            );
          }),
    );

  }

  Widget itemlist() {
    return Container(
      child: Column(
          children: [
            Container(
              color: AppColors.white,
              padding: EdgeInsets.only(top:10,left:10,bottom: 10),
              child:  ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: cartlist.length,//services.length,
                itemBuilder: (BuildContext context, int i) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Container(
                          padding:EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,

                            children: [
                              Container(

                                  child:
                                  Text(
                                    cartlist[i]['tittle'],
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400
                                    ),
                                  )),
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                      padding:EdgeInsets.only(right: 10),
                                      child:  Text(
                                        "₹"+cartlist[i]['service_price'], //+ //services[i]['data']['service'][index]['new_price'],
                                        style: TextStyle(
                                            color: AppColors
                                                .receiveColor2,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontSize: 14),
                                        textAlign:
                                        TextAlign.start,
                                      ),
                                    ),
                                    Container(
                                      child:  Text(
                                        "₹"+cartlist[i]['old_price'], //+ services[i]['data']['service'][index]['old_price'],
                                        style: TextStyle(
                                          color:
                                          AppColors.black,
                                          fontSize: 12,
                                          decoration:
                                          TextDecoration
                                              .lineThrough,
                                        ),
                                        textAlign:
                                        TextAlign.start,
                                      ),
                                    )
                                  ],
                                ),
                              )

                            ],
                          ),

                        ),
                      ),

                      Container(
                          padding: EdgeInsets.only(right: 10,top: 20),

                          child:Row(
                            children: [
                              Container(
                                child: Material(
                                  child: InkWell(
                                    onTap: () {
                                      // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                      updateCart(int.parse(cartlist[i]['qty']), cartlist[i]['service_id'],int.parse(cartlist[i]['service_price']),"sub");
                                    },
                                    child: Container(
                                      height: 30,
                                      width: MediaQuery.of(context).size.width/8,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: const Radius.circular(5),
                                            bottomLeft: const Radius.circular(5)),
                                        color: Color(0xFF008e76),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "-",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),

                              ),
                              Container(
                                height: 30,
                                width: MediaQuery.of(context).size.width/8,
                                decoration: BoxDecoration(
                                  color: AppColors.white,
                                ),
                                child: Center(
                                  child: Text(
                                    // snapshot.data[0].toString() == "Instance of 'Cart'"
                                    //    ? snapshot.data[0].qty.toString()
                                    // :
                                    cartlist[i]['qty'],
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                              ),
                              Container(
                                child: Material(
                                  child: InkWell(
                                    onTap: () {
                                      // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                      updateCart(int.parse(cartlist[i]['qty']), cartlist[i]['service_id'],int.parse(cartlist[i]['service_price']),"add");
                                    },
                                    child: Container(
                                      height: 30,
                                      width: MediaQuery.of(context).size.width/8,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topRight: const Radius.circular(5),
                                            bottomRight: const Radius.circular(5)),
                                        color: Color(0xFF008e76),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "+",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),

                              )




                            ],)

                      )


                    ],
                  );

                },
              ),),

            // Container(
            //   child:Text("jgjkgkj,gjhgjgjhvbhv hmhvh hkvvvvvvvvvvvhhjjvContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of")
            // )






          ]
      ),
    );
  }
}
