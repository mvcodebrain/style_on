import 'dart:convert';
import 'dart:io';

import 'package:badges/badges.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/productcart.dart';
import 'package:yesmadam/pages/productdetali.dart';
import 'package:yesmadam/services/apis.dart';

import 'login.dart';

class ProductLoader extends StatefulWidget {
   final selected;
  // final image;
   ProductLoader({this.selected});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProductView();
  }
}

class ProductView extends State<ProductLoader> {
  var sellPrice ="";
  var productid="";
  var cartstatus="";


  
  // _filterBottomSheetMenu() {
  //   showModalBottomSheet(
  //       enableDrag: true,
  //       isScrollControlled: true,
  //       context: context,
  //       builder: (builder) {
  //         return new Container(
  //           padding: EdgeInsets.only(left: 0),
  //           height: 140.0,
  //           color: AppColors.whiteTransparent, //could change this to Color(0xFF737373),
  //           //so you don't have to change MaterialApp canvasColor
  //           child: SingleChildScrollView(
  //             child: Column(
  //               children: [
  //                 InkWell(
  //                   onTap: () {
  //                     Navigator.pop(context);
  //                   },
  //                   child: Container(
  //                     padding: EdgeInsets.only(top: 15, left: 10, bottom: 15),
  //                     //height : 100,
  //                     child: Text(
  //                       "Brand",
  //                       style: TextStyle(fontSize: 15, color: AppColors.black),
  //                     ),
  //                     alignment: Alignment.topLeft,
  //                   ),
  //                 ),

  //                 InkWell(
  //                   onTap: () {
  //                     Navigator.pop(context);
  //                   },
  //                   child: Container(
  //                     padding: EdgeInsets.only(top: 15, left: 10, bottom: 15),
  //                     //height : 100,
  //                     child: Text(
  //                       "Categories",
  //                       style: TextStyle(fontSize: 15, color: AppColors.black),
  //                     ),
  //                     alignment: Alignment.topLeft,
  //                   ),
  //                 ),

  //                 InkWell(
  //                   onTap: () {},
  //                   child: Container(
  //                     padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
  //                     // height : 30,
  //                     child: Text(
  //                       "Price",
  //                       style: TextStyle(fontSize: 15),
  //                     ),
  //                     alignment: Alignment.centerLeft,
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }

  // _SortBottomSheetMenu() {
  //   showModalBottomSheet(
  //       enableDrag: true,
  //       isScrollControlled: true,
  //       context: context,
  //       builder: (builder) {
  //         return new Container(
  //           padding: EdgeInsets.only(left: 0),
  //           //height: 280.0,
  //           color: AppColors.whiteTransparent, //could change this to Color(0xFF737373),
  //           //so you don't have to change MaterialApp canvasColor
  //           child: SingleChildScrollView(
  //             child: Column(
  //               children: [
  //                 Container(
  //                   padding: EdgeInsets.only(top: 15, bottom: 15, left: 10),
  //                   child: Text(
  //                     "SORT BY",
  //                     style: TextStyle(fontSize: 14, color: AppColors.dividerColor),
  //                   ),
  //                   alignment: Alignment.centerLeft,
  //                 ),
  //                 Container(
  //                   padding: EdgeInsets.only(top: 1, left: 0, right: 0),
  //                   color: AppColors.black12,
  //                   height: 1,
  //                 ),
  //                 InkWell(
  //                   onTap: () {
  //                     Navigator.pop(context);
  //                   },
  //                   child: Container(
  //                     padding: EdgeInsets.only(top: 15, left: 10, bottom: 15),
  //                     //height : 100,
  //                     child: Text(
  //                       "What's new",
  //                       style: TextStyle(fontSize: 15, color: AppColors.black),
  //                     ),
  //                     alignment: Alignment.topLeft,
  //                   ),
  //                 ),
  //                 InkWell(
  //                   onTap: () {},
  //                   child: Container(
  //                     padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
  //                     // height : 30,
  //                     child: Text(
  //                       "Price - high to low",
  //                       style: TextStyle(fontSize: 15),
  //                     ),
  //                     alignment: Alignment.centerLeft,
  //                   ),
  //                 ),
  //                 InkWell(
  //                   onTap: () {},
  //                   child: Container(
  //                     padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
  //                     // height : 30,
  //                     child: Text(
  //                       "Popularity",
  //                       style: TextStyle(fontSize: 15),
  //                     ),
  //                     alignment: Alignment.centerLeft,
  //                   ),
  //                 ),
  //                 InkWell(
  //                   onTap: () {},
  //                   child: Container(
  //                     padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
  //                     // height : 30,
  //                     child: Text(
  //                       "Discount",
  //                       style: TextStyle(fontSize: 15),
  //                     ),
  //                     alignment: Alignment.centerLeft,
  //                   ),
  //                 ),
  //                 InkWell(
  //                   onTap: () {},
  //                   child: Container(
  //                     padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
  //                     // height : 30,
  //                     child: Text(
  //                       "Price - low to high",
  //                       style: TextStyle(fontSize: 15),
  //                     ),
  //                     alignment: Alignment.centerLeft,
  //                   ),
  //                 ),
  //                 InkWell(
  //                   onTap: () {},
  //                   child: Container(
  //                     padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
  //                     // height : 30,
  //                     child: Text(
  //                       "Customer Rating",
  //                       style: TextStyle(fontSize: 15),
  //                     ),
  //                     alignment: Alignment.centerLeft,
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }
 
 
 
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  List product =[];
  List cartlist =[];
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
   Future getProduct() async {
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.PRODUCT_API;
      var data = {
        "user_id":user,
        'cat_id': widget.selected,
      };
      print(data);

      var res = await apiPostRequests(url, data);

      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          product = json.decode(res)['data'];
        });
        print(product);
      }
      else {
        setState(() {
          product = [];
        });
      }


    }
    else{
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
        // Navigator.pop(context);
      });

      var url = Apis.PRODUCT_API;
      // var data = json.encode(map);
      var data = {
        'cat_id': widget.selected,
      };
      print(data);

      var res = await apiPostRequests(url, data);

      print(res);
      setState(() {
        if (json.decode(res)['status'].toString() == "1")
        {
          product = json.decode(res)['data'];
          print(product);
        }
        else{
          product = [];

        }
        loading = false;
      });

    }

  }
  Future<String> apiPostRequests(String url, data) async {
     //_onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   // Navigator.pop(context);
    return reply;
  }

  Future getProductAddtocart(productid,sellPrice) async {
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.PRODUCTADDTOCART_API;

      var data = {
        "user_id": sp.getString("userid"),
        "product_id":productid,
        "quantity":"1",
        "unit_price":sellPrice
      };
      print(data);

      var res = await apiPostRequestss(url, data);
      // print(res);


      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
    getProduct();
    getProductCartlist();
  }

  Future<String> apiPostRequestss(String url, data) async {
     _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getProductRemoveAddtocart(productid) async {
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.REMOVEPRODUCTADDTOCART_API;

      var data = {
        "user_id": sp.getString("userid"),
        "product_id":productid



      };
      print(data);

      var res = await apiPostRequestsse(url, data);
      // print(res);


      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
    getProduct();
    getProductCartlist();
  }

  Future<String> apiPostRequestsse(String url, data) async {
     _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future getProductCartlist() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.PRODUCTCARTLIST_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "user_id":  sp.getString("userid")  };
    print(data);
    var res = await apiPostRequestse(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        cartlist = json.decode(res)['data']['records'];

       // itemcart =json.decode(res)['data'][0]['cart_quantity'];
        // oldcost =json.decode(res)['response']['cartList'][0]['old_price'];
        // totalprice=json.decode(res)['response']['total_amount'].toString();
        // addonprice=json.decode(res)['response']['addOnTotal'].toString();
        //
        // saveprice=(int.parse(
        //     json.decode(res)['response']['cartList'][0]['old_price']) -
        //     int.parse( json.decode(res)['response']['cartList'][0]['service_price']))
        //     .toString();
        //
        // totalpricedetail= (int.parse(
        //     json.decode(res)['response']['total_amount'].toString()) +
        //     int.parse( json.decode(res)['response']['addOnTotal'].toString()))
        //     .toString();
        //
        // sp.setString("payprice", (int.parse(
        //     json.decode(res)['response']['total_amount'].toString()) +
        //     int.parse( json.decode(res)['response']['addOnTotal'].toString()))
        //     .toString());
        // sp.setString("payaddonprice", json.decode(res)['response']['addOnTotal'].toString());
        //
        //
        // print(sp.getString("payaddonprice"));

      }
      else{
        cartlist = [];
        // totalprice="0";
        // itemcost= "";
        // oldcost="";
        // saveprice="";
        // addonprice="0";
        // totalpricedetail="0";
        //
        // payprice="";
        // payaddonprice="";


      }

      loading = false;
    });
  }
  Future<String> apiPostRequestse(String url, data) async {
   // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
   // Navigator.pop(context);
    return reply;
  }
  @override
  void initState() {
    // TODO: implement initState
    getProduct();
    getProductCartlist();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              Container(
                  padding: EdgeInsets.only(top: 5, right: 5),
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Feather.arrow_left,
                      color: AppColors.black,
                    ),
                  )),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text("Product",
                          style: TextStyle(color: AppColors.black, fontSize: 16)),
                    ],
                  ),
                  Row(
                    children: [
                      Text(product.length.toString()+"items",
                          style:
                              TextStyle(color: AppColors.black75, fontSize: 10)),
                    ],
                  )
                ],
              )
            ],
          ),
          elevation: 5,
          backgroundColor: AppColors.white,
          iconTheme: IconThemeData(color: AppColors.black),
          actions: [
         Badge(
           badgeColor: AppColors.white,
           position: BadgePosition.topEnd(top: 5, end: 5),
           badgeContent: Text(cartlist.length.toString()),
            child:
            IconButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductCartitems()));
              },
              icon: Icon(
                Icons.shopping_cart,size: 40,
                color: Color(0xFF000000),
              ),
            ),
         )
          ],
        ),
        backgroundColor: AppColors.white,
        body: SingleChildScrollView(
          child: Container(
            child: Slider(),
          ),
        ),


        // bottomNavigationBar: Container(
        //   padding: EdgeInsets.only(left: 10),
        //   height: 50,
        //   child: Column(
        //     children: [
        //       Container(
        //         height: 50,
        //         // alignment: Alignment.center,
        //        // padding: EdgeInsets.only(left: 30, top: 15, bottom: 15),
        //         decoration: BoxDecoration(
        //           borderRadius: BorderRadius.all(Radius.circular(2)),
        //           color: AppColors.white,
        //         ),
        //         child: Row(
        //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //           children: [
        //             // InkWell(
        //             //   onTap: () async {
        //             //     _MenBottomSheetMenu();
        //             //   },
        //             //   child: Container(
        //             //     padding: EdgeInsets.only(left: 10, top: 0, right: 20),
        //             //     decoration: BoxDecoration(
        //             //         border: Border(
        //             //             right: BorderSide(
        //             //       color: Colors.black26,
        //             //     ))
        //             //         // borderRadius:
        //             //         // BorderRadius.all(Radius.circular(4)),
        //             //         ),
        //             //     child: Text(
        //             //       "MEN",
        //             //       style: TextStyle(
        //             //           fontWeight: FontWeight.w600,
        //             //           color: Colors.black87),
        //             //     ),
        //             //     //alignment: Alignment.center,
        //             //   ),
        //             // ),
        //             // InkWell(
        //             //   onTap: () async {
        //             //    // _SortBottomSheetMenu();
        //             //   },
        //             //   child: Container(
        //             //     padding: EdgeInsets.only(left: 30, right: 5),
        //             //     child: Row(
        //             //       children: [
        //             //         Container(
        //             //           child: Icon(
        //             //             Icons.arrow_downward,
        //             //             size: 12,
        //             //           ),
        //             //         ),
        //             //         Container(
        //             //           child: Icon(
        //             //             Feather.arrow_up,
        //             //             size: 12,
        //             //           ),
        //             //         ),
        //             //         Container(
        //             //           padding: EdgeInsets.only(right: 85, left: 0),
        //             //           decoration: BoxDecoration(
        //             //               border: Border(
        //             //                   right: BorderSide(
        //             //             color: AppColors.black75,
        //             //           ))
        //             //               // borderRadius:
        //             //               // BorderRadius.all(Radius.circular(4)),
        //             //               ),
        //             //           child: Text(
        //             //             "SORT",
        //             //             style: TextStyle(
        //             //                 fontWeight: FontWeight.w600,
        //             //                 color: AppColors.black75),
        //             //           ),
        //             //           // alignment: Alignment.center,
        //             //         ),
        //             //       ],
        //             //     ),
        //             //   ),
        //             // ),
        //             //  Container(
        //             //   padding: EdgeInsets.only(left: 0, right:50),
        //             //   child:
        //             //   InkWell(onTap: ()async {
        //             //     _filterBottomSheetMenu();
        //             //   },
        //             //     child: Row(
        //             //       children: [
        //             //         Container(
        //             //           child: Icon(
        //             //             Feather.filter,
        //             //             size: 12,
        //             //           ),
        //             //         ),
        //             //         Container(
        //             //           padding: EdgeInsets.only(right: 0),
        //             //           child: Text(
        //             //             "FILTER",
        //             //             style: TextStyle(
        //             //                 fontWeight: FontWeight.w600,
        //             //                 color: AppColors.black75),
        //             //           ),
        //             //           alignment: Alignment.center,
        //             //         ),
        //             //       ],
        //             //     ),
        //             //   )
        //             // ),
                 
                 
        //           ],
        //         ),
        //       )
        //     ],
        //   ),
        // )
        
        );
 
  }

  Widget Slider() {
    return Column(
      children: [
        Container(
          //height: 300,

            //height: MediaQuery.of(context).size.height/2.35,
            //  width: 415,
            child: GridView.builder(
          padding: EdgeInsets.only(left: 5, top: 5),
          itemCount: product.length,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 0.5,
              crossAxisSpacing: 0.9,
              mainAxisSpacing: 1.0),
          //scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              padding: EdgeInsets.only(right: 4, bottom: 4),
              child:
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductdetailLoader(productid:product[i]['product_id']))).then((value) => getProduct()).then((value) => getProductCartlist());

                },
                child: Container(
                  //height: 500,
                  //width: 165,
                  padding: EdgeInsets.only(top: 0),

                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: AppColors.white,
                      border: Border.all(color: AppColors.black12)),
                  child: Column(
                    children: [

                      Container(
                        height: 220,
                        //width: MediaQuery.of(context).size.width/2.2,
                        decoration: BoxDecoration(
                          //color: AppColors.redcolor3,
                            border: Border.all(color: AppColors.white),
                            borderRadius:
                            BorderRadius.all(Radius.circular(0)),
                            image: DecorationImage(

                                image:
                                    NetworkImage(product[i]['image'] != null ? product[i]['image']:""),
                                fit: BoxFit.contain)),
                      ),
                      Flexible(
                        flex: 4,
                        child: Container(

                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 10,top: 5),
                                child: Container(
                                  child: Text(product[i]['name'] != null ? product[i]['name']:""),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 4,left: 10,right: 3),
                                child: Container(
                                  child: Text(product[i]['description'] != null ? product[i]['description']:"",
                                    style: TextStyle(fontSize: 12,
                                        color: AppColors.black75),maxLines: 2,),
                                ),
                              ),
                              Container(


                                child: Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 10,
                                          bottom: 10,
                                          left: 3,
                                          right: 3
                                      ),

                                    ),
                                    Container(

                                      child: Container(
                                        padding: EdgeInsets.only(left: 5,right: 5,bottom: 2,top: 2),


                                        child: Text(
                                          "₹"+product[i]['sellPrice'] != null ? "₹"+product[i]['sellPrice']:"",style: TextStyle(color: AppColors.black,
                                            fontWeight: FontWeight.w600,fontSize: 12),
                                        ),
                                      ),
                                    ),


                                    Container(
                                      //padding: EdgeInsets.only(left: 2),
                                      child: Container(
                                        padding: EdgeInsets.only(left: 5,right: 5,bottom: 2,top: 2),


                                        child: Text(
                                          "₹"+product[i]['mrp'] != null ?"₹"+ product[i]['mrp']:"",style: TextStyle(color: AppColors.black75,
                                            fontSize: 12,decoration: TextDecoration.lineThrough,),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 0),
                                      child: Container(
                                        padding: EdgeInsets.only(left: 0,right: 0,bottom: 2,top: 2),


                                        child: Text(
                                            getPercent(
                                                product[i]['mrp'],
                                                product[i]['sellPrice']).toString()+
                                                "%Off"
                                          ,style: TextStyle(color: AppColors.redcolor3,
                                            fontSize: 12),
                                        ),
                                      ),
                                    ),

                                  ],


                                ),

                              ),





                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 2,

                        child: Container(
                          child: InkWell(
                              onTap: ()
                              {
                                if(product[i]['in_cart_status']=="0"){
                                  productid = product[i]['product_id'];
                                  sellPrice = product[i]['sellPrice'];
                                  getProductAddtocart(productid,sellPrice);
                                  getProduct();


                                }
                                else{
                                  productid = product[i]['product_id'];
                                  sellPrice = product[i]['sellPrice'];

                                  getProductRemoveAddtocart(productid);
                                  getProduct();
                                  // getCartlist();

                                }
                              },
                            child:product[i]['in_cart_status']=="0"? Container(
                                decoration: BoxDecoration(
                                    border: Border(
                                        top: BorderSide(
                                            color: Colors.black12))),
                                width: MediaQuery.of(context).size.width,
                                // color:Colors.redAccent,
                                child: Center(
                                    child: Text(
                                      "ADD TO CART",
                                      style: TextStyle(
                                          color: Color(0xFFFff3f6c),
                                          fontWeight: FontWeight.w600),
                                    ))
                            ):Container(

                                child: Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            top: BorderSide(
                                                color: Colors.black12))),
                                    width: MediaQuery.of(context).size.width,
                                    // color:Colors.redAccent,
                                    child: Center(
                                        child: Text(
                                          "GO TO CART",
                                          style: TextStyle(
                                              color: Color(0xFFFff3f6c),
                                              fontWeight: FontWeight.w600),
                                        )))


                            ),
                          ),
                        ),
                      ),

                    ],
                  ),
                  // child: Column(
                  //   children: [
                  //     // Container(
                  //     //   height: 250,
                  //     //   //MediaQuery.of(context).size.height/3.1,
                  //     //   //  width: 145,
                  //     //   decoration: BoxDecoration(
                  //     //       border: Border.all(color: Colors.white),
                  //     //       borderRadius: BorderRadius.all(Radius.circular(0)),
                  //     //       image: DecorationImage(
                  //     //           image: i < 1
                  //     //               ? NetworkImage(
                  //     //                   "https://d2wvwvig0d1mx7.cloudfront.net/data/org/912/media/img/cache/768x0/1435956_768x0.jpg")
                  //     //               : NetworkImage(
                  //     //                   "https://2.imimg.com/data2/PL/LQ/MY-210654/sai24-250x250-250x250.jpg"),
                  //     //           fit: BoxFit.cover)),
                  //     // ),
                  //
                  //
                  //     // Column(
                  //     //   children: [
                  //     //      Container(
                  //     //       padding: EdgeInsets.only(left: 7),
                  //     //       child: Container(
                  //     //         child: Text("Qraa Men"),
                  //     //         alignment: Alignment.topLeft,
                  //     //       ),
                  //     //     ),
                  //     //
                  //     //     Container(
                  //     //       padding: EdgeInsets.only(bottom: 15),
                  //     //       child: Row(
                  //     //         children: [
                  //     //           Padding(
                  //     //             padding: EdgeInsets.only(
                  //     //                 top: 15, bottom: 10, left: 0, right: 3),
                  //     //           ),
                  //     //           Container(
                  //     //             padding: EdgeInsets.only(left: 0),
                  //     //             child: Container(
                  //     //               padding: EdgeInsets.only(
                  //     //                   left: 5, right: 5, bottom: 2, top: 2),
                  //     //               child: Text(
                  //     //                 "₹2345",
                  //     //                 style: TextStyle(
                  //     //                     color: Colors.black,
                  //     //                     fontWeight: FontWeight.w600,
                  //     //                     fontSize: 12),
                  //     //               ),
                  //     //               alignment: Alignment.topLeft,
                  //     //             ),
                  //     //           ),
                  //     //           Container(
                  //     //             padding: EdgeInsets.only(left: 0),
                  //     //             child: Container(
                  //     //               padding: EdgeInsets.only(
                  //     //                   left: 5, right: 5, bottom: 2, top: 2),
                  //     //               child: Text(
                  //     //                 "₹3345",
                  //     //                 style: TextStyle(
                  //     //                     color: Colors.black38, fontSize: 12),
                  //     //               ),
                  //     //             ),
                  //     //           ),
                  //     //           Container(
                  //     //             padding: EdgeInsets.only(left: 2),
                  //     //             child: Container(
                  //     //               padding: EdgeInsets.only(
                  //     //                   left: 2, right: 3, bottom: 2, top: 2),
                  //     //               child: Text(
                  //     //                 "20%OFF",
                  //     //                 style: TextStyle(
                  //     //                     color: Color(0xFFFff3f6c),
                  //     //                     fontSize: 12),
                  //     //               ),
                  //     //             ),
                  //     //           )
                  //     //         ],
                  //     //       ),
                  //     //     ),
                  //     //     Container(
                  //     //       padding: EdgeInsets.only(top: 1, bottom: 0),
                  //     //       decoration: BoxDecoration(
                  //     //           color: Colors.black12,
                  //     //           borderRadius:
                  //     //           BorderRadius.all(Radius.circular(10))),
                  //     //     ),
                  //     //
                  //     //   ],
                  //     //
                  //     // ),
                  //     // Expanded(
                  //     //   child: InkWell(
                  //     //       onTap: (){},
                  //     //       child:Container(
                  //     //           width : MediaQuery.of(context).size.width,
                  //     //           // color:Colors.redAccent,
                  //     //           child:Center(
                  //     //               child:Text("ADD TO CART",style: TextStyle(
                  //     //                   color: Color(0xFFFff3f6c),fontWeight: FontWeight.w600
                  //     //               ),)
                  //     //           )
                  //     //       )
                  //     //   ),
                  //     // ),
                  //
                  //   ],
                  // ),
                ),
              ),
            );
          },
        )),
      ],
    );
  }
  getPercent(mrp, sell_price){
    var percent = (double.parse(mrp)-double.parse(sell_price))/double.parse(mrp)*100;
    percent = double.parse(num.parse(percent.toString()).toString());
    return percent.round().toString();
  }
}
