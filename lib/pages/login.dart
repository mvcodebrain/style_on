import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/pages/signupname.dart';
import 'package:yesmadam/seatbooking/Home.dart';
import 'package:yesmadam/seatbooking/enteringpage.dart';

import 'package:yesmadam/services/apis.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

import 'package:sms_autofill/sms_autofill.dart';
import 'package:yesmadam/pages/Home.dart';
import 'otppage.dart';

class LoginLoader extends StatefulWidget {
  // final logintype;
  // LoginLoader({this.logintype});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginView();
  }
}

class LoginView extends State<LoginLoader> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  getFcmToken() async {
    var sp = await SharedPreferences.getInstance();
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      // setState(() {
      //   // _homeScreenText = "Push Messaging token: $token";
      // });
      print("Push Messaging token: $token");
      sp.setString("device_token", '$token');
      print(token);
    });
    _firebaseMessaging.configure(
        onBackgroundMessage: myBackgroundMessageHandler);
  }

  Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      print(message['data']);
      // Handle data message
      _onLoading();
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  String appSignature;

  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }

  var mobile = TextEditingController();
  var key = TextEditingController();
  List data = [];
  var userid = "";

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var user_exist="";
  Future getLogin() async {
    var sp = await SharedPreferences.getInstance();
    var devicetoken = sp.getString("device_token");
    print(devicetoken);
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });

    var url = Apis.LOGIN_API;
    var data = {
      "mobile": mobile.text,
      "message_key": appSignature,
      "device_type": "1", //1- for android, 2- for ios
      "device_id": "454543fdfd",
      "device_name": "zen phone max pro m1",
      "device_token": "$devicetoken"

      //"mobile": mobile.text, "message_key": appSignature
    };
    print(data.toString());
    var res = await apiPostRequest(url, data);

    print(res);
    setState(() {
      loading = false;
    });
    var chekExist = int.parse(json.decode(res)['status']);
    print(chekExist);
    // if (chekExist == 1) {
    //   setState(() {
    //     print(chekExist);
    //   });
    //   Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //           builder: (context) => Otp(mobile.text,
    //               json.decode(res)['user_data']['customer_id'].toString()
    //               //json.decode(res)['user_data']['customer_id']
    //               )));
    // }
    if (chekExist == 1) {
      user_exist=json.decode(res)['user_exist'].toString();


      setState(() {
        print(user_exist);
      });
      if(user_exist=="0"){
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Signup(mobile.text,
                    json.decode(res)['user_data']['customer_id'].toString()

                )));
      }
      else{
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Otp(mobile.text,
                    json.decode(res)['user_data']['customer_id'].toString()

                )));
      }

    }
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);

    return reply;
  }

  @override
  void initState() {
    super.initState();
    getFcmToken();

    SmsAutoFill().getAppSignature.then((signature) {
      //("signatures " + signature);
      print(signature);
      setState(() {
        appSignature = signature;
      });
      print(appSignature);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(   
            // height: 80,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/backgroun.png"),
                    fit: BoxFit.fill)),
          ),
          Positioned(
            top: MediaQuery.of(context).size.longestSide / 3.3,
            left: 0,
            right: 0,
            child: Container(
              // margin: EdgeInsets.only(top: MediaQuery.of(context).size.height/5),
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, right: 10, left: 10),
              child: Row(
                children: [
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 5, bottom: 5, right: 10, left: 10),
                      child: Text(
                        "Login with your mobile number",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 30.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
              top: MediaQuery.of(context).size.height /2.2,
              left: 0,
              right: 0,
              child:
              Card(
                margin: EdgeInsets.only(left: 10,right: 10),
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10),

                  //height: 50,
                  width: MediaQuery.of(context).size.width / 1.1,
                  padding: EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                      //color: AppColors.lightblue,
                    //  borderRadius: BorderRadius.all(Radius.circular(10)),
                     // border: Border.all(color: AppColors.grayBorder)
                  ),
                  child: Container(

                    margin: EdgeInsets.only(left: 10, right: 0),
                    child: TextField(
                      controller: mobile,
                      //controller: mobile,
                      autofocus: false,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        prefix: Text(
                          "+91 ",
                          style: TextStyle(color: Colors.black, fontSize: 20),
                        ),
//                                border: InputBorder.none,

                        hintText: "Mobile Number",
                        // hintStyle: TextStyle(color: Colors.grey[400]),
                        contentPadding: EdgeInsets.only(
                          left: 10,
                        ),
                        counterText: "",
                      ),
                      maxLength: 10,
                      keyboardType: TextInputType.number,
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
              )),
          Positioned(
            top: MediaQuery.of(context).size.height / 1.8,
            left: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.only(bottom: 20, left: 10, right: 10),
              child: Material(
                color: AppColors.sentColor,
                //Color.fromRGBO(0, 0, 0, 1),
                borderRadius: BorderRadius.circular(10),
                child: InkWell(
                  borderRadius: BorderRadius.circular(10),
                  splashColor: Colors.blueAccent,
                  onTap: () {
                    if (mobile.text.length == 10) {
                      if (loading == true) {
                      } else {
                        getLogin();
                      }
                    } else {
                      alertDailog("please enter number");
                    }
                    //Navigator.push(context, MaterialPageRoute(builder: (context)=>Otp()));
                  },
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                    ),
                    child: Center(
                      child: Text(
                        loading == true ? "Loading.." : "Login",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 20),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
              bottom: 10,
              right: 0,
              left: 0,
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pushAndRemoveUntil(
                            SlideTopRoute(page: Home1()),
                            (Route<dynamic> route) => false);
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width/4,
                          height:MediaQuery.of(context).size.longestSide/18,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: AppColors.white,
                              style: BorderStyle.solid,
                              width: 1.0,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(120)),
                            color: AppColors.sentColor,

                            //color: Color(0xFFFff3f6c),
                          ),
                          child:
                          Center(
                            child: Text(
                              "SKIP",
                              style:
                                  TextStyle(color: AppColors.white, fontSize: 16),
                            ),
                          )),
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
