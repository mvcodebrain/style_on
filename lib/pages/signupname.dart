



import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:io';

import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

import 'otppage.dart';






class Signup extends StatefulWidget {
  final mobilenumber;
  final customer_id;



  Signup(this.mobilenumber,this.customer_id);


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Signuploader();
  }
}

class Signuploader extends State<Signup>
{
  alertDailog(text) async{
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: (){
                  Navigator.pop(context);
                  // Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }

  var value="";






  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  var fname = TextEditingController();
  var email = TextEditingController();
  var referalcode=TextEditingController();
  var mobile = TextEditingController();
  var lastname = TextEditingController();

  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 1),
      // action: ,
    ));
  }


  getmobile() async{

    setState(() {
      mobile.text=widget.mobilenumber.toString();


      print(mobile.text);

    });
  }


  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
   bool fail;
   var message="";
  Future getsignup() async {


    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });

    var url = Apis.Signupname;
    var data = {
      "first_name":fname.text,
      "last_name":lastname.text,
      "mobile":widget.mobilenumber,
      "user_id":widget.customer_id,
      "email":email.text,
      "referral_code":referalcode.text


    };
    print(data.toString());
    var res = await apiPostRequest(url, data);

    print(res);
    setState(() {
      loading = false;
    });
    // var chekExist = json.decode(res)['status'];

    if (json.decode(res)['status'] == 1) {


      setState(() {
        print(json.decode(res)['status']);
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Otp(widget.mobilenumber,
                  widget.customer_id

              )));

    }
    else{
      message= json.decode(res)['message'];
      _showInSnackBar(message);


    }
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);

    return reply;
  }

  @override
  void initState() {
    getmobile();

    // TODO: implement initState
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: AppColors.white..withOpacity(.9),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(""),
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
      ),
      body: SingleChildScrollView(

        child: Container(

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
              children: [
//             Center(
//               child: Container(
//                 padding: EdgeInsets.only(top: 80),
//                 height: 200,
//                 child: Column(children: [
//                   Container(
//                     child: Text(
//                       "Let's Get Started!",
//                       style: TextStyle(
//                           fontSize: 25,
//                           color: AppColors.black,
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                   // width: MediaQuery.of(context).size.width,
// //color: Colors.white,
//                   //    ),
//                   Container(
//                     padding: EdgeInsets.only(top: 10),
//                     child: Text(
//                       "Create an account to VGO to get all features",
//                       style: TextStyle(fontSize: 16),
//                     ),
//                   ),
//                 ]),
//               ),
//             ),

            // Container(
            //   margin: EdgeInsets.only(top: 0),
            //   height: 170,
            //   decoration: BoxDecoration(
            //       image: DecorationImage(
            //           image:AssetImage("assets/images/VLogo.png"),
            //           fit: BoxFit.contain
            //       )
            //
            //   ),
            //
            // ),

            Container(
              margin: EdgeInsets.only(
                  top: 20,
                  bottom: 20,
                  right: 10,
                  left: 20

              ),
              child: Text(
                "Signup ",
                style: TextStyle(
                    color : Colors.black.withOpacity(0.9),
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
            Row(
              children: [
                Flexible(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(top: 20, bottom: 5),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Row(
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    margin: EdgeInsets.only(
                                      right: 2.5
                                    ),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        border: Border.all(color: Colors.black12)),
                                    child: Row(
                                      children: [
                                        // Container(
                                        //   padding: EdgeInsets.only(left: 20),
                                        //   child: Icon(
                                        //     Icons.perm_identity,
                                        //     color: AppColors.redcolor3,
                                        //   ),
                                        // ),
                                        Expanded(
                                          child: Container(
                                            width: 250,
                                            child: TextField(
                                              autofocus: false,
                                              controller: fname,

                                              decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "First Name",
                                                  hintStyle:
                                                      TextStyle(color: AppColors.black),
                                                  contentPadding: EdgeInsets.only(
                                                    left: 10,
                                                  ),
                                                  counterText: ""),
                                                textCapitalization: TextCapitalization.characters,

                                                style: TextStyle(


                                                ),

                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                              ),

                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(top: 20, bottom: 5),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Row(
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                    margin: EdgeInsets.only(
                                        right: 2.5
                                    ),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        border: Border.all(color: Colors.black12)),
                                    child: Row(
                                      children: [
                                        // Container(
                                        //   padding: EdgeInsets.only(left: 20),
                                        //   child: Icon(
                                        //     Icons.perm_identity,
                                        //     color: AppColors.redcolor3,
                                        //   ),
                                        // ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(left: 10),
                                            width: 250,
                                            child: TextField(

                                              autofocus: false,
                                              controller: lastname,
                                              decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: " Last Name",
                                                  hintStyle:
                                                  TextStyle(color: AppColors.black),
                                                  contentPadding: EdgeInsets.only(
                                                    left: 10,
                                                  ),
                                                  counterText: ""),
                                                textCapitalization: TextCapitalization.characters,

                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                              ),

                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),

            Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.black12)
                        ),
                        child: Row(
                          children: [
                            // Container(
                            //   padding: EdgeInsets.only(left: 20),
                            //   child: Icon(
                            //     Icons.email,
                            //     color: AppColors.redcolor3,
                            //   ),
                            // ),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              width: 250,
                              child: TextField(
                                autofocus: false,

                                controller: email,

                                decoration: InputDecoration(

                                    border: InputBorder.none,
                                    hintText:"Email",
                                    //"Mobile Number",
                                    //widget.mobilenumber.toString(),
                                    hintStyle:
                                    TextStyle(color: AppColors.black),
                                    contentPadding: EdgeInsets.only(
                                      left: 10,
                                    ),
                                    counterText: ""),
                                // textCapitalization: TextCapitalization.characters,


                              ),
                            ),
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                border: Border.all(color: Colors.black12)
                            ),
                            child: Row(
                              children: [
                                // Container(
                                //   padding: EdgeInsets.only(left: 20),
                                //   child: Icon(
                                //     Icons.email,
                                //     color: AppColors.redcolor3,
                                //   ),
                                // ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  width: 250,
                                  child: TextField(
                                    autofocus: false,

                                    controller: referalcode,

                                    decoration: InputDecoration(

                                        border: InputBorder.none,
                                        hintText:"Referral Code",
                                        //"Mobile Number",
                                        //widget.mobilenumber.toString(),
                                        hintStyle:
                                        TextStyle(color: AppColors.black),
                                        contentPadding: EdgeInsets.only(
                                          left: 10,
                                        ),
                                        counterText: ""),
                                    textCapitalization: TextCapitalization.characters,


                                  ),
                                ),
                              ],
                            )),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                    ],
                  ),
                ),
            Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.black12)
                         ),
                        child: Row(
                          children: [
                            // Container(
                            //   padding: EdgeInsets.only(left: 20),
                            //   child: Icon(
                            //     Icons.phone,
                            //     color: AppColors.redcolor3,
                            //   ),
                            // ),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              width: 250,
                              child: TextField(
                                autofocus: true,
                                readOnly: true,
                                controller: mobile,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    // hintText:
                                    //"Mobile Number",
                                    //widget.mobilenumber.toString(),
                                    hintStyle:
                                        TextStyle(color: AppColors.black),
                                    contentPadding: EdgeInsets.only(
                                      left: 10,
                                    ),
                                    counterText: ""),
                                keyboardType: TextInputType.number,
                                maxLength: 10,
                              ),
                            ),
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                ],
              ),
            ),



          ]),
        ),
      ),
      bottomNavigationBar:
      Container(

        padding: EdgeInsets.only(left: 20, right: 20,bottom: 20),
        child: Material(


          child: InkWell(
            onTap: () {
              if(fname.text.length==0){
                alertDailog("Please Enter First Name");

              }
              else{
                getsignup();


              }



            },
            child: Container(
              margin: EdgeInsets.only(left: 20,right: 20),
              height: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: AppColors.sentColor

              ),



              child: Center(
                child:
                Text(
                  loading == true ? "Loading.." :
                  "Signup",
                  style: TextStyle(color: Colors.white,
                      fontWeight:FontWeight.bold ,fontSize: 16),
                ),
              ),
            ),
            // Container(
            //   decoration: BoxDecoration(
            //       borderRadius: BorderRadius.all(Radius.circular(15))),
            //   height: 60,
            //   child: Stack(
            //     children: [
            //       Container(
            //         child: Center(
            //           child: Text(
            //             loading == true ? "Loading.." : "CONTINUE",
            //             style: TextStyle(
            //                 color: Colors.white,
            //                 fontWeight: FontWeight.bold,
            //                 fontSize: 18),
            //           ),
            //         ),
            //       ),
            //       Positioned(
            //         right: 20,
            //         top: 0,
            //         bottom: 0,
            //         child: Container(
            //           padding: EdgeInsets.only(
            //             top: 10,
            //             bottom: 10,
            //           ),
            //           child: Container(
            //             width: 40,
            //             height: 50,
            //             decoration: BoxDecoration(
            //                 color: AppColors.sentColor.withOpacity(0.5),
            //                 borderRadius:
            //                     BorderRadius.all(Radius.circular(120))),
            //             child: Icon(
            //               Icons.chevron_right,
            //               color: Colors.white,
            //             ),
            //           ),
            //         ),
            //       )
            //     ],
            //   ),
            // ),
          ),
        ),
      ),
    );
  }
}
class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
