import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';

import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/Home.dart';
import 'package:yesmadam/pages/login.dart';
import 'package:yesmadam/seatbooking/Home.dart';
import 'package:yesmadam/seatbooking/enteringpage.dart';
import 'package:yesmadam/servicepage/servicedetailp.dart';
import 'package:yesmadam/services/apis.dart';
class Splashscreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SplashsreenView();
  }


}
class SplashsreenView extends State<Splashscreen>{

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future chectuser() async {
    try {
      var sp = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
        // Navigator.pop(context);
      });

      var url = Apis.CHECKUSER_API;
      // var data = json.encode(map);
      var data = {
        "user_id": sp.getString("userid")
      };
      print(data);
      var res = await apiPostRequest(url,data);

      print(res);
      setState(() {
        if(json.decode(res)['status'].toString() == "1"){
          redirectAfter();


        }else{
          setState(() {
            sp.remove("userid");
          });
          redirectAfter();



        }
        loading = false;
      });




    }

    catch (e)
    {
      print("Network Fail");
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  StreamSubscription _sub;
  Uri _initialUri;
  Uri _latestUri;
  Object _err;

  // void _handleIncomingLinks() {
  //   if (!kIsWeb) {
  //     // It will handle app links while the app is already started - be it in
  //     // the foreground or in the background.
  //     print(uriLinkStream);
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => ServiceLoader("sad",76)));
  //   }
  // }

  Future<Null> initUniLinks() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      String initialLink = await getInitialLink();
      // Parse the link and warn the user, if it is not correct,
      // but keep in mind it could be `null`.

      if(initialLink != null){
        Navigator.push(context, MaterialPageRoute(builder: (context) => ServicePageviews(initialLink)));
      }else{
        chectuser();
      }
    } on PlatformException {
      chectuser();
      // Handle exception by warning the user their action did not succeed
      // return?
    }
  }

  @override
  void initState(){
    super.initState();

   // redirectAfter();
    initUniLinks();

  }

  redirectAfter() async{
    var sp = await SharedPreferences.getInstance();
    new Future.delayed(Duration(seconds: 2),()
    {
      // Navigator.of(context).pushAndRemoveUntil(
      //     SlideTopRoute(page: Homebloc()), (Route<dynamic> route) => false);

      if(sp.containsKey("userid")){
        Navigator.of(context).pushAndRemoveUntil(
            SlideTopRoute(page: Home1()), (Route<dynamic> route) => false);


      }else{
        Navigator.of(context).pushAndRemoveUntil(
            SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);

      }

//      Navigator.push(context, SlideTopRoute(page: MainpagePage(pageview: 0,)));
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.splash,
      body: Container(

        child: Center(
          // child: SizedBox(
          //   width: 30,
          //   height: 30,
          //   child: CircularProgressIndicator(),
          // ),
          child: Image(

              image : AssetImage("assets/images/launcher.png"),
              width: 160,
            height: 160,
          ),
        ),
      ),
    );
  }

}