


import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/pages/Home.dart';
import 'package:yesmadam/seatbooking/Home.dart';
import 'package:yesmadam/seatbooking/enteringpage.dart';
import 'package:yesmadam/services/apis.dart';
import 'package:flutter/material.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';


import 'package:sms_autofill/sms_autofill.dart';
import 'package:pin_code_fields/pin_code_fields.dart';


class Otp extends StatelessWidget {
  final mobile;

  final customer_id;
  Otp(this.mobile,this.customer_id
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return OtpLoader(this.mobile,this.customer_id);
    //this.customer_id);
  }
}

class OtpLoader extends StatefulWidget {
  final mobile;
  final customer_id;
  OtpLoader(this.mobile,
  this.customer_id
  );
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OtpView();
  }
}

class OtpView extends State<OtpLoader> with CodeAutoFill {
  String otpCode;
  var otp = "";
  var otps = TextEditingController();

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  Timer _timer;
  int _start = 10;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;

  Future checkOtp() async
  {
    try
    {
      var sp = await SharedPreferences.getInstance();
      var devicetoken = sp.getString("device_token");
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
      });

      var url = Apis.OTP_API;
      // var data = json.encode(map);
      var data = {

       // "customer_id" :widget.customer_id,
        "otp":otp,
        "mobile":widget.mobile,

      };
      print(data);
     // print(data.toString());
      
      //http.Response res = await http.post(url, body: data);
      var res = await apiPostRequest(url,data);
      print(res);
      setState(() {
      // sp.setString("customer_id",widget.customer_id);
        loading = false;
      });
      var chekExist = json.decode(res)['status'].toString();

      //Navigator.pop(context);

      //Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));

      print(chekExist);
      if (json.decode(res)['status'].toString() == "1") {
        sp.setString("userid",widget.customer_id);
        // Navigator.pop(context);
       // Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Home1() ),(Route<dynamic> route) => false);

      }else{
        _showInSnackBar("Wrong Otp");
      }
    }
    catch(e)
    {
      print("Network Fail");
    }
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    //request.headers.set('api-key' , Apis.SIGNUP_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 1),
      // action: ,
    ));
  }

  Future listenCode() async{
    var p = await SmsAutoFill().listenForCode;
  }

  var appSignature = "";

  @override
  void initState() {
    super.initState();
    listenCode();
    listenForCode();
    SmsAutoFill().getAppSignature.then((signature) {
      //("signatures " + signature);
      setState(() {
        appSignature = signature;
      });
      print(appSignature);
    });
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        // title: Text("Login"),
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Column(
                children: <Widget>[
                  // Container(
                  //   // padding: EdgeInsets.only(
                  //   //     top: 0
                  //   // ),
                  //   height: 200,
                  //   width: 200,
                  //   decoration: BoxDecoration(
                  //     borderRadius: BorderRadius.all(Radius.circular(10)),
                  //     image: DecorationImage(
                  //       image: AssetImage("assets/images/otp_sms.png"),
                  //       fit: BoxFit.fill,
                  //     ),
                  //   ),
                  //
                  // ),
                  Container(
                    padding: EdgeInsets.only(
                        top: 10, bottom: 0, right: 10, left: 10),
                    child: Column(
                      children: [
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 5, bottom: 5, right: 10, left: 10),
                              child: Text(
                                "Enter the 6-digit code has been sent to:",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.normal),
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 0, bottom: 0, right: 10, left: 10),
                              child: Text(
                                widget.mobile,
                                style: TextStyle(
                                    color: AppColors.black,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w600),
                              ),
                            )
                          ],
                        ),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.center,
                        //   children: [
                        //     Padding(
                        //       padding: EdgeInsets.only(
                        //           top: 15,
                        //           bottom: 5,
                        //           right: 10,
                        //           left: 10
                        //       ),
                        //
                        //       child: Text(
                        //         "ENTER OTP",
                        //         style: TextStyle(
                        //             color : Colors.black.withOpacity(0.6),
                        //             fontSize: 25.0,
                        //             fontWeight: FontWeight.bold
                        //         ),
                        //       ),
                        //     )
                        //   ],
                        // ),
                      ],
                    ),
                  ),

                  Container(
//                     padding: EdgeInsets.only(right: 190),
                    padding: EdgeInsets.only(left: 15, top: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child:
                      SizedBox(
                        width: MediaQuery.of(context).size.width ,
                        // child:  PinCodeTextField(
                        //   length: 6,
                        //   autoDismissKeyboard: false,
                        //
                        //   obscureText: false,
                        //   // animationType: AnimationType.fade,
                        //   // shape: PinCodeFieldShape.underline,
                        //   animationDuration: Duration(milliseconds: 300),
                        //
                        //   //borderRadius: BorderRadius.circular(5),
                        //
                        // //  fieldHeight: 50,
                        //   //fieldWidth: 50,
                        //   controller: otp,
                        //  // activeFillColor: Colors.grey,
                        //   //activeColor: Colors.greenAccent,
                        //  // inactiveColor: Color.fromRGBO(0, 186, 142, 1),
                        //   //selectedColor: Colors.redAccent,
                        //   keyboardType: TextInputType.number,
                        //   // onChanged: (value) {
                        //   //   if (value.length == 6) {
                        //   //     setState(() {
                        //   //       // otpset = true;
                        //   //       otpget = value;
                        //   //       //(value);
                        //   //     });
                        //   //   } else if (value.length < 6) {
                        //   //     setState(() {
                        //   //       // otpset = false;
                        //   //     });
                        //   //   }
                        //   // },
                        //   onCompleted: (value){
                        //     checkOtp();
                        //   },
                        // ),
                        child: OTPTextField(

                          onChanged:(val){
                            setState(() {
                              otp = val.toString();
                            });
                          },
                          length: 6,
                          width: MediaQuery.of(context).size.width,
                          textFieldAlignment: MainAxisAlignment.spaceAround,
                          fieldWidth: 40,
                          fieldStyle: FieldStyle.underline,
                          style: TextStyle(
                              fontSize: 17
                          ),
                          onCompleted: (value) {
                            checkOtp();
                          },
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15, bottom: 5, right: 10, left: 20),
                        child: Text(
                          "Resend code in $_start",
                          style: TextStyle(
                              color: AppColors.grayText,
                              fontSize: 15.0,
                              fontWeight: FontWeight.w500),
                          textAlign: TextAlign.start,
                        ),
                      )
                    ],
                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void codeUpdated() {
    setState(() {
      otpCode = code;
      otp = otpCode;
    });
  }
}
