import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Refer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ReferView();
  }
}
class ReferView extends State<Refer> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var vendorprofile="";
  var referralcode="0";


  Future getreferlist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.REFERCODE_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "user_id":sp.getString("userid")
    };
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        // vendorprofile = json.decode(res)['data'];
        referralcode = json.decode(res)['data'][0]['referral_code'];

      } else {

      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  @override
  void initState() {
    getreferlist();

    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Container(
          child: Text("Refer"),
        ),
      ),
      backgroundColor: AppColors.white,
      body: CustomScrollView(

        slivers: [
          SliverToBoxAdapter(
            child:
            Container(
              color: AppColors.white,
                child: refer()),
          )
        ],
      ),

    );
  }
  Widget refer(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 0),
            height: 200,
           // width: 100,
            //width: MediaQuery.of(context).size.width/2.2,
            decoration: BoxDecoration(
                color: AppColors.white,
                border: Border.all(color: Colors.white),
                borderRadius:
                BorderRadius.all(Radius.circular(1)),
                image: DecorationImage(

                    image: NetworkImage("https://d3sezmnsirbd9l.cloudfront.net/wp-content/uploads/2018/04/02145848/107544268.png")
                    ,

                    fit: BoxFit.contain)
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child:RichText(
              text: new TextSpan(
                  text: "Refer & Earn Upto ",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight:
                    FontWeight.bold,
                    fontSize: 16,
                  ),
                  children: [
                    new TextSpan(
                        text: " ₹ 10000",
                        //"$addressname",
                        style: TextStyle(
                            color: Colors.amber,
                            fontWeight:
                            FontWeight.bold,
                            fontSize: 16))
                  ]),
              textAlign: TextAlign.left,
            )
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text("Your Referral Code"),
          ),
          Container(
            color: AppColors.lightblue,
              margin: EdgeInsets.only(top: 10),

            height: 40,
             width: 120,
            //width: MediaQuery.of(context).size.width/2.2,
            // decoration: BoxDecoration(
            //     color: AppColors.grayBorder,
            //     border: Border.all(color: Colors.black),
            //     borderRadius:
            //     BorderRadius.all(Radius.circular(1)),
            //
            //
            // ),
              child:
              DottedBorder(

                //dashPattern: [6, 3, 2, 3],
                color: AppColors.redcolor3,
                strokeWidth: 2,
                child:
                Center(child: Text(referralcode==null?"":referralcode,style: TextStyle(fontSize: 18,),)),
              )

            // child: Text("999889"),
          ),
          Container(
            height: 40,
            width: 120,
            margin: EdgeInsets.only(top: 30),

            color: AppColors.sentColor,
            child: FlatButton(
              textColor: Colors.black,
              child: Text(
                  "REFER NOW",style: TextStyle(color: AppColors.white,fontSize: 16),),
              onPressed: () {
                Share.share(
                    "$referralcode");

              },
            ),
          )
        ],
      ),
    );
  }

}