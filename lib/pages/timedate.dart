import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/payment.dart';
import 'package:yesmadam/servicepage/termandconditionpage.dart';
import 'package:yesmadam/servicepage/vendorpofile.dart';

import 'package:yesmadam/services/apis.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

import 'alladdress.dart';




class timePage extends StatefulWidget {
  final totalprice;
  final catid;
  final coupondiscountprices;
  final couponid;
  timePage(this.totalprice,this.catid,this.coupondiscountprices,this.couponid);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return timePageLoader();
  }
}

class timePageLoader extends State<timePage>{
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }
  List records =[];
  List timerecord =[];
  var dates = "";
  var dateslen = 0;

  List times =[];
  var timelen = 0;

  var valuefirst = "";







  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var selectedindex = 0;
  var vendorname="";
  double vendorrating=0.0;
  var vendorimage="";
  var vendorrat="";
  var checkexit="0";
  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }
  var vendorid="";

  Future getrecentvendor() async {
    // print("checking for login");
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid"))
    {
      var user = sp.getString("userid");
      var url = Apis.RECENTVENDOR_API;
      var data = {
        "user_id":
        //"20"
        user,
      };
      var res = await apiPostRequests(url, data);
      print(res);
      checkexit=json.decode(res)['status'].toString();
      //print(checkexit);
      if (checkexit == "1") {
        setState(() {

          vendorname=json.decode(res)['data']['username'];
          vendorid =json.decode(res)['data']['id'];
          vendorrating=double.tryParse(json.decode(res)['data']['average_rating'].toString());
          vendorrat =vendorrating.toStringAsFixed(1);
          print(vendorrat);

          print("ccddd"+vendorid.toString());

          vendorimage=json.decode(res)['data']['image'];



        });
        // print();
      } else {
        setState(() {

        });
      }
    } else {
      print("Please login");
    }
  }



  Future getdate() async {

    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });
    var url = Apis.GETDATESLOT_API;
    var data = {
      "vendor_id":vendid
    };
    print(data);


    var res = await apiPostRequestss(url, data);
    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        setState(() {
          records = json.decode(res)['data'];
          date = records[0]['date'].toString();
          times = json.decode(res)['data'][selectedindex]['time_slot'];
          print(dates);
        });


      }
      else{
        setState(() {

          records =[];
          date = "";
          times =[];

        });
        _showInSnackBar("No schedule Available");


      }
      loading = false;
    });


    // Navigator.pop(context);


  }
  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future<String> apiPostRequestss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }



  // _selectTimeSlot(i,dates) async{
  //   setState(() {
  //     selectedindex = i;
  //     times = timerecord[i]['start_time'];
  //     valuefirst = "";
  //     date = dates;
  //
  //   });
  // }
  //
  // slectTime(startTimeset){
  //   setState(() {
  //     starttime = startTimeset;
  //   });
  // }
  _selectTimeSlot(index,dates) async{
    setState(() {
      selectedindex = index;
      times = records[index]['time_slot'];
      valuefirst = "";
      date = dates;
      print(date);

    });
  }

  slectTime(startTimeset, endtimeset,id){
    setState(() {
      starttime = startTimeset;
      endTime = endtimeset;
      timeid = id;

      print(timeid);
      print(endTime);
    });
  }


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 1),
      // action: ,
    ));
  }


  // Future<String> apiPostRequest(String url) async {
  //  // _onLoading();
  //
  //   HttpClient httpClient = new HttpClient();
  //   HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
  //   request.headers.set('content-type', 'application/json');
  //   // request.headers.set('api-key' , Apis.LOGIN_API);
  //   //request.add(utf8.encode(json.encode(data)));
  //   _onLoading();
  //   HttpClientResponse response = await request.close();
  //   // todo - you should check the response.statusCode
  //   String reply = await response.transform(utf8.decoder).join();
  //   httpClient.close();
  // //  Navigator.pop(context);
  //
  //   return reply;
  // }

  // Future gettime(dateget,i) async {
  //
  //   setState(() {
  //     selectedindex = i;
  //     loading = true;
  //     ani = false;
  //     fail = false;
  //     _color = Colors.black.withOpacity(0.3);
  //   });
  //
  //   var url = Apis.GETTIMESLOT_API;
  //   // var data = json.encode(map);
  //
  //   // const headers = {'Content-Type': 'application/json'};
  //   // http.Response res = await http.post(url,body: data);;
  //   var data = {
  //     "date":dateget
  //   };
  //
  //   var res = await apiPostRequests(url,data);
  //
  //
  //
  //   setState(() {
  //     if(json.decode(res)['status'].toString() == "1"){
  //       timerecord = json.decode(res)['time_slot']['time'];
  //
  //
  //     }else{
  //
  //     }
  //     loading = false;
  //   });
  //
  //
  //
  //
  //
  // }
  // Future<String> apiPostRequests(String url, data) async {
  //    //_onLoading();
  //   HttpClient httpClient = new HttpClient();
  //   HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  //   request.headers.set('content-type', 'application/json');
  //   // request.headers.set('api-key' , Apis.LOGIN_API);
  //   request.add(utf8.encode(json.encode(data)));
  //   HttpClientResponse response = await request.close();
  //   // todo - you should check the response.statusCode
  //   String reply = await response.transform(utf8.decoder).join();
  //   httpClient.close();
  //   //Navigator.pop(context);
  //   return reply;
  // }
  var date = "";
  var starttime = "";
  var endTime = "";
  var timeid ="";

  var addressname = "";
  var addressid = "";
  var term="";
  var termdetail="";
  getAddress() async{
    var sp = await SharedPreferences.getInstance();
    if(sp.containsKey("addressid")){
      setState((){
        addressname = sp.getString("addressname");
        addressid = sp.getString("addressid");
        term=sp.getString("term");
        termdetail=sp.getString("termdetalis");
        // print("hhhhbhg"+term);
      });

    }else{

    }

  }
  var vendid="";
  int _currentPage = -1;
  _setIndex(int index,vendoid)async{
    var sp = await SharedPreferences.getInstance();
    setState((){
      _currentPage = index;
      vendid=vendoid;
      print(vendid);
      // sp.setString("vendid",vendid);

    });
    getdate();
  }



  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      this.getdate();
    });
    getrecentvendor();

    // getrecentvendor();
    getAddress();


    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: AppColors.sentColor,
          elevation: 0,
          title: Text(
            "",
            style: TextStyle(color: AppColors.white),
          ),
          iconTheme: IconThemeData(color: Colors.white),
        ),

        body:CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: checkexit=="0"?Container(
                height: 0,
              ):Container(
                color: AppColors.white,
                padding: EdgeInsets.only(left: 20,right: 10,top: 5,bottom: 5),
                child: Text("Select Your Favorite Professional",style: TextStyle(
                    color: AppColors.black,fontWeight: FontWeight.w500,fontSize: 18
                ),),
              ),
            ),
            SliverToBoxAdapter(
              child: checkexit=="0"?Container(
                height: 0,
              ):Container(
                // padding: EdgeInsets.only(left: 20),
                child: vendor(),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                color: AppColors.white,
                padding: EdgeInsets.only(left: 20,right: 10,top: 5,bottom: 5),
                child: Text("Select the time Slot",style: TextStyle(
                    color: AppColors.black,fontWeight: FontWeight.w500,fontSize: 18
                ),),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                // padding: EdgeInsets.only(left: 20),
                child: time(),
              ),
            ),
            // SliverToBoxAdapter(
            //   child:  Container(
            //
            //
            //     child: SingleChildScrollView(
            //       child:  ListView(
            //
            //         shrinkWrap: true,
            //         physics: NeverScrollableScrollPhysics(),
            //         children: [
            //           Container(
            //             padding: EdgeInsets.only(left: 20,right: 20),
            //
            //             child: Card(
            //               child: Container(
            //                 color: Colors.white,
            //
            //                 child: Container(
            //                     padding: EdgeInsets.only(
            //                       top: 10,
            //                       right: 20,
            //                       left: 20,
            //                       bottom: 10,
            //                     ),
            //                     child:Column(
            //                       children: [
            //                         Row(
            //                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                           children: [
            //                             Padding(
            //                               padding: EdgeInsets.only(
            //                                   left: 10,
            //                                   right: 10,
            //                                   top: 10
            //                               ),
            //                               child: Text("Service Charger",
            //                                 style: TextStyle(
            //                                     fontSize: 14,
            //                                     color: Colors.black45
            //                                 ),),
            //
            //                             ),
            //                             Text("₹",//+widget.price,
            //                               style: TextStyle(
            //                                   fontSize: 14,
            //                                   color: Colors.black45
            //                               ),)
            //
            //
            //                           ],
            //                         ),
            //                         // Row(
            //                         //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                         //   children: [
            //                         //     Padding(padding: EdgeInsets.only(
            //                         //         left: 10,
            //                         //         right: 10,
            //                         //         top: 10
            //                         //     ),
            //                         //       child: Text("Discount",
            //                         //         style: TextStyle(
            //                         //             fontSize: 14,
            //                         //             color: Colors.black45
            //                         //         ),),
            //                         //     ),
            //                         //     Text("₹500",
            //                         //       style: TextStyle(
            //                         //           fontSize: 14,
            //                         //           color: Colors.black45
            //                         //       ),)
            //                         //
            //                         //   ],
            //                         // ),
            //                         Divider(),
            //                         Row(
            //                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                           children: [
            //                             Padding(padding: EdgeInsets.only(
            //                                 left: 10,
            //                                 right: 10,
            //                                 top: 10
            //                             ),
            //                               child: Text("Total",
            //                                 style: TextStyle(
            //                                     fontSize: 16,
            //                                     fontWeight: FontWeight.bold,
            //                                     color: Colors.black
            //                                 ),),
            //                             ),
            //                             Text("₹",//+widget.price,
            //                             style: TextStyle(
            //                                 fontSize: 16,
            //                                 fontWeight: FontWeight.bold,
            //                                 color: Colors.black
            //                             ),)
            //
            //
            //                           ],
            //                         ),
            //                       ],
            //                     )
            //
            //
            //                 ),
            //
            //               ),
            //             ),
            //           ),
            //           // Container(
            //           //
            //           //   child: Container(
            //           //     // height: 100,
            //           //     child: Container(
            //           //
            //           //       child: ListTile(
            //           //         title: Container(
            //           //           child: TextField(
            //           //             decoration: InputDecoration(
            //           //                 labelText: "Type Coupon code"
            //           //             ),
            //           //           ),
            //           //         ),
            //           //         trailing: FlatButton(
            //           //           onPressed: (){},
            //           //           child: Text(
            //           //             "APPLY",style: TextStyle(
            //           //               color: AppColors.black
            //           //           ),
            //           //           ),
            //           //         ),
            //           //       ),
            //           //
            //           //     ),
            //           //   ),
            //           //
            //           // ),
            //           // Card(
            //           //
            //           //   child: Container(
            //           //       padding: EdgeInsets.only(
            //           //           top: 10,
            //           //           right: 10,
            //           //           left: 10,
            //           //           bottom: 10
            //           //       ),
            //           //       child:Column(
            //           //
            //           //         children: [
            //           //
            //           //           Row(
            //           //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           //             children: [
            //           //               Padding(
            //           //                 padding: EdgeInsets.only(
            //           //                     left: 10,
            //           //                     right: 10,
            //           //                     top: 5
            //           //                 ),
            //           //                 child: Text("Service Name", style: TextStyle(
            //           //                     fontSize: 11,
            //           //                     color: Colors.black45
            //           //                 ),),
            //           //
            //           //               ),
            //           //               Text("Hair Cut"
            //           //                 , style: TextStyle(
            //           //                     fontSize: 11,
            //           //                     color: Colors.black45
            //           //                 ),)
            //           //
            //           //
            //           //             ],
            //           //           ),
            //           //
            //           //
            //           //         ],
            //           //       )
            //           //
            //           //
            //           //   ),
            //           // ),
            //           //Divider(),
            //           // Container(
            //           //     padding: EdgeInsets.only(top: 10,
            //           //         bottom: 5,
            //           //         left: 15),
            //           //
            //           //     child: Text("Payment Option",
            //           //       style:TextStyle (fontSize: 15,
            //           //           color: Colors.black,
            //           //           fontWeight: FontWeight.bold
            //           //
            //           //       ),)
            //           // ),
            //           // Card
            //           //   (
            //           //   child: Container(
            //           //     height: 100,
            //           //       padding: EdgeInsets.only(
            //           //         top: 5,
            //           //         right: 10,
            //           //         left: 10,
            //           //         bottom: 10,
            //           //       ),
            //           //       child:Column(
            //           //         children: [
            //           //
            //           //           InkWell(
            //           //             onTap: ()
            //           //             {
            //           //
            //           //             },
            //           //             child: Row(
            //           //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           //               children: [
            //           //                 Padding(padding: EdgeInsets.only(
            //           //                     left: 5,
            //           //                     right: 10,
            //           //                     top: 10
            //           //                 ),
            //           //
            //           //                   child:
            //           //                   Row(
            //           //                     children: [
            //           //                       Icon(Icons.payment,
            //           //                         size: 20.0,
            //           //                       ),
            //           //                       Padding(padding: EdgeInsets.only(left: 10),),
            //           //                       Text("Cash",
            //           //                         style: TextStyle(
            //           //                             fontSize: 15
            //           //
            //           //                         ),),
            //           //                     ],
            //           //                   ),
            //           //                 ),
            //           //                 Icon(Icons.radio_button_unchecked,
            //           //                   size: 17.0,
            //           //
            //           //                 ),
            //           //
            //           //               ],
            //           //             ),
            //           //           ),
            //           //           Row(
            //           //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           //             children: [
            //           //               Padding(padding: EdgeInsets.only(
            //           //                   left: 5,
            //           //                   right: 10,
            //           //                   top: 10
            //           //               ),
            //           //                 child:
            //           //                 Row(
            //           //                   children: [
            //           //
            //           //                     Icon(Icons.payment,
            //           //                       size: 17.0,
            //           //                     ),
            //           //                     Padding(padding: EdgeInsets.only(left: 10),),
            //           //
            //           //                     Text("Online",style: TextStyle(
            //           //                       fontSize: 15,
            //           //
            //           //                     ),
            //           //                     ),
            //           //                   ],
            //           //                 ),
            //           //               ),
            //           //               Icon(Icons.radio_button_unchecked,
            //           //                 size: 17.0,
            //           //               )
            //           //
            //           //
            //           //             ],
            //           //           ),
            //           //         ],
            //           //       )
            //           //
            //           //
            //           //   ),
            //           // ),
            //
            //
            //         ],
            //
            //       ),
            //
            //     ),
            //   ),
            //
            // ),



          ],

        ) ,
        bottomNavigationBar:


        Container(
          height: 130,
          child: Column(
            children: [
              new Container(
                decoration: BoxDecoration(
                    color: Color.fromRGBO(236, 239, 241, 1),
                    boxShadow: [BoxShadow(blurRadius: 1, color: Colors.grey)]),
                height: 65,
                width: MediaQuery.of(context).size.width,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, bottom: 5, top: 5),
                      child: Text("Dilevered to this address"),
                    ),
                    new Container(
                      padding: EdgeInsets.only(
                          left: 20, bottom: 5, top: 5, right: 20),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Flexible(
                            child: new Row(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Icon(
                                    Icons.home,
                                    size: 20,
                                    color: Colors.indigo,
                                  ),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: Text(
                                    " $addressname",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.indigo),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                          ),
                          Material(
                            color: Color.fromRGBO(216, 67, 21, 1),
                            borderRadius: BorderRadius.all(Radius.circular(3)),
                            child: new InkWell(
                              onTap: ()
                              async {
                                await
                                Navigator.push(context,
                                    EnterExitRoute(enterPage: AllAddress()))
                                    .then((value) {
                                  getAddress();
                                });
                              },
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 5, right: 5, bottom: 5, top: 5),
                                child: Text(
                                  "Change",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),

              Container(
                height: 65,
                padding: EdgeInsets.only(left: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2)),

                  color: AppColors.white,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 0, top: 20),
                            child: Row(
                              children: [
                                Text(
                                  "₹ "+ widget.totalprice+".00",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(right: 10),
                        child: SizedBox(
                          child: InkWell(
                            onTap: () {
                              if(valuefirst==""){
                                alertDailog("Please Selected  Date");



                              }else{
                                if(addressname==""){
                                  alertDailog("Please Selected  Address");
                                }else{
                                  if(term=="1")
                                  {
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>termpage(widget.totalprice,date,starttime,widget.catid,endTime,timeid,vendid,widget.coupondiscountprices,widget.couponid,termdetail)));

                                  }else{
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>PaymentPage(widget.totalprice,date,starttime,widget.catid,endTime,timeid,vendid,widget.coupondiscountprices,widget.couponid)));

                                  }


                                }



                              }

                            },
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                color: AppColors.sentColor,
                              ),
                              child: Center(
                                child: Text(
                                  "Process to Pay",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
    );

  }
  Widget time()
  {
    return Container(
      child: Column(
        children: [
          Container(
            child: Column(

              // mainAxisAlignment: MainAxisAlignment.start,
              children: [

                Container(
                  height : 95,

                  padding: EdgeInsets.only(
                      top: 2,
                      bottom: 0,
                      right: 0,
                      left: 0

                  ),
                  alignment: Alignment.centerLeft,
                  color: AppColors.white,
                  child:
                  ListView.builder(
                    padding: EdgeInsets.only(left: 15),

                    scrollDirection: Axis.horizontal,

                    shrinkWrap: true,
                    itemCount:
                    records.length,
                    itemBuilder: (context, i){
                      return Container(
                          padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
                          child:
                          InkWell(
                            onTap: (){
                              _selectTimeSlot(i,records[i]['date'].toString());
                              // _selectTimeSlot(i.toString(),dates[i]);
                              //gettime(records[i],i);
                            },
                            child: Card(
                              color: selectedindex == i?AppColors.lightblue:Colors.white,
                              child:
                              Container(
                                padding: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 2,
                                              bottom: 0
                                          ),
                                          child:  Text(//records[i]['date'].toString(),
                                            //getdays(dates[i].toString()),
                                            getdays(records[i]['date'].toString()),
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 2,
                                              bottom: 0
                                          ),
                                          child:  Text(
                                            getdates(records[i]['date'].toString()
                                              //dates[i].toString()
                                            ),
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )

                      );
                    },


                  ),

                )
              ],
            ),
          ),
          Container(
            child:
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 0, left: 10, right: 10),
              child: GridView.builder(
                  itemCount: times.length != null ?times.length:"",
                  //category.length==null?category.length:"",
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 2.8,
                      crossAxisSpacing: 1,
                      mainAxisSpacing: 1),
                  itemBuilder: (Context, i) {
                    return Container(
                      child:
                      Card(
                        color: valuefirst==i.toString()?AppColors.lightblue:AppColors.white,
                        child: ListTile(
                          contentPadding: EdgeInsets.only(
                              top : 0,
                              bottom: 0,
                              left: 15,
                              right : 15
                          ),
                          title:
                          Center(
                            child: Text(times[i]['start_time'],
                              //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
                              //times[i]['Time'],
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold
                              ),

                            ),
                          ),
                          trailing:  Checkbox(
                            checkColor: AppColors.black,
                            activeColor: Colors.white,
                            value: valuefirst == i.toString()?true:false,
                            onChanged: (bool value) {
                              setState(() {
                                valuefirst = i.toString();
                                // slectTime(timerecord[i]);
                                slectTime(times[i]['start_time'],times[i]['end_time'],times[i]['id']);
                              });
                              //getcheckseat();
                            },
                          ),


                          // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),

                        ),
                      ),

                    );
                  }),
            ),
            // child: Column(
            //
            //   // mainAxisAlignment: MainAxisAlignment.start,
            //   children: [
            //
            //     Container(
            //       // height : 180,
            //       padding: EdgeInsets.only(
            //           top: 2,
            //           bottom: 0,
            //           right: 20,
            //           left: 20
            //
            //       ),
            //       alignment: Alignment.centerLeft,
            //       color: Colors.white12,
            //       child: ListView.builder(
            //         padding: EdgeInsets.only(
            //             top : 10
            //         ),
            //         physics: NeverScrollableScrollPhysics(),
            //         shrinkWrap: true,
            //         itemCount:
            //         //timerecord.length,
            //         times.length,
            //         //timelen,
            //         itemBuilder: (context, i){
            //           return Container(
            //             // padding : EdgeInsets.only(
            //             //   bottom: 10
            //             // ),
            //             child:
            //             Row(
            //               children: [
            //                 Flexible(
            //                   flex: 1,
            //                   child: Card(
            //                     color: Color(0xFFFFFFFFF),
            //                     child: ListTile(
            //                       contentPadding: EdgeInsets.only(
            //                           top : 0,
            //                           bottom: 0,
            //                           left: 15,
            //                           right : 15
            //                       ),
            //                       title: Text(times[i]['start_time'],
            //                           //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
            //                         //times[i]['Time'],
            //                         style: TextStyle(
            //                             color: Colors.black,
            //                             fontSize: 13,
            //                             fontWeight: FontWeight.bold
            //                         ),
            //
            //                       ),
            //                       trailing:  Checkbox(
            //                         checkColor: AppColors.black,
            //                         activeColor: Colors.white,
            //                         value: valuefirst == i.toString()?true:false,
            //                         onChanged: (bool value) {
            //                           setState(() {
            //                             valuefirst = i.toString();
            //                             // slectTime(timerecord[i]);
            //                             slectTime(times[i]['start_time'],times[i]['end_time'],times[i]['id']);
            //                           });
            //                           //getcheckseat();
            //                         },
            //                       ),
            //
            //
            //                       // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),
            //
            //                     ),
            //                   ),
            //                 ),
            //                 Flexible(
            //                   flex: 1,
            //                   child: Card(
            //                     color: Color(0xFFFFFFFFF),
            //                     child: ListTile(
            //                       contentPadding: EdgeInsets.only(
            //                           top : 0,
            //                           bottom: 0,
            //                           left: 15,
            //                           right : 15
            //                       ),
            //                       title: Text(times[i]['start_time'],
            //                         //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
            //                         //times[i]['Time'],
            //                         style: TextStyle(
            //                             color: Colors.black,
            //                             fontSize: 13,
            //                             fontWeight: FontWeight.bold
            //                         ),
            //
            //                       ),
            //                       trailing:  Checkbox(
            //                         checkColor: AppColors.black,
            //                         activeColor: Colors.white,
            //                         value: valuefirst == i.toString()?true:false,
            //                         onChanged: (bool value) {
            //                           setState(() {
            //                             valuefirst = i.toString();
            //                             // slectTime(timerecord[i]);
            //                             slectTime(times[i]['start_time'],times[i]['end_time'],times[i]['id']);
            //                           });
            //                           //getcheckseat();
            //                         },
            //                       ),
            //
            //
            //                       // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),
            //
            //                     ),
            //                   ),
            //                 ),
            //               ],
            //             ),
            //           );
            //         },
            //
            //
            //       ),
            //
            //     )
            //   ],
            // ),
          ),
        ],
      ),
    );
  }

  Widget vendor()
  {
    return
      checkexit=="0"?Container(
        height: 1,
      ): Container(
          color: Colors.white,
          padding: EdgeInsets.only(top: 10,bottom: 0,left: 10,right: 5),

          height: MediaQuery.of(context).size.longestSide/3.3,

          child: ListView.builder(
            padding: EdgeInsets.only(left: 5,top: 0),
            itemCount: 2,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int i)
            {

              return Container(
                padding: EdgeInsets.only(right: 10),

                child:
                InkWell(
                  onTap: (){
                    // getdate();
                    _setIndex(i,i==0?"0":vendorid);
                    print(vendorid);
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => ProductdetailLoader(productLink: bestselling[i]['links']['details'])));
                  },
                  child: Container(
                    //height: 300,
                    width: MediaQuery.of(context).size.width/2.8,
                    padding: EdgeInsets.only(top: 0),

                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: _currentPage==i?AppColors.lightblue:AppColors.white,
                        border: Border.all(
                            color: _currentPage==i?AppColors.sentColor:AppColors.grayBorder
                        )
                    ),
                    child: Column(
                      children: [

                        InkWell(
                          onTap: (){

                            i==0?"": Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        vendorprofile(vendorid)));
                          },
                          child: Center(
                            child:
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              height: 100,
                              width: 100,
                              //width: MediaQuery.of(context).size.width/2.2,
                              decoration: BoxDecoration(
                                  color: AppColors.grayBorder,
                                  border: Border.all(color: Colors.white),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(120)),
                                  image: DecorationImage(

                                      image: i==0?AssetImage("assets/images/launcher.png",

                                      ):i==1?NetworkImage(vendorimage):null
                                      ,

                                      fit: BoxFit.fill)
                              ),
                            ),
                          ),
                        ),
                        Flexible(

                          child: Container(

                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  padding: EdgeInsets.only(left: 0,top: 5),
                                  child: Container(
                                    child: Text(i==0?"Auto Assist":
                                    i==1?"$vendorname":""
                                      ,style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 4,left: 10,right: 3),
                                  child: Container(
                                    child: Text(
                                      i==0?"We'll assign you the top pro person":
                                      i==1?"You rated":
                                      "",
                                      style: TextStyle(fontSize: 12,
                                          color: Colors.black38),),
                                  ),
                                ),

                                Center(
                                  child:
                                  i==0?Container():
                                  Container(
                                    padding: EdgeInsets.only(left:0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,

                                      children: [
                                        Container(

                                            color:Colors.white,
                                            child:
                                            Icon(Icons.star,size: 18,color: AppColors.redcolor3,)
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Text(
                                            "$vendorrat",style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14,
                                          ),


                                          ),
                                        )
                                      ],
                                    ),


                                  ),
                                )





                              ],
                            ),
                          ),
                        ),


                      ],
                    ),
                  ),
                ),
              );
            },
          )
      );

  }


  getdates(String text) {
    var string = text;

    // var now = new DateTime.now();
    var formatter = new DateFormat('E');
    String formatted = formatter.format(DateTime.parse(string));
    return formatted.toUpperCase();
  }

  getdays(String text) {
    var string = text;
//    var now = new DateTime.now();
    var formatter = new DateFormat('MMM d');
    String formatted = formatter.format(DateTime.parse(string));
    return formatted.toUpperCase();
  }

}
