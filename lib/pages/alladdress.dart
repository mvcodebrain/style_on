import "package:flutter/material.dart";



import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:yesmadam/animators/navianimator.dart';
//import 'package:flare_flutter/flare_actor.dart';
import 'dart:async';
import 'dart:convert';

import 'dart:io';

import 'package:yesmadam/services/apis.dart';

import 'addressadd.dart';


class AllAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AllAdderessLoader();
  }
}

class TimeValue {
  final int _key;
  final String _value;
  TimeValue(this._key, this._value);
}

class AllAdderessLoader extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AllAddressView();
  }
}



class AllAddressView extends State<AllAdderessLoader> {
  List address;
  var len = 0;
  var loading = false;
  Future getAddress() async {
    var url = Uri.tryParse(Apis.ADDRESSLIST_API);
       // Apis.ADDRESSLIST_API;
    var sp = await SharedPreferences.getInstance();
    var userid = sp.getString("userid");
    // var data = {"user_id":userid};
    var map = new Map<String,dynamic>();
    map['user_id'] = userid;
    // var res = await apiPostRequest(url,map);
    http.Response res = await http.post(url,body:map);
    print(res.body);
    if(json.decode(res.body)['status'].toString() == "1"){
      setState(() {
        address = json.decode(res.body)['data'];
        len = address.length;
        loading = false;
      });
    }else{
      setState(() {
        loading = false;
        len=0;
      });
     // Navigator.pop(context);
      print("No Data fount");
    }
    // print(res);
  }

  int selectedRadio = 0;


  var setcuradd;
  var addressid="";

  Future deleteAddress() async{
    setState(() {
      loading = true;
    });
    var url = Uri.tryParse(Apis.DELETEADDRESSLIST_API);
   // Apis.DELETEADDRESSLIST_API;
    var sp = await SharedPreferences.getInstance();
    var userid = sp.getString("userid");
    var map = new Map<String,dynamic>();
    map['user_id'] = userid;
    map['address_id']= setcuradd;
    sp.setString(addressid, setcuradd);
    // var data ={
    //   "address_id" : setcuradd
    // };
    print(map);
    http.Response res = await http.post(url,body: map);
    print(res.body);
    getAddress();
    Navigator.pop(context);

  }

  // Future setAddresses() async {
  //   setState(() {
  //     loading = true;
  //   });
  //   var url = Apis.BASE_URL + "Address/addAddress";
  //   var sp = await SharedPreferences.getInstance();

  // }

  Future<String> apiPostRequests(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }

  Future<String> apiPostRequest(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }

  var addresstype = "";
  var addressname = "";

  setAddresses() async{
    print(addressname);
    var sp = await SharedPreferences.getInstance();
    setState(() {
      sp.setString("addressid",selectedRadio.toString());
      sp.setString("addressname", addressname);
      sp.setString("addresstype",addresstype);
    });
    Navigator.pop(context);
  }


  Future setSelectedRadio(val,addresstypes,addressnames) async {
    setState(() {
      selectedRadio = val;
      addresstype = addresstypes;
      addressname = addressnames;
    });
    print(addresstype);
  }



  void _settingModalBottomSheet(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
          return Container(
            color: Colors.black.withOpacity(0.97),
            child: new Wrap(
            children: <Widget>[
// new ListTile(
//             leading: new Icon(
//               FontAwesomeIcons.solidEdit,
//               color: Colors.greenAccent,
//             ),
//             title: new Text(
//               'Edit',
//               style: TextStyle(
//                 color: Colors.white.withOpacity(0.8),
//                 fontFamily: "opensan",
//                 fontSize: 16,
//               ),
//             ),
//             onTap: () => {
//
//             }
//           ),
          new ListTile(
            leading: new Icon(
              Icons.delete,
              color: Colors.redAccent,
            ),
            title: new Text(
              'Delete',
              style: TextStyle(
                color: Colors.white.withOpacity(0.8),
                fontFamily: "opensan",
                fontSize: 16,
              ),
            ),
            onTap: () => {
               deleteAddress()
            },          
          ),
            ],
          ),
          );
      }
    );
}


  @override
  void initState() {
    super.initState();
    getAddress();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Addresses",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        elevation: 0.5,
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            new Container(
              child: new CustomScrollView(
                slivers: <Widget>[
                  SliverPadding(
                    padding: EdgeInsets.only(top: 10),
                    sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          return new Card(
                            child: new Container(
                              child: new ListTile(
                                onTap: (){
                                  setSelectedRadio(int.parse(address[index]['id']),'home',address[index]['address']);
                                },
                                leading: Material(
                                  color: Colors.white,
                                  child: Radio(
                                    value: int.parse(address[index]['id']),
                                    groupValue: selectedRadio,
                                    onChanged: (val){
                                      setSelectedRadio(int.parse(address[index]['id']),'home',address[index]['address']);
                                    },
                                  ),
                                ),
                                title: Text(
                                  address[index]['name']
                                ),
                                subtitle: Text(
                                  address[index]['address']
                                ),
                                trailing: IconButton(
                                  icon: Icon(
                                    Icons.chevron_right_sharp,
                                    size: 17,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      setcuradd = address[index]['id'];
                                    });
                                    _settingModalBottomSheet(context);
                                    
                                  },
                                ),
                              ),
                            ),
                          );
                        },

                        /// Set childCount to limit no.of items
                        childCount: len,
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: new Container(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: new FlatButton(
                          onPressed: ()
                          async{
                            await Navigator.push(
                              context, EnterExitRoute(enterPage: AddressPage())
                              ).then((value){
                                getAddress();
                              });
                          },
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(right: 10),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.blueAccent,
                                ),
                              ),
                              Text(
                                "Add new address",
                                style: TextStyle(
                                    fontFamily: "opensan",
                                    fontSize: 16,
                                    color: Colors.black),
                                textAlign: TextAlign.left,
                              )
                            ],
                          )),
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: loading == true
                  ? new Container(
                      color: Colors.white.withOpacity(0.8),
                      child: new Center(
                        child: new Container(
                          // color: Colors.black26,
                          width: 200,
                          height: 200,
//                          child: FlareActor(
//                            "assets/animations/loadKnife.flr",
//                            animation: "Untitled",
//                            // color: Colors.blueAccent,
//                            // isPaused: true,
//                          ),
                        ),
                      ),
                    )
                  : new Container(),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: new Container(
          height: 60,
          padding: EdgeInsets.only(
            left: 5,
            right: 5,
            bottom: 5
          ),
          child: new SizedBox(
            child: FlatButton(
              // disabledColor: selectedRadio == 0 ? Colors.black : Colors.blueAccent,
              color: selectedRadio == 0 ? Colors.blueAccent.withOpacity(0.3): Colors.blueAccent,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Continue",
                    style: TextStyle(
                      color: selectedRadio == 0 ? Colors.blueAccent.withOpacity(0.5): Colors.white, 
                      fontSize: 17,
                      fontFamily: "opensan",
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  new Icon(
                    Icons.chevron_right,
                    color: selectedRadio == 0 ? Colors.blueAccent.withOpacity(0.5): Colors.white,
                  )
                ],
              ),
              onPressed: () {
                if(selectedRadio != 0){
                  setAddresses();
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
