import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

class Wallet extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return WalletView();
  }
}

class WalletView extends State<Wallet> {
  Razorpay _razorpay;
  var pricess = TextEditingController();


  List wallet =[];
  var amu="";

  var balance ="";
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  List price =[];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 1),
      // action: ,
    ));
  }
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  Future getwallet() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });
    var sp = await SharedPreferences.getInstance();
    var url = Apis.WALLET_API;
    // var data = json.encode(map);
    var data = {

      "user_id":
      //1,
      sp.getString("userid"),
      "page":"0",
      "limit":"100"



      //"user_id":sp.getString("userid")

    };

    var res = await apiPostRequests(url,data);

    print(res);
    setState(() {

      if(json.decode(res)['status'].toString() == "1"){

        setState(() {
          // price=json.decode(res)['data'];
          if(json.decode(res)['data'].length==0){
            balance="0";
            sp.setString("balance", "0");

          }else{
            amu = json.decode(res)['data']['balance'];
            wallet = json.decode(res)['data']['transactions'];
            balance = json.decode(res)['data']['transactions'][0]['amount'].toString();
            sp.setString("balance", json.decode(res)['data']['transactions'][0]['amount'].toString());
            print(balance);
          }
          print(json.decode(res)['data'].length);



        });

      }
      else{
        setState(() {
          balance="0";
          sp.setString("balance", "0");
          print(balance);
        });

      }
      loading = false;
    });
  }
  Future getwalletprice() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });
    var sp = await SharedPreferences.getInstance();
    var url = Apis.WALLETADD_API;
    // var data = json.encode(map);
    var data = {

      "user_id":
      //1,
      sp.getString("userid"),

      "amount":pricess.text



      //"user_id":sp.getString("userid")

    };

    var res = await apiPostRequests(url,data);

    print(res);
    setState(() {

      if(json.decode(res)['status'].toString() == "1"){

        setState(() {
          // _showInSnackBar("Amount Added To Your Wallet");
          // price=json.decode(res)['data'];
          pricess.text="";
          getwallet();
          // Navigator.pop(context);
          // Navigator.pop(context);





        });

      }
      else{
        getwallet();


      }
      loading = false;
      getwallet();
    });
  }
  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  @override
  void initState() {
    // TODO: implement initState
    getwallet();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }


  alertDailogd(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Container(height: 130, child: Text(text)),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }
  alertDailoga(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Amount"),
            content: Container(
              padding: EdgeInsets.all(0.0),
              height: 50,
              width: MediaQuery.of(context).size.width / 1.1,
              decoration: BoxDecoration(
                border: Border.all(
                  // color: AppColors.selectedItemColor,
                  style: BorderStyle.solid,
                  width: .5,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              child: TextField(
                //controller: ,
                controller: pricess,
                decoration: InputDecoration(
                    border: InputBorder.none,

                    //hintText: "Mobile Number",
                    hintStyle: TextStyle(color: Colors.grey[400]),
                    contentPadding: EdgeInsets.only(
                      left: 10,
                    ),
                    counterText: ""),
                //  maxLength: 10,
              ),
            ),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  //  _filterBottomSheetMenu();
                  if (pricess.text.isEmpty) {
                    alertDailogd("Please Enter Price ");
                  } else {
                    openCheckout();

                  }
                },
                child: Text("Submit"),
              )
            ],
          );
        });
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_1DP5mmOlF5G5ag',
      'amount': pricess.text+"00",
      'name': 'DOS',
      'description': '',
      'prefill': {'contact': '', 'email': ''},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint('Error: e');
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    getwalletprice();
    Navigator.pop(context);

    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, toastLength: Toast.LENGTH_SHORT);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        toastLength: Toast.LENGTH_SHORT);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, toastLength: Toast.LENGTH_SHORT);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Text(
          "My Wallet",
          style: TextStyle(
            fontFamily: "opensan",
            color: Colors.white,
//              fontWeight: FontWeight.bold
          ),
        ),
//          centerTitle: true,
        elevation: 0,
      ),
      backgroundColor: AppColors.white,
      body: new SingleChildScrollView(
        child: new Column(
          children: <Widget>[
            new Container(
              color : AppColors.sentColor,
              child: new ListTile(
                leading: SizedBox(
                  width: 45,
                  height: 45,
                  child: CircleAvatar(
                    backgroundColor: AppColors.white,
                    child: Icon(
                      MaterialIcons.account_balance_wallet,
                      size: 30,
                      color: AppColors.sentColor,
                    ),
                  ),
                ),
                title: Text(
                  "Balance",
                  style: TextStyle(
                      fontFamily: "opensan",
                      color: Colors.white
                  ),
                ),
                subtitle: Text(
                  "\u20B9 "+amu,
                  style: TextStyle(
                      fontFamily: "opensan",
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                  ),
                ),
              ),
            ),

            Container(
              child: walletdetail(),
            )
            // new Container(
            //   child: loading == true?
            //   SizedBox(
            //     height: 3,
            //     child: LinearProgressIndicator(
            //       valueColor: new AlwaysStoppedAnimation<Color>(AppColors.primary2),
            //       backgroundColor: Colors.white,
            //     ),
            //   )
            //     //     :
            //     // new Container(),
            // ),
            // new Container(
            //   padding: EdgeInsets.only(
            //       top: 10
            //   ),
            //   child: ListView.builder(
            //     physics: NeverScrollableScrollPhysics(),
            //     shrinkWrap: true,
            //     itemCount: 4,//wallhitLen,
            //     itemBuilder: (context, i){
            //       return new Container(
            //         child: new Column(
            //           children: <Widget>[
            //             new Container(
            //               padding: EdgeInsets.only(
            //                   left: 20,
            //                   top: 20,
            //                   bottom: 5
            //               ),
            //               child: Align(
            //                 alignment: Alignment.centerLeft,
            //                 child: Text("hjhk",
            //                     //getdates(wallethist[i]['date_time'].toString()),
            //                     style: TextStyle(
            //                         fontSize: 13,
            //                         color: Colors.black54
            //                     )
            //                 ),
            //               ),
            //             ),
            //             new Container(
            //               padding : EdgeInsets.only(
            //                   left: 10,
            //                   right : 10,
            //                   bottom: 10
            //               ),
            //               child: new Container(
            //                 decoration: BoxDecoration(
            //                     color : Colors.white,
            //                     borderRadius: BorderRadius.all(
            //                         Radius.circular(5)
            //                     ),
            //                     boxShadow: [
            //                       BoxShadow(
            //                           color: Colors.black26,
            //                           offset: Offset(0.0,1.0)
            //                       )
            //                     ]
            //                 ) ,
            //                 child: ListTile(
            //                   title: Text("",
            //                       // txtConvert(
            //                       //     wallethist[i]['resource_details'].toString().toLowerCase())
            //                   ),
            //                   leading:
            //                   //wallethist[i]['tr_type'].toString() == "credit"
            //                       //?
            //      // Icon(
            //                   //   AntDesign.arrowdown,
            //                   //   color: AppColors.primary2,
            //                   // )
            //                     //  :
            //       Icon(
            //                       AntDesign.arrowup,
            //                       color: Colors.redAccent
            //                   ),
            //                   trailing: Text(
            //                       "\u20B9 "
            //                           //+wallethist[i]['amount'].toString()
            //                   ),
            //                   subtitle: Text("",
            //                       //wallethist[i]['tr_type'].toString().toUpperCase()
            //                   ),
            //                 ),
            //               ),
            //             )
            //           ],
            //         ),
            //       );
            //     },
            //   ),
            // )
          ],
        ),
      ),
      floatingActionButton:FloatingActionButton(
        onPressed: (){
          alertDailoga("");

        },
        isExtended: true,backgroundColor: AppColors.sentColor,
        child: Icon(Icons.add,size: 40,),
      ),

    );

  }
  Widget walletdetail() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5, right: 5, left: 5),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: wallet.length,
          //service.length,
          itemBuilder: (BuildContext context, int i) {
            return Container(
              //  height: 150,
              padding: EdgeInsets.only(top: 10, bottom: 15, left: 5, right: 5,),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            padding: EdgeInsets.only(top: 10,bottom: 5,left: 5,),
                            child:Text("transaction id".toUpperCase())
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 5,left: 5,right: 20),
                            child:Text(wallet[i]['transaction_id'].toUpperCase())
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: [
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 5,left: 5),
                            child:Text("transaction type".toUpperCase())
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 5,right: 20),
                            child:Text(wallet[i]['transaction_type'].toUpperCase())
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: [
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 5,left: 5),
                            child:Text("amount".toUpperCase())
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 5,right: 20),
                            child:Text("₹"+wallet[i]['amount'].toUpperCase())
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: [
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 5,left: 5),
                            child:Text("date".toUpperCase())
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 5,right: 20),
                            child:Text(wallet[i]['date_time'].toUpperCase())
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: [
                        Container(
                            padding: EdgeInsets.only(top: 5,bottom: 15,left: 5),
                            child:Text("description".toUpperCase())
                        ),
                        Flexible(
                          child: Container(
                              padding: EdgeInsets.only(top: 5,bottom: 15,right: 20,left: 10),
                              child:Text(wallet[i]['description'])
                          ),
                        ),
                      ],
                    ),

                  ],
                ),
              ),
            );
          }),
    );
  }


  txtConvert(String txt) {
    String s = txt;
    return '${s[0].toUpperCase()}${s.substring(1)}';
  }

//   getdates(String text){
//     var string = text;
// //    var now = new DateTime.now();
//     var formatter = new DateFormat('E, d MMMM, hh:mm a');
//     String formatted = formatter.format(DateTime.parse(string.substring(0,19)));
//     return formatted;
//   }

}

