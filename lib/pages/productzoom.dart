import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:photo_view/photo_view.dart';

class Productzoom extends StatefulWidget{
  final List image;
  Productzoom({this.image});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Productzoomview();
  }

}
class Productzoomview extends State<Productzoom>{
  final imageList = [



    // 'https://images.all-free-download.com/images/graphicthumb/cosmetics_collection_of_highdefinition_picture_166413.jpg',
    // 'https://2.imimg.com/data2/PL/LQ/MY-210654/sai24-250x250-250x250.jpg',
    // 'https://img.freepik.com/free-vector/set-cosmetics-with-essence-face-cream-product-placement-cherry-blossom-splash-waves-drops-place-brand-realistic-s_1268-15062.jpg?size=626&ext=jpg',
  ];
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

      appBar:AppBar (
        leading: IconButton(
          icon: Icon(
            Icons.close,size: 25,
            color: AppColors.white,
          ),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        automaticallyImplyLeading: false,

      ),
      body: Container(
        //color: AppColors.white,

          child:
          Center(

            child: PhotoViewGallery.builder(
             itemCount: widget.image.length,
              builder: (context, index) {

                return PhotoViewGalleryPageOptions(
                imageProvider: NetworkImage(widget.image[index]['image']),
               minScale: PhotoViewComputedScale.contained * 0.8,
                 maxScale: PhotoViewComputedScale.covered * 2,
                );
                },
              scrollPhysics: BouncingScrollPhysics(),
              backgroundDecoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
              ),


     ),
          ),
      ),

    );
  }

}