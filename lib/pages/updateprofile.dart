

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';


import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';
import 'dart:convert';

import 'otppage.dart';


class updated extends StatefulWidget{
  final firstname;
  final lastname;
  final email;
  final image;
  updated(this.firstname,this.lastname,this.email,this.image);
  // final mobile;
  // final appSignature;
  // updated(this.mobile,this.appSignature);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return updateloader();
  }

}
class updateloader extends State<updated>{
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  var name = TextEditingController();
  var email = TextEditingController();

  alertDailog(text) async{
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: (){
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;

  // Future getUpdateprofile() async
  //
  // {
  //   var url = Uri.tryParse(Apis.UPDATEPROFILE_API);
  //   // Apis.UPDATEPROFILE_API;
  //   var sp = await SharedPreferences.getInstance();
  //   var userid = sp.getString("userid");
  //   var map = new Map<String,dynamic>();
  //   map['user_id'] = userid;
  //   map['first_name'] = name.text;
  //   map['last_name'] = "";
  //   map['email'] = email.text;
  //   map['image'] = base64Image;
  //   map['gender']= "";
  //   // var data ={
  //   //   "user_id":userid,
  //   //   "name":name.text,
  //   //   "phone":phone.text,
  //   //   "address":location.text,
  //   //   "pincode":pincode.text
  //   //
  //   // };
  //   print(map);
  //   // var res = await apiPostRequest(url,map);
  //   http.Response res = await http.post(url,body: map);
  //   print("jjjhj");
  //   Navigator.pop(context);
  //   print(res.body);
  // }
  Future getUpdateprofile() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.UPDATEPROFILE_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "user_id": sp.getString("userid"),
      "first_name":name.text,
      "last_name":last.text,
      "email":email.text,
      "mobile":"",
      "image":"base64Image",
      "gender":"1"
    };
    print(data);



    // var res = await apiPostRequest(url,data);
    http.Response res = await http.post(Uri.parse(url),body: json.encode(data));
    print(res.body);
    setState(() {
      if (json.decode(res.body)['status'].toString() == "1")
      {
        Navigator.pop(context);
        // orderid=json.decode(res)['data'][0]['id'];
        // print(orderid);
      } else {
      }

      loading = false;
    });
  }

  Future<String> apiPostRequest(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    // request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    // Navigator.pop(context);
    return reply;
  }


  @override
  void initState(){
    super.initState();
    getmobile();

  }
  File _image;
  String base64Image="";
  final picker = ImagePicker();
  Future getImagefromGallery() async {
    final pickedFile = await ImagePicker.pickImage(source: ImageSource.gallery);;

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        var imageBytes = _image.readAsBytesSync();
        base64Image = base64Encode(imageBytes);
        print(base64Image);
        // postprofile();
      } else {
        print('No image selected.');
      }
    });
  }
  Future getImagefromCamera() async {
    final pickedFile = await ImagePicker.pickImage(source:ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        var imageBytes = _image.readAsBytesSync();
        base64Image = base64Encode(imageBytes);
        print(base64Image);
        // postprofile();
      } else {
        print('No image selected.');
      }
    });
  }
  // Future getImagefromGallery() async {
  //   final pickedFile = await picker.getImage(source: ImageSource.gallery);
  //
  //   setState(() {
  //     if (pickedFile != null) {
  //       _image = File(pickedFile.path);
  //       var imageBytes = _image.readAsBytesSync();
  //       base64Image = base64Encode(imageBytes);
  //       print(base64Image);
  //       // postprofile();
  //     } else {
  //       print('No image selected.');
  //     }
  //   });
  // }
  // Future getImagefromCamera() async {
  //   final pickedFile = await picker.getImage(source: ImageSource.camera);
  //
  //   setState(() {
  //     if (pickedFile != null) {
  //       _image = File(pickedFile.path);
  //       var imageBytes = _image.readAsBytesSync();
  //       base64Image = base64Encode(imageBytes);
  //       print(base64Image);
  //       // postprofile();
  //     } else {
  //       print('No image selected.');
  //     }
  //   });
  // }
  void _modalBottomSheetMenu(){
    showModalBottomSheet(
        context: context,
        builder: (builder){
          return new Container(
            height: 150.0,
            color: Colors.transparent, //could change this to Color(0xFF737373),
            //so you don't have to change MaterialApp canvasColor
            child: Column(
              children: [
                ListTile(
                  leading: Icon(
                      Icons.image
                  ),
                  title: Text("Gallery"),
                  onTap: (){
                    Navigator.pop(context);
                    getImagefromGallery();
                  },
                ),
                ListTile(
                  leading: Icon(
                      Icons.camera_alt
                  ),
                  title: Text("Camera"),
                  onTap: (){
                    Navigator.pop(context);
                   getImagefromCamera();
                  },
                )
              ],
            ),
          );
        }
    );
  }
  var last = TextEditingController();

  getmobile() async{

    setState(() {
      name.text=widget.firstname.toString();
      last.text=widget.lastname.toString();
      email.text=widget.email.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Text(
          "Update Profile",
          style: TextStyle(color: Colors.white),

        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body:  SingleChildScrollView(
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: [
            Container(
              padding: EdgeInsets.only(top: 20,left: 10,bottom: 20),
              child: Text("Take your profile photo",style: TextStyle(
                  fontWeight: FontWeight.w600,fontSize: 18
              ),),
            ),
            Container(
              padding: EdgeInsets.all(22),
              child: Center(
                child:
                InkWell(
                  onTap: (){
                    _modalBottomSheetMenu();
                  },
                  child: Container(




                    height: 210,
                    width: 210,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(120)),
                        border: Border.all(
                            color: Colors.black
                        ),
                        image: DecorationImage(
                            image:
                            _image == null
                                ? NetworkImage(widget.image==null?
                            "https://1.bp.blogspot.com/-0EXggyGpmms/W4aLLAy8COI/AAAAAAAAFeA/lS-Jp4NTUccNDKcw33ScqY5NCvgR1mtAQCPcBGAYYCw/s320/cute-kitty-on-cup.jpg"
                                :widget.image)
                                :
                            FileImage(_image),
                            fit: BoxFit.cover
                        )
                    ),
                  ),
                ),
              ),
            ),



            // Container(
            //   child: Container(
            //     padding: EdgeInsets.only(right: 0,top: 20,left: 0,bottom: 10),
            //
            //
            //     child: SizedBox(
            //       child: InkWell(
            //         onTap: (){
            //           // Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginLoader()));
            //         },
            //
            //         child:
            //         ListTile(
            //             title: Text("First Name"),
            //             subtitle:
            //             Card(
            //               child: Container(
            //                 padding: EdgeInsets.all(0.0),
            //                 height: 40,
            //                 width: 40,
            //                 decoration:  BoxDecoration(
            //                   // border: Border.all(
            //                   //  // color: AppColors.selectedItemColor,
            //                   //   style: BorderStyle.solid,
            //                   //   width: 1.0,
            //                   // ),
            //                   borderRadius: BorderRadius.all(Radius.circular(1)),
            //                   color: Colors.white,
            //
            //                 ),
            //                 child: TextField(
            //                   controller: name,
            //                   //controller: mobile,
            //                   decoration: InputDecoration(
            //                       border: InputBorder.none,
            //
            //                       //hintText: "Mobile Number",
            //                       hintStyle: TextStyle(color: Colors.grey[400]
            //                       ),
            //                       contentPadding: EdgeInsets.only(
            //                         left: 10,
            //                       ),
            //                       counterText: ""
            //
            //                   ),
            //                   //  maxLength: 10,
            //
            //                 ),
            //
            //               ),
            //             )
            //         ),
            //
            //
            //       ),
            //     ),
            //   ),
            // ),
            // Container(
            //   child: Container(
            //     padding: EdgeInsets.only(right: 0,top: 20,left: 0,bottom: 10),
            //
            //
            //     child: SizedBox(
            //       child: InkWell(
            //         onTap: (){
            //           // Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginLoader()));
            //         },
            //
            //         child:
            //         ListTile(
            //             title: Text("Last Name"),
            //             subtitle:
            //             Card(
            //               child: Container(
            //                 padding: EdgeInsets.all(0.0),
            //                 height: 40,
            //                 width: 40,
            //                 decoration:  BoxDecoration(
            //                   // border: Border.all(
            //                   //  // color: AppColors.selectedItemColor,
            //                   //   style: BorderStyle.solid,
            //                   //   width: 1.0,
            //                   // ),
            //                   borderRadius: BorderRadius.all(Radius.circular(1)),
            //                   color: Colors.white,
            //
            //                 ),
            //                 child: TextField(
            //                   controller: last,
            //                   //controller: mobile,
            //                   decoration: InputDecoration(
            //                       border: InputBorder.none,
            //
            //                       //hintText: "Mobile Number",
            //                       hintStyle: TextStyle(color: Colors.grey[400]
            //                       ),
            //                       contentPadding: EdgeInsets.only(
            //                         left: 10,
            //                       ),
            //                       counterText: ""
            //
            //                   ),
            //                   //  maxLength: 10,
            //
            //                 ),
            //
            //               ),
            //             )
            //         ),
            //
            //
            //       ),
            //     ),
            //   ),
            // ),
            Row(
              children: [
                Flexible(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(top: 20, bottom: 5),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Row(
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    margin: EdgeInsets.only(
                                        right: 2.5
                                    ),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        border: Border.all(color: Colors.black12)),
                                    child: Row(
                                      children: [
                                        // Container(
                                        //   padding: EdgeInsets.only(left: 20),
                                        //   child: Icon(
                                        //     Icons.perm_identity,
                                        //     color: AppColors.redcolor3,
                                        //   ),
                                        // ),
                                        Expanded(
                                          child: Container(
                                            width: 250,
                                            child: TextField(
                                              autofocus: true,
                                              controller: name,
                                              decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "First Name",
                                                  hintStyle:
                                                  TextStyle(color: AppColors.black),
                                                  contentPadding: EdgeInsets.only(
                                                    left: 10,
                                                  ),
                                                  counterText: ""),
                                              keyboardType: TextInputType.text,

                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                              ),
                              // Flexible(
                              //   flex: 1,
                              //   child: Container(
                              //     margin: EdgeInsets.only(
                              //         left: 2.5
                              //       ),
                              //       decoration: BoxDecoration(
                              //           color: Colors.white,
                              //           borderRadius: BorderRadius.all(Radius.circular(10)),
                              //           border: Border.all(color: Colors.black12)),
                              //       child: Row(
                              //         children: [
                              //           Container(
                              //             padding: EdgeInsets.only(left: 20),
                              //             child: Icon(
                              //               Icons.perm_identity,
                              //               color: AppColors.redcolor3,
                              //             ),
                              //           ),
                              //           Expanded(
                              //             child: Container(
                              //               width: 250,
                              //               child: TextField(
                              //                 autofocus: true,
                              //                 controller: lname,
                              //                 decoration: InputDecoration(
                              //                     border: InputBorder.none,
                              //                     hintText: "Last Name",
                              //                     hintStyle:
                              //                         TextStyle(color: AppColors.redcolor3),
                              //                     contentPadding: EdgeInsets.only(
                              //                       left: 10,
                              //                     ),
                              //                     counterText: ""),
                              //                 keyboardType: TextInputType.text,
                              //                 maxLength: 10,
                              //               ),
                              //             ),
                              //           ),
                              //         ],
                              //       )
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(top: 20, bottom: 5),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Row(
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                    margin: EdgeInsets.only(
                                        right: 2.5
                                    ),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        border: Border.all(color: Colors.black12)),
                                    child: Row(
                                      children: [
                                        // Container(
                                        //   padding: EdgeInsets.only(left: 20),
                                        //   child: Icon(
                                        //     Icons.perm_identity,
                                        //     color: AppColors.redcolor3,
                                        //   ),
                                        // ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(left: 10),
                                            width: 250,
                                            child: TextField(
                                              autofocus: true,
                                              controller: last,
                                              decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: " Last Name",
                                                  hintStyle:
                                                  TextStyle(color: AppColors.black),
                                                  contentPadding: EdgeInsets.only(
                                                    left: 10,
                                                  ),
                                                  counterText: ""),
                                              keyboardType: TextInputType.text,

                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                              ),
                              // Flexible(
                              //   flex: 1,
                              //   child: Container(
                              //     margin: EdgeInsets.only(
                              //         left: 2.5
                              //       ),
                              //       decoration: BoxDecoration(
                              //           color: Colors.white,
                              //           borderRadius: BorderRadius.all(Radius.circular(10)),
                              //           border: Border.all(color: Colors.black12)),
                              //       child: Row(
                              //         children: [
                              //           Container(
                              //             padding: EdgeInsets.only(left: 20),
                              //             child: Icon(
                              //               Icons.perm_identity,
                              //               color: AppColors.redcolor3,
                              //             ),
                              //           ),
                              //           Expanded(
                              //             child: Container(
                              //               width: 250,
                              //               child: TextField(
                              //                 autofocus: true,
                              //                 controller: lname,
                              //                 decoration: InputDecoration(
                              //                     border: InputBorder.none,
                              //                     hintText: "Last Name",
                              //                     hintStyle:
                              //                         TextStyle(color: AppColors.redcolor3),
                              //                     contentPadding: EdgeInsets.only(
                              //                       left: 10,
                              //                     ),
                              //                     counterText: ""),
                              //                 keyboardType: TextInputType.text,
                              //                 maxLength: 10,
                              //               ),
                              //             ),
                              //           ),
                              //         ],
                              //       )
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            // Container(
            //   child: Container(
            //     padding: EdgeInsets.only(right: 0,top: 20,left: 0,bottom: 10),
            //
            //
            //     child: SizedBox(
            //       child: InkWell(
            //         onTap: (){
            //           // Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginLoader()));
            //         },
            //
            //         child:
            //         ListTile(
            //             title: Text("Email"),
            //             subtitle:
            //             Card(
            //               child: Container(
            //                 padding: EdgeInsets.all(0.0),
            //                 height: 40,
            //                 width: 40,
            //                 decoration:  BoxDecoration(
            //                   // border: Border.all(
            //                   //  // color: AppColors.selectedItemColor,
            //                   //   style: BorderStyle.solid,
            //                   //   width: 1.0,
            //                   // ),
            //                   borderRadius: BorderRadius.all(Radius.circular(1)),
            //                   color: Colors.white,
            //
            //                 ),
            //                 child: TextField(
            //                   controller: email,
            //                   //controller: mobile,
            //                   decoration: InputDecoration(
            //                       border: InputBorder.none,
            //
            //                       //hintText: "Mobile Number",
            //                       hintStyle: TextStyle(color: Colors.grey[400]
            //                       ),
            //                       contentPadding: EdgeInsets.only(
            //                         left: 10,
            //                       ),
            //                       counterText: ""
            //
            //                   ),
            //                   //  maxLength: 10,
            //
            //                 ),
            //
            //               ),
            //             )
            //         ),
            //
            //
            //       ),
            //     ),
            //   ),
            // ),
            Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.black12)
                        ),
                        child: Row(
                          children: [
                            // Container(
                            //   padding: EdgeInsets.only(left: 20),
                            //   child: Icon(
                            //     Icons.email,
                            //     color: AppColors.redcolor3,
                            //   ),
                            // ),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              width: 250,
                              child: TextField(
                                autofocus: true,

                                controller: email,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText:"Email",
                                    //"Mobile Number",
                                    //widget.mobilenumber.toString(),
                                    hintStyle:
                                    TextStyle(color: AppColors.black),
                                    contentPadding: EdgeInsets.only(
                                      left: 10,
                                    ),
                                    counterText: ""),


                              ),
                            ),
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),





            Container(
              child: Container(
                padding: EdgeInsets.only(right: 10,top: 10,left: 10,bottom: 10),


                child: SizedBox(
                  child: InkWell(
                    onTap: (){
                      getUpdateprofile();
                      // if(firstnam.text.length==0)
                      // {
                      //   alertDailog("Please enter name");
                      //
                      // }else{
                      //   getSignup();
                      // }

                    },

                    child: Container(

                      height: 45,
                      // width: 20,
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: AppColors.sentColor,
                            style: BorderStyle.solid,
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: AppColors.sentColor
                      ),


                      child: Center(
                        child: Text(
                          loading == true ? "Loading.." :
                          "Update",style: TextStyle(
                            color : Colors.white,
                            fontSize: 19.0,
                            fontWeight: FontWeight.w400
                        ),

                        ),
                      ),
                    ),
                  ),
                ),
              ),

            ),

          ],
        ),
      ),

    );
  }

}