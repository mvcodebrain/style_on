import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;
import 'dart:async';
 import 'package:http/http.dart' as http;

class LocationPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return LocationLoader();
  }

}

class LocationLoader extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LocationView();
  }

}

class LocationView extends State<LocationLoader>{
  List places;
  var placecounts = 0;
  Future getLocations(String locationName) async{
    var kGoogleApiKey = "AIzaSyAyRY9zVqzDFo8Qc2puq8T6gYiTARTdnNo";
    // var url = Uri.tryParse("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="+locationName+"&inputtype=textquery&fields=formatted_address,geometry&location=20.7711857,73.729974&radius=10000&key="+kGoogleApiKey);
    var url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="+locationName+"&inputtype=textquery&fields=formatted_address,geometry&location=20.7711857,73.729974&radius=10000&key="+kGoogleApiKey;
    http.Response res = await http.get(Uri.parse(url));
    setState(() {
      places = json.decode(res.body)['candidates'];
      placecounts = places.length;
    });
//    print(res.body);
  }

  Future _setLocation(locations,lattitude,longitude) async{
    var sp = await SharedPreferences.getInstance();
    sp.setString("location", locations);
    sp.setString("lat", lattitude.toString());
    sp.setString("log", longitude.toString());
    Navigator.pop(context);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: new TextField(
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Search your location",
              hintStyle: TextStyle(
                fontFamily: "opensan",
                fontWeight: FontWeight.bold
              )
            ),
            onChanged: (value){
              getLocations(value);
            },
            autofocus: true,
          ),
          leading: IconButton(
            icon: Icon(
                AntDesign.arrowleft,
              color: Colors.black,
            ),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          elevation: 0,
        ),
        body: new Container(
          child: Container(
            child: new ListView.builder(
              itemCount: placecounts,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 50,
                  child: ListTile(
                    onTap: (){
                      var locations = places[index]['formatted_address'];
                      var lattitude = places[index]['geometry']['location']['lat'];
                      var longitude = places[index]['geometry']['location']['lng'];
                      _setLocation(locations,lattitude,longitude);
                    },
                    title: new Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                            right : 10
                          ),
                          child: Icon(
                              MaterialIcons.location_on
                          ),
                        ),
                        Flexible(
                          child: Text(
                            places[index]['formatted_address'],
                            style: TextStyle(
                              fontFamily: "opensan",
                            ),
                            maxLines: 1,
                          ),
                        )
                      ],
                    ),
                  )
                );
              },
            ),
          ),
        ),
      ),
    );
  }

}