
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/seatbooking/Home.dart';
import 'package:yesmadam/services/apis.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'Home.dart';


class RatingPage extends StatefulWidget {
   final booking_id;
   RatingPage( this.booking_id);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RatingPageLoader();
  }
}

class RatingPageLoader extends State<RatingPage> {
  final _ratingController = TextEditingController();
  var review = TextEditingController();
  int _rating;

  int _ratingBarMode = 1;
  bool _isRTLMode = false;
  alertDailog(text) async{
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: (){
                  Navigator.of(context).pushAndRemoveUntil(
                      SlideTopRoute(page: Home1()), (Route<dynamic> route) => false);

                },
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }
  alertDailoga(text) async{
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: (){
                  Navigator.pop(context);

                },
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.only(
                  left:20,
                  right:20,
                  top : 20,
                  bottom: 20
              ),
              child : new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(
                  ),
                  new Container(
                    padding : EdgeInsets.only(
                        left:30
                    ),
                    child: new Text("Please wait..."),
                  )
                ],
              )
          ),
        );
      },
    );
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future getrating() async {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });

    var url = Apis.RATING_API;
    // var data = json.encode(map);
    var data = {

      "user_id":sp.getString("userid").toString(),
      "booking_id":widget.booking_id,
      "rating_point":"$_rating",
      "review":review.text

    };

    //    "rating":"$_rating",
    //  "customer_id":sp.getString("userid").toString(),
    // // "saloon_id":widget.saloon_id,
    //  "service_id":"",
    //  "title":"",
    //  "comments":""};
    //

    print(data.toString());
    var res = await apiPostRequest(url,data);
    sp.containsKey("userid");

    print(res);
    setState(() {

      loading = false;
    });
    var chekExist = json.decode(res)['status'].toString();
    print(chekExist);
  }

  Future<String> apiPostRequest(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);

    return reply;
  }



  @override
  void initState() {

    super.initState();
    // getrating();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        title: Text('Rating'),

      ),
    body:  Directionality(
  textDirection: _isRTLMode ? TextDirection.rtl : TextDirection.ltr,
  child: SingleChildScrollView(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 40.0,
        ),
        Container
          (
          padding: EdgeInsets.only(left: 20),
            child: _heading('Rating')),
        Container(

            child:
            Center(child: _ratingBar(
                _ratingBarMode
            ))),
        SizedBox(
          height: 20.0,
        ),
        Container(




          child: _rating != null
              ?
          Text(
            'Rating: $_rating',
            style: TextStyle(fontWeight: FontWeight.bold),
          )
              : Container(),
        ),


        Container(
          child: Container(
            padding: EdgeInsets.only(right: 0,top: 20,left: 0,bottom: 10),


            child: SizedBox(
              child: InkWell(
                onTap: (){
                  // Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginLoader()));
                },

                child:
                ListTile(
                    title:
                    Center(child: Text("Review",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),)),
                    subtitle:
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(0.0),
                        height: 140,
                        width: 40,
                        decoration:  BoxDecoration(
                          // border: Border.all(
                          //  // color: AppColors.selectedItemColor,
                          //   style: BorderStyle.solid,
                          //   width: 1.0,
                          // ),
                          borderRadius: BorderRadius.all(Radius.circular(1)),
                          color: Colors.white,

                        ),
                        child: TextField(
                          controller: review,
                          //controller: mobile,
                          decoration: InputDecoration(
                              border: InputBorder.none,

                              //hintText: "Mobile Number",
                              hintStyle: TextStyle(color: Colors.grey[400]
                              ),
                              contentPadding: EdgeInsets.only(
                                left: 10,
                              ),
                              counterText: ""

                          ),
                          //  maxLength: 10,

                        ),

                      ),
                    )
                ),


              ),
            ),
          ),
        ),

        Container(
          padding: EdgeInsets.only(top: 10),
          height: 65,
          width: MediaQuery.of(context).size.width,
          child: InkWell(
            onTap: (){
              if(_rating==null){
                alertDailoga("Please Select Ratings  ");
              }
              else{
                getrating();
                alertDailog("Thanks for Ratings!!! ");
              }
            },
            child: Card(
              color: AppColors.black,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)
              ),
              child: Container(
                //color: AppColors.black,
                // height: 50,
                // width: 50,
                padding: EdgeInsets.only(top: 0,left: 10),
                child:
                Center(
                  child: Text(
                    "Submit",style: TextStyle(fontSize: 20,color: AppColors.white),

                  ),
                ),
              ),
            ),
          ),
        )










      ],
    ),
  ),
),

    );
  }


  Widget _ratingBar(int mode) {


    return RatingBar.builder(


      initialRating: 3,
      direction:  Axis.horizontal,
      itemCount: 5,
      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
      itemBuilder: (context, index)
      {

        switch (index) {
          case 0:
            return Icon(
              Icons.star,
              //sentiment_very_dissatisfied,
              color: Colors.red,
            );
          case 1:
            return Icon(
              Icons.star,
              //sentiment_dissatisfied,
              color: Colors.redAccent,
            );
          case 2:
            return Icon(
              Icons.star,
              //sentiment_neutral,
              color: Colors.amber,
            );
          case 3:
            return Icon(
              Icons.star,
              //sentiment_satisfied,
              color: Colors.lightGreen,
            );
          case 4:
            return Icon(
              Icons.star,
              //sentiment_very_satisfied,
              color: Colors.green,
            );
          default:
            return Container();
        }
      },
      onRatingUpdate: (rating) {
        setState(() {
          _rating = rating.toInt();
        });
      },
      updateOnDrag: true,
    );

    }
  }



  Widget _heading(String text) => Column(
    children: [
      Text(
        text,
        style: TextStyle(
          fontWeight: FontWeight.w300,
          fontSize: 24.0,
        ),
      ),
      SizedBox(
        height: 20.0,
      ),
    ],
  );


