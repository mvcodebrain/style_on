class Apis{
  // static const String BASE_URL = "https://styleon.thedigitalkranti.com/api/";
  static const String BASE_URL = "https://styleonindia.com/api/";
  static const String LOGIN_API = BASE_URL + "customer/auth/signup";
  static const String OTP_API = BASE_URL + "customer/auth/check_otp";
  static const String CATEGORY_API = BASE_URL + "home/Category/getCategory";
   static const String SLIDER_API = BASE_URL + "home/slider/homeSlider";
   static const String SUBCATEGORY_API = BASE_URL + "home/Category/getSubCategory";
  static const String SUBSUBCATEGORY_API = BASE_URL + "home/Category/getSubSubCategory";
   static const String GETSERVICE_API = BASE_URL + "customer/Services/serviceList";
  static const String PRODUCT_API = BASE_URL + "customer/Product/getProductList";
  static const String CATEGORYPRODUCT_API = BASE_URL + "customer/Product/getCategory";
  static const String CATPRODUCTDETAIL_API = BASE_URL + "customer/Product/getProductDetails";
  // static const String WALLET_API = BASE_URL + "customer/User/wallet";
  static const String WALLET_API = BASE_URL + "customer/User/getWalletAmount";
  static const String PROFILE_API = BASE_URL + "home/Home/myProfile";
  static const String ADDTOCART_API = BASE_URL + "home/Home/addTocart";
  static const String REMOVETOCART_API = BASE_URL + "home/Home/remove_cart";
  static const String CARTLIST_API = BASE_URL + "home/Home/cart";
  static const String COUPON_API = BASE_URL + "home/Home/couponApply";
  static const String ADDON_API = BASE_URL + "customer/Services/addOn";
  static const String SAVEADDON_API = BASE_URL + "customer/Services/addOnSave";
  // static const String GETDATESLOT_API = BASE_URL + "home/Home/dateList";
  static const String GETDATESLOT_API = BASE_URL + "home/Home/getTimeList";
  // static const String GETTIMESLOT_API = BASE_URL + "home/Home/getTimeList";
  static const String UPDATECARTLIST_API = BASE_URL + "home/Home/update_cart";
  static const String ADDRESSLIST_API = BASE_URL + "home/Home/getaddresslist";
  static const String ADDADDRESSLIST_API = BASE_URL + "home/Home/addresslist";
  static const String DELETEADDRESSLIST_API = BASE_URL + "home/Home/deleteAddress";
  static const String BOOKINGLIST_API = BASE_URL + "customer/booking";
  static const String PRODUCTADDTOCART_API = BASE_URL + "customer/Product/addToCart";
  static const String CHECKUSER_API = BASE_URL + "home/Home/checkUserActive";
  static const String EMAIL_API = BASE_URL + "home/Home/email";
  static const String WHATAPP_API = BASE_URL + "customer/Services/getWhatsappNo";
  static const String CALL_API = BASE_URL + "customer/Services/getMobileNo";
  static const String REMOVEPRODUCTADDTOCART_API = BASE_URL + "customer/Product/deleteCartElement";
  static const String PRODUCTSLIDER_API = BASE_URL + "customer/Product/getProductSlider";
  static const String PRODUCTCARTLIST_API = BASE_URL + "customer/Product/cartList";
  static const String UPDATEDCARTPRODUCT_API = BASE_URL + "customer/Product/addToCart";
  static const String TIMEPRODUCT_API = BASE_URL + "customer/Order/getDeliveryTime";
  static const String PRODUCTORDERPLACE_API = BASE_URL + "customer/Order/placeOrder";
  static const String ORDERHISTORY_API = BASE_URL + "customer/Order/getOrderHistory";
  static const String CANCELPRODUCTORDER_API = BASE_URL + "customer/Order/cancelOrder";
  static const String ORDERHISTORYDETAIL_API = BASE_URL + "customer/Order/getOrderDetail2";
  static const String BOOKINGHISTORY_API = BASE_URL + "customer/booking/getBookingHistory";
  static const String BOOKINGHISTORYDETAIL_API = BASE_URL + "customer/booking/getbookingById";
  static const String CANCELBOOKINGDETAIL_API = BASE_URL + "customer/booking/calcelBookingUser";
  static const String UPDATEPROFILE_API = BASE_URL + "customer/auth/updateUserProfile";
  static const String RATING_API = BASE_URL + "customer/auth/giveRatingToVendor";
  static const String SERVICEDETAIL_API = BASE_URL + "customer/Services/getServiceDetail/";
  static const String TESTIMONIAL_API = BASE_URL + "customer/auth/testimonialData";
  static const String NOTIFICATION_API = BASE_URL + "customer/User/inAppNotification";
  static const String MINIMUMAMOUNTPRICE_API = BASE_URL + "home/Home/getMinimumOrderAmount";
  static const String RECENTVENDOR_API = BASE_URL + "home/Home/lastSErvedVendor";
  static const String RESCHEDULETIME_API = BASE_URL + "customer/booking/rescheduleBookingUser";
  static const String ONGOINGBOOKINGHISTORY_API = BASE_URL + "customer/booking/onGoingBooking";
  static const String VENDORPROFILE_API = BASE_URL + "home/Home/getVendorProfile";
  static const String REFERCODE_API = BASE_URL + "customer/auth/getRefferalCode";
  static const String CHAT_API = BASE_URL + "home/Help_Support/userChat";
  static const String CancellationReason_API = BASE_URL + "home/Home/getCancellationReason";
  static const String PAAGEVIEW_API = BASE_URL + "customer/Content/getContentlist";
  static const String CouponLISTS_API = BASE_URL + "customer/Product/getcoupon";
  static const String WALLETADD_API = BASE_URL + "customer/Booking/add_balance_from_wallet";
  static const String Blocktimedate = BASE_URL + "home/Home/get_reschedule_time_list";
  static const String Servicepaymentid = BASE_URL + "customer/Booking/update_payment";
  static const String Signupname = BASE_URL + "customer/Auth/signup_name";
  static const String Productpaymentid = BASE_URL + "customer/Order/update_payment";
  static const String Payafternow = BASE_URL + "customer/booking/pay_after_service/";
  static const String Payafter = BASE_URL + "customer/Booking/update_payment_for_pay_after_service";


  static const String app_name = "Style on";
  static const String payment_key =
  "rzp_test_1DP5mmOlF5G5ag"
      ;
  static const String package = "com.style.on";
  static const String registerpackage = "com.styleon.vendor";









  //booking sheet

  static const String PINCODE_API = BASE_URL + "customer/saloon/find";
  static const String GETDETAIL_API = BASE_URL + "customer/saloon/services";
  // static const String GETTIMESLOT_API = BASE_URL + "customer/saloon/bussinesstime";
  static const String GETTIMESLOT_API = BASE_URL + "customer/Services/calculate_time_using_serviece";
  static const String BOOKING_API = BASE_URL + "customer/saloon/booking";
  static const String BOOKINGHISTORY_API2 = BASE_URL + "customer/booking/mybookinghistory";
  static const String CANCEL_API2 = BASE_URL + "customer/booking/cancelmybooking";
  static const String BOOKINGHISTORYdetalis_API2 = BASE_URL + "customer/booking/mybookingdetails";
  static const String RATING2_API = BASE_URL + "customer/saloon/rateing";
  static const String GETSLIDERS_API = BASE_URL + "home/slider/homeSlider";
  static const String saloonCATEGORY_API = BASE_URL + "/home/Category/getCategory";


















}