


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class Allview extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AllviewsLoader();
  }

}
class AllviewsLoader extends State<Allview>{
  static const maxCount = 100;
  AutoScrollController controller;
  final scrollDirection = Axis.vertical;
  List<List<int>> randomList;

  @override
  void initState() {
    super.initState();
    controller = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: scrollDirection);
    // randomList = List.generate(maxCount,
    //         (index) => <int>[index, (1000 * random.nextDouble()).toInt()]);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        child: ListView.builder(
          controller: controller,
          padding: EdgeInsets.only(
              top: 10,
              left: 20,
              right: 20
          ),
          //        shrinkWrap: true,
          //        physics: NeverScrollableScrollPhysics(),
          itemCount: maxCount,
          itemBuilder: (context,i){
            return AutoScrollTag(
              key: ValueKey(i),
              controller: controller,
              index: i,
              highlightColor: Colors.black.withOpacity(0.1),
              child: Container(
                padding: EdgeInsets.only(
                    bottom: 10
                ),
                child: Container(
                  padding: EdgeInsets.only(
                      bottom: 10
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(10)
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black45
                        )
                      ]
                  ),
                  child: Container(
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              left: 10,
                              right: 10,
                              top: 10
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Container(
                                  child: Column(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(
                                            bottom: 10
                                        ),
                                        child: Row(
                                          children: [
                                            Text(
                                              "Sanad S.",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 17
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Flexible(child: Text(
                                              "Tower 1, ATS Greens Paradiso,"
                                                  "Lagerstroemia Estate, Chi IV, Greate "
                                                  "Noida, Utter Pradesh 201310, India",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14
                                              ),
                                            ))
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  height: 50,
                                  //                                          color: Colors.blueAccent,
                                  //                                          padding: EdgeInsets.only(
                                  //                                            left: 15
                                  //                                          ),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          left: BorderSide(
                                              color: Colors.black12
                                          )
                                      )
                                  ),
                                  child: Column(
                                    //                                            mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      new Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                                right: 5
                                            ),
                                            child: Icon(
                                              Icons.wb_incandescent,
                                              size: 15,
                                            ),
                                          ),
                                          Text(
                                            "3:00 PM",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.w500
                                            ),
                                          )
                                        ],
                                      ),
                                      i == 1?Container(
                                        padding: EdgeInsets.only(
                                            top: 5
                                        ),
                                        child: Text(
                                          "Today".toUpperCase(),
                                          style: TextStyle(
                                              color: Colors.black45
                                          ),
                                        ),
                                      ):i == 3?Container(
                                        padding: EdgeInsets.only(
                                            top: 5
                                        ),
                                        child: Text(
                                          "Yesterday".toUpperCase(),
                                          style: TextStyle(
                                              color: Colors.black45
                                          ),
                                        ),
                                      ):Container(
                                        padding: EdgeInsets.only(
                                            top: 5
                                        ),
                                        child: Text(
                                          "Tomorrow".toUpperCase(),
                                          style: TextStyle(
                                              color: Colors.black45
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        i == 1 ?Container(
                          padding: EdgeInsets.only(
                              top: 15,
                              left: 10,
                              right : 10
                          ),
                          child: Row(
                            children: [
                              Container(
                                padding:EdgeInsets.only(
                                    left: 10,
                                    right: 10,
                                    top: 5,
                                    bottom: 5
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(120)
                                    )
                                ),
                                child: Text(
                                  "Customer Unreachable - Cancel Request".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      shadows: [
                                        Shadow(
                                          color: Colors.black,
                                          blurRadius:1,
                                        )
                                      ]
                                  ),
                                ),
                              )
                            ],
                          ),
                        ):Container(),
                        i == 3 ?Container(
                          padding: EdgeInsets.only(
                              top: 15,
                              left: 10,
                              right : 10
                          ),
                          child: Row(
                            children: [
                              Container(
                                padding:EdgeInsets.only(
                                    left: 10,
                                    right: 10,
                                    top: 5,
                                    bottom: 5
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(120)
                                    )
                                ),
                                child: Text(
                                  "Job Ended".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      shadows: [
                                        Shadow(
                                          color: Colors.black,
                                          blurRadius:1,
                                        )
                                      ]
                                  ),
                                ),
                              )
                            ],
                          ),
                        ):Container()
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _scrollToIndex,
        tooltip: 'Increment',
        child: Text(counter.toString()),
      ),
    );

  }

  int counter = -1;
  Future _scrollToIndex() async {
    setState(() {
      counter++;

      if (counter >= maxCount) counter = 0;
    });

    await controller.scrollToIndex(90,
        preferPosition: AutoScrollPosition.begin);
    controller.highlight(90);
  }


}