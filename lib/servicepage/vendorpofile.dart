


import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/services/apis.dart';

class vendorprofile extends StatefulWidget{
  final  vendorid;
  vendorprofile(this.vendorid);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return vendorprofileview();
  }

}
class vendorprofileview extends State<vendorprofile>{
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var vendorprofile="";
  var vendorimage="";
  var vendorname="";
  double rating=0.0;
  var totalrating="0";
  List review=[];
  var phone ="";
  var ratingrecod="0";

  Future getvendorlist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.VENDORPROFILE_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "vendor_id":widget.vendorid
    };
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        // vendorprofile = json.decode(res)['data'];
        vendorimage = json.decode(res)['data']['image'];
        vendorname = json.decode(res)['data']['username'];
        //  rating = json.decode(res)['data']['average_rating']);
        ratingrecod  = json.decode(res)['data']['average_rating'].toString().replaceRange(1,json.decode(res)['data']['average_rating'].length, "."+json.decode(res)['data']['average_rating'][2]);
        print(ratingrecod);
        totalrating = json.decode(res)['data']['total_booking'].toString();
        review = json.decode(res)['data']['reviews'];
        phone = json.decode(res)['data']['mobile'];
      } else {

      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }
  @override
  void initState() {
    getvendorlist();

    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Container(
          child: Text("Vendor Profile"),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              color: AppColors.white,
              child: Column(
                children: [
                  Center(
                    child:
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 100,
                      width: 100,
                      //width: MediaQuery.of(context).size.width/2.2,
                      decoration: BoxDecoration(
                          color: AppColors.grayBorder,
                          border: Border.all(color: Colors.white),
                          borderRadius:
                          BorderRadius.all(Radius.circular(120)),
                          image: DecorationImage(

                              image: NetworkImage("$vendorimage")==null?"": NetworkImage("$vendorimage")
                              ,

                              fit: BoxFit.fill)
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 0,top: 5),
                    child: Container(
                      child: Text(vendorname==null?"":vendorname,
                        style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 0,top: 5),
                    child: Container(
                      child: Text(phone==null?"":phone,
                        style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5,bottom: 10,left: 40,right: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.only(left: 0,bottom: 5),

                                        color:Colors.white,
                                        child:
                                        Icon(Icons.star,size: 25,color: AppColors.redcolor3,)
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 5,bottom: 5),
                                      child: Text(
                                        ratingrecod==null?"0":ratingrecod,style: TextStyle(
                                        color: AppColors.redcolor3,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                      ),


                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                child: Text("Rating",style: TextStyle(color: AppColors.grayBorder,fontSize: 16),),
                              )
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 5,bottom: 5),
                                child: Text(
                                  totalrating==null?"0":totalrating,style: TextStyle(
                                  color: AppColors.redcolor3,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),


                                ),
                              ),
                              Container(
                                child: Text("Booking",style: TextStyle(color: AppColors.grayBorder,fontSize: 16),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    color: AppColors.grayBorder,
                    padding: EdgeInsets.only(top: 5,bottom: 5),


                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10,bottom: 10),
                    child: Text("Most helpful reviews",style: TextStyle(
                        fontWeight: FontWeight.bold,fontSize: 20
                    ),),

                  ),
                  Container(
                    height: 2,
                    color: AppColors.grayText,
                    padding: EdgeInsets.only(top: 1,bottom: 1),


                  ),
                  Container(

                    child: vendorrating(),
                  )



                ],
              ),

            ),
          )
        ],
      ),
    );
  }
  Widget vendorrating(){
    return Container(
        color: AppColors.white,
        child: ListView.builder(shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount:review.length==null?"":review.length,
            itemBuilder: (BuildContext context, int i){
              return Container(
                margin: EdgeInsets.only(top: 5,left: 10),
                //height: 200,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Container(
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            flex: 1,
                            child: Container(
                              width: 60,
                              height: 60,
                              decoration: BoxDecoration(

                                  color: AppColors.white,
                                  border: Border.all(color: AppColors.grayBorder),
                                  // boxShadow: [
                                  //   BoxShadow(
                                  //       color: Colors.black,
                                  //       spreadRadius: 1,
                                  //       blurRadius: 10
                                  //   )
                                  // ],

                                  borderRadius: BorderRadius.all(
                                      Radius.circular(120)

                                  ),
                                  image: DecorationImage(
                                    image: review[i]['image']==""?AssetImage("assets/images/man.png"):NetworkImage(review[i]['image']=="https://www.pngfind.com/pngs/m/608-6087418_man-icon-free-icon-man-hd-png-download.png"?"":review[i]['image']),
                                  )

                              ),

                            ),
                          ),
                          Flexible(
                            flex: 3,
                            child: Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                children: [
                                  Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Text(review[i]['full_name']==null?"":review[i]['full_name'],style: TextStyle(fontWeight: FontWeight.bold),),
                                        ),
                                        Container(
                                          child: Text(review[i]['date_time']==null?"":review[i]['date_time'],style: TextStyle(color: AppColors.grayBorder),),
                                        ),

                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left:0),
                                    child: Row(


                                      children: [
                                        Container(

                                            color:Colors.white,
                                            child:
                                            Icon(Icons.star,size: 18,color: AppColors.redcolor3,)
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Text(
                                            review[i]['rating_point']==null?"":review[i]['rating_point'],style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14,
                                          ),


                                          ),
                                        )
                                      ],
                                    ),


                                  ),

                                ],
                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5,bottom: 5,left: 70),
                      child: Text("'"+review[i]['review']==null?"":""+review[i]['review'],style: TextStyle(
                          color: AppColors.grayBorder
                      ),),
                    ),
                    Container(
                      color: AppColors.grayText,
                      height: 1,
                    )
                  ],
                ),

              );
            }

        ));
  }

}