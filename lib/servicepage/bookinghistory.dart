
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:yesmadam/color/AppColors.dart';

class Bookinghistory extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return BookinghistoryLoader();
  }
}
class BookinghistoryLoader extends State<Bookinghistory>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title:Container(
          child: Text("Booking Details"),
        ),



      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child:
            Container(

                child: data()),

          )
        ],
      ),


    );
  }
  Widget data(){

    return Container(

      padding: EdgeInsets.only(top: 5,bottom: 5,right: 5,left: 5),
      child: Card(
        child: Container(
            child:Column(
              // crossAxisAlignment: CrossAxisAlignment.,
              children: [
                Container(
                  padding: EdgeInsets.only(top: 10,bottom: 10,left: 5,right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text("Order Date: 18 Mar 2021"),
                      ),
                      Container(
                        child: Text("Order Id: 18 Mar 2021"),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5,bottom: 5,left: 5,right: 5),
                  color: AppColors.grayBorder,
                  height: 1,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 15,bottom:8),
                      child: Text("Name"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 10, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "Sonu",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Address"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "jh,mmgkhhjvbhjvhjvjhvhvhv hvjhvhvhvvhgftyftftfgvgfsshsgystrjbhhgbhyy",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(top: 5,bottom: 5,left: 5,right: 5),
                  color: AppColors.grayBorder,
                  height: 1,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Service Type".toUpperCase()),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "STANDARD",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("ADD ON".toUpperCase()),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "STANDARD",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Category"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "PREMIUM MALE MASSAL",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Service Minutes"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "60 Min",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Booking Date"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "Thu,18Mar-2021",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Booking Time"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "9.00AM",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(top: 5,bottom: 5,left: 5,right: 5),
                  color: AppColors.grayBorder,
                  height: 1,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Payment Medium"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "CASH ON DELIVERY",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top:5,bottom:8),
                      child: Text("Total Amount"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 5, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "1555",
                                    style: TextStyle(
                                      color: AppColors.sentColor,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10,top: 5,bottom:8),
                      child: Text("Status"),alignment: Alignment.topLeft,
                    ),
                    Container(
                      //height: 25,
                      width: MediaQuery.of(context).size.width/2.2,
                      // color: AppColors.sentColor,
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 25, top: 05, right: 10,bottom: 5),
                                //color: AppColors.primary,
                                child:
                                RichText(
                                  text: new TextSpan(
                                    text: "Done",
                                    style: TextStyle(
                                      color: AppColors.grayBorder,
                                      fontSize: 12,
                                    ),
                                  ),
                                  textAlign: TextAlign.left,
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),



              ],
            )
        ),
      ),
    );
  }
}
