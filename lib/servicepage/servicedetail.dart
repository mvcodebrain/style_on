import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/login.dart';
import 'package:yesmadam/servicepage/servicedetailp.dart';
import 'package:yesmadam/services/apis.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';

import 'addons.dart';
import 'cartitem.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'dart:math' as math;
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

const numberOfItems = 5001;
const minItemHeight = 20.0;
const maxItemHeight = 150.0;
const scrollDuration = Duration(seconds: 2);

const randomMax = 1 << 32;
// class Servicebloc extends StatelessWidget {
//   final categoryname;
//   final catid;
//   final List list;
//   final catindex;
//
//
//   Servicebloc(
//     this.categoryname,
//     this.catid,
//     this.list,
//       this.catindex,
//
//   );
//   @override
//   Widget build(BuildContext context) {
//
//     // TODO: implement build
//     return MultiBlocProvider(
//         providers: [
//           BlocProvider(
//             create: (BuildContext context) =>
//                 SubcategoryBloc(repos: SubcategoryRepoImp(), catid: this.catid),
//             child: ServiceLoader(this.categoryname, this.catid, this.list, this.catindex),
//           ),
//           BlocProvider(
//             create: (BuildContext context) => SersubcategoryBloc(
//                 repos: ServsubcategoryReposImp()), //,subId: this.subId),
//             child: ServiceLoader(this.categoryname, this.catid, this.list, this.catindex),
//           )
//         ],
//         child: ServiceLoader(this.categoryname, this.catid,
//             this.list, this.catindex,)); // ServiceLoader(this.categoryname, this.id, this.list);
//   }
// }

class ServiceLoader extends StatefulWidget {
  final categoryname;
  final catid;
  final term;
  final termdetail;


  ServiceLoader(this.categoryname, this.catid,this.term,this.termdetail );
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ServiceView();
  }
}

class ServiceView extends State<ServiceLoader>with SingleTickerProviderStateMixin {
  final scrollDirection = Axis.vertical;

  var controller = ScrollController();

  ItemScrollController _scrollController = ItemScrollController();
  final ItemPositionsListener itemPositionListener = ItemPositionsListener.create();

  TabController _tabController;
  var indextab = 0;
  List services = [];
  var data="";

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  //var subcat = "";
  var serviceid="";
  var price ="";
  //var cart_staus ="";
  List cartlist =[];
  //List servlists = [];
  List subsubcat=[];
  var subcatid="";
  var subsubid="";
  var totalprice= "0";
  var catid;
  List subcatss=[];
  var status="";

  void getsubcat() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.SUBCATEGORY_API;
    var sp = await SharedPreferences.getInstance();

    var data = { "category_id": widget.catid,
    };

    // print(widget.catindex);
    var res = await apiPostRequestsdd(url,data);
    print(res);



    setState(() {
      if(json.decode(res)['status'].toString() == "1"){

        subcatss = json.decode(res)['data'];
        subcatid=json.decode(res)['data'][0]['id'];
        print(subcatid);
        status=json.decode(res)['status'].toString();
        print(status);



      }
      loading = false;
    });

    _tabController = TabController(length: subcatss.length,vsync: this);
    _tabController.addListener(()
    {
      setState(() {
        services = [];
      });
      getserviceOntabChange(_tabController.index);

    });
    getsubsubcat();
  }
  Future<String> apiPostRequestsdd(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future getsubsubcat() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.SUBSUBCATEGORY_API;
    var sp = await SharedPreferences.getInstance();

    var data = {
      "sub_cat_id":subcatid

    };
    print(data);
    var res = await apiPostRequested(url,data);
    print(res);


    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        subsubcat = json.decode(res)['data'];
        subsubid=json.decode(res)['data'][0]['id'];
        print(subsubid);
      }

      loading = false;
    });
    getProductdetail();
  }
  Future<String> apiPostRequested(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getProductdetail() async {
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.GETSERVICE_API;
      var data = {

        "user_id":user,
        "cat_id":subcatid
        // "sub_sub_id":subsubid,
        // "limit":"100",
        // "page":"0"


        // "user_id":user,
        // 'cat_id': widget.selected,
      };
      print(data);

      var res = await apiPostRequests(url, data);

      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          services = json.decode(res)['data'];
        });
        print(services);
      }
      else {
        setState(() {
          services = [];
        });
      }



    }
    else{
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
        // Navigator.pop(context);
      });

      var url = Apis.GETSERVICE_API;
      // var data = json.encode(map);
      var data = {
        "cat_id":subcatid
        // "sub_sub_id":subsubid,
        // "limit":"100",
        // "page":"0"
      };
      print(data);

      var res = await apiPostRequests(url, data);

      print(res);
      setState(() {
        if (json.decode(res)['status'].toString() == "1")
        {
          services = json.decode(res)['data'];
          print(services);
        }
        else{
          services = [];

        }
        loading = false;
      });

    }
    // controller.addListener(() {
    // controller.
    _scrollListener();
    // });
  }

  _scrollListener() async{
    if (controller.offset >= controller.position.maxScrollExtent &&
        !controller.position.outOfRange) {
      setState(() {
        controller.position.moveTo(12,curve:Curves.easeInBack ,clamp: true);
        debugPrint("reach the top");
      });
    }
    if (controller.offset <= controller.position.minScrollExtent &&
        !controller.position.outOfRange) {
      setState(() {
        debugPrint("reach the top top");
      });
    }
  }


  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }


  Future getAddtocart(serviceid,price) async {

    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.ADDTOCART_API;
      var data = {
        "service_id":serviceid,
        "user_id":  user,
        //sp.getString("userid"),
        "qty":"1",
        "amount"  :price ,
        "cat_id":catid

      };

      var res = await apiPostRequestss(url,data);
      print(res);
      print(data);

      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {


          loading = false;
        });
      }
      else
      {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
    getProductdetail();
    getCartlist();
  }
  Future<String> apiPostRequestss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future getRemovetocart(serviceid,price) async {

    var sp = await SharedPreferences.getInstance();

    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.REMOVETOCART_API;
      var data = {
        "service_id":serviceid,
        "user_id":  sp.getString("userid"),
        "cat_id":catid
      };

      var res = await apiPostRequestsss(url,data);
      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {
          loading = false;
        });
      }
      else
      {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
    getProductdetail();
    getCartlist();
  }
  Future<String> apiPostRequestsss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  var qutty="";

  Future getCartlist() async {


    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.CARTLIST_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "cat_id":catid,
      //catid,
      "user_id":  sp.getString("userid")  };
    var res = await apiPostRequestx(url,data);
    print(data);
    print(res);

    setState(() {
      // print("jhhhjj");
      if(json.decode(res)['status'].toString() == "1"){
        // print("12344");
        cartlist = json.decode(res)['response']['cartList'];
        totalprice=json.decode(res)['response']['total_amount'].toString();
        qutty=json.decode(res)['response']['cartList'][0]['qty'];
        print(qutty);

      }else{
        totalprice = "0";
      }

      loading = false;
    });
  }
  Future<String> apiPostRequestx(String url, data) async {
    //_onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    // Navigator.pop(context);
    return reply;
  }



  var catname = "";

  getCatName() async{
    var sp = await SharedPreferences.getInstance();
    setState(() {
      catname = widget.categoryname;
      catid = widget.catid;
      sp.setString("term", widget.term);
      sp.setString("termdetalis", widget.termdetail);

    });
  }
  var selectedind = 0;

  _selectsubcatindex(ind,id)async{
    setState(() {
      selectedind = ind;
      subsubid=id;
    });
    getProductdetail();

  }



  getserviceOntabChange(ind) async{
    //print("jjjjjhjhjh");


    setState(() {
      subcatid=subcatss[ind]['id'];
    });
    getsubsubcat();


  }

  Future<String> apiPostRequesth(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  Future getUpdateCartlist(qty,String prodid,int price) async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.UPDATECARTLIST_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "user_id":  sp.getString("userid"),
      "id":prodid,
      "qty":qty,
      "amount":price};
    print(data);
    var res = await apiPostRequestssss(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        print(cartlist);
      }
      else{

      }
      getProductdetail();
      getCartlist();
      loading = false;
    });
  }
  Future<String> apiPostRequestssss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  updateCart(int qty, String prodid,int price,type){

    if(qty  > 0){

      // print(qty+1);
      var qtys;
      var prices;
      if(type == "add"){
        qtys = qty+1;
        // prices = price*qtys;
      }else{
        qtys = qty-1;
        // prices = price*qtys;
        print(qtys);
      }
      if(qty  == 1 && type == "sub"){
        getRemovetocart(prodid,price);

      }else{
        getUpdateCartlist(qtys,prodid,price);


      }

    }
  }

  static const maxCount = 1000;
  // AutoScrollController controller;
  // final scrollDirection = Axis.vertical;
  final random = math.Random();
  List<List<int>> randomList;

  @override
  void initState() {

    super.initState();
    getsubcat();

    getCatName();

    getCartlist();
    itemPositionListener.itemPositions.addListener((){
      print(itemPositionListener.itemPositions.value.single.index.toString());
      setState(() {
        selectedind = itemPositionListener.itemPositions.value.single.index;
      });
    });

    // _tabController.animateTo(widget.catindex);

    // controller = AutoScrollController(
    //     viewportBoundaryGetter: () =>
    //         Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
    //     axis: scrollDirection);
    // randomList = List.generate(services.length,
    //         (index) => <int>[index, (1000 * random.nextDouble()).toInt()]);





  }

  _handleTabSelection() {
    // _tabController.animateTo(widget.catindex);


  }
  var bottomsheet=false;
  selectbottom() async{
    setState(() {

      bottomsheet = true;


    });
  }
  selectbottomff() async{
    setState(() {

      bottomsheet = false;


    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return subcatss.length == 0?Scaffold(
      backgroundColor: AppColors.dividerColor,

      body: Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    ):DefaultTabController(
        length: subcatss.length,
        child: Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: AppColors.white),
            backgroundColor: AppColors.sentColor,
            title: Text(
              //catname
              widget.categoryname.toUpperCase(),
              style: TextStyle(
                  fontWeight: FontWeight.w500, color: AppColors.white),
            ),

            actions: [
              // IconButton(
              //   onPressed: () {
              //     getAddtocart(serviceid,price);
              //     //getsubcat();
              //     // getsubsubcat();
              //     // getProductdetail();
              //     //getCartlist();
              //   },
              //   icon: Icon(
              //     Icons.shopping_cart,
              //     color: AppColors.white,
              //   ),
              // ),
            ],
            //toolbarHeight: 60,

            bottom: PreferredSize(
              child: Container(
                color: AppColors.sentColor,
                child: TabBar(
                  onTap: (i){


                  },
                  controller: _tabController,
                  isScrollable: true,
                  indicatorColor: AppColors.black,
                  labelColor: AppColors.black,
                  labelStyle: TextStyle(color: AppColors.priceControl),
                  unselectedLabelStyle: TextStyle(color: Colors.white),
                  unselectedLabelColor: Colors.white,
                  tabs: List.generate(subcatss.length, (i){
                    return Tab(
                      child:
                      Text(


                        // "",
                        subcatss[i]['sub_cat_name'],
                        style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                    );
                  }),
                ),
              ),
              preferredSize: Size.fromHeight(50),
            ),
          ),
          body: TabBarView(

            controller: _tabController,
            children: subcatss.map((i) {
              return Container(
                child: CustomScrollView(
                  slivers: [
                    SliverStickyHeader(
                        header: Container(
                            child: subcatgory()

                        ),
                        // SliverToBoxAdapter(
                        //   child: Container(
                        //     child: subcatgory()
                        //
                        //   ),
                        // ),
                        sliver:
                        SliverFillRemaining(
                            child:Container(
                              child: Servicelist(),
                            ))
                    )],
                ),
              );
            }).toList(),
          ),
          bottomNavigationBar
              :
          totalprice=="0"?Container(
            height: 0,
          ):
          Container(
            height: 110,
            child: Column(
              children: [
                Container(
                  width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(2)),
                    color: Color(0xFFFfff1ec),
                  ),
                  padding: EdgeInsets.only(left: 95, top: 4, bottom: 4),
                  child: Text(cartlist.length.toString()+"Items selected for order"),
                ),
                Container(
                  height: 75,
                  padding: EdgeInsets.only(left: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(2)),
                    color: Colors.white,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 0, top: 20),
                              child: Row(
                                children: [
                                  Text(
                                    "₹"+totalprice,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: (){
                                selectbottom();
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: 0, top: 5),
                                child: Text(
                                  "VIEW DETAILS",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xFFFff3f6c)),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(right: 10),
                          child: SizedBox(
                            child: InkWell(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>Addons(widget.catid,widget.categoryname)));
                              },
                              child: Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                                  color: AppColors.sentColor,
                                ),
                                child: Center(
                                  child: Text(
                                    "Check Out",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          bottomSheet:bottomsheet==false?Container(
            height: 1,
          ):
          SingleChildScrollView(
            child:

            Column(
              children: [


                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: (){
                        selectbottomff();

                      },
                      child: Container(
                          margin: EdgeInsets.only(top: 10, bottom: 10,left: 20,right: 50),
                          child: Icon(Icons.arrow_back,size: 30,)
                      ),
                    ),
                    Center(
                      child:
                      Container(
                        margin: EdgeInsets.only(top: 10, bottom: 10,left: 50),
                        child: Text(
                          "Details",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 10),
                  child:  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount:
                    //cartlist.length,
                    cartlist.length != null ?cartlist.length:"",
                    //services.length,
                    itemBuilder: (BuildContext context, int i) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Container(
                              padding:EdgeInsets.only(top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,

                                children: [
                                  Container(

                                      child:
                                      Text(
                                        cartlist[i]['tittle'] != null
                                            ? cartlist[i]['tittle']
                                            : "",
                                        //  cartlist[i]['tittle'],
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400
                                        ),
                                      )),
                                  Container(
                                    child: Row(
                                      children: [
                                        Container(
                                          padding:EdgeInsets.only(right: 10),
                                          child:  Text(

                                            cartlist[i]['service_price'] != null
                                                ? "₹" + cartlist[i]['service_price']
                                                : "",
                                            //cartlist[i]['service_price'], //+ //services[i]['data']['service'][index]['new_price'],
                                            style: TextStyle(
                                                color: AppColors
                                                    .receiveColor2,
                                                fontWeight:
                                                FontWeight.bold,
                                                fontSize: 14),
                                            textAlign:
                                            TextAlign.start,
                                          ),
                                        ),
                                        Container(
                                          child:  Text(
                                            cartlist[i]['old_price'] != null
                                                ? "₹" +  cartlist[i]['old_price']
                                                : "",
                                            //cartlist[i]['old_price'], //+ services[i]['data']['service'][index]['old_price'],
                                            style: TextStyle(
                                              color:
                                              AppColors.black,
                                              fontSize: 12,
                                              decoration:
                                              TextDecoration
                                                  .lineThrough,
                                            ),
                                            textAlign:
                                            TextAlign.start,
                                          ),
                                        )
                                      ],
                                    ),
                                  )

                                ],
                              ),

                            ),
                          ),

                          Container(
                              padding: EdgeInsets.only(right: 10,top: 20),

                              child:Row(
                                children: [
                                  Container(
                                    child: Material(
                                      child: InkWell(
                                        onTap: () {
                                          // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                          updateCart(int.parse(cartlist[i]['qty']), cartlist[i]['service_id'],int.parse(cartlist[i]['service_price']),"sub");
                                        },
                                        child: Container(
                                          height: 30,
                                          width: MediaQuery.of(context).size.width/9,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: const Radius.circular(5),
                                                bottomLeft: const Radius.circular(5)),
                                            color: AppColors.sentColor,
                                          ),
                                          child: Center(
                                              child: Icon(Icons.remove,color: AppColors.white,)
                                            // Text(
                                            //   "-",
                                            //   style: TextStyle(
                                            //       color: Colors.white, fontSize: 18),
                                            // ),
                                          ),
                                        ),
                                      ),
                                    ),

                                  ),
                                  Container(
                                    height: 30,
                                    width: MediaQuery.of(context).size.width/9,
                                    decoration: BoxDecoration(
                                      color: AppColors.white,
                                    ),
                                    child: Center(
                                      child: Text(
                                        // snapshot.data[0].toString() == "Instance of 'Cart'"
                                        //    ? snapshot.data[0].qty.toString()
                                        // :
                                        cartlist[i]['qty'],
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Material(
                                      child: InkWell(
                                        onTap: () {
                                          // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                          updateCart(int.parse(cartlist[i]['qty']), cartlist[i]['service_id'],int.parse(cartlist[i]['service_price']),"add");
                                        },
                                        child: Container(
                                          height: 30,
                                          width: MediaQuery.of(context).size.width/9,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  topRight: const Radius.circular(5),
                                                  bottomRight: const Radius.circular(5)),
                                              color: AppColors.sentColor
                                          ),
                                          child: Center(
                                              child: Icon(Icons.add,color: AppColors.white,)
                                            // Text(
                                            //   "+",
                                            //   style: TextStyle(
                                            //       color: Colors.white, fontSize: 18),
                                            // ),
                                          ),
                                        ),
                                      ),
                                    ),

                                  )




                                ],)

                          )


                        ],
                      );

                    },
                  ),
                ),


              ],
            ),
          ),

        )
    );
  }



  Widget buildLoading() {
    return
      Container(
        color: AppColors.whiteTransparent,
        height: 600,
        child: Center(
          child:
          // FadeInImage.memoryNetwork(
          //   placeholder: kTransparentImage,
          //   image: 'https://picsum.photos/250?image=9',
          // )
          CircularProgressIndicator(),

        ),
      );
  }
  Widget serviceList(){

  }
  Widget Servicelist() {
    return  Container(
        color: AppColors.dividerColor,
        // height:500,
        child:
        ScrollablePositionedList.builder(
          // shrinkWrap: true,
            itemPositionsListener: itemPositionListener,
            itemScrollController: _scrollController,
            scrollDirection: scrollDirection,
            // physics: NeverScrollableScrollPhysics(),
            itemCount: services.length,

            itemBuilder: (BuildContext context, int i) {
              return  Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 5, left: 20,bottom: 5),
                    child: Text(
                      services[i]['sub_sub_cat'],
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    // child: ListView.builder(
                    //     shrinkWrap: true,
                    //     physics: NeverScrollableScrollPhysics(),
                    //     itemCount: services[i]['service'].length,
                    //     itemBuilder: (BuildContext context, int index) {
                    child:Column(
                        children: List.generate(services[i]['service'].length, (index){
                          var dec=Html(
                            data: services[i]['service'][index]['service_desc'],
                            useRichText: true,);
                          return Container(
                            padding:
                            EdgeInsets.only(left: 10, right: 10, top: 5),
                            child: Card(
                              color: AppColors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child:
                              Container(
                                // color: AppColors.selectedItemColor,
                                height: MediaQuery.of(context).size.height/2.2,
                                child: Column(
                                  children: [
                                    Container(
                                      //color: AppColors.sentColor,
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          top: 5,
                                          bottom: 0,
                                          right: 10),
                                      child: Row(
                                        children: [
                                          Container(
                                            //padding: EdgeInsets.only(top: 20),

                                            height: 125,
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width /
                                                2.5,
                                            decoration: BoxDecoration(
                                              //color: AppColors.primary,
                                                borderRadius: BorderRadius
                                                    .all(
                                                    Radius.circular(10)),
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        services[i]['service'][index]['images']
                                                    ),
                                                    fit: BoxFit.cover)),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 10,
                                                  bottom: 5,
                                                  right: 10),
                                              height: 155,
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  InkWell(
                                                    onTap: (){
                                                      Share.share(
                                                          services[i]['service'][index]['share_url']);
                                                      //services[i]['service']['share_url']
                                                    },
                                                    child:
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Container(
                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width/3),
                                                          child:  Icon(
                                                            Icons.share,
                                                            color: AppColors.black,
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                  ),
                                                  Row(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            right: 10,
                                                            left: 0,
                                                            bottom: 5,
                                                            top: 5),
                                                        child: Text(
                                                          "₹ " + services[i]['service'][index]['new_price'],
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .receiveColor2,
                                                              fontWeight:
                                                              FontWeight.bold,
                                                              fontSize: 12),
                                                          textAlign:
                                                          TextAlign.start,
                                                        ),
                                                      ),
                                                      Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            right: 10,
                                                            left: 0,
                                                            bottom: 5,
                                                            top: 5),
                                                        child: Text(
                                                          "₹ " + services[i]['service'][index]['old_price'],
                                                          style: TextStyle(
                                                            color:
                                                            AppColors.black,
                                                            fontSize: 12,
                                                            decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                          ),
                                                          textAlign:
                                                          TextAlign.start,
                                                        ),
                                                      )


                                                    ],
                                                  ),
                                                  Row(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            right: 10,
                                                            left: 0,
                                                            bottom: 5,
                                                            top: 5),
                                                        child: Text(
                                                          getPercent(
                                                              services[i]['service'][index]['old_price'],
                                                              services[i]['service'][index]['new_price']).toString()+
                                                              " % Off",
                                                          style: TextStyle(
                                                              fontSize: 12),
                                                          textAlign:
                                                          TextAlign.start,
                                                        ),
                                                      ),
                                                      Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            right: 10,
                                                            left: 0,
                                                            bottom: 5,
                                                            top: 5),
                                                        child: Text(
                                                          "Save ₹ "
                                                              + (int.parse(
                                                              services[i]['service'][index]['old_price']) -
                                                              int.parse( services[i]['service'][index]['new_price']))
                                                              .toString(),
                                                          style: TextStyle(
                                                              fontSize: 12),
                                                          textAlign:
                                                          TextAlign.start,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  Row(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            right: 5,
                                                            left: 0,
                                                            bottom: 5,
                                                            top: 0),
                                                        child: Icon(
                                                          Icons
                                                              .watch_later_outlined,
                                                          color:
                                                          AppColors.sentColor,
                                                        ),
                                                      ),
                                                      Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            right: 10,
                                                            left: 0,
                                                            bottom: 5,
                                                            top: 0),
                                                        child: Text(
                                                          services[i]['service'][index]['duration']+
                                                              " min",
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              fontWeight:
                                                              FontWeight
                                                                  .w200),
                                                          textAlign:
                                                          TextAlign.start,
                                                        ),
                                                      )
                                                    ],
                                                  ),


                                                  InkWell(
                                                    onTap: () async {
                                                      var sp = await SharedPreferences.getInstance();
                                                      // print(sp.containsKey("userid"));
                                                      if(sp.containsKey("userid")){
                                                        if(services[i]['service'][index]['qty']==0){
                                                          serviceid = services[i]['service'][index]['id'];
                                                          price = services[i]['service'][index]['new_price'];
                                                          getAddtocart(serviceid,price);
                                                          getProductdetail();
                                                          getCartlist();
                                                        }
                                                        else{
                                                          serviceid = services[i]['service'][index]['id'];
                                                          price = services[i]['service'][index]['new_price'];

                                                          getRemovetocart(serviceid,price);
                                                          getProductdetail();
                                                          getCartlist();

                                                        }
                                                      }
                                                      else{
                                                        Navigator.of(context).pushAndRemoveUntil(
                                                            SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
                                                      }


                                                    },
                                                    child: Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            left: 5,
                                                            bottom: 0,
                                                            right: 5,
                                                            top: 5),
                                                        child:
                                                        services[i]['service'][index]['qty']==0?
                                                        Container(
                                                          height:35,
                                                          width: MediaQuery
                                                              .of(
                                                              context)
                                                              .size
                                                              .width /
                                                              3,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                              BorderRadius
                                                                  .all(Radius
                                                                  .circular(
                                                                  1)),
                                                              border: Border
                                                                  .all(
                                                                  color: Colors
                                                                      .black12),
                                                              color: AppColors
                                                                  .sentColor),
                                                          child: Container(
                                                            child: Center(
                                                              child: Text(
                                                                // loading == false ? "Loading.." :
                                                                "ADD TO CART",
                                                                style: TextStyle(
                                                                  color: AppColors
                                                                      .white,
                                                                  fontSize: 14.0,
                                                                  //fontWeight: FontWeight.w500
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ):
                                                        Container(
                                                            padding: EdgeInsets.only(right: 0,top: 10),

                                                            child:Row(
                                                              children: [
                                                                Container(
                                                                  child: Material(
                                                                    child: InkWell(
                                                                      onTap: () {
                                                                        // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                                                        //updateCart(int.parse(cartlist[i]['qty']), cartlist[i]['service_id'],int.parse(cartlist[i]['service_price'].toString()),"sub");
                                                                        updateCart(int.parse(services[i]['service'][index]['qty']), services[i]['service'][index]['id'],int.parse(services[i]['service'][index]['new_price'].toString()),"sub");
                                                                      },
                                                                      child: Container(
                                                                        height: 30,
                                                                        width: MediaQuery.of(context).size.width/8,
                                                                        decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.only(
                                                                              topLeft: const Radius.circular(5),
                                                                              bottomLeft: const Radius.circular(5)),
                                                                          color: AppColors.sentColor,
                                                                        ),
                                                                        child: Center(
                                                                          child: Text(
                                                                            "-",
                                                                            style: TextStyle(
                                                                                color: Colors.white, fontSize: 18),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),

                                                                ),
                                                                Container(
                                                                  height: 30,
                                                                  width: MediaQuery.of(context).size.width/8,
                                                                  decoration: BoxDecoration(
                                                                    color: AppColors.white,
                                                                  ),
                                                                  child: Center(
                                                                    child: Text(
                                                                      // snapshot.data[0].toString() == "Instance of 'Cart'"
                                                                      //    ? snapshot.data[0].qty.toString()
                                                                      // :
                                                                      services[i]['service'][index]['qty'],
                                                                      // cartlist[i]['qty'],
                                                                      style: TextStyle(color: Colors.black),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  child: Material(
                                                                    child: InkWell(
                                                                      onTap: () {
                                                                        // var price = int.parse(cartlist[i]['qty']) *int.parse(cartlist[i]['service_price']);
                                                                        updateCart(int.parse(services[i]['service'][index]['qty']), services[i]['service'][index]['id'],int.parse(services[i]['service'][index]['new_price'].toString()),"add");
                                                                      },
                                                                      child: Container(
                                                                        height: 30,
                                                                        width: MediaQuery.of(context).size.width/8,
                                                                        decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.only(
                                                                              topRight: const Radius.circular(5),
                                                                              bottomRight: const Radius.circular(5)),
                                                                          color:AppColors.sentColor,
                                                                        ),
                                                                        child: Center(
                                                                          child: Text(
                                                                            "+",
                                                                            style: TextStyle(
                                                                                color: Colors.white, fontSize: 18),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),

                                                                )




                                                              ],)

                                                        )
                                                      //:
                                                      // Container(
                                                      //   height: 35,
                                                      //   width: MediaQuery
                                                      //       .of(
                                                      //       context)
                                                      //       .size
                                                      //       .width /
                                                      //       3,
                                                      //   decoration: BoxDecoration(
                                                      //       borderRadius:
                                                      //       BorderRadius
                                                      //           .all(Radius
                                                      //           .circular(
                                                      //           1)),
                                                      //       border: Border
                                                      //           .all(
                                                      //           color: Colors
                                                      //               .black12),
                                                      //       color: AppColors
                                                      //           .sentColor),
                                                      //   child: Container(
                                                      //     child: Center(
                                                      //       child: Text(
                                                      //       //  loading == false ? "Loading.." :
                                                      //         "REMOVE FROM CART",
                                                      //         style: TextStyle(
                                                      //           color: AppColors
                                                      //               .white,
                                                      //           //fontSize: 16.0,
                                                      //           //fontWeight: FontWeight.w500
                                                      //         ),
                                                      //       ),
                                                      //     ),
                                                      //   ),
                                                      // )
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "---------------------------------------------",
                                        style:
                                        TextStyle(color: AppColors.sentColor),maxLines: 1,
                                      ),
                                    ),
                                    Expanded(

                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: 5, left: 10, right: 5),
                                            //color: AppColors.sentColor,
                                            child: Text(
                                              services[i]['service'][index]['tittle']
                                              ,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 16),
                                            ),
                                          ),
                                          Container(
                                            // height: 50,
                                            padding: EdgeInsets.only(
                                                top: 0, left: 10, right: 10),
                                            //color: AppColors.sentColor,
                                            child:
                                            Html(
                                              data: services[i]['service'][index]['service_description']
                                                 // .substring(0, 80)+'..'
                                              ,
                                              useRichText: true,

                                              //   // Text(
                                              //   //   description[i]['description_key'], //+services[i].description,
                                              //   //   style: TextStyle(
                                              //   //     color: AppColors.black,
                                              //   //   ),
                                              //   // ),
                                            ),
                                            // Text(
                                            //   "-: " +services[i]['service'][index]['service_description'],
                                            //   style:
                                            //   TextStyle(
                                            //       color: AppColors.grayText),maxLines: 3,
                                            // ),
                                          ),

                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.end,
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                          builder: (
                                                              context) =>
                                                              ServicePageviews(services[i]['service'][index]['share_url']))).then((value) => getProductdetail()).then((value) => getCartlist());
                                                },
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                      top: 20,
                                                      left: 10,
                                                      right: 25,
                                                      bottom: 10),
                                                  //color: AppColors.sentColor,
                                                  child: Text("View Details",
                                                      style: TextStyle(
                                                        color:
                                                        AppColors.sentColor,
                                                      )),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    )


                                  ],
                                ),
                              ),
                            ),
                          );
                        })),
                  )
                ],
              );
            }
        ));

  }

  Widget subcatgory() {
    return Container(
      color: AppColors.white,
      padding: EdgeInsets.only(top: 5, left: 10, bottom: 5, right: 10),
      child: Container(

        height: 35,
        //width: 40,
        child: ListView.builder(
          //shrinkWrap: true,
          // physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: subsubcat.length,
            itemBuilder: (BuildContext context, int i) {



              return InkWell(
                onTap: () {
                  // _selectsubcatindex(i,subsubcat[i]['id']);
                  _scrollToIndex(i);
                },
                child: Container(
                  padding: EdgeInsets.only(right: 10),
                  color: AppColors.white,
                  child: Container(
                    // width: 100,
                    // height: 40,

                    padding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 5, right: 5),
                    decoration: BoxDecoration(
                        color: selectedind == i?AppColors.lightblue:Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all( color: selectedind == i?AppColors.lightblue:Colors.black)),
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.only(left: 0),
                        child: Text(subsubcat[i]['sub_sub_category'] != null
                            ? subsubcat[i]['sub_sub_category']
                            : ""),
                      ),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }

  int counter = -1;
  _scrollToIndex(i) async {
    // setState(() async{
    //   await controller.scrollToIndex(i,
    //       preferPosition: AutoScrollPosition.begin);
    //   controller.highlight(i);
    // });
    setState(() {
      selectedind = i;
      itemPositionListener.itemPositions.addListener(() {

      });
    });
    _scrollController.scrollTo(index: i, duration: Duration(seconds: 1));

  }

  getPercent(old_price, new_price){
    var percent = (int.parse(old_price)-int.parse(new_price))/int.parse(old_price)*100;
    percent = double.parse(num.parse(percent.toString()).toString());
    return percent.round();
  }
  Widget datanotfound(){
    return Container(
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.only(top:40),

              child: Image(
                image: AssetImage("assets/images/sorry.png"),
                height: 180,
              )
          ),
          Center(
            child: Container(
              padding: EdgeInsets.only(top: 8,bottom: 8,left: 5,right: 5),
              decoration: BoxDecoration(
                  color: AppColors.redcolor3,
                  borderRadius: BorderRadius.all(
                      Radius.circular(10)


                  )
              ),

              width: MediaQuery.of(context).size.width/1.5,
              child: Center(child: Text(
                "OOPS! Data Not Found !!",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: AppColors.white,
                    fontWeight: FontWeight.bold
                ),
              )),
            ),
          ),

        ],),

    );
  }
}
