
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/servicepage/cartitem.dart';
import 'package:yesmadam/services/apis.dart';

class Addons extends StatefulWidget {
  final catide;
  final catname;
  Addons(this.catide,this.catname);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Addonsloader();
  }


}
class Addonsloader extends State<Addons>
{
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  List addon = [];
  List checkedadds = [];
  List checkelist = [];
  List cartlist =[];
  var totalprice="0";
  var itemcost= "";
  var oldcost="";
  var saveprice="";
  var addonprice="0";
  var couponid="0";
  var coupondiscountprices="0";
  String totalpricedetail="0";

  var payprice="";
  var payaddonprice="";
  var minimumprice ="";
  String miniprice="";

  var donotsel="";


  List<String> adonId = [];
  List adonName = [];
  List adonPrice = [];

  Future getCartlist() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.CARTLIST_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "cat_id":widget.catide,
      "user_id":  sp.getString("userid")  };
    print(data);
    var res = await apiPostRequests(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        cartlist = json.decode(res)['response']['cartList'];
        itemcost =json.decode(res)['response']['cartList'][0]['service_price'];
        oldcost =json.decode(res)['response']['cartList'][0]['old_price'];
        totalprice=json.decode(res)['response']['total_amount'].toString();
        addonprice=json.decode(res)['response']['addOnTotal'].toString();


        totalpricedetail= (int.parse(
            json.decode(res)['response']['total_amount'].toString()) +
            int.parse( json.decode(res)['response']['addOnTotal'].toString()))
            .toString();

        sp.setString("payprice", (int.parse(
            json.decode(res)['response']['total_amount'].toString()) +
            int.parse( json.decode(res)['response']['addOnTotal'].toString()))
            .toString());
        sp.setString("payaddonprice", json.decode(res)['response']['addOnTotal'].toString());


        print(sp.getString("payaddonprice"));

      }
      else{
        cartlist = [];
        totalprice="0";
        itemcost= "";
        oldcost="";
        saveprice="";
        addonprice="0";
        totalpricedetail="0";

        payprice="";
        payaddonprice="";


      }

      loading = false;
    });
  }
  Future<String> apiPostRequests(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getaddonlist() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.ADDON_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "cat_id":  widget.catide };
    print(data);
    var res = await apiPostRequestsw(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'].toString() == "1"){
        addon = json.decode(res)['response'];
        for(var i = 0; i < addon.length; i++){
          var resp = {
            "id":json.decode(res)['response'][i]['id'],
            "checked":false
          };
          checkedadds.add(resp);
        }
        print(addon);
      }
      loading = false;
    });
  }
  Future<String> apiPostRequestsw(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }
  var minprice;

  Future getminimumprice() async {
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.MINIMUMAMOUNTPRICE_API;

    var res = await apiPostRequestww(url);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1")
      {
        //minimumprice = json.decode(res)['data'];
        miniprice= json.decode(res)['data']['amount'];

        print(miniprice);

        if(int.parse(totalprice) >= int.parse(miniprice))
        {

          postaddonlist();

        }
        else {
          minprice=(int.parse(miniprice.toString()))-(int.parse(totalprice.toString()));
          _showInSnackBar("Please Order above this Min Amount $minprice");


        }
      }
      // else {
      //   //_showInSnackBar("Please Order above this Min Amount $miniprice");
      //
      // }

    });
  }
  Future<String> apiPostRequestww(String url) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //  Navigator.pop(context);
    return reply;
  }

  Future postaddonlist() async {

    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.SAVEADDON_API;
    var sp = await SharedPreferences.getInstance();
    setState(() {
      sp.setStringList("adonid", adonId);
    });
    // var data = json.encode(map);
    var data  ={
      "user_id": sp.getString("userid"),
      "cat_id":widget.catide,
      "addOn":adonId,
      "price":adonPrice,
      "name":adonName




    } ;
    print(data);
    var res = await apiPostRequestss(url,data);

    print(res);
    setState(() {
      if(json.decode(res)['status'] == true){

        Navigator.push(context, MaterialPageRoute(builder: (context)=>Cartitems(widget.catide)));



      }
      loading = false;
    });
  }
  Future<String> apiPostRequestss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  @override
  void initState() {
    getaddonlist();
    getCartlist();

    // TODO: implement initState
    super.initState();
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 4),
      // action: ,
    ));
  }
  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(widget.catname,style: TextStyle(color: AppColors.white),),
          iconTheme: IconThemeData(color: Colors.white),
        ),
        body: CustomScrollView(slivers: [
          SliverToBoxAdapter(
            child: Container(
                color: AppColors.white,
                padding: EdgeInsets.only(top:30,right:10,bottom: 30,left: 10),
                child:
                Center(
                  child: Text("Do you need any Add-ons",style: TextStyle(color: AppColors.black,
                      fontSize: 18
                  ),
                  ),
                )
            ),
          ),
          SliverToBoxAdapter(
              child:Container(child: addonlist(),)
          )
        ],),
        bottomNavigationBar: Container(
          height: 110,
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                  color: Color(0xFFFfff1ec),
                ),
                padding: EdgeInsets.only(left: 95, top: 4, bottom: 4),
                child: Text(adonId.length.toString()+" Items selected for order"),
              ),
              Container(
                height: 75,
                padding: EdgeInsets.only(left: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                  color: Colors.white,
                ),
                child: Row(
                  children: [


                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(right: 10),
                        child: SizedBox(
                          child: InkWell(
                            onTap: () {
                              getminimumprice();

                              // if (adonId.length >=1 )
                              // {
                              //
                              //   postaddonlist();
                              // } else {
                              //   alertDailog("Please Check Addon ");
                              // }

                            },
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                color: AppColors.sentColor,
                              ),
                              child: Center(
                                child: Text(
                                  //loading == true ? "Loading.." :
                                  "NEXT",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }


  Widget addonlist(){

    return Container(
      //height: 00,
      padding: EdgeInsets.all(10),
      child:
      Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 30,bottom: 0,left: 10,right: 10),
            child: GridView.builder(
                itemCount: addon.length,//category.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: .9,
                    crossAxisSpacing: 1.0,
                    mainAxisSpacing: 1.0),
                itemBuilder: (Context, i) {
                  return
                    InkWell(
                      onTap: () async{
                        setState(() {
                          var data = {
                            "id":addon[i]['id'],
                            "name":addon[i]['name'],
                            "price":addon[i]['price']
                          };
                          if(checkedadds[i]['checked'] == false)
                          {
                            checkedadds[i]['checked'] = true;
                            adonId.add(addon[i]['id'].toString());
                            adonName.add(addon[i]['name']);
                            adonPrice.add(addon[i]['price'].toString());


                            // if(i == 0)
                            // {
                            //     adonId.clear();
                            //     adonName.clear();
                            //     adonPrice.clear();
                            //     // checkedadds.clear();
                            //   // Navigator.push(context, MaterialPageRoute(builder: (context)=>Cartitems(widget.catide))).then((value) {
                            //   //   Navigator.pop(context);
                            //   // });
                            //
                            // }
                            // else{
                            //   checkedadds[i]['checked'] = true;
                            //   adonId.add(addon[i]['id'].toString());
                            //   adonName.add(addon[i]['name']);
                            //   adonPrice.add(addon[i]['price'].toString());
                            // }

                          }
                          else{

                            adonId.remove(addon[i]['id'].toString());
                            adonName.remove(addon[i]['name']);
                            adonPrice.remove(addon[i]['price'].toString());
                            checkedadds[i]['checked'] = false;
                            if(i == 0){
                              donotsel = "false";
                            }else{

                            }
                          }
                        });
                        print(adonId);
                      },
                      child: Card(
                        child: Stack(
                          children: [
                            Container(
                              // height: 640,
                              child:Column(
                                children: [
                                  Container(



                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(1.0),
                                      border: Border.all(
                                        color: AppColors.grayBorder,

                                      ),
                                      image: DecorationImage(
                                          image: NetworkImage(addon[i]['images'] != null ? addon[i]['images']:"")
                                          ,
                                          fit: BoxFit.cover),
                                    ),
                                    padding: EdgeInsets.only(
                                        top: 0, bottom: 0, right: 0, left: 10),
                                    height: 115,
                                    width: MediaQuery.of(context).size.width,
                                  ),
                                  Expanded(

                                    child:
                                    Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Center
                                        (
                                        child: Text( addon[i]['name'] !=null ? addon[i]['name']+"   -   "+   addon[i]['price']:"",

                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 15,

                                          ),
                                          textAlign: TextAlign.center,
                                          maxLines: 2,

                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            checkedadds[i]['checked'] == true?Positioned(
                              top: 0,
                              bottom: 0,
                              left: 0,
                              right: 0,
                              child: Container(
                                color: AppColors.redcolor3.withOpacity(0.4)
                                ,
                                child: Center(
                                    child : Icon(
                                      Icons.check,
                                      color : Colors.white,size: 40,
                                    )
                                ),
                              ),
                            ):Container()
                          ],
                        ),
                      ),
                    );
                }),
          ),
        ],
      ),

    );



  }

}


