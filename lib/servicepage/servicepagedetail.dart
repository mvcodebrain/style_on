import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/login.dart';
import 'package:yesmadam/servicepage/addons.dart';
import 'package:yesmadam/services/apis.dart';

class ServicePageview extends StatefulWidget {
  final List servicelist  ;

  ServicePageview(this.servicelist);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ServiceLoaderpage();
  }
}

class ServiceLoaderpage extends State<ServicePageview> {
  List services =[];
  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }
  stackServices() async{
    print(widget.servicelist.length);
    for(var i = 0; i < widget.servicelist.length; i++){
      for(var j = 0; j < widget.servicelist[i]['data']['service'].length; j++){
        setState(() {
          services.add(widget.servicelist[i]['data']['service'][j]);
        });
        print(services.length);
      }
    }
  }
 var serviceids="";
  var prices="";
  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future getAddtocart(serviceids,prices) async {
    //print("checking for login");
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.ADDTOCART_API;
      var data = { "service_id":serviceids,
        "user_id":  user,
        "qty":"1",
        "amount"  :prices       };
      print(data);
      var res = await apiPostRequestss(url,data);
      print(res);
      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {
          loading = false;
        });
      }
      else
      {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }

  }
  Future<String> apiPostRequestss(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }
  Future getRemovetocart(serviceids,prices) async {
    print("checking for login");
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.REMOVETOCART_API;
      var data = { "service_id":serviceids,
        "user_id":  user,
              };
      print(data);
      var res = await apiPostRequestsss(url,data);
      print(res);
      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {
          loading = false;
        });
      }
      else
      {
        setState(() {
          loading = false;
        });
      }
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }

  }
  Future<String> apiPostRequestsss(String url, data) async {
    // _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }


  PageController _pageController =
      PageController(initialPage: 0, viewportFraction: 0.85);
  var currentpage = 0;
  @override
  void initState() {
   // alertDailog("text");
    stackServices();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          backgroundColor: AppColors.black,
          centerTitle: true,
          title: Container(

            width: 120,
            //child: Text(services[index]['subcategoryname'],style: TextStyle(color: AppColors.white),),

          ),
          leading: IconButton(
            icon: Icon(
              Icons.close_sharp,
              size: 25,
              color: AppColors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            // IconButton(
            //   onPressed: () {
            //     //  Navigator.push(
            //     //      context,
            //     //      MaterialPageRoute(
            //     //          builder: (context) => NotificationPage()));
            //   },
            //   icon: Icon(
            //     Icons.shopping_cart,
            //     color: AppColors.white,
            //   ),
            // ),
          ],
          elevation: 0),
      body: Container(

        color: AppColors.black,
        child: PageView.builder(
          //controller: _pageController,
          allowImplicitScrolling: true,
          scrollDirection: Axis.horizontal,
          itemCount: services.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(

                padding: EdgeInsets.only(
                  right: 10,
                ),
                // transform: Matrix4.translationValues(20.0,0,0.0),
                child: Stack(
                  children: [
                    CustomScrollView(
                      slivers: [
                        SliverAppBar(
                          expandedHeight: 200.0,
                          floating: false,
                          automaticallyImplyLeading: false,
                          pinned: false,
                          flexibleSpace: FlexibleSpaceBar(
                              centerTitle: true,
                              background: Image.network(
                               services[index]['images'],
                                fit: BoxFit.cover,
                              )),
                        ),
                        SliverToBoxAdapter(
                          child: Container(
                              // height: MediaQuery.of(context).size.height,
                              color: AppColors.white, //,
                              //height: 50,
                              child: listitems(index)),
                        ),
                      ],
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: FlatButton(
                          onPressed: ()

                            {




                              if(services[index]['in_cart']==false){
                                serviceids = services[index]['id'];
                                prices = services[index]['new_price'];
                                getAddtocart(serviceids,prices);
                                stackServices();

                              }
                              else{
                                serviceids = services[index]['id'];
                                prices = services[index]['new_price'];
                                getRemovetocart(serviceids,prices);
                                stackServices();


                              };

                            },
                          child:
                          services[index]['in_cart']==true?Container(
                            height: 30,
                            width: 120,
                            decoration: BoxDecoration(
                              color: AppColors.redcolor3,
                              borderRadius:
                              BorderRadius.all(Radius.circular(12)),
                            ),

                            child: Center(
                                child: Text(
                                  "Remove From Cart",
                                  style: TextStyle(color: AppColors.black),
                                )),
                            // backgroundColor: Colors.green,
                          ):
                          Container(
                            height: 30,
                            width: 90,
                            decoration: BoxDecoration(
                              color: AppColors.sentColortransparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                            ),

                            child: Center(
                                child: Text(
                              "Add To Cart",
                              style: TextStyle(color: AppColors.black),
                            )),
                            // backgroundColor: Colors.green,
                          )),
                    )
                  ],
                ));
          },
        ),
      ),
    );
  }

  Widget listitems(ind) {
    return Container(
        //color: AppColors.black,
        child: ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 1,
      itemBuilder: (context, i){
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, left: 10, right: 5),
              //color: AppColors.sentColor,
              child: Text(
                // "External scripts can be referenced with a full URL or with a path relative to the current web page."
                // "This example uses a full URL to link to a script:"
                services[ind]['tittle']
                ,
                style: TextStyle(
                    color: AppColors.segcolor2,
                    fontWeight: FontWeight.w600,
                    fontSize: 16),
              ),
            ),
            Container(
              padding:
              EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              //color: AppColors.sentColor,
              child: Text(
                services[ind]['subcategoryname'], //+services[i].description,
                style: TextStyle(color: AppColors.grayText),
              ),
            ),
            Container(height: 1, color: AppColors.grayBorder),
            Container(
              padding:
              EdgeInsets.only(top: 10, right: 10, left: 10, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    right: 5, left: 0, bottom: 5, top: 0),
                                child: Icon(
                                  Icons.watch_later_outlined,
                                  color: AppColors.sentColor,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    right: 10, left: 0, bottom: 5, top: 0),
                                child: Text(
                                  services[ind]['duration']+ "  minute",
                                  // services[i].duration+" min",
                                  style: TextStyle(
                                      fontSize: 12, fontWeight: FontWeight.w200),
                                  textAlign: TextAlign.start,
                                ),
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                right: 0, left: 0, bottom: 5, top: 0),
                            child: Text(
                              "₹ "+services[ind]['new_price'], //+services[i].oldPrice,
                              style: TextStyle(
                                color: AppColors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                // decoration: TextDecoration.lineThrough,
                              ),
                              textAlign: TextAlign.start,
                            ),
                          )
                        ],
                      )),
                  Container(
                    height: 100,
                    child: Image(
                      image: NetworkImage(
                          services[ind]['product_images']),
                    ),
                  )
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.only(right: 10, left: 10),
                height: 1,
                color: AppColors.grayBorder),
            Container(
              padding:
              EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              //color: AppColors.sentColor,
              child: Text(
                 services[ind]['description'],
                style: TextStyle(
                    color: AppColors.black, fontWeight: FontWeight.w400),
              ),
            ),
            Container(
              padding:
              EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              //color: AppColors.sentColor,
              child: Text(
                "THINGS TO KNOW", //+services[i].description,
                style: TextStyle(
                    color: AppColors.priceControl, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
                padding: EdgeInsets.only(right: 10, left: 10),
                height: 1,
                color: AppColors.grayBorder),
            Container(
              padding:
              EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
              child: Text(
                "•	Use of mono sachets", //+services[i].description,
                style: TextStyle(
                  color: AppColors.black,
                ),
              ),
            ),
            Container(
              padding:
              EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              //color: AppColors.sentColor,
              child: Text(
                "-: DOS Services", //+services[i].description,
                style: TextStyle(
                    color: AppColors.priceControl, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
                padding: EdgeInsets.only(right: 10, left: 10),
                height: 1,
                color: AppColors.grayBorder),
            Container(
              padding:
              EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
              child: Text(
                " Benefits"
                    ,
                //+services[i].description,
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.black75,
                ),
              ),
            ),
          ],
        );
      }
    ));
  }
}
