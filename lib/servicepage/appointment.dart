// import 'dart:convert';
// import 'dart:io';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:yesmadam/color/AppColors.dart';
// import 'package:yesmadam/servicepage/vendorpofile.dart';
// import 'package:yesmadam/services/apis.dart';
//
// import 'bookingdetail.dart';
// import 'bookinghistory.dart';
//
// class Booking extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//     return BookingView();
//   }
// }
//
// class BookingView extends State<Booking> {
//
//   void _onLoading() {
//     showDialog(
//       context: context,
//       barrierDismissible: false,
//       builder: (BuildContext context) {
//         return Dialog(
//           child: Padding(
//               padding:
//               EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
//               child: new Row(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   new CircularProgressIndicator(),
//                   new Container(
//                     padding: EdgeInsets.only(left: 30),
//                     child: new Text("Please wait..."),
//                   )
//                 ],
//               )),
//         );
//       },
//     );
//   }
//
//   bool loading = false;
//   bool ani = false;
//   Color _color = Colors.black.withOpacity(0.3);
//   bool fail;
//   List history = [];
//   List ongoinghistory=[];
//   var orderid = "";
//   var vendordetail="";
//
//   Future getOrderhistorylist() async {
//     // var sp = await SharedPreferences.getInstance();
//     setState(() {
//       loading = true;
//       ani = false;
//       fail = false;
//       _color = Colors.black.withOpacity(0.3);
//       // Navigator.pop(context);
//     });
//
//     var url = Apis.BOOKINGHISTORY_API;
//     var sp = await SharedPreferences.getInstance();
//     // var data = json.encode(map);
//     var data = {
//       //"cat_id":widget.catid,
//       "user_id": sp.getString("userid")
//     };
//     print(data);
//     var res = await apiPostRequestse(url, data);
//
//     print(res);
//     setState(() {
//       if (json.decode(res)['status'].toString() == "1") {
//         history = json.decode(res)['data'];
//         // orderid=json.decode(res)['data'][0]['id'];
//         // print(orderid);
//       } else {
//         history = [];
//       }
//
//       loading = false;
//     });
//   }
//   Future getOngoingOrderhistorylist() async {
//     // var sp = await SharedPreferences.getInstance();
//     setState(() {
//       loading = true;
//       ani = false;
//       fail = false;
//       _color = Colors.black.withOpacity(0.3);
//       // Navigator.pop(context);
//     });
//
//     var url = Apis.ONGOINGBOOKINGHISTORY_API;
//     var sp = await SharedPreferences.getInstance();
//     // var data = json.encode(map);
//     var data = {
//       //"cat_id":widget.catid,
//       "user_id": sp.getString("userid")
//     };
//     print(data);
//     var res = await apiPostRequestse(url, data);
//
//     print(res);
//     setState(() {
//       if (json.decode(res)['status'].toString() == "1") {
//         ongoinghistory = json.decode(res)['data'];
//       } else {
//         history = [];
//       }
//
//       loading = false;
//     });
//   }
//
//   Future<String> apiPostRequestse(String url, data) async {
//     _onLoading();
//     HttpClient httpClient = new HttpClient();
//     HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
//     request.headers.set('content-type', 'application/json');
//     // request.headers.set('api-key' , Apis.LOGIN_API);
//     request.add(utf8.encode(json.encode(data)));
//     HttpClientResponse response = await request.close();
//     // todo - you should check the response.statusCode
//     String reply = await response.transform(utf8.decoder).join();
//     httpClient.close();
//     Navigator.pop(context);
//     return reply;
//   }
//   var refreshKey = GlobalKey<RefreshIndicatorState>();
//   Future<Null> refreshList() async {
//     refreshKey.currentState?.show(atTop: false);
//     await Future.delayed(Duration(seconds: 2));
//
//     setState(() {
//       Booking();
//       // new HomePage();
//     });
//
//     return null;
//   }
//   var  vendorrating="";
//
//   @override
//   void initState() {
//     getOrderhistorylist();
//     getOngoingOrderhistorylist();
//     // TODO: implement initState
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return MaterialApp(
//       home: DefaultTabController(
//         length: 2,
//         child: Scaffold(
//             appBar: AppBar(
//               backgroundColor: AppColors.sentColor,
//               toolbarHeight: 60,
//               bottom: TabBar(
//                 indicatorColor: AppColors.white,
//
//                 tabs: [
//
//
//                   Tab(child: Text("ONGOING",style: TextStyle(fontSize: 18),)),
//                   Tab(child: Text("History",style: TextStyle(fontSize: 18))),
//                 ],
//               ),
//             ),
//             body: RefreshIndicator(
//               key: refreshKey,
//               child:  TabBarView(
//
//                 children: [
//                   ongoinghistory.length==0?Container(
//                       child:
//                       Container(
//                         color: AppColors.white,
//
//
//                         child: Image(
//
//                           image: AssetImage("assets/images/datanotfound.jpg"),
//                           height: 400,
//                         ),
//                       )
//
//                   ): Container(
//                     child:
//                     SingleChildScrollView(
//                       child: ListView.builder(
//                           shrinkWrap: true,
//                           physics: NeverScrollableScrollPhysics(),
//                           itemCount: ongoinghistory.length,
//                           itemBuilder: (BuildContext context, int i) {
//                             return Container(
//                               padding: EdgeInsets.only(
//                                   top: 5, bottom: 5, left: 5, right: 5),
//                               child: Container(
//                                 child: Card(
//                                   child: Container(
//                                     child: Column(
//                                       crossAxisAlignment: CrossAxisAlignment.start,
//                                       children: [
//                                         Container(
//                                           padding: EdgeInsets.only(
//                                               top: 10, bottom: 10, left: 5, right: 5),
//                                           child: Row(
//                                             mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                             children: [
//                                               Container(
//                                                 child: Text("Order Date:  " +
//                                                     ongoinghistory[i]['booking_date']),
//                                               ),
//                                             ],
//                                           ),
//                                         ),
//                                         InkWell(
//                                           onTap: (){
//                                             var id=ongoinghistory[i]['vendor']['id']=="0"?"":ongoinghistory[i]['vendor']['id'];
//                                             //  print(id);
//                                             Navigator.push(
//                                                 context,
//                                                 MaterialPageRoute(
//                                                     builder: (context) =>
//
//                                                         vendorprofile(id)));
//
//                                           },
//                                           child: ongoinghistory[i]['vendor']==null?Container(height: 1,): Container(
//                                             //height: 100,
//                                             child:
//                                             Row(
//                                               children: [
//                                                 Flexible(
//                                                   flex: 1,
//                                                   child: Container(
//                                                     margin: EdgeInsets.only(
//                                                         top: 10, left: 10, bottom: 10),
//                                                     height: 80,
//                                                     width: 80,
//                                                     //width: MediaQuery.of(context).size.width/2.2,
//                                                     decoration: BoxDecoration(
//                                                         color: AppColors.redcolor3,
//                                                         border: Border.all(
//                                                             color: Colors.white),
//                                                         borderRadius: BorderRadius.all(
//                                                             Radius.circular(120)),
//                                                         image: DecorationImage(
//                                                             image: NetworkImage(
//                                                               ongoinghistory[i]['vendor']['image']==null?"":ongoinghistory[i]['vendor']['image'],
//                                                             ),
//                                                             fit: BoxFit.fill)),
//                                                   ),
//                                                 ),
//                                                 Flexible(
//                                                   flex: 2,
//                                                   child: Row(
//                                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                                     children: [
//                                                       Container(
//                                                         padding: EdgeInsets.only(left: 10),
//                                                         child: Column(
//                                                           crossAxisAlignment:
//                                                           CrossAxisAlignment.start,
//                                                           children: [
//                                                             Container(
//                                                               child: Text(
//                                                                 ongoinghistory[i]['vendor']['username'] == null
//                                                                     ? ""
//                                                                     : ongoinghistory[i]['vendor']['username'],
//                                                                 style: TextStyle(
//                                                                     fontWeight:
//                                                                     FontWeight.bold,
//                                                                     fontSize: 16),
//                                                               ),
//                                                             ),
//                                                             Container(
//                                                               child: Row(
//                                                                 children: [
//                                                                   Container(
//                                                                     padding: EdgeInsets.only(
//                                                                         left: 0, top: 5),
//                                                                     child: Row(
//                                                                       mainAxisAlignment:
//                                                                       MainAxisAlignment
//                                                                           .center,
//                                                                       children: [
//                                                                         Container(
//                                                                             color:
//                                                                             Colors.white,
//                                                                             child: Icon(
//                                                                               Icons.star,
//                                                                               size: 18,
//                                                                               color: AppColors
//                                                                                   .redcolor3,
//                                                                             )),
//                                                                         Container(
//                                                                           padding:
//                                                                           EdgeInsets.only(
//                                                                               left: 5),
//                                                                           child: Text(
//                                                                             ongoinghistory[i]['vendor']['average_rating'] == null
//                                                                                 ? "0"
//                                                                                 :
//                                                                             ongoinghistory[i]['vendor']['average_rating'],
//                                                                             //vendorrat =vendorrating.toStringAsFixed(1);
//                                                                             style: TextStyle(
//                                                                               color: Colors
//                                                                                   .black,
//                                                                               fontWeight:
//                                                                               FontWeight
//                                                                                   .bold,
//                                                                               fontSize: 14,
//                                                                             ),
//                                                                           ),
//                                                                         )
//                                                                       ],
//                                                                     ),
//                                                                   ),
//                                                                   Container(
//                                                                     padding: EdgeInsets.only(
//                                                                         left: 10,top: 5),
//                                                                     child: Text(
//                                                                       ongoinghistory[i]['vendor']['total_number_rating'] == null
//                                                                           ? "0"
//                                                                           : ongoinghistory[i]['vendor']['total_number_rating'] +"  Ratings",
//                                                                       style: TextStyle(
//                                                                           color: AppColors
//                                                                               .black),
//                                                                     ),
//                                                                   )
//                                                                 ],
//                                                               ),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       ),
//                                                       InkWell(
//                                                         onTap: ()
//                                                         async {
//                                                           var number= ongoinghistory[i]['vendor']['mobile']==null?"0":ongoinghistory[i]['vendor']['mobile'];
//                                                           var url = "tel:"+"$number";
//                                                           if (await canLaunch(url)) {
//                                                             await launch(url);
//                                                           } else {
//                                                             throw 'Could not launch $url';
//                                                           }
//
//                                                         },
//                                                         child: Container(
//                                                           child: Column(
//                                                             children: [
//                                                               Container(
//                                                                 child: Padding(
//                                                                   padding: EdgeInsets.only(
//                                                                       right: 5,bottom: 5
//                                                                   ),
//                                                                   child: Icon(
//                                                                       Icons.call,size: 25,
//                                                                       color: AppColors.primary2
//                                                                   ),
//
//                                                                 ),
//                                                               ),
//                                                               Text(
//                                                                 "Call Vendor",
//                                                                 style: TextStyle(
//                                                                     color: AppColors.primary2,
//                                                                     fontSize: 17),
//                                                               )
//
//                                                             ],
//                                                           ),
//
//                                                         ),
//                                                       )
//
//
//                                                     ],
//                                                   ),
//                                                 ),
//                                               ],
//                                             ),
//                                           ),
//                                         ),
//                                         Material(
//                                           child: InkWell(
//                                             onTap: () {
//                                               vendordetail=ongoinghistory[i]['vendor']==null?"0": vendordetail=ongoinghistory[i]['vendor']['id'];
//
//                                               Navigator.push(
//                                                   context,
//                                                   MaterialPageRoute(
//                                                       builder: (context) =>
//                                                           Bookingdetail(ongoinghistory[i]
//                                                           ['booking_id'],vendordetail)))
//                                                   .then(
//                                                       (value) =>getOngoingOrderhistorylist());
//                                             },
//                                             child: Container(
//                                               child: Column(
//                                                 children: [
//                                                   Container(
//                                                     padding: EdgeInsets.only(
//                                                         top: 5,
//                                                         bottom: 0,
//                                                         left: 5,
//                                                         right: 5),
//                                                     color: AppColors.grayBorder,
//                                                     height: 1,
//                                                   ),
//                                                   // Container(
//                                                   //   padding: EdgeInsets.only(
//                                                   //       top: 0,
//                                                   //       bottom: 10,
//                                                   //       left: 5,
//                                                   //       right: 5),
//                                                   //   child: Text("hhhh",
//                                                   //     // history[i]['service_title'] ==
//                                                   //     //         null
//                                                   //     //     ? ""
//                                                   //     //     : history[i]['service_title'],
//                                                   //     style: TextStyle(
//                                                   //       color: AppColors.black,
//                                                   //       fontWeight: FontWeight.bold,
//                                                   //     ),
//                                                   //   ),
//                                                   // ),
//                                                   Row(
//                                                     mainAxisAlignment:
//                                                     MainAxisAlignment.spaceBetween,
//                                                     children: [
//                                                       Container(
//                                                         padding: EdgeInsets.only(
//                                                             left: 10,
//                                                             top: 5,
//                                                             bottom: 0),
//                                                         child: Text("Booking Id:"),
//                                                         alignment: Alignment.topLeft,
//                                                       ),
//                                                       Container(
//                                                         //height: 25,
//                                                         width: MediaQuery.of(context)
//                                                             .size
//                                                             .width /
//                                                             2.2,
//                                                         // color: AppColors.sentColor,
//                                                         child: Row(
//                                                           children: [
//                                                             Flexible(
//                                                               child: Container(
//                                                                   padding:
//                                                                   EdgeInsets.only(
//                                                                       left: 25,
//                                                                       top: 05,
//                                                                       right: 10,
//                                                                       bottom: 0),
//                                                                   //color: AppColors.primary,
//                                                                   child: RichText(
//                                                                     text: new TextSpan(
//                                                                       text: ongoinghistory[i][
//                                                                       'booking_id'],
//                                                                       style: TextStyle(
//                                                                         color: AppColors
//                                                                             .black,
//                                                                         fontSize: 14,
//                                                                       ),
//                                                                     ),
//                                                                     textAlign:
//                                                                     TextAlign.left,
//                                                                   )),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       )
//                                                     ],
//                                                   ),
//
//                                                   Row(
//                                                     mainAxisAlignment:
//                                                     MainAxisAlignment.spaceBetween,
//                                                     children: [
//                                                       Container(
//                                                         padding: EdgeInsets.only(
//                                                             left: 10,
//                                                             top: 5,
//                                                             bottom: 8),
//                                                         child: Text("Booking Date/Time"),
//                                                         alignment: Alignment.topLeft,
//                                                       ),
//                                                       Container(
//                                                         //height: 25,
//                                                         width: MediaQuery.of(context)
//                                                             .size
//                                                             .width /
//                                                             2.2,
//                                                         // color: AppColors.sentColor,
//                                                         child: Row(
//                                                           children: [
//                                                             Flexible(
//                                                               child: Container(
//                                                                 padding:
//                                                                 EdgeInsets.only(
//                                                                     left: 25,
//                                                                     top: 05,
//                                                                     right: 10,
//                                                                     bottom: 5),
//                                                                 //color: AppColors.primary,
//                                                                 child: Text(
//                                                                   ongoinghistory[i]
//                                                                   ['date' ] +  ongoinghistory[i]
//                                                                   ['start_time']  +  ongoinghistory[ i]
//                                                                   ['end_time' ],
//                                                                   style: TextStyle(
//                                                                     color: AppColors
//                                                                         .black,
//                                                                     fontSize: 14,
//                                                                   ),
//                                                                 ),
//
//                                                               ),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       )
//                                                     ],
//                                                   ),
//
//                                                   Row(
//                                                     mainAxisAlignment:
//                                                     MainAxisAlignment.spaceBetween,
//                                                     children: [
//                                                       Container(
//                                                         padding: EdgeInsets.only(
//                                                             left: 10,
//                                                             top: 5,
//                                                             bottom: 8),
//                                                         child: Text("Service Name"),
//                                                         alignment: Alignment.topLeft,
//                                                       ),
//                                                       Container(
//                                                         //height: 25,
//                                                         width: MediaQuery.of(context)
//                                                             .size
//                                                             .width /
//                                                             2.2,
//                                                         // color: AppColors.sentColor,
//                                                         child: Row(
//                                                           children: [
//                                                             Flexible(
//                                                               child: Container(
//                                                                   padding:
//                                                                   EdgeInsets.only(
//                                                                       left: 25,
//                                                                       top: 05,
//                                                                       right: 10,
//                                                                       bottom: 5),
//                                                                   //color: AppColors.primary,
//                                                                   child: RichText(
//                                                                     text: new TextSpan(
//                                                                       text: ongoinghistory[i]
//                                                                       ['service_title'],
//                                                                       style: TextStyle(
//                                                                         color: AppColors
//                                                                             .sentColor,
//                                                                         fontSize: 14,
//                                                                       ),
//                                                                     ),
//                                                                     textAlign:
//                                                                     TextAlign.left,
//                                                                   )),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       )
//                                                     ],
//                                                   ),
//                                                   // Row(
//                                                   //   mainAxisAlignment:
//                                                   //       MainAxisAlignment.spaceBetween,
//                                                   //   children: [
//                                                   //     Container(
//                                                   //       padding: EdgeInsets.only(
//                                                   //           left: 10,
//                                                   //           top: 5,
//                                                   //           bottom: 8),
//                                                   //       child: Text("Payment Mode"),
//                                                   //       alignment: Alignment.topLeft,
//                                                   //     ),
//                                                   //     Container(
//                                                   //       //height: 25,
//                                                   //       width: MediaQuery.of(context)
//                                                   //               .size
//                                                   //               .width /
//                                                   //           2.2,
//                                                   //       // color: AppColors.sentColor,
//                                                   //       child: Row(
//                                                   //         children: [
//                                                   //           Flexible(
//                                                   //             child: Container(
//                                                   //                 padding:
//                                                   //                     EdgeInsets.only(
//                                                   //                         left: 25,
//                                                   //                         top: 05,
//                                                   //                         right: 10,
//                                                   //                         bottom: 5),
//                                                   //                 //color: AppColors.primary,
//                                                   //                 child: RichText(
//                                                   //                   text: new TextSpan(
//                                                   //                     text: history[i]
//                                                   //                         ['pay_mode'],
//                                                   //                     style: TextStyle(
//                                                   //                       color: AppColors
//                                                   //                           .grayBorder,
//                                                   //                       fontSize: 12,
//                                                   //                     ),
//                                                   //                   ),
//                                                   //                   textAlign:
//                                                   //                       TextAlign.left,
//                                                   //                 )),
//                                                   //           ),
//                                                   //         ],
//                                                   //       ),
//                                                   //     )
//                                                   //   ],
//                                                   // ),
//                                                   // Row(
//                                                   //   mainAxisAlignment:
//                                                   //       MainAxisAlignment.spaceBetween,
//                                                   //   children: [
//                                                   //     Container(
//                                                   //       padding: EdgeInsets.only(
//                                                   //           left: 10, top: 5, bottom: 8),
//                                                   //       child: Text("Order Status"),
//                                                   //       alignment: Alignment.topLeft,
//                                                   //     ),
//                                                   //     Container(
//                                                   //       //height: 25,
//                                                   //       width:
//                                                   //           MediaQuery.of(context).size.width /
//                                                   //               2.2,
//                                                   //       // color: AppColors.sentColor,
//                                                   //       child: Row(
//                                                   //         children: [
//                                                   //           Flexible(
//                                                   //             child: Container(
//                                                   //                 padding: EdgeInsets.only(
//                                                   //                     left: 25,
//                                                   //                     top: 05,
//                                                   //                     right: 10,
//                                                   //                     bottom: 5),
//                                                   //                 //color: AppColors.primary,
//                                                   //                 child: RichText(
//                                                   //                   text: new TextSpan(
//                                                   //                     text: history[i]['status'] == "0" ? "Pending"
//                                                   //           : history[i]['status'] == "1"?"Vendor Accepted"
//                                                   //           :history[i]['status'] == "2" ? "Service Started"
//                                                   //       :history[i]['status'] == "3"?"Cancel"
//                                                   //       :history[i]['status'] == "4"?"Cancel":
//                                                   //       history[i]['status'] == "5"?"Completed":"",
//                                                   //                     style: TextStyle(
//                                                   //                       color: AppColors
//                                                   //                           .grayBorder,
//                                                   //                       fontSize: 12,
//                                                   //                     ),
//                                                   //                   ),
//                                                   //                   textAlign: TextAlign.left,
//                                                   //                 )),
//                                                   //           ),
//                                                   //         ],
//                                                   //       ),
//                                                   //     )
//                                                   //   ],
//                                                   // ),
//                                                   // // Row(
//                                                   //   mainAxisAlignment:
//                                                   //       MainAxisAlignment.spaceBetween,
//                                                   //   children: [
//                                                   //     Container(
//                                                   //       padding: EdgeInsets.only(
//                                                   //           left: 10, top: 5, bottom: 8),
//                                                   //       child: Text("Service Type"),
//                                                   //       alignment: Alignment.topLeft,
//                                                   //     ),
//                                                   //     Container(
//                                                   //       //height: 25,
//                                                   //       width:
//                                                   //           MediaQuery.of(context).size.width /
//                                                   //               2.2,
//                                                   //       // color: AppColors.sentColor,
//                                                   //       child: Row(
//                                                   //         children: [
//                                                   //           Flexible(
//                                                   //             child: Container(
//                                                   //                 padding: EdgeInsets.only(
//                                                   //                     left: 25,
//                                                   //                     top: 05,
//                                                   //                     right: 10,
//                                                   //                     bottom: 5),
//                                                   //                 //color: AppColors.primary,
//                                                   //                 child: RichText(
//                                                   //                   text: new TextSpan(
//                                                   //                     text: "STANDARD",
//                                                   //                     style: TextStyle(
//                                                   //                       color: AppColors
//                                                   //                           .grayBorder,
//                                                   //                       fontSize: 12,
//                                                   //                     ),
//                                                   //                   ),
//                                                   //                   textAlign: TextAlign.left,
//                                                   //                 )),
//                                                   //           ),
//                                                   //         ],
//                                                   //       ),
//                                                   //     )
//                                                   //   ],
//                                                   // ),
//
//                                                   Row(
//                                                     mainAxisAlignment:
//                                                     MainAxisAlignment.spaceBetween,
//                                                     children: [
//                                                       Container(
//                                                         padding: EdgeInsets.only(
//                                                             left: 10,
//                                                             top: 5,
//                                                             bottom: 0),
//                                                         child: Text(
//                                                           "₹" +
//                                                               ongoinghistory[i]
//                                                               ['total_charge'],
//                                                           style: TextStyle(
//                                                               color:
//                                                               AppColors.redcolor3),
//                                                         ),
//                                                         alignment: Alignment.topLeft,
//                                                       ),
//                                                     ],
//                                                   ),
//                                                   Row(
//                                                     mainAxisAlignment:
//                                                     MainAxisAlignment.spaceBetween,
//                                                     children: [
//                                                       Container(
//                                                         padding: EdgeInsets.only(
//                                                             left: 10,
//                                                             top: 5,
//                                                             bottom: 8),
//                                                         child: Text("Status"),
//                                                         alignment: Alignment.topLeft,
//                                                       ),
//                                                       Container(
//                                                         //height: 25,
//                                                         width: MediaQuery.of(context)
//                                                             .size
//                                                             .width /
//                                                             2.2,
//                                                         // color: AppColors.sentColor,
//                                                         child: Row(
//                                                           children: [
//                                                             Flexible(
//                                                               child: Container(
//                                                                   padding:
//                                                                   EdgeInsets.only(
//                                                                       left: 25,
//                                                                       top: 05,
//                                                                       right: 10,
//                                                                       bottom: 5),
//                                                                   //color: AppColors.primary,
//                                                                   child: RichText(
//                                                                     text: new TextSpan(
//                                                                       text: ongoinghistory[i][
//                                                                       'status'] ==
//                                                                           "0"
//                                                                           ? "Pending"
//                                                                           : ongoinghistory[i][
//                                                                       'status'] ==
//                                                                           "1"
//                                                                           ? "Vendor Accepted"
//                                                                           : ongoinghistory[i]['status'] ==
//                                                                           "2"
//                                                                           ? "Service Started"
//                                                                           : ongoinghistory[i]['status'] == "3"
//                                                                           ? "Cancel"
//                                                                           : ongoinghistory[i]['status'] == "4"
//                                                                           ? "Cancel"
//                                                                           : ongoinghistory[i]['status'] == "5"
//                                                                           ? "Completed"
//                                                                           : "",
//                                                                       style: TextStyle(
//                                                                           color: ongoinghistory[i]
//                                                                           ['status'] ==
//                                                                               "3"
//                                                                               ?AppColors.redcolor3:ongoinghistory[i]
//                                                                           ['status'] ==
//                                                                               "4"
//                                                                               ?AppColors.redcolor3:AppColors.black,
//                                                                           fontSize: 14,
//                                                                           fontWeight: FontWeight.bold
//                                                                       ),
//                                                                     ),
//                                                                     textAlign:
//                                                                     TextAlign.left,
//                                                                   )),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       )
//                                                     ],
//                                                   ),
//                                                   Container(
//                                                     padding: EdgeInsets.only(top: 5,bottom: 10,right: 20),
//                                                     child: InkWell(onTap: (){
//                                                       vendordetail=ongoinghistory[i]['vendor']==null?"0": vendordetail=ongoinghistory[i]['vendor']['id'];
//
//                                                       Navigator.push(
//                                                           context,
//                                                           MaterialPageRoute(
//                                                               builder: (context) =>
//                                                                   Bookingdetail(ongoinghistory[i]
//                                                                   ['booking_id'],vendordetail)))
//                                                           .then(
//                                                               (value) =>getOngoingOrderhistorylist());
//                                                       // orderid=history[i]['id'];
//                                                       // getOrderCancel(orderid);
//                                                     },
//                                                       child:
//                                                       Row(
//                                                         mainAxisAlignment: MainAxisAlignment.end,
//                                                         children: [
//
//                                                           Container(
//
//                                                             height: 40,
//                                                             width: 120,
//                                                             decoration: BoxDecoration(
//                                                               color: AppColors.sentColor,
//                                                               borderRadius: BorderRadius.circular(10),
//                                                             ),
//                                                             child:
//                                                             Center(child: Text(loading == true ? "Loading.." :"VIEW DETAILS",style: TextStyle(color: AppColors.white,
//                                                               fontSize: 16,),)),
//                                                           ),
//                                                         ],
//                                                       ),
//                                                     ),
//                                                   )
//                                                 ],
//                                               ),
//                                             ),
//                                           ),
//                                         )
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             );
//                           }),
//                     ),
//                     // child: SingleChildScrollView(
//                     //   child: ListView(
//                     //     shrinkWrap: true,
//                     //     physics: NeverScrollableScrollPhysics(),
//                     //     children: [
//                     //       Container(
//                     //
//                     //         child: Image(image: NetworkImage("https://www.flashsaletricks.com/wp-content/uploads/2017/11/Mr_Voonik_refer_earn.jpg"),
//                     //         ),
//                     //       ),
//                     //       Container(
//                     //         child:
//                     //         Center(
//                     //           child: Card(
//                     //
//                     //             color: AppColors.grayBorder,
//                     //             shape: RoundedRectangleBorder(
//                     //               borderRadius: BorderRadius.circular(5.0),
//                     //             ),
//                     //             child: Text("It Pays to Have Friends",),
//                     //
//                     //           ),
//                     //         ),
//                     //       )
//                     //     ],
//                     //   ),
//                     // ),
//                   ),
//                   history.length==0?Container(
//                       child:
//                       Container(
//                         color: AppColors.white,
//
//
//                         child: Image(
//
//                           image: AssetImage("assets/images/datanotfound.jpg"),
//                           height: 400,
//                         ),
//                       )
//
//                   ): Container(
//                     child:
//                     SingleChildScrollView(
//                       child: ListView.builder(
//                           shrinkWrap: true,
//                           physics: NeverScrollableScrollPhysics(),
//                           itemCount: history.length,
//                           itemBuilder: (BuildContext context, int i) {
//                             return Container(
//                               padding: EdgeInsets.only(
//                                   top: 5, bottom: 5, left: 5, right: 5),
//                               child: Container(
//                                 child: Card(
//                                   child: Container(
//                                     child: Column(
//                                       crossAxisAlignment: CrossAxisAlignment.start,
//                                       children: [
//                                         Container(
//                                           padding: EdgeInsets.only(
//                                               top: 10, bottom: 10, left: 5, right: 5),
//                                           child: Row(
//                                             mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                             children: [
//                                               Container(
//                                                 child: Text("Order Date:  " +
//                                                     history[i]['booking_date']),
//                                               ),
//                                             ],
//                                           ),
//                                         ),
//                                         InkWell(
//                                           onTap: (){
//                                             var id=history[i]['vendor']['id']=="0"?"":history[i]['vendor']['id'];
//                                             //  print(id);
//                                             Navigator.push(
//                                                 context,
//                                                 MaterialPageRoute(
//                                                     builder: (context) =>
//
//                                                         vendorprofile(id)));
//
//                                           },
//                                           child: history[i]['vendor']==null?Container(height: 1,): Container(
//                                             //height: 100,
//                                             child: Row(
//                                               children: [
//                                                 Flexible(
//                                                   flex: 1,
//                                                   child: Container(
//                                                     margin: EdgeInsets.only(
//                                                         top: 10, left: 10, bottom: 10),
//                                                     height: 80,
//                                                     width: 80,
//                                                     //width: MediaQuery.of(context).size.width/2.2,
//                                                     decoration: BoxDecoration(
//                                                         color: AppColors.redcolor3,
//                                                         border: Border.all(
//                                                             color: Colors.white),
//                                                         borderRadius: BorderRadius.all(
//                                                             Radius.circular(120)),
//                                                         image: DecorationImage(
//                                                             image: NetworkImage(
//                                                               history[i]['vendor']['image']==null?"":history[i]['vendor']['image'],
//                                                             ),
//                                                             fit: BoxFit.fill)),
//                                                   ),
//                                                 ),
//                                                 Flexible(
//                                                   flex: 2,
//                                                   child: Row(
//                                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                                     children: [
//                                                       Container(
//                                                         padding: EdgeInsets.only(left: 10),
//                                                         child: Column(
//                                                           crossAxisAlignment:
//                                                           CrossAxisAlignment.start,
//                                                           children: [
//                                                             Container(
//                                                               child: Text(
//                                                                 history[i]['vendor']['username'] == null
//                                                                     ? ""
//                                                                     : history[i]['vendor']['username'],
//                                                                 style: TextStyle(
//                                                                     fontWeight:
//                                                                     FontWeight.bold,
//                                                                     fontSize: 16),
//                                                               ),
//                                                             ),
//                                                             Container(
//                                                               child: Row(
//                                                                 children: [
//                                                                   Container(
//                                                                     padding: EdgeInsets.only(
//                                                                         left: 0, top: 5),
//                                                                     child: Row(
//                                                                       mainAxisAlignment:
//                                                                       MainAxisAlignment
//                                                                           .center,
//                                                                       children: [
//                                                                         Container(
//                                                                             color:
//                                                                             Colors.white,
//                                                                             child: Icon(
//                                                                               Icons.star,
//                                                                               size: 18,
//                                                                               color: AppColors
//                                                                                   .redcolor3,
//                                                                             )),
//                                                                         Container(
//                                                                           padding:
//                                                                           EdgeInsets.only(
//                                                                               left: 5),
//                                                                           child: Text(
//                                                                             history[i]['vendor']['average_rating'] == null
//                                                                                 ? "0"
//                                                                                 : history[i]['vendor']['average_rating'],
//                                                                             style: TextStyle(
//                                                                               color: Colors
//                                                                                   .black,
//                                                                               fontWeight:
//                                                                               FontWeight
//                                                                                   .bold,
//                                                                               fontSize: 14,
//                                                                             ),
//                                                                           ),
//                                                                         )
//                                                                       ],
//                                                                     ),
//                                                                   ),
//                                                                   Container(
//                                                                     padding: EdgeInsets.only(
//                                                                         left: 10,top: 5),
//                                                                     child: Text(
//                                                                       history[i]['vendor']['total_number_rating'] == null
//                                                                           ? "0"
//                                                                           : history[i]['vendor']['total_number_rating'] +"  Ratings",
//                                                                       style: TextStyle(
//                                                                           color: AppColors
//                                                                               .black),
//                                                                     ),
//                                                                   )
//                                                                 ],
//                                                               ),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       ),
//                                                       InkWell(
//                                                         onTap: ()
//                                                         async {
//                                                           var number= history[i]['vendor']['mobile']==null?"0":history[i]['vendor']['mobile'];
//                                                           var url = "tel:"+"$number";
//                                                           if (await canLaunch(url)) {
//                                                             await launch(url);
//                                                           } else {
//                                                             throw 'Could not launch $url';
//                                                           }
//
//                                                         },
//                                                         child: Container(
//                                                           child: Column(
//                                                             children: [
//                                                               Container(
//                                                                 child: Padding(
//                                                                   padding: EdgeInsets.only(
//                                                                       right: 5,bottom: 5
//                                                                   ),
//                                                                   child: Icon(
//                                                                       Icons.call,size: 25,
//                                                                       color: AppColors.primary2
//                                                                   ),
//
//                                                                 ),
//                                                               ),
//                                                               Text(
//                                                                 "Call Vendor",
//                                                                 style: TextStyle(
//                                                                     color: AppColors.primary2,
//                                                                     fontSize: 17),
//                                                               )
//
//                                                             ],
//                                                           ),
//
//                                                         ),
//                                                       )
//
//
//                                                     ],
//                                                   ),
//                                                 ),
//                                               ],
//                                             ),
//                                           ),
//                                         ),
//                                         InkWell(
//                                           onTap: () {
//                                             vendordetail=history[i]['vendor']==null?"0": vendordetail=history[i]['vendor']['id'];
//                                             Navigator.push(
//                                                 context,
//                                                 MaterialPageRoute(
//                                                     builder: (context) =>
//                                                         Bookingdetail(history[i]
//                                                         ['booking_id'],vendordetail))).then(
//                                                     (value) => getOrderhistorylist());
//                                           },
//                                           child: Container(
//                                             child: Column(
//                                               children: [
//                                                 Container(
//                                                   padding: EdgeInsets.only(
//                                                       top: 5,
//                                                       bottom: 0,
//                                                       left: 5,
//                                                       right: 5),
//                                                   color: AppColors.grayBorder,
//                                                   height: 1,
//                                                 ),
//                                                 // Container(
//                                                 //   padding: EdgeInsets.only(
//                                                 //       top: 0,
//                                                 //       bottom: 10,
//                                                 //       left: 5,
//                                                 //       right: 5),
//                                                 //   child: Text("hhhh",
//                                                 //     // history[i]['service_title'] ==
//                                                 //     //         null
//                                                 //     //     ? ""
//                                                 //     //     : history[i]['service_title'],
//                                                 //     style: TextStyle(
//                                                 //       color: AppColors.black,
//                                                 //       fontWeight: FontWeight.bold,
//                                                 //     ),
//                                                 //   ),
//                                                 // ),
//                                                 Row(
//                                                   mainAxisAlignment:
//                                                   MainAxisAlignment.spaceBetween,
//                                                   children: [
//                                                     Container(
//                                                       padding: EdgeInsets.only(
//                                                           left: 10,
//                                                           top: 5,
//                                                           bottom: 0),
//                                                       child: Text("Booking Id:"),
//                                                       alignment: Alignment.topLeft,
//                                                     ),
//                                                     Container(
//                                                       //height: 25,
//                                                       width: MediaQuery.of(context)
//                                                           .size
//                                                           .width /
//                                                           2.2,
//                                                       // color: AppColors.sentColor,
//                                                       child: Row(
//                                                         children: [
//                                                           Flexible(
//                                                             child: Container(
//                                                                 padding:
//                                                                 EdgeInsets.only(
//                                                                     left: 25,
//                                                                     top: 05,
//                                                                     right: 10,
//                                                                     bottom: 0),
//                                                                 //color: AppColors.primary,
//                                                                 child: RichText(
//                                                                   text: new TextSpan(
//                                                                     text: history[i][
//                                                                     'booking_id'],
//                                                                     style: TextStyle(
//                                                                       color: AppColors
//                                                                           .black,
//                                                                       fontSize: 14,
//                                                                     ),
//                                                                   ),
//                                                                   textAlign:
//                                                                   TextAlign.left,
//                                                                 )),
//                                                           ),
//                                                         ],
//                                                       ),
//                                                     )
//                                                   ],
//                                                 ),
//
//                                                 Row(
//                                                   mainAxisAlignment:
//                                                   MainAxisAlignment.spaceBetween,
//                                                   children: [
//                                                     Container(
//                                                       padding: EdgeInsets.only(
//                                                           left: 10,
//                                                           top: 5,
//                                                           bottom: 8),
//                                                       child: Text("Booking Date/Time"),
//                                                       alignment: Alignment.topLeft,
//                                                     ),
//                                                     Container(
//                                                       //height: 25,
//                                                       width: MediaQuery.of(context)
//                                                           .size
//                                                           .width /
//                                                           2.2,
//                                                       // color: AppColors.sentColor,
//                                                       child: Row(
//                                                         children: [
//                                                           Flexible(
//                                                             child: Container(
//                                                               padding:
//                                                               EdgeInsets.only(
//                                                                   left: 25,
//                                                                   top: 05,
//                                                                   right: 10,
//                                                                   bottom: 5),
//                                                               //color: AppColors.primary,
//                                                               child: Text(
//                                                                 history[i]
//                                                                 ['date' ] +  history[i]
//                                                                 ['start_time']  +  history[ i]
//                                                                 ['end_time' ],
//                                                                 style: TextStyle(
//                                                                   color: AppColors
//                                                                       .black,
//                                                                   fontSize: 14,
//                                                                 ),
//                                                               ),
//
//                                                             ),
//                                                           ),
//                                                         ],
//                                                       ),
//                                                     )
//                                                   ],
//                                                 ),
//
//                                                 Row(
//                                                   mainAxisAlignment:
//                                                   MainAxisAlignment.spaceBetween,
//                                                   children: [
//                                                     Container(
//                                                       padding: EdgeInsets.only(
//                                                           left: 10,
//                                                           top: 5,
//                                                           bottom: 8),
//                                                       child: Text("Service Name"),
//                                                       alignment: Alignment.topLeft,
//                                                     ),
//                                                     Container(
//                                                       //height: 25,
//                                                       width: MediaQuery.of(context)
//                                                           .size
//                                                           .width /
//                                                           2.2,
//                                                       // color: AppColors.sentColor,
//                                                       child: Row(
//                                                         children: [
//                                                           Flexible(
//                                                             child: Container(
//                                                                 padding:
//                                                                 EdgeInsets.only(
//                                                                     left: 25,
//                                                                     top: 05,
//                                                                     right: 10,
//                                                                     bottom: 5),
//                                                                 //color: AppColors.primary,
//                                                                 child: RichText(
//                                                                   text: new TextSpan(
//                                                                     text: history[i]
//                                                                     ['service_title'],
//                                                                     style: TextStyle(
//                                                                         color: AppColors
//                                                                             .sentColor,
//                                                                         fontSize: 14,
//                                                                         fontWeight: FontWeight.w400
//                                                                     ),
//                                                                   ),
//                                                                   textAlign:
//                                                                   TextAlign.left,
//                                                                 )),
//                                                           ),
//                                                         ],
//                                                       ),
//                                                     )
//                                                   ],
//                                                 ),
//                                                 // Row(
//                                                 //   mainAxisAlignment:
//                                                 //       MainAxisAlignment.spaceBetween,
//                                                 //   children: [
//                                                 //     Container(
//                                                 //       padding: EdgeInsets.only(
//                                                 //           left: 10,
//                                                 //           top: 5,
//                                                 //           bottom: 8),
//                                                 //       child: Text("Payment Mode"),
//                                                 //       alignment: Alignment.topLeft,
//                                                 //     ),
//                                                 //     Container(
//                                                 //       //height: 25,
//                                                 //       width: MediaQuery.of(context)
//                                                 //               .size
//                                                 //               .width /
//                                                 //           2.2,
//                                                 //       // color: AppColors.sentColor,
//                                                 //       child: Row(
//                                                 //         children: [
//                                                 //           Flexible(
//                                                 //             child: Container(
//                                                 //                 padding:
//                                                 //                     EdgeInsets.only(
//                                                 //                         left: 25,
//                                                 //                         top: 05,
//                                                 //                         right: 10,
//                                                 //                         bottom: 5),
//                                                 //                 //color: AppColors.primary,
//                                                 //                 child: RichText(
//                                                 //                   text: new TextSpan(
//                                                 //                     text: history[i]
//                                                 //                         ['pay_mode'],
//                                                 //                     style: TextStyle(
//                                                 //                       color: AppColors
//                                                 //                           .grayBorder,
//                                                 //                       fontSize: 12,
//                                                 //                     ),
//                                                 //                   ),
//                                                 //                   textAlign:
//                                                 //                       TextAlign.left,
//                                                 //                 )),
//                                                 //           ),
//                                                 //         ],
//                                                 //       ),
//                                                 //     )
//                                                 //   ],
//                                                 // ),
//                                                 // Row(
//                                                 //   mainAxisAlignment:
//                                                 //       MainAxisAlignment.spaceBetween,
//                                                 //   children: [
//                                                 //     Container(
//                                                 //       padding: EdgeInsets.only(
//                                                 //           left: 10, top: 5, bottom: 8),
//                                                 //       child: Text("Order Status"),
//                                                 //       alignment: Alignment.topLeft,
//                                                 //     ),
//                                                 //     Container(
//                                                 //       //height: 25,
//                                                 //       width:
//                                                 //           MediaQuery.of(context).size.width /
//                                                 //               2.2,
//                                                 //       // color: AppColors.sentColor,
//                                                 //       child: Row(
//                                                 //         children: [
//                                                 //           Flexible(
//                                                 //             child: Container(
//                                                 //                 padding: EdgeInsets.only(
//                                                 //                     left: 25,
//                                                 //                     top: 05,
//                                                 //                     right: 10,
//                                                 //                     bottom: 5),
//                                                 //                 //color: AppColors.primary,
//                                                 //                 child: RichText(
//                                                 //                   text: new TextSpan(
//                                                 //                     text: history[i]['status'] == "0" ? "Pending"
//                                                 //           : history[i]['status'] == "1"?"Vendor Accepted"
//                                                 //           :history[i]['status'] == "2" ? "Service Started"
//                                                 //       :history[i]['status'] == "3"?"Cancel"
//                                                 //       :history[i]['status'] == "4"?"Cancel":
//                                                 //       history[i]['status'] == "5"?"Completed":"",
//                                                 //                     style: TextStyle(
//                                                 //                       color: AppColors
//                                                 //                           .grayBorder,
//                                                 //                       fontSize: 12,
//                                                 //                     ),
//                                                 //                   ),
//                                                 //                   textAlign: TextAlign.left,
//                                                 //                 )),
//                                                 //           ),
//                                                 //         ],
//                                                 //       ),
//                                                 //     )
//                                                 //   ],
//                                                 // ),
//                                                 // // Row(
//                                                 //   mainAxisAlignment:
//                                                 //       MainAxisAlignment.spaceBetween,
//                                                 //   children: [
//                                                 //     Container(
//                                                 //       padding: EdgeInsets.only(
//                                                 //           left: 10, top: 5, bottom: 8),
//                                                 //       child: Text("Service Type"),
//                                                 //       alignment: Alignment.topLeft,
//                                                 //     ),
//                                                 //     Container(
//                                                 //       //height: 25,
//                                                 //       width:
//                                                 //           MediaQuery.of(context).size.width /
//                                                 //               2.2,
//                                                 //       // color: AppColors.sentColor,
//                                                 //       child: Row(
//                                                 //         children: [
//                                                 //           Flexible(
//                                                 //             child: Container(
//                                                 //                 padding: EdgeInsets.only(
//                                                 //                     left: 25,
//                                                 //                     top: 05,
//                                                 //                     right: 10,
//                                                 //                     bottom: 5),
//                                                 //                 //color: AppColors.primary,
//                                                 //                 child: RichText(
//                                                 //                   text: new TextSpan(
//                                                 //                     text: "STANDARD",
//                                                 //                     style: TextStyle(
//                                                 //                       color: AppColors
//                                                 //                           .grayBorder,
//                                                 //                       fontSize: 12,
//                                                 //                     ),
//                                                 //                   ),
//                                                 //                   textAlign: TextAlign.left,
//                                                 //                 )),
//                                                 //           ),
//                                                 //         ],
//                                                 //       ),
//                                                 //     )
//                                                 //   ],
//                                                 // ),
//
//                                                 Row(
//                                                   mainAxisAlignment:
//                                                   MainAxisAlignment.spaceBetween,
//                                                   children: [
//                                                     Container(
//                                                       padding: EdgeInsets.only(
//                                                           left: 10,
//                                                           top: 5,
//                                                           bottom: 0),
//                                                       child: Text(
//                                                         "₹" +
//                                                             history[i]
//                                                             ['total_charge'],
//                                                         style: TextStyle(
//                                                             color:
//                                                             AppColors.redcolor3),
//                                                       ),
//                                                       alignment: Alignment.topLeft,
//                                                     ),
//                                                   ],
//                                                 ),
//                                                 Row(
//                                                   mainAxisAlignment:
//                                                   MainAxisAlignment.spaceBetween,
//                                                   children: [
//                                                     Container(
//                                                       padding: EdgeInsets.only(
//                                                           left: 10,
//                                                           top: 5,
//                                                           bottom: 8),
//                                                       child: Text("Status"),
//                                                       alignment: Alignment.topLeft,
//                                                     ),
//                                                     Container(
//                                                       //height: 25,
//                                                       width: MediaQuery.of(context)
//                                                           .size
//                                                           .width /
//                                                           2.2,
//                                                       // color: AppColors.sentColor,
//                                                       child: Row(
//                                                         children: [
//                                                           Flexible(
//                                                             child: Container(
//                                                                 padding:
//                                                                 EdgeInsets.only(
//                                                                     left: 25,
//                                                                     top: 05,
//                                                                     right: 10,
//                                                                     bottom: 5),
//                                                                 //color: AppColors.primary,
//                                                                 child: RichText(
//                                                                   text: new TextSpan(
//                                                                     text: history[i][
//                                                                     'status'] ==
//                                                                         "0"
//                                                                         ? "Pending"
//                                                                         : history[i][
//                                                                     'status'] ==
//                                                                         "1"
//                                                                         ? "Vendor Accepted"
//                                                                         : history[i]['status'] ==
//                                                                         "2"
//                                                                         ? "Service Started"
//                                                                         : history[i]['status'] == "3"
//                                                                         ? "Cancel"
//                                                                         : history[i]['status'] == "4"
//                                                                         ? "Cancel"
//                                                                         : history[i]['status'] == "5"
//                                                                         ? "Completed"
//                                                                         : "",
//                                                                     style: TextStyle(
//                                                                         color: history[i]
//                                                                         ['status'] ==
//                                                                             "3"
//                                                                             ?AppColors.redcolor3:history[i]
//                                                                         ['status'] ==
//                                                                             "4"
//                                                                             ?AppColors.redcolor3:AppColors.black,
//                                                                         fontSize: 14,
//                                                                         fontWeight: FontWeight.bold
//                                                                     ),
//                                                                   ),
//                                                                   textAlign:
//                                                                   TextAlign.left,
//                                                                 )),
//                                                           ),
//                                                         ],
//                                                       ),
//                                                     )
//                                                   ],
//                                                 ),
//                                                 Container(
//                                                   padding: EdgeInsets.only(top: 5,bottom: 10,right: 20),
//                                                   child: InkWell(onTap: (){
//                                                     vendordetail=history[i]['vendor']==null?"0": vendordetail=history[i]['vendor']['id'];
//                                                     Navigator.push(
//                                                         context,
//                                                         MaterialPageRoute(
//                                                             builder: (context) =>
//                                                                 Bookingdetail(history[i]
//                                                                 ['booking_id'],vendordetail))).then(
//                                                             (value) => getOrderhistorylist());
//                                                     // orderid=history[i]['id'];
//                                                     // getOrderCancel(orderid);
//                                                   },
//                                                     child:
//                                                     Row(
//                                                       mainAxisAlignment: MainAxisAlignment.end,
//                                                       children: [
//
//                                                         Container(
//
//                                                           height: 40,
//                                                           width: 120,
//                                                           decoration: BoxDecoration(
//                                                             color: AppColors.sentColor,
//                                                             borderRadius: BorderRadius.circular(10),
//                                                           ),
//                                                           child:
//                                                           Center(child: Text(loading == true ? "Loading.." :"VIEW DETAILS",style: TextStyle(color: AppColors.white,
//                                                             fontSize: 16,),)),
//                                                         ),
//                                                       ],
//                                                     ),
//                                                   ),
//                                                 )
//                                               ],
//                                             ),
//                                           ),
//                                         )
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             );
//                           }),
//                     ),
//                     // child: SingleChildScrollView(
//                     //   child: ListView(
//                     //     shrinkWrap: true,
//                     //     physics: NeverScrollableScrollPhysics(),
//                     //     children: [
//                     //       Container(
//                     //
//                     //         child: Image(image: NetworkImage("https://www.flashsaletricks.com/wp-content/uploads/2017/11/Mr_Voonik_refer_earn.jpg"),
//                     //         ),
//                     //       ),
//                     //       Container(
//                     //         child:
//                     //         Center(
//                     //           child: Card(
//                     //
//                     //             color: AppColors.grayBorder,
//                     //             shape: RoundedRectangleBorder(
//                     //               borderRadius: BorderRadius.circular(5.0),
//                     //             ),
//                     //             child: Text("It Pays to Have Friends",),
//                     //
//                     //           ),
//                     //         ),
//                     //       )
//                     //     ],
//                     //   ),
//                     // ),
//                   ),
//                 ],
//               ),
//               onRefresh: refreshList,
//             )
//
//         ),
//       ),
//     );
//   }
// }
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/servicepage/vendorpofile.dart';
import 'package:yesmadam/services/apis.dart';

import 'bookingdetail.dart';
import 'bookinghistory.dart';

class Booking extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return BookingView();
  }
}

class BookingView extends State<Booking> {

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  List history = [];
  List ongoinghistory=[];
  var orderid = "";
  var vendordetail="";

  Future getOrderhistorylist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.BOOKINGHISTORY_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "user_id": sp.getString("userid")
    };
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        history = json.decode(res)['data'];
        // orderid=json.decode(res)['data'][0]['id'];
        // print(orderid);
      } else {
        history = [];
      }

      loading = false;
    });
  }
  Future getOngoingOrderhistorylist() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.ONGOINGBOOKINGHISTORY_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      //"cat_id":widget.catid,
      "user_id": sp.getString("userid")
    };
    print(data);
    var res = await apiPostRequestse(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        ongoinghistory = json.decode(res)['data'];
      } else {
        history = [];
      }

      loading = false;
    });
  }

  Future<String> apiPostRequestse(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));

    setState(() {
      Booking();
      // new HomePage();
    });

    return null;
  }
  var rating="";

  @override
  void initState() {
    getOrderhistorylist();
    getOngoingOrderhistorylist();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: AppColors.sentColor,
              toolbarHeight: 60,
              bottom: TabBar(
                indicatorColor: AppColors.white,

                tabs: [


                  Tab(child: Text("ONGOING",style: TextStyle(fontSize: 18),)),
                  Tab(child: Text("History",style: TextStyle(fontSize: 18))),
                ],
              ),
            ),
            body: RefreshIndicator(
              key: refreshKey,
              child:  TabBarView(

                children: [
                  ongoinghistory.length==0?Container(
                      child:
                      Container(
                        color: AppColors.white,


                        child: Image(

                          image: AssetImage("assets/images/datanotfound.jpg"),
                          height: 400,
                        ),
                      )

                  ): Container(
                    child:
                    SingleChildScrollView(
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: ongoinghistory.length,
                          itemBuilder: (BuildContext context, int i) {
                            return Container(
                              padding: EdgeInsets.only(
                                  top: 5, bottom: 5, left: 5, right: 5),
                              child: Container(
                                child: Card(
                                  child: Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: 10, bottom: 10, left: 5, right: 5),
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                child: Text("Order Date:  " +
                                                    ongoinghistory[i]['booking_date']),
                                              ),
                                            ],
                                          ),
                                        ),
                                        InkWell(
                                          onTap: (){
                                            var id=ongoinghistory[i]['vendor']['id']=="0"?"":ongoinghistory[i]['vendor']['id'];
                                            //  print(id);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>

                                                        vendorprofile(id)));

                                          },
                                          child: ongoinghistory[i]['vendor']==null?Container(height: 1,): Container(
                                            //height: 100,
                                            child:
                                            Row(
                                              children: [
                                                Flexible(
                                                  flex: 1,
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10, left: 10, bottom: 10),
                                                    height: 80,
                                                    width: 80,
                                                    //width: MediaQuery.of(context).size.width/2.2,
                                                    decoration: BoxDecoration(
                                                      // color: AppColors.redcolor3,
                                                        border: Border.all(
                                                            color: AppColors.toggele),
                                                        borderRadius: BorderRadius.all(
                                                            Radius.circular(120)),
                                                        image: DecorationImage(
                                                            image: NetworkImage(
                                                              ongoinghistory[i]['vendor']['image']==null?"":ongoinghistory[i]['vendor']['image'],
                                                            ),
                                                            fit: BoxFit.fill)),
                                                  ),
                                                ),
                                                Flexible(
                                                  flex: 2,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(left: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment.start,
                                                          children: [
                                                            Container(
                                                              child: Text(
                                                                ongoinghistory[i]['vendor']['username'] == null
                                                                    ? ""
                                                                    : ongoinghistory[i]['vendor']['username'],
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                    FontWeight.bold,
                                                                    fontSize: 16),
                                                              ),
                                                            ),
                                                            Container(
                                                              child: Row(
                                                                children: [
                                                                  Container(
                                                                    padding: EdgeInsets.only(
                                                                        left: 0, top: 5),
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                      children: [
                                                                        Container(
                                                                            color:
                                                                            Colors.white,
                                                                            child: Icon(
                                                                              Icons.star,
                                                                              size: 18,
                                                                              color: AppColors
                                                                                  .redcolor3,
                                                                            )),
                                                                        Container(
                                                                          padding:
                                                                          EdgeInsets.only(
                                                                              left: 5),
                                                                          child: Text(
                                                                            ongoinghistory[i]['vendor']['average_rating'] == null
                                                                                ? "0"
                                                                                :ongoinghistory[i]['vendor']['average_rating'].toString().replaceRange(1,ongoinghistory[i]['vendor']['average_rating'].length, "."+ongoinghistory[i]['vendor']['average_rating'][2]),

                                                                            style: TextStyle(
                                                                              color: Colors
                                                                                  .black,
                                                                              fontWeight:
                                                                              FontWeight
                                                                                  .bold,
                                                                              fontSize: 14,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    padding: EdgeInsets.only(
                                                                        left: 10,top: 5),
                                                                    child: Text(
                                                                      ongoinghistory[i]['vendor']['total_number_rating'] == null
                                                                          ? "0"
                                                                          : ongoinghistory[i]['vendor']['total_number_rating'] +"  Ratings",
                                                                      style: TextStyle(
                                                                          color: AppColors
                                                                              .black),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      InkWell(
                                                        onTap: ()
                                                        async {
                                                          var number= ongoinghistory[i]['vendor']['mobile']==null?"0":ongoinghistory[i]['vendor']['mobile'];
                                                          var url = "tel:"+"$number";
                                                          if (await canLaunch(url)) {
                                                            await launch(url);
                                                          } else {
                                                            throw 'Could not launch $url';
                                                          }

                                                        },
                                                        child: Container(
                                                          child: Column(
                                                            children: [
                                                              Container(
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                      right: 5,bottom: 5
                                                                  ),
                                                                  child: Icon(
                                                                      Icons.call,size: 25,
                                                                      color: AppColors.primary2
                                                                  ),

                                                                ),
                                                              ),
                                                              Text(
                                                                "Call Vendor",
                                                                style: TextStyle(
                                                                    color: AppColors.primary2,
                                                                    fontSize: 17),
                                                              )

                                                            ],
                                                          ),

                                                        ),
                                                      )


                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Material(
                                          child: InkWell(
                                            onTap: () {
                                              vendordetail=ongoinghistory[i]['vendor']==null?"0": vendordetail=ongoinghistory[i]['vendor']['id'];

                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Bookingdetail(ongoinghistory[i]
                                                          ['booking_id'],vendordetail)))
                                                  .then(
                                                      (value) =>getOngoingOrderhistorylist());
                                            },
                                            child: Container(
                                              child: Column(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        top: 5,
                                                        bottom: 0,
                                                        left: 5,
                                                        right: 5),
                                                    color: AppColors.grayBorder,
                                                    height: 1,
                                                  ),
                                                  // Container(
                                                  //   padding: EdgeInsets.only(
                                                  //       top: 0,
                                                  //       bottom: 10,
                                                  //       left: 5,
                                                  //       right: 5),
                                                  //   child: Text("hhhh",
                                                  //     // history[i]['service_title'] ==
                                                  //     //         null
                                                  //     //     ? ""
                                                  //     //     : history[i]['service_title'],
                                                  //     style: TextStyle(
                                                  //       color: AppColors.black,
                                                  //       fontWeight: FontWeight.bold,
                                                  //     ),
                                                  //   ),
                                                  // ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(
                                                            left: 10,
                                                            top: 5,
                                                            bottom: 0),
                                                        child: Text("Booking Id:"),
                                                        alignment: Alignment.topLeft,
                                                      ),
                                                      Container(
                                                        //height: 25,
                                                        width: MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                            2.2,
                                                        // color: AppColors.sentColor,
                                                        child: Row(
                                                          children: [
                                                            Flexible(
                                                              child: Container(
                                                                  padding:
                                                                  EdgeInsets.only(
                                                                      left: 25,
                                                                      top: 05,
                                                                      right: 10,
                                                                      bottom: 0),
                                                                  //color: AppColors.primary,
                                                                  child: RichText(
                                                                    text: new TextSpan(
                                                                      text: ongoinghistory[i][
                                                                      'booking_id'],
                                                                      style: TextStyle(
                                                                        color: AppColors
                                                                            .black,
                                                                        fontSize: 14,
                                                                      ),
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.left,
                                                                  )),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),

                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(
                                                            left: 10,
                                                            top: 5,
                                                            bottom: 8),
                                                        child: Text("Booking Date/Time"),
                                                        alignment: Alignment.topLeft,
                                                      ),
                                                      Container(
                                                        //height: 25,
                                                        width: MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                            2.2,
                                                        // color: AppColors.sentColor,
                                                        child: Row(
                                                          children: [
                                                            Flexible(
                                                              child: Container(
                                                                padding:
                                                                EdgeInsets.only(
                                                                    left: 25,
                                                                    top: 05,
                                                                    right: 10,
                                                                    bottom: 5),
                                                                //color: AppColors.primary,
                                                                child: Text(
                                                                  ongoinghistory[i]
                                                                  ['date' ] +  " "+ongoinghistory[i]
                                                                  ['start_time'] ,
                                                                  style: TextStyle(
                                                                    color: AppColors
                                                                        .black,
                                                                    fontSize: 14,
                                                                  ),
                                                                ),

                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),

                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(
                                                            left: 10,
                                                            top: 5,
                                                            bottom: 8),
                                                        child: Text("Service Name"),
                                                        alignment: Alignment.topLeft,
                                                      ),
                                                      Container(
                                                        //height: 25,
                                                        width: MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                            2.2,
                                                        // color: AppColors.sentColor,
                                                        child: Row(
                                                          children: [
                                                            Flexible(
                                                              child: Container(
                                                                  padding:
                                                                  EdgeInsets.only(
                                                                      left: 25,
                                                                      top: 05,
                                                                      right: 10,
                                                                      bottom: 5),
                                                                  //color: AppColors.primary,
                                                                  child: RichText(
                                                                    text: new TextSpan(
                                                                      text: ongoinghistory[i]
                                                                      ['service_title'],
                                                                      style: TextStyle(
                                                                        color: AppColors
                                                                            .sentColor,
                                                                        fontSize: 14,
                                                                      ),
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.left,
                                                                  )),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  // Row(
                                                  //   mainAxisAlignment:
                                                  //       MainAxisAlignment.spaceBetween,
                                                  //   children: [
                                                  //     Container(
                                                  //       padding: EdgeInsets.only(
                                                  //           left: 10,
                                                  //           top: 5,
                                                  //           bottom: 8),
                                                  //       child: Text("Payment Mode"),
                                                  //       alignment: Alignment.topLeft,
                                                  //     ),
                                                  //     Container(
                                                  //       //height: 25,
                                                  //       width: MediaQuery.of(context)
                                                  //               .size
                                                  //               .width /
                                                  //           2.2,
                                                  //       // color: AppColors.sentColor,
                                                  //       child: Row(
                                                  //         children: [
                                                  //           Flexible(
                                                  //             child: Container(
                                                  //                 padding:
                                                  //                     EdgeInsets.only(
                                                  //                         left: 25,
                                                  //                         top: 05,
                                                  //                         right: 10,
                                                  //                         bottom: 5),
                                                  //                 //color: AppColors.primary,
                                                  //                 child: RichText(
                                                  //                   text: new TextSpan(
                                                  //                     text: history[i]
                                                  //                         ['pay_mode'],
                                                  //                     style: TextStyle(
                                                  //                       color: AppColors
                                                  //                           .grayBorder,
                                                  //                       fontSize: 12,
                                                  //                     ),
                                                  //                   ),
                                                  //                   textAlign:
                                                  //                       TextAlign.left,
                                                  //                 )),
                                                  //           ),
                                                  //         ],
                                                  //       ),
                                                  //     )
                                                  //   ],
                                                  // ),
                                                  // Row(
                                                  //   mainAxisAlignment:
                                                  //       MainAxisAlignment.spaceBetween,
                                                  //   children: [
                                                  //     Container(
                                                  //       padding: EdgeInsets.only(
                                                  //           left: 10, top: 5, bottom: 8),
                                                  //       child: Text("Order Status"),
                                                  //       alignment: Alignment.topLeft,
                                                  //     ),
                                                  //     Container(
                                                  //       //height: 25,
                                                  //       width:
                                                  //           MediaQuery.of(context).size.width /
                                                  //               2.2,
                                                  //       // color: AppColors.sentColor,
                                                  //       child: Row(
                                                  //         children: [
                                                  //           Flexible(
                                                  //             child: Container(
                                                  //                 padding: EdgeInsets.only(
                                                  //                     left: 25,
                                                  //                     top: 05,
                                                  //                     right: 10,
                                                  //                     bottom: 5),
                                                  //                 //color: AppColors.primary,
                                                  //                 child: RichText(
                                                  //                   text: new TextSpan(
                                                  //                     text: history[i]['status'] == "0" ? "Pending"
                                                  //           : history[i]['status'] == "1"?"Vendor Accepted"
                                                  //           :history[i]['status'] == "2" ? "Service Started"
                                                  //       :history[i]['status'] == "3"?"Cancel"
                                                  //       :history[i]['status'] == "4"?"Cancel":
                                                  //       history[i]['status'] == "5"?"Completed":"",
                                                  //                     style: TextStyle(
                                                  //                       color: AppColors
                                                  //                           .grayBorder,
                                                  //                       fontSize: 12,
                                                  //                     ),
                                                  //                   ),
                                                  //                   textAlign: TextAlign.left,
                                                  //                 )),
                                                  //           ),
                                                  //         ],
                                                  //       ),
                                                  //     )
                                                  //   ],
                                                  // ),
                                                  // // Row(
                                                  //   mainAxisAlignment:
                                                  //       MainAxisAlignment.spaceBetween,
                                                  //   children: [
                                                  //     Container(
                                                  //       padding: EdgeInsets.only(
                                                  //           left: 10, top: 5, bottom: 8),
                                                  //       child: Text("Service Type"),
                                                  //       alignment: Alignment.topLeft,
                                                  //     ),
                                                  //     Container(
                                                  //       //height: 25,
                                                  //       width:
                                                  //           MediaQuery.of(context).size.width /
                                                  //               2.2,
                                                  //       // color: AppColors.sentColor,
                                                  //       child: Row(
                                                  //         children: [
                                                  //           Flexible(
                                                  //             child: Container(
                                                  //                 padding: EdgeInsets.only(
                                                  //                     left: 25,
                                                  //                     top: 05,
                                                  //                     right: 10,
                                                  //                     bottom: 5),
                                                  //                 //color: AppColors.primary,
                                                  //                 child: RichText(
                                                  //                   text: new TextSpan(
                                                  //                     text: "STANDARD",
                                                  //                     style: TextStyle(
                                                  //                       color: AppColors
                                                  //                           .grayBorder,
                                                  //                       fontSize: 12,
                                                  //                     ),
                                                  //                   ),
                                                  //                   textAlign: TextAlign.left,
                                                  //                 )),
                                                  //           ),
                                                  //         ],
                                                  //       ),
                                                  //     )
                                                  //   ],
                                                  // ),

                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(
                                                            left: 10,
                                                            top: 5,
                                                            bottom: 0),
                                                        child: Text(
                                                          "₹" +
                                                              ongoinghistory[i]
                                                              ['total_charge'],
                                                          style: TextStyle(
                                                              color:
                                                              AppColors.redcolor3),
                                                        ),
                                                        alignment: Alignment.topLeft,
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(
                                                            left: 10,
                                                            top: 5,
                                                            bottom: 8),
                                                        child: Text("Status"),
                                                        alignment: Alignment.topLeft,
                                                      ),
                                                      Container(
                                                        //height: 25,
                                                        width: MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                            2.2,
                                                        // color: AppColors.sentColor,
                                                        child: Row(
                                                          children: [
                                                            Flexible(
                                                              child: Container(
                                                                  padding:
                                                                  EdgeInsets.only(
                                                                      left: 25,
                                                                      top: 05,
                                                                      right: 10,
                                                                      bottom: 5),
                                                                  //color: AppColors.primary,
                                                                  child: RichText(
                                                                    text: new TextSpan(
                                                                      text: ongoinghistory[i][
                                                                      'status'] ==
                                                                          "0"
                                                                          ? "Pending"
                                                                          : ongoinghistory[i][
                                                                      'status'] ==
                                                                          "1"
                                                                          ? "Partner Accepted"
                                                                          : ongoinghistory[i]['status'] ==
                                                                          "2"
                                                                          ? "Service Started"
                                                                          : ongoinghistory[i]['status'] == "3"
                                                                          ? "Cancel"
                                                                          : ongoinghistory[i]['status'] == "4"
                                                                          ? "Cancel"
                                                                          : ongoinghistory[i]['status'] == "5"
                                                                          ? "Completed"
                                                                          : "",
                                                                      style: TextStyle(
                                                                          color: ongoinghistory[i]
                                                                          ['status'] ==
                                                                              "3"
                                                                              ?AppColors.redcolor3:ongoinghistory[i]
                                                                          ['status'] ==
                                                                              "4"
                                                                              ?AppColors.redcolor3:AppColors.black,
                                                                          fontSize: 14,
                                                                          fontWeight: FontWeight.bold
                                                                      ),
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.left,
                                                                  )),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  Container(
                                                    padding: EdgeInsets.only(top: 5,bottom: 10,right: 20),
                                                    child: InkWell(onTap: (){
                                                      vendordetail=ongoinghistory[i]['vendor']==null?"0": vendordetail=ongoinghistory[i]['vendor']['id'];

                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  Bookingdetail(ongoinghistory[i]
                                                                  ['booking_id'],vendordetail)))
                                                          .then(
                                                              (value) =>getOngoingOrderhistorylist());
                                                      // orderid=history[i]['id'];
                                                      // getOrderCancel(orderid);
                                                    },
                                                      child:
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        children: [

                                                          Container(

                                                            height: 40,
                                                            width: 120,
                                                            decoration: BoxDecoration(
                                                              color: AppColors.sentColor,
                                                              borderRadius: BorderRadius.circular(10),
                                                            ),
                                                            child:
                                                            Center(child: Text(loading == true ? "Loading.." :"VIEW DETAILS",style: TextStyle(color: AppColors.white,
                                                              fontSize: 16,),)),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                    // child: SingleChildScrollView(
                    //   child: ListView(
                    //     shrinkWrap: true,
                    //     physics: NeverScrollableScrollPhysics(),
                    //     children: [
                    //       Container(
                    //
                    //         child: Image(image: NetworkImage("https://www.flashsaletricks.com/wp-content/uploads/2017/11/Mr_Voonik_refer_earn.jpg"),
                    //         ),
                    //       ),
                    //       Container(
                    //         child:
                    //         Center(
                    //           child: Card(
                    //
                    //             color: AppColors.grayBorder,
                    //             shape: RoundedRectangleBorder(
                    //               borderRadius: BorderRadius.circular(5.0),
                    //             ),
                    //             child: Text("It Pays to Have Friends",),
                    //
                    //           ),
                    //         ),
                    //       )
                    //     ],
                    //   ),
                    // ),
                  ),
                  history.length==0?Container(
                      child:
                      Container(
                        color: AppColors.white,


                        child: Image(

                          image: AssetImage("assets/images/datanotfound.jpg"),
                          height: 400,
                        ),
                      )

                  ): Container(
                    child:
                    SingleChildScrollView(
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: history.length,
                          itemBuilder: (BuildContext context, int i) {
                            return Container(
                              padding: EdgeInsets.only(
                                  top: 5, bottom: 5, left: 5, right: 5),
                              child: Container(
                                child: Card(
                                  child: Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: 10, bottom: 10, left: 5, right: 5),
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                child: Text("Order Date:  " +
                                                    history[i]['booking_date']),
                                              ),
                                            ],
                                          ),
                                        ),
                                        history[i]['status'] == "3"?Container(height: 1,):InkWell(
                                          onTap: (){
                                            var id=history[i]['vendor']['id']=="0"?"":history[i]['vendor']['id'];
                                            //  print(id);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>

                                                        vendorprofile(id)));

                                          },
                                          child: history[i]['vendor']==null?Container(height: 1,): Container(
                                            //height: 100,
                                            child: Row(
                                              children: [
                                                Flexible(
                                                  flex: 1,
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10, left: 10, bottom: 10),
                                                    height: 80,
                                                    width: 80,
                                                    //width: MediaQuery.of(context).size.width/2.2,
                                                    decoration: BoxDecoration(
                                                      // color: AppColors.redcolor3,
                                                        border: Border.all(
                                                            color: AppColors.toggele),
                                                        borderRadius: BorderRadius.all(
                                                            Radius.circular(120)),
                                                        image: DecorationImage(
                                                            image: NetworkImage(
                                                              history[i]['vendor']['image']==null?"":history[i]['vendor']['image'],
                                                            ),
                                                            fit: BoxFit.fill)),
                                                  ),
                                                ),
                                                Flexible(
                                                  flex: 2,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(left: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment.start,
                                                          children: [
                                                            Container(
                                                              child: Text(
                                                                history[i]['vendor']['username'] == null
                                                                    ? ""
                                                                    : history[i]['vendor']['username'],
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                    FontWeight.bold,
                                                                    fontSize: 16),
                                                              ),
                                                            ),
                                                            Container(
                                                              child: Row(
                                                                children: [
                                                                  Container(
                                                                    padding: EdgeInsets.only(
                                                                        left: 0, top: 5),
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                      children: [
                                                                        Container(
                                                                            color:
                                                                            Colors.white,
                                                                            child: Icon(
                                                                              Icons.star,
                                                                              size: 18,
                                                                              color: AppColors
                                                                                  .redcolor3,
                                                                            )),
                                                                        Container(
                                                                          padding:
                                                                          EdgeInsets.only(
                                                                              left: 5),
                                                                          child: Text(
                                                                            history[i]['vendor']['average_rating'] == null
                                                                                ? "0"
                                                                                : history[i]['vendor']['average_rating'].toString().replaceRange(1,history[i]['vendor']['average_rating'].length, "."+history[i]['vendor']['average_rating'][2]),
                                                                            style: TextStyle(
                                                                              color: Colors
                                                                                  .black,
                                                                              fontWeight:
                                                                              FontWeight
                                                                                  .bold,
                                                                              fontSize: 14,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    padding: EdgeInsets.only(
                                                                        left: 10,top: 5),
                                                                    child: Text(
                                                                      history[i]['vendor']['total_number_rating'] == null
                                                                          ? "0"
                                                                          : history[i]['vendor']['total_number_rating'] +"  Ratings",
                                                                      style: TextStyle(
                                                                          color: AppColors
                                                                              .black),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      history[i]['status'] == "5"
                                                          ? Container(height: 1,):InkWell(
                                                        onTap: ()
                                                        async {
                                                          var number= history[i]['vendor']['mobile']==null?"0":history[i]['vendor']['mobile'];
                                                          var url = "tel:"+"$number";
                                                          if (await canLaunch(url)) {
                                                            await launch(url);
                                                          } else {
                                                            throw 'Could not launch $url';
                                                          }

                                                        },
                                                        child: Container(
                                                          child: Column(
                                                            children: [
                                                              Container(
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                      right: 5,bottom: 5
                                                                  ),
                                                                  child: Icon(
                                                                      Icons.call,size: 25,
                                                                      color: AppColors.primary2
                                                                  ),

                                                                ),
                                                              ),
                                                              Text(
                                                                "Call Vendor",
                                                                style: TextStyle(
                                                                    color: AppColors.primary2,
                                                                    fontSize: 17),
                                                              )

                                                            ],
                                                          ),

                                                        ),
                                                      )


                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            vendordetail=history[i]['vendor']==null?"0": vendordetail=history[i]['vendor']['id'];
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Bookingdetail(history[i]
                                                        ['booking_id'],vendordetail))).then(
                                                    (value) => getOrderhistorylist());
                                          },
                                          child: Container(
                                            child: Column(
                                              children: [
                                                Container(
                                                  padding: EdgeInsets.only(
                                                      top: 5,
                                                      bottom: 0,
                                                      left: 5,
                                                      right: 5),
                                                  color: AppColors.grayBorder,
                                                  height: 1,
                                                ),
                                                // Container(
                                                //   padding: EdgeInsets.only(
                                                //       top: 0,
                                                //       bottom: 10,
                                                //       left: 5,
                                                //       right: 5),
                                                //   child: Text("hhhh",
                                                //     // history[i]['service_title'] ==
                                                //     //         null
                                                //     //     ? ""
                                                //     //     : history[i]['service_title'],
                                                //     style: TextStyle(
                                                //       color: AppColors.black,
                                                //       fontWeight: FontWeight.bold,
                                                //     ),
                                                //   ),
                                                // ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          top: 5,
                                                          bottom: 0),
                                                      child: Text("Booking Id:"),
                                                      alignment: Alignment.topLeft,
                                                    ),
                                                    Container(
                                                      //height: 25,
                                                      width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                          2.2,
                                                      // color: AppColors.sentColor,
                                                      child: Row(
                                                        children: [
                                                          Flexible(
                                                            child: Container(
                                                                padding:
                                                                EdgeInsets.only(
                                                                    left: 25,
                                                                    top: 05,
                                                                    right: 10,
                                                                    bottom: 0),
                                                                //color: AppColors.primary,
                                                                child: RichText(
                                                                  text: new TextSpan(
                                                                    text: history[i][
                                                                    'booking_id'],
                                                                    style: TextStyle(
                                                                      color: AppColors
                                                                          .black,
                                                                      fontSize: 14,
                                                                    ),
                                                                  ),
                                                                  textAlign:
                                                                  TextAlign.left,
                                                                )),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),

                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          top: 5,
                                                          bottom: 8),
                                                      child: Text("Booking Date/Time"),
                                                      alignment: Alignment.topLeft,
                                                    ),
                                                    Container(
                                                      //height: 25,
                                                      width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                          2.2,
                                                      // color: AppColors.sentColor,
                                                      child: Row(
                                                        children: [
                                                          Flexible(
                                                            child: Container(
                                                              padding:
                                                              EdgeInsets.only(
                                                                  left: 25,
                                                                  top: 05,
                                                                  right: 10,
                                                                  bottom: 5),
                                                              //color: AppColors.primary,
                                                              child: Text(
                                                                history[i]
                                                                ['date' ] +  "  "+  history[i]
                                                                ['start_time']  ,
                                                                style: TextStyle(
                                                                  color: AppColors
                                                                      .black,
                                                                  fontSize: 14,
                                                                ),
                                                              ),

                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),

                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          top: 5,
                                                          bottom: 8),
                                                      child: Text("Service Name"),
                                                      alignment: Alignment.topLeft,
                                                    ),
                                                    Container(
                                                      //height: 25,
                                                      width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                          2.2,
                                                      // color: AppColors.sentColor,
                                                      child: Row(
                                                        children: [
                                                          Flexible(
                                                            child: Container(
                                                                padding:
                                                                EdgeInsets.only(
                                                                    left: 25,
                                                                    top: 05,
                                                                    right: 10,
                                                                    bottom: 5),
                                                                //color: AppColors.primary,
                                                                child: RichText(
                                                                  text: new TextSpan(
                                                                    text: history[i]
                                                                    ['service_title'],
                                                                    style: TextStyle(
                                                                        color: AppColors
                                                                            .sentColor,
                                                                        fontSize: 14,
                                                                        fontWeight: FontWeight.w400
                                                                    ),
                                                                  ),
                                                                  textAlign:
                                                                  TextAlign.left,
                                                                )),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                // Row(
                                                //   mainAxisAlignment:
                                                //       MainAxisAlignment.spaceBetween,
                                                //   children: [
                                                //     Container(
                                                //       padding: EdgeInsets.only(
                                                //           left: 10,
                                                //           top: 5,
                                                //           bottom: 8),
                                                //       child: Text("Payment Mode"),
                                                //       alignment: Alignment.topLeft,
                                                //     ),
                                                //     Container(
                                                //       //height: 25,
                                                //       width: MediaQuery.of(context)
                                                //               .size
                                                //               .width /
                                                //           2.2,
                                                //       // color: AppColors.sentColor,
                                                //       child: Row(
                                                //         children: [
                                                //           Flexible(
                                                //             child: Container(
                                                //                 padding:
                                                //                     EdgeInsets.only(
                                                //                         left: 25,
                                                //                         top: 05,
                                                //                         right: 10,
                                                //                         bottom: 5),
                                                //                 //color: AppColors.primary,
                                                //                 child: RichText(
                                                //                   text: new TextSpan(
                                                //                     text: history[i]
                                                //                         ['pay_mode'],
                                                //                     style: TextStyle(
                                                //                       color: AppColors
                                                //                           .grayBorder,
                                                //                       fontSize: 12,
                                                //                     ),
                                                //                   ),
                                                //                   textAlign:
                                                //                       TextAlign.left,
                                                //                 )),
                                                //           ),
                                                //         ],
                                                //       ),
                                                //     )
                                                //   ],
                                                // ),
                                                // Row(
                                                //   mainAxisAlignment:
                                                //       MainAxisAlignment.spaceBetween,
                                                //   children: [
                                                //     Container(
                                                //       padding: EdgeInsets.only(
                                                //           left: 10, top: 5, bottom: 8),
                                                //       child: Text("Order Status"),
                                                //       alignment: Alignment.topLeft,
                                                //     ),
                                                //     Container(
                                                //       //height: 25,
                                                //       width:
                                                //           MediaQuery.of(context).size.width /
                                                //               2.2,
                                                //       // color: AppColors.sentColor,
                                                //       child: Row(
                                                //         children: [
                                                //           Flexible(
                                                //             child: Container(
                                                //                 padding: EdgeInsets.only(
                                                //                     left: 25,
                                                //                     top: 05,
                                                //                     right: 10,
                                                //                     bottom: 5),
                                                //                 //color: AppColors.primary,
                                                //                 child: RichText(
                                                //                   text: new TextSpan(
                                                //                     text: history[i]['status'] == "0" ? "Pending"
                                                //           : history[i]['status'] == "1"?"Vendor Accepted"
                                                //           :history[i]['status'] == "2" ? "Service Started"
                                                //       :history[i]['status'] == "3"?"Cancel"
                                                //       :history[i]['status'] == "4"?"Cancel":
                                                //       history[i]['status'] == "5"?"Completed":"",
                                                //                     style: TextStyle(
                                                //                       color: AppColors
                                                //                           .grayBorder,
                                                //                       fontSize: 12,
                                                //                     ),
                                                //                   ),
                                                //                   textAlign: TextAlign.left,
                                                //                 )),
                                                //           ),
                                                //         ],
                                                //       ),
                                                //     )
                                                //   ],
                                                // ),
                                                // // Row(
                                                //   mainAxisAlignment:
                                                //       MainAxisAlignment.spaceBetween,
                                                //   children: [
                                                //     Container(
                                                //       padding: EdgeInsets.only(
                                                //           left: 10, top: 5, bottom: 8),
                                                //       child: Text("Service Type"),
                                                //       alignment: Alignment.topLeft,
                                                //     ),
                                                //     Container(
                                                //       //height: 25,
                                                //       width:
                                                //           MediaQuery.of(context).size.width /
                                                //               2.2,
                                                //       // color: AppColors.sentColor,
                                                //       child: Row(
                                                //         children: [
                                                //           Flexible(
                                                //             child: Container(
                                                //                 padding: EdgeInsets.only(
                                                //                     left: 25,
                                                //                     top: 05,
                                                //                     right: 10,
                                                //                     bottom: 5),
                                                //                 //color: AppColors.primary,
                                                //                 child: RichText(
                                                //                   text: new TextSpan(
                                                //                     text: "STANDARD",
                                                //                     style: TextStyle(
                                                //                       color: AppColors
                                                //                           .grayBorder,
                                                //                       fontSize: 12,
                                                //                     ),
                                                //                   ),
                                                //                   textAlign: TextAlign.left,
                                                //                 )),
                                                //           ),
                                                //         ],
                                                //       ),
                                                //     )
                                                //   ],
                                                // ),

                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          top: 5,
                                                          bottom: 0),
                                                      child: Text(
                                                        "₹" +
                                                            history[i]
                                                            ['total_charge'],
                                                        style: TextStyle(
                                                            color:
                                                            AppColors.redcolor3),
                                                      ),
                                                      alignment: Alignment.topLeft,
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          top: 5,
                                                          bottom: 8),
                                                      child: Text("Status"),
                                                      alignment: Alignment.topLeft,
                                                    ),
                                                    Container(
                                                      //height: 25,
                                                      width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                          2.2,
                                                      // color: AppColors.sentColor,
                                                      child: Row(
                                                        children: [
                                                          Flexible(
                                                            child: Container(
                                                                padding:
                                                                EdgeInsets.only(
                                                                    left: 25,
                                                                    top: 05,
                                                                    right: 10,
                                                                    bottom: 5),
                                                                //color: AppColors.primary,
                                                                child: RichText(
                                                                  text: new TextSpan(
                                                                    text: history[i][
                                                                    'status'] ==
                                                                        "0"
                                                                        ? "Pending"
                                                                        : history[i][
                                                                    'status'] ==
                                                                        "1"
                                                                        ? "Partner Accepted"
                                                                        : history[i]['status'] ==
                                                                        "2"
                                                                        ? "Service Started"
                                                                        : history[i]['status'] == "3"
                                                                        ? "Cancel"
                                                                        : history[i]['status'] == "4"
                                                                        ? "Cancel"
                                                                        : history[i]['status'] == "5"
                                                                        ? "Completed"
                                                                        : "",
                                                                    style: TextStyle(
                                                                        color: history[i]
                                                                        ['status'] ==
                                                                            "3"
                                                                            ?AppColors.redcolor3:history[i]
                                                                        ['status'] ==
                                                                            "4"
                                                                            ?AppColors.redcolor3:AppColors.black,
                                                                        fontSize: 14,
                                                                        fontWeight: FontWeight.bold
                                                                    ),
                                                                  ),
                                                                  textAlign:
                                                                  TextAlign.left,
                                                                )),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(top: 5,bottom: 10,right: 20),
                                                  child: InkWell(onTap: (){
                                                    vendordetail=history[i]['vendor']==null?"0": vendordetail=history[i]['vendor']['id'];
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                Bookingdetail(history[i]
                                                                ['booking_id'],vendordetail))).then(
                                                            (value) => getOrderhistorylist());
                                                    // orderid=history[i]['id'];
                                                    // getOrderCancel(orderid);
                                                  },
                                                    child:
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [

                                                        Container(

                                                          height: 40,
                                                          width: 120,
                                                          decoration: BoxDecoration(
                                                            color: AppColors.sentColor,
                                                            borderRadius: BorderRadius.circular(10),
                                                          ),
                                                          child:
                                                          Center(child: Text(loading == true ? "Loading.." :"VIEW DETAILS",style: TextStyle(color: AppColors.white,
                                                            fontSize: 16,),)),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                    // child: SingleChildScrollView(
                    //   child: ListView(
                    //     shrinkWrap: true,
                    //     physics: NeverScrollableScrollPhysics(),
                    //     children: [
                    //       Container(
                    //
                    //         child: Image(image: NetworkImage("https://www.flashsaletricks.com/wp-content/uploads/2017/11/Mr_Voonik_refer_earn.jpg"),
                    //         ),
                    //       ),
                    //       Container(
                    //         child:
                    //         Center(
                    //           child: Card(
                    //
                    //             color: AppColors.grayBorder,
                    //             shape: RoundedRectangleBorder(
                    //               borderRadius: BorderRadius.circular(5.0),
                    //             ),
                    //             child: Text("It Pays to Have Friends",),
                    //
                    //           ),
                    //         ),
                    //       )
                    //     ],
                    //   ),
                    // ),
                  ),
                ],
              ),
              onRefresh: refreshList,
            )

        ),
      ),
    );
  }
}
