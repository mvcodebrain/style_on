import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/payment.dart';

class termpage extends StatefulWidget{
  final totalprice;
  final date;
  final time;
  final catid;
  final endtime;
  final timeid;
  final vendorid;
  final coupondiscountprices;
  final couponid;
  final termdetail;
  termpage(this.totalprice,this.date,this.time,this.catid,this.endtime,this.timeid,this.vendorid,this.coupondiscountprices,this.couponid,this.termdetail);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return termpageloader();

  }

}
class termpageloader extends State<termpage>{
  int selectedRadio = 0;
  var valuefirst = "";
  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.sentColor,
        title: Text("Term Page",style: TextStyle(color: AppColors.white),),
        actionsIconTheme: IconThemeData(
          color: AppColors.white
        ),
        // elevation: loading == true ? 0 : 1,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: new Container(

              padding: EdgeInsets.only(top: 10),
              child: Container(
                color: Colors.white,
                child: new Column(
                  children: <Widget>[




                    new Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Divider(),
                    ),
                    new ListTile(

                      // onTap: () {
                      //   setListing();
                      //   _setRadio(2);
                      // },
                      title: Text(
                        "Terms And Condition",
                        // style: TextStyle(fontSize: 18),
                      ),
                      leading:  Checkbox(
                        checkColor: AppColors.black,
                        activeColor: Colors.white,

                        value: valuefirst == "1" ? true : false,
                        onChanged: (bool value) {
                          setState(() {
                            valuefirst = "1";


                          });
                          //getcheckseat();
                        },
                      ),
                      // trailing: IconButton(
                      //   onPressed: () {},
                      //   icon: Icon(Icons.supervised_user_circle_outlined),
                      // ),
                    )
                  ],
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(
                  left: 20,
                  right : 20,
                  top: 10,
                  bottom: 20
              ),
              child: Html(
                data: widget.termdetail,
              ),
            ),
          ),


        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(bottom: 20, left: 10, right: 10),
        child: Material(
          color: AppColors.sentColor,
          //Color.fromRGBO(0, 0, 0, 1),
          borderRadius: BorderRadius.circular(10),
          child: InkWell(
            borderRadius: BorderRadius.circular(10),
            splashColor: Colors.blueAccent,
            onTap: () {
              if(valuefirst==""){
                alertDailog("Select The Term And Condition") ;


              }
              else{
                Navigator.push(context, MaterialPageRoute(builder: (context)=>PaymentPage(widget.totalprice,widget.date,widget.time,widget.catid,widget.endtime,widget.timeid,widget.vendorid,widget.coupondiscountprices,widget.couponid)));

              }

              //Navigator.push(context, MaterialPageRoute(builder: (context)=>Otp()));
            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(0),
              ),
              child: Center(
                child: Text(
                  "Continue",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontSize: 20),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

}