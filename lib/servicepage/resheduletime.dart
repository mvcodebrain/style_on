import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/payment.dart';

import 'package:yesmadam/services/apis.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class rescheduletimePage extends StatefulWidget {
  final bookingid;
  final vendorid;
  final timeslot;
  final date;
  rescheduletimePage(this.bookingid, this.vendorid, this.timeslot,this.date);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return rescheduletimePageLoader();
  }
}

class rescheduletimePageLoader extends State<rescheduletimePage> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  List records = [];
  List timerecord = [];
  var dates = "";
  var dateslen = 0;

  List times = [];
  var timelen = 0;

  var valuefirst = "";

  var scheduledtime="";


  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  var selectedindex = 0;
  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }

  _selectTimeSlot(index, dates) async {
    setState(() {
      selectedindex = index;
      times = records[index]['time_slot'];
      valuefirst = "";
      date = dates;
    });
  }

  var timeid = "";

  slectTime(startTimeset, endtimeset, id) {
    setState(() {
      starttime = startTimeset;
      endTime = endtimeset;
      timeid = id;
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),

      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 5),

      // action: ,
    ));
  }

  _showInSnackBarr(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 5),
      // action: ,
    ));
  }
  var dateindex=0;
  Future getdate() async {
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
    });
    var url = Apis.Blocktimedate;
    var data = {
      "vendor_id": widget.vendorid,
      "booking_id":widget.bookingid

    };
    print(data);

    var res = await apiPostRequestss(url, data);
    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          records = json.decode(res)['data'];
          date = records[0]['date'].toString();
          times = json.decode(res)['data'][selectedindex]['time_slot'];
          print(dates);
          debugPrint("bbbbb"+widget.date);

          for(var i=0;i<records.length;i++){
            debugPrint("kjjjj"+dateindex.toString());
            debugPrint(records[i]['date'].toString()+" 00:00:00"+" == "+widget.date.toString());
            if(records[i]['date'].toString()+" 00:00:00" == widget.date.toString()){
              dateindex = i;
              print("found"+i.toString());
              debugPrint("kjjjj"+dateindex.toString());
            }else{
              print("not found");
            }
          }

          for(var i =0; i < times.length; i++){
            if(times[i]['start_time'].toString() == widget.timeslot.toString()){
              dateindex = i;
            }
          }

        });
      } else {
        setState(() {
          records = [];
          date = "";
          times = [];
        });
        _showInSnackBar("No schedule Available");
      }
      loading = false;
    });

    // Navigator.pop(context);
  }

  Future<String> apiPostRequestss(String url, data) async {
    //_onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    //Navigator.pop(context);
    return reply;
  }
  // Future getblocktime() async {
  //   // var sp = await SharedPreferences.getInstance();
  //   setState(() {
  //     loading = true;
  //     ani = false;
  //     fail = false;
  //     _color = Colors.black.withOpacity(0.3);
  //     // Navigator.pop(context);
  //   });
  //
  //   var url = Apis.Blocktime;
  //   var sp = await SharedPreferences.getInstance();
  //   // var data = json.encode(map);
  //   var data = {
  //     "booking_id": widget.bookingid,
  //
  //   };
  //   print(data);
  //   var res = await apiPostRequests(url, data);
  //
  //   print(res);
  //   setState(() {
  //     if (json.decode(res)['status'] == 1) {
  //       scheduledtime=json.decode(res)['data']['scheduled_time'];
  //       print(scheduledtime);
  //       // alertDailog("Booking Re-scheduled succesfully");
  //     } else {
  //       // _showInSnackBarr("Booking Re-scheduled failed");
  //       // history = [];
  //
  //     }
  //
  //     loading = false;
  //   });
  //   // getdate();
  // }


  Future getbookingreschedule() async {
    // var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });

    var url = Apis.RESCHEDULETIME_API;
    var sp = await SharedPreferences.getInstance();
    // var data = json.encode(map);
    var data = {
      "booking_id": widget.bookingid,
      "time_slot_id": timeid,
      "date": date
    };
    print(data);
    var res = await apiPostRequests(url, data);

    print(res);
    setState(() {
      if (json.decode(res)['status'].toString() == "1") {
        alertDailog("Booking Re-scheduled succesfully");
      } else {
        _showInSnackBarr("Booking Re-scheduled failed");
        // history = [];

      }

      loading = false;
    });

  }

  Future<String> apiPostRequests(String url, data) async {
    //_onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    // Navigator.pop(context);
    return reply;
  }

  var date = "";
  var starttime = "";
  var endTime = "";

  var addressname = "";
  var addressid = "";
  getAddress() async {
    var sp = await SharedPreferences.getInstance();
    if (sp.containsKey("addressid")) {
      setState(() {
        addressname = sp.getString("addressname");
        addressid = sp.getString("addressid");
      });
    } else {}
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      this.getdate();
    });


    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: AppColors.sentColor,
          elevation: 0,
          title: Text(
            "Reschedule Time",
            style: TextStyle(color: AppColors.white),
          ),
        ),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                color: AppColors.white,
                padding:
                EdgeInsets.only(left: 20, right: 10, top: 10, bottom: 10),
                child: Text(
                  "Select the time Slot",
                  style: TextStyle(
                      color: AppColors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 18),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                // padding: EdgeInsets.only(left: 20),
                child: time(),
              ),
            ),
            // SliverToBoxAdapter(
            //   child:  Container(
            //
            //
            //     child: SingleChildScrollView(
            //       child:  ListView(
            //
            //         shrinkWrap: true,
            //         physics: NeverScrollableScrollPhysics(),
            //         children: [
            //           Container(
            //             padding: EdgeInsets.only(left: 20,right: 20),
            //
            //             child: Card(
            //               child: Container(
            //                 color: Colors.white,
            //
            //                 child: Container(
            //                     padding: EdgeInsets.only(
            //                       top: 10,
            //                       right: 20,
            //                       left: 20,
            //                       bottom: 10,
            //                     ),
            //                     child:Column(
            //                       children: [
            //                         Row(
            //                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                           children: [
            //                             Padding(
            //                               padding: EdgeInsets.only(
            //                                   left: 10,
            //                                   right: 10,
            //                                   top: 10
            //                               ),
            //                               child: Text("Service Charger",
            //                                 style: TextStyle(
            //                                     fontSize: 14,
            //                                     color: Colors.black45
            //                                 ),),
            //
            //                             ),
            //                             Text("₹",//+widget.price,
            //                               style: TextStyle(
            //                                   fontSize: 14,
            //                                   color: Colors.black45
            //                               ),)
            //
            //
            //                           ],
            //                         ),
            //                         // Row(
            //                         //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                         //   children: [
            //                         //     Padding(padding: EdgeInsets.only(
            //                         //         left: 10,
            //                         //         right: 10,
            //                         //         top: 10
            //                         //     ),
            //                         //       child: Text("Discount",
            //                         //         style: TextStyle(
            //                         //             fontSize: 14,
            //                         //             color: Colors.black45
            //                         //         ),),
            //                         //     ),
            //                         //     Text("₹500",
            //                         //       style: TextStyle(
            //                         //           fontSize: 14,
            //                         //           color: Colors.black45
            //                         //       ),)
            //                         //
            //                         //   ],
            //                         // ),
            //                         Divider(),
            //                         Row(
            //                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                           children: [
            //                             Padding(padding: EdgeInsets.only(
            //                                 left: 10,
            //                                 right: 10,
            //                                 top: 10
            //                             ),
            //                               child: Text("Total",
            //                                 style: TextStyle(
            //                                     fontSize: 16,
            //                                     fontWeight: FontWeight.bold,
            //                                     color: Colors.black
            //                                 ),),
            //                             ),
            //                             Text("₹",//+widget.price,
            //                             style: TextStyle(
            //                                 fontSize: 16,
            //                                 fontWeight: FontWeight.bold,
            //                                 color: Colors.black
            //                             ),)
            //
            //
            //                           ],
            //                         ),
            //                       ],
            //                     )
            //
            //
            //                 ),
            //
            //               ),
            //             ),
            //           ),
            //           // Container(
            //           //
            //           //   child: Container(
            //           //     // height: 100,
            //           //     child: Container(
            //           //
            //           //       child: ListTile(
            //           //         title: Container(
            //           //           child: TextField(
            //           //             decoration: InputDecoration(
            //           //                 labelText: "Type Coupon code"
            //           //             ),
            //           //           ),
            //           //         ),
            //           //         trailing: FlatButton(
            //           //           onPressed: (){},
            //           //           child: Text(
            //           //             "APPLY",style: TextStyle(
            //           //               color: AppColors.black
            //           //           ),
            //           //           ),
            //           //         ),
            //           //       ),
            //           //
            //           //     ),
            //           //   ),
            //           //
            //           // ),
            //           // Card(
            //           //
            //           //   child: Container(
            //           //       padding: EdgeInsets.only(
            //           //           top: 10,
            //           //           right: 10,
            //           //           left: 10,
            //           //           bottom: 10
            //           //       ),
            //           //       child:Column(
            //           //
            //           //         children: [
            //           //
            //           //           Row(
            //           //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           //             children: [
            //           //               Padding(
            //           //                 padding: EdgeInsets.only(
            //           //                     left: 10,
            //           //                     right: 10,
            //           //                     top: 5
            //           //                 ),
            //           //                 child: Text("Service Name", style: TextStyle(
            //           //                     fontSize: 11,
            //           //                     color: Colors.black45
            //           //                 ),),
            //           //
            //           //               ),
            //           //               Text("Hair Cut"
            //           //                 , style: TextStyle(
            //           //                     fontSize: 11,
            //           //                     color: Colors.black45
            //           //                 ),)
            //           //
            //           //
            //           //             ],
            //           //           ),
            //           //
            //           //
            //           //         ],
            //           //       )
            //           //
            //           //
            //           //   ),
            //           // ),
            //           //Divider(),
            //           // Container(
            //           //     padding: EdgeInsets.only(top: 10,
            //           //         bottom: 5,
            //           //         left: 15),
            //           //
            //           //     child: Text("Payment Option",
            //           //       style:TextStyle (fontSize: 15,
            //           //           color: Colors.black,
            //           //           fontWeight: FontWeight.bold
            //           //
            //           //       ),)
            //           // ),
            //           // Card
            //           //   (
            //           //   child: Container(
            //           //     height: 100,
            //           //       padding: EdgeInsets.only(
            //           //         top: 5,
            //           //         right: 10,
            //           //         left: 10,
            //           //         bottom: 10,
            //           //       ),
            //           //       child:Column(
            //           //         children: [
            //           //
            //           //           InkWell(
            //           //             onTap: ()
            //           //             {
            //           //
            //           //             },
            //           //             child: Row(
            //           //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           //               children: [
            //           //                 Padding(padding: EdgeInsets.only(
            //           //                     left: 5,
            //           //                     right: 10,
            //           //                     top: 10
            //           //                 ),
            //           //
            //           //                   child:
            //           //                   Row(
            //           //                     children: [
            //           //                       Icon(Icons.payment,
            //           //                         size: 20.0,
            //           //                       ),
            //           //                       Padding(padding: EdgeInsets.only(left: 10),),
            //           //                       Text("Cash",
            //           //                         style: TextStyle(
            //           //                             fontSize: 15
            //           //
            //           //                         ),),
            //           //                     ],
            //           //                   ),
            //           //                 ),
            //           //                 Icon(Icons.radio_button_unchecked,
            //           //                   size: 17.0,
            //           //
            //           //                 ),
            //           //
            //           //               ],
            //           //             ),
            //           //           ),
            //           //           Row(
            //           //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           //             children: [
            //           //               Padding(padding: EdgeInsets.only(
            //           //                   left: 5,
            //           //                   right: 10,
            //           //                   top: 10
            //           //               ),
            //           //                 child:
            //           //                 Row(
            //           //                   children: [
            //           //
            //           //                     Icon(Icons.payment,
            //           //                       size: 17.0,
            //           //                     ),
            //           //                     Padding(padding: EdgeInsets.only(left: 10),),
            //           //
            //           //                     Text("Online",style: TextStyle(
            //           //                       fontSize: 15,
            //           //
            //           //                     ),
            //           //                     ),
            //           //                   ],
            //           //                 ),
            //           //               ),
            //           //               Icon(Icons.radio_button_unchecked,
            //           //                 size: 17.0,
            //           //               )
            //           //
            //           //
            //           //             ],
            //           //           ),
            //           //         ],
            //           //       )
            //           //
            //           //
            //           //   ),
            //           // ),
            //
            //
            //         ],
            //
            //       ),
            //
            //     ),
            //   ),
            //
            // ),
          ],
        ),
        bottomNavigationBar: Container(
          height: 75,
          child: Column(
            children: [
              Container(
                height: 65,
                padding: EdgeInsets.only(left: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                  color: AppColors.white,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(right: 10),
                        child: SizedBox(
                          child: InkWell(
                            onTap: () {
                              if (valuefirst == "") {
                                alertDailog("Please Selected  Date");
                              } else {
                                getbookingreschedule();
                              }
                            },
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                color: AppColors.sentColor,
                              ),
                              child: Center(
                                child: Text(
                                  loading == true ? "Loading.." : "Done",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Widget time() {
    return Container(
      child: Column(
        children: [
          Container(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 95,
                  padding:
                  EdgeInsets.only(top: 2, bottom: 0, right: 0, left: 0),
                  alignment: Alignment.centerLeft,
                  color: AppColors.white,
                  child: ListView.builder(
                    padding: EdgeInsets.only(left: 15),
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: records.length,
                    itemBuilder: (context, i) {
                      return Container(
                          padding: EdgeInsets.only(
                              left: 5, right: 5, top: 5, bottom: 5),
                          child: InkWell(
                            onTap: () {
                              _selectTimeSlot(i, records[i]['date'].toString());
                              // _selectTimeSlot(i.toString(),dates[i]);
                              //gettime(records[i],i);
                            },
                            child: Card(
                              color: selectedindex == i
                                  ? AppColors.lightblue
                                  : Colors.white,
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 10, bottom: 10),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 2, bottom: 0),
                                          child: Text(
                                            //records[i]['date'].toString(),
                                            //getdays(dates[i].toString()),
                                            getdays(
                                                records[i]['date'].toString()),
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 2, bottom: 0),
                                          child: Text(
                                            getdates(
                                                records[i]['date'].toString()
                                              //dates[i].toString()
                                            ),
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ));
                    },
                  ),
                )
              ],
            ),
          ),
          Container(
            child: Container(
              padding: EdgeInsets.only(top: 10, bottom: 0, left: 10, right: 10),
              child: GridView.builder(
                  itemCount: times.length != null ? times.length : "",
                  //category.length==null?category.length:"",
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 2.8,
                      crossAxisSpacing: 1,
                      mainAxisSpacing: 1),
                  itemBuilder: (Context, i) {

                    return  times[i]['start_time'].toString() != ""?Container(
                      child: scheduledtime ==
                          times[i]['start_time'].toString()
                          ? Card(
                        color: valuefirst == i.toString()
                            ? AppColors.lightblue
                            : AppColors.white.withOpacity(0.8),
                        child: ListTile(
                          contentPadding: EdgeInsets.only(
                              top: 0, bottom: 0, left: 15, right: 15),
                          title: Center(
                            child: Text(
                              times[i]['start_time'],
                              //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
                              //times[i]['Time'],
                              style: TextStyle(
                                  color: Colors.black38,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          trailing: Checkbox(
                            checkColor: AppColors.black,
                            activeColor: Colors.white,
                            value:
                            valuefirst == i.toString() ? true : false, onChanged: (bool value) {  },
                            // onChanged: (bool value) {
                            //   setState(() {
                            //     valuefirst = i.toString();
                            //     // slectTime(timerecord[i]);
                            //     slectTime(times[i]['start_time'],
                            //         times[i]['end_time'], times[i]['id']);
                            //   });
                            //   //getcheckseat();
                            // },
                          ),

                          // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),
                        ),
                      )
                          : Card(
                        color: valuefirst == i.toString()
                            ? AppColors.lightblue
                            : AppColors.white,
                        child: ListTile(
                          contentPadding: EdgeInsets.only(
                              top: 0, bottom: 0, left: 15, right: 15),
                          title: Center(
                            child: Text(
                              times[i]['start_time'],
                              //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
                              //times[i]['Time'],
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          trailing: Checkbox(
                            checkColor: AppColors.black,
                            activeColor: Colors.white,
                            value:
                            valuefirst == i.toString() ? true : false,
                            onChanged: (bool value) {
                              setState(() {
                                valuefirst = i.toString();
                                // slectTime(timerecord[i]);
                                slectTime(times[i]['start_time'],
                                    times[i]['end_time'], times[i]['id']);
                              });
                              //getcheckseat();
                            },
                          ),

                          // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),
                        ),
                      ),
                    ):Container(
                      child: Stack(
                        children: [
                          Card(
                            color: valuefirst == i.toString()
                                ? AppColors.lightblue
                                : AppColors.white,
                            child: ListTile(
                              contentPadding: EdgeInsets.only(
                                  top: 0, bottom: 0, left: 15, right: 15),
                              title: Center(
                                child: Text(
                                  widget.timeslot
                                  ,
                                  //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
                                  //times[i]['Time'],
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),


                              // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),
                            ),
                          ),
                          Positioned(

                              child: Container(
                                margin: EdgeInsets.all(5),

                                color: AppColors.grayText,
                              ))
                        ],
                      ),

                    );
                  }),
            ),
            // child: Column(
            //
            //   // mainAxisAlignment: MainAxisAlignment.start,
            //   children: [
            //
            //     Container(
            //       // height : 180,
            //       padding: EdgeInsets.only(
            //           top: 2,
            //           bottom: 0,
            //           right: 20,
            //           left: 20
            //
            //       ),
            //       alignment: Alignment.centerLeft,
            //       color: Colors.white12,
            //       child: ListView.builder(
            //         padding: EdgeInsets.only(
            //             top : 10
            //         ),
            //         physics: NeverScrollableScrollPhysics(),
            //         shrinkWrap: true,
            //         itemCount:
            //         //timerecord.length,
            //         times.length,
            //         //timelen,
            //         itemBuilder: (context, i){
            //           return Container(
            //             // padding : EdgeInsets.only(
            //             //   bottom: 10
            //             // ),
            //             child:
            //             Row(
            //               children: [
            //                 Flexible(
            //                   flex: 1,
            //                   child: Card(
            //                     color: Color(0xFFFFFFFFF),
            //                     child: ListTile(
            //                       contentPadding: EdgeInsets.only(
            //                           top : 0,
            //                           bottom: 0,
            //                           left: 15,
            //                           right : 15
            //                       ),
            //                       title: Text(times[i]['start_time'],
            //                           //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
            //                         //times[i]['Time'],
            //                         style: TextStyle(
            //                             color: Colors.black,
            //                             fontSize: 13,
            //                             fontWeight: FontWeight.bold
            //                         ),
            //
            //                       ),
            //                       trailing:  Checkbox(
            //                         checkColor: AppColors.black,
            //                         activeColor: Colors.white,
            //                         value: valuefirst == i.toString()?true:false,
            //                         onChanged: (bool value) {
            //                           setState(() {
            //                             valuefirst = i.toString();
            //                             // slectTime(timerecord[i]);
            //                             slectTime(times[i]['start_time'],times[i]['end_time'],times[i]['id']);
            //                           });
            //                           //getcheckseat();
            //                         },
            //                       ),
            //
            //
            //                       // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),
            //
            //                     ),
            //                   ),
            //                 ),
            //                 Flexible(
            //                   flex: 1,
            //                   child: Card(
            //                     color: Color(0xFFFFFFFFF),
            //                     child: ListTile(
            //                       contentPadding: EdgeInsets.only(
            //                           top : 0,
            //                           bottom: 0,
            //                           left: 15,
            //                           right : 15
            //                       ),
            //                       title: Text(times[i]['start_time'],
            //                         //+" to "+times[i]['end_time'],//times[i]['start']+" to "+times[i]['end'],
            //                         //times[i]['Time'],
            //                         style: TextStyle(
            //                             color: Colors.black,
            //                             fontSize: 13,
            //                             fontWeight: FontWeight.bold
            //                         ),
            //
            //                       ),
            //                       trailing:  Checkbox(
            //                         checkColor: AppColors.black,
            //                         activeColor: Colors.white,
            //                         value: valuefirst == i.toString()?true:false,
            //                         onChanged: (bool value) {
            //                           setState(() {
            //                             valuefirst = i.toString();
            //                             // slectTime(timerecord[i]);
            //                             slectTime(times[i]['start_time'],times[i]['end_time'],times[i]['id']);
            //                           });
            //                           //getcheckseat();
            //                         },
            //                       ),
            //
            //
            //                       // leading: Icon(Icons.access_time,color: AppColors.redcolor3,),
            //
            //                     ),
            //                   ),
            //                 ),
            //               ],
            //             ),
            //           );
            //         },
            //
            //
            //       ),
            //
            //     )
            //   ],
            // ),
          ),
        ],
      ),
    );
  }

  getdates(String text) {
    var string = text;

    // var now = new DateTime.now();
    var formatter = new DateFormat('E');
    String formatted = formatter.format(DateTime.parse(string));
    return formatted.toUpperCase();
  }

  getdays(String text) {
    var string = text;
//    var now = new DateTime.now();
    var formatter = new DateFormat('MMM d');
    String formatted = formatter.format(DateTime.parse(string));
    return formatted.toUpperCase();
  }
}
