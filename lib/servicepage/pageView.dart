import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class Pageview extends StatefulWidget{

  final pagename;
  final pagedetail;

  Pageview({this.pagename,this.pagedetail});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageviewLoader(pagename: this.pagename, pagedetail: this.pagename);
  }

}

class PageviewLoader extends State<Pageview>{

  String pagename;
  String pagedetail;

  PageviewLoader({this.pagename,this.pagedetail});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pagename),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            left: 20,
            right : 20,
            top: 10,
            bottom: 20
          ),
          child: Html(
            data: widget.pagedetail,
          ),
        ),
      ),
    );
  }

}