import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yesmadam/animators/navianimator.dart';
import 'package:yesmadam/color/AppColors.dart';
import 'package:yesmadam/pages/login.dart';
import 'package:yesmadam/services/apis.dart';
import 'package:flutter_html/flutter_html.dart';

class ServicePageviews extends StatefulWidget {
  // final serviceid;
  // final catid;
  final url;

  ServicePageviews(this.url);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ServiceLoaderpage();
  }
}

class ServiceLoaderpage extends State<ServicePageviews> {
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  new Container(
                    padding: EdgeInsets.only(left: 30),
                    child: new Text("Please wait..."),
                  )
                ],
              )),
        );
      },
    );
  }

  alertDailog(text) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Alert"),
            content: Text(text),
            actions: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        });
  }

  // stackServices() async {
  //   print(widget.servicelist.length);
  //   for (var i = 0; i < widget.servicelist.length; i++) {
  //     for (var j = 0;
  //         j < widget.servicelist[i]['data']['service'].length;
  //         j++) {
  //       setState(() {
  //         services.add(widget.servicelist[i]['data']['service'][j]);
  //       });
  //       print(services.length);
  //     }
  //   }
  // }
  var services = "";
  var serviceids = "";
  var prices = "";
  var img = "";
  var prodimag = "";
  var dur = "";
  var descr = "";
  var cartstaus = "";
  var detail = "";
  var subcat = "";
  var serid = "";
  var price = "";
  var linkshare="";
  List heading=[];
  List description=[];
  var catid="";


  bool loading = false;
  bool ani = false;
  Color _color = Colors.black.withOpacity(0.3);
  bool fail;
  Future getservicedetail() async {
    var sp = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      ani = false;
      fail = false;
      _color = Colors.black.withOpacity(0.3);
      // Navigator.pop(context);
    });
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");

      var url = widget.url+"/$user";
      // var data = {"user_id": user, "service_id": widget.serviceid};

      var res = await apiPostRequests(url);
      print(res);

      setState(() {
        if (json.decode(res)['status'].toString() == "1") {
          services = json.decode(res)['data']['tittle'];
          img = json.decode(res)['data']['images'];
          prodimag = json.decode(res)['data']['product_images'];
          prices = json.decode(res)['data']['new_price'];
          dur = json.decode(res)['data']['duration'];
          descr = json.decode(res)['data']['description'];
          cartstaus = json.decode(res)['data']['in_cart'];
          // detail=json.decode(res)['data']['details'];
          subcat = json.decode(res)['data']['sub_sub_cat'];
          serid = json.decode(res)['data']['id'];
          linkshare=json.decode(res)['data']['share_url'];
          heading=json.decode(res)['data']['details']['heading'];
          description=json.decode(res)['data']['details']['description'];
          catid=json.decode(res)['data']['cat_id'];
        } else {
          // services = [];

        }
      });

    }
    else {
      setState(() {
        loading = true;
        ani = false;
        fail = false;
        _color = Colors.black.withOpacity(0.3);
        // Navigator.pop(context);
      });

      var url = widget.url;

      // var data = {"service_id": widget.serviceid};

      var res = await apiPostRequests(url);

      print(res);
      setState(() {
        if (json.decode(res)['status'].toString() == "1") {
          services = json.decode(res)['data']['tittle'];
          img = json.decode(res)['data']['images'];
          prodimag = json.decode(res)['data']['product_images'];
          prices = json.decode(res)['data']['new_price'];
          dur = json.decode(res)['data']['duration'];
          descr = json.decode(res)['data']['description'];
          cartstaus = json.decode(res)['data']['in_cart'].toString();
          //  detail=json.decode(res)['data']['details']['heading'];
          subcat = json.decode(res)['data']['sub_sub_cat'];
          serid = json.decode(res)['data']['id'];
          linkshare=json.decode(res)['data']['share_url'];
          heading=json.decode(res)['data']['details']['heading'];
          description=json.decode(res)['data']['details']['description'];
          catid=json.decode(res)['data']['cat_id'];

        } else {
          //services = [];

        }
        loading = false;
      });
    }
  }

  Future<String> apiPostRequests(String url) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    // request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getAddtocart(serviceids, price) async {
    //print("checking for login");
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    // if (sp.containsKey("userid")) {
    //   var user = sp.getString("userid");
    //   var url = Apis.ADDTOCART_API;
    //   var data = {
    //     "service_id": serviceids,
    //     "user_id": user,
    //     "qty": "1",
    //     "amount": prices
    //   };
    //   print(data);
    //   var res = await apiPostRequestss(url, data);
    //   print(res);
    //   if (json.decode(res)['status'].toString() == "1") {
    //     setState(() {
    //       loading = false;
    //     });
    //   } else {
    //     setState(() {
    //       loading = false;
    //     });
    //   }
    //   getservicedetail();
    // }
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.ADDTOCART_API;
      var data = {
        "service_id":serviceids,
        "user_id":  user,
        //sp.getString("userid"),
        "qty":"1",
        "amount"  :price ,
        "cat_id":catid

      };

      var res = await apiPostRequestss(url,data);
      print(res);
      print(data);

      if (json.decode(res)['status'].toString() == "1")
      {
        setState(() {

          loading = false;
        });
      }
      else
      {
        setState(() {
          loading = false;
        });
      }
      getservicedetail();
    }
    else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
  }

  Future<String> apiPostRequestss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  Future getRemovetocart(serviceids, price) async {
    print("checking for login");
    var sp = await SharedPreferences.getInstance();
    // sp.containsKey("userid");
    if (sp.containsKey("userid")) {
      var user = sp.getString("userid");
      var url = Apis.REMOVETOCART_API;
      var data = {
        "service_id":serviceids,
        "user_id":  sp.getString("userid"),
        "cat_id":catid
      };
      print(data);
      var res = await apiPostRequestsss(url, data);
      print(res);
      if (json.decode(res)['status'].toString() == "1") {
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
      getservicedetail();
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          SlideTopRoute(page: LoginLoader()), (Route<dynamic> route) => false);
    }
  }

  Future<String> apiPostRequestsss(String url, data) async {
    _onLoading();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    // request.headers.set('api-key' , Apis.LOGIN_API);
    request.add(utf8.encode(json.encode(data)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    Navigator.pop(context);
    return reply;
  }

  @override
  void initState() {
    getservicedetail();
    // alertDailog("text");
    //stackServices();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          //backgroundColor: AppColors.black,
          // centerTitle: true,
            title: Container(
              width: 120,
              child: Text(
                subcat,
                style: TextStyle(color: AppColors.white),
              ),
            ),
            leading: IconButton(
              icon: Icon(
                Icons.close_sharp,
                size: 25,
                color: AppColors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            actions: [
              IconButton(
                onPressed: ()
                {
                  Share.share(
                      linkshare.toString());
                  //Navigator.push(context, MaterialPageRoute(builder: (context)=>PersonalLoader()));
                },

                icon: Icon(
                  Icons.share,
                  color: AppColors.white,
                ),
              ),
            ],
            elevation: 0),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                // height: MediaQuery.of(context).size.height,
                  color: AppColors.white, //,
                  //height: 50,
                  child: listitems()),
            ),
            //   ],
            // ),
            // Positioned(
            // bottom: 0,
            // right: 0,
            // child: FlatButton(
            // onPressed: ()
            //
            // {
            //
            //
            //
            //
            // if(services[index]['in_cart']==false){
            // serviceids = services[index]['id'];
            // prices = services[index]['new_price'];
            // getAddtocart(serviceids,prices);
            // stackServices();
            //
            // }
            // else{
            // serviceids = services[index]['id'];
            // prices = services[index]['new_price'];
            // getRemovetocart(serviceids,prices);
            // stackServices();
            //
            //
            // };
            //
            // },
            // child:
            // services[index]['in_cart']==true?Container(
            // height: 30,
            // width: 120,
            // decoration: BoxDecoration(
            // color: AppColors.redcolor3,
            // borderRadius:
            // BorderRadius.all(Radius.circular(12)),
            // ),
            //
            // child: Center(
            // child: Text(
            // "Remove From Cart",
            // style: TextStyle(color: AppColors.black),
            // )),
            // // backgroundColor: Colors.green,
            // ):
            // Container(
            // height: 30,
            // width: 90,
            // decoration: BoxDecoration(
            // color: AppColors.sentColortransparent,
            // borderRadius:
            // BorderRadius.all(Radius.circular(12)),
            // ),
            //
            // child: Center(
            // child: Text(
            // "Add To Cart",
            // style: TextStyle(color: AppColors.black),
            // )),
            // // backgroundColor: Colors.green,
            // )),
            // )
          ],
        ),
        floatingActionButton: FlatButton(
            onPressed: () {
              if (cartstaus == "0") {
                serviceids = serid;
                price = prices;
                getAddtocart(serviceids, price);
                getservicedetail();
                // getCartlist();
              } else {
                serviceids = serid;
                price = prices;
                getservicedetail();
                getRemovetocart(serviceids, price);

                // getCartlist();

              }

              //
            },
            child: cartstaus == "false"
                ?Container(
                height: 30,
                width: 90,
                decoration: BoxDecoration(
                  color: AppColors.sentColortransparent,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                ),
                child: Center(
                    child: Text(
                      "Add To Cart",
                      style: TextStyle(color: AppColors.black),
                    )))
                : cartstaus == "0"
                ? Container(
                height: 30,
                width: 90,
                decoration: BoxDecoration(
                  color: AppColors.sentColortransparent,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                ),
                child: Center(
                    child: Text(
                      "Add To Cart",
                      style: TextStyle(color: AppColors.black),
                    )))
                : Container(
                height: 30,
                width: 100,
                decoration: BoxDecoration(
                  color: AppColors.sentColortransparent,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                ),
                child: Center(
                  child: Text(
                    "Remove To Cart",
                    style: TextStyle(color: AppColors.black),
                  ),
                  // backgroundColor: Colors.green,
                ))));
  }

  Widget listitems() {
    return Container(
      //color: AppColors.black,
        child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 1, //services.length,
            itemBuilder: (context, i) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    //padding: EdgeInsets.only(top: 20),

                    height: 180,
                    // width: MediaQuery
                    //     .of(context)
                    //     .size
                    //     .width /
                    //     2.5,
                    decoration: BoxDecoration(
                      //color: AppColors.primary,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        image: DecorationImage(
                            image: NetworkImage(img), fit: BoxFit.cover)),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 10, right: 5),
                    //color: AppColors.sentColor,
                    child: Text(
                      services,
                      style: TextStyle(
                          color: AppColors.segcolor2,
                          fontWeight: FontWeight.w600,
                          fontSize: 16),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: 10, left: 10, right: 10, bottom: 10),
                    //color: AppColors.sentColor,
                    child: Text(
                      subcat, //+services[i].description,
                      style: TextStyle(color: AppColors.grayText),
                    ),
                  ),
                  Container(height: 1, color: AppColors.grayBorder),
                  Container(
                    padding: EdgeInsets.only(
                        top: 10, right: 10, left: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(
                                          right: 5, left: 0, bottom: 5, top: 0),
                                      child: Icon(
                                        Icons.watch_later_outlined,
                                        color: AppColors.sentColor,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                          right: 10, left: 0, bottom: 5, top: 0),
                                      child: Text(
                                        dur + "  minute",
                                        // services[i].duration+" min",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w200),
                                        textAlign: TextAlign.start,
                                      ),
                                    )
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      right: 0, left: 0, bottom: 5, top: 0),
                                  child: Text(
                                    "₹ " + prices, //+services[i].oldPrice,
                                    style: TextStyle(
                                      color: AppColors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      // decoration: TextDecoration.lineThrough,
                                    ),
                                    textAlign: TextAlign.start,
                                  ),
                                )
                              ],
                            )),
                        Container(
                          height: 100,
                          child: Image(
                            image: NetworkImage(prodimag),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(right: 10, left: 10),
                      height: 1,
                      color: AppColors.grayBorder),
                  Container(
                    padding: EdgeInsets.only(
                        top: 10, left: 10, right: 10, bottom: 10),
                    //color: AppColors.sentColor,
                    child: Html(
                      data: descr,
                      useRichText: true,
                      // Text(
                      //   description[i]['description_key'], //+services[i].description,
                      //   style: TextStyle(
                      //     color: AppColors.black,
                      //   ),
                      // ),
                    ),
                    // Text(
                    //   descr,
                    //   style: TextStyle(
                    //       color: AppColors.black, fontWeight: FontWeight.w400),
                    // ),
                  ),
                  Container(
                    color: AppColors.white,
                    padding: EdgeInsets.only(top:10,left:10,bottom: 10),
                    child:  ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount:heading.length,
                      //cartlist.length,
                      //cartlist.length != null ?cartlist.length:"",
                      //services.length,
                      itemBuilder: (BuildContext context, int i) {
                        return
                          Container(
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,

                              children: [
                                Container(
                                  padding: EdgeInsets.only(
                                      top: 10, left: 2, right: 10, bottom: 5),
                                  //color: AppColors.sentColor,
                                  child: Text(
                                    heading[i]['heading_key'], //+services[i].description,
                                    style: TextStyle(
                                        color: AppColors.priceControl,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),

                                Container(
                                  padding: EdgeInsets.only(
                                      left: 2, right: 10, top: 0, bottom: 5),
                                  child:Html(
                                    data: description[i]['description_key'],
                                    useRichText: true,
                                    // Text(
                                    //   description[i]['description_key'], //+services[i].description,
                                    //   style: TextStyle(
                                    //     color: AppColors.black,
                                    //   ),
                                    // ),
                                  ),),
                                Container(
                                    padding: EdgeInsets.only(right: 10, left: 10),
                                    height: 1,
                                    color: AppColors.grayBorder),



                              ],
                            ),
                          );

                      },
                    ),),
                  // Container(
                  //   padding: EdgeInsets.only(
                  //       top: 10, left: 10, right: 10, bottom: 10),
                  //   //color: AppColors.sentColor,
                  //   child: Text(
                  //     "THINGS TO KNOW", //+services[i].description,
                  //     style: TextStyle(
                  //         color: AppColors.priceControl,
                  //         fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  // Container(
                  //     padding: EdgeInsets.only(right: 10, left: 10),
                  //     height: 1,
                  //     color: AppColors.grayBorder),
                  // Container(
                  //   padding: EdgeInsets.only(
                  //       left: 10, right: 10, top: 10, bottom: 10),
                  //   child: Text(
                  //     "•	Use of mono sachets", //+services[i].description,
                  //     style: TextStyle(
                  //       color: AppColors.black,
                  //     ),
                  //   ),
                  // ),
                  // Container(
                  //   padding: EdgeInsets.only(
                  //       top: 10, left: 10, right: 10, bottom: 10),
                  //   //color: AppColors.sentColor,
                  //   child: Text(
                  //     "-: DOS Services", //+services[i].description,
                  //     style: TextStyle(
                  //         color: AppColors.priceControl,
                  //         fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  // Container(
                  //     padding: EdgeInsets.only(right: 10, left: 10),
                  //     height: 1,
                  //     color: AppColors.grayBorder),
                  // Container(
                  //   padding: EdgeInsets.only(
                  //       left: 10, right: 10, top: 10, bottom: 10),
                  //   child: Text(
                  //     " Benefits",
                  //     //+services[i].description,
                  //     style: TextStyle(
                  //       fontSize: 14,
                  //       color: AppColors.black75,
                  //     ),
                  //   ),
                  // ),
                ],
              );
            }));
  }

  Widget picture() {
    return Container(
      child: Container(
        //padding: EdgeInsets.only(top: 20),

        height: 150,
        // width: MediaQuery
        //     .of(context)
        //     .size
        //     .width /
        //     2.5,
        decoration: BoxDecoration(
          //color: AppColors.primary,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            image: DecorationImage(image: NetworkImage(""), fit: BoxFit.cover)),
      ),
    );
  }
}
