
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:yesmadam/modals/get_categories_model.dart';

abstract class CategoryState extends Equatable{

}

class CategoryInitialState extends CategoryState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CategoryLoadingState extends CategoryState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CategoryLoadedState extends CategoryState{

  List<Categories> category;

  CategoryLoadedState({@required this.category});

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CategoryErrorState extends CategoryState{

  String msg;
  CategoryErrorState({@required this.msg});

  @override
  // TODO: implement props
  List<Object> get props => [];
}