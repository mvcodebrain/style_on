import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yesmadam/bloc/service/sersubcat_event.dart';
import 'package:yesmadam/bloc/service/sersubcat_state.dart';

import 'package:yesmadam/modals/sersubcat_model.dart';
import 'package:yesmadam/respos/servicesubcat_repo.dart';




class SersubcategoryBloc extends Bloc<SersubcategoryEvent,SersubcategoryState>{

  ServsubcategoryRepos repos;

  SersubcategoryBloc({@required this.repos}) : super(null);

  @override
  //TODO: implement initialState
  SersubcategoryState get initialSate => SersubcategoryInitialState();

  @override
  Stream<SersubcategoryState> mapEventToState(SersubcategoryEvent event) async*{
    // TODO: implement mapEventToState
    if(event is FetchSersubcategoryEvent){
      yield SersubcategoryLoadingState(); 
      try{
        List<Data> sercats = await repos.getServices(event.catid,event.subid);
        if(sercats.length == null){
           yield SersubcategoryErrorState(msg : "Data Must Not Blank");
        }else{
          yield SersubcategoryLoadedState(services: sercats);
        }
        
      }catch (e){
        yield SersubcategoryErrorState(msg : e.toString());
      }
    }
  }
}