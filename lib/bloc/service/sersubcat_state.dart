import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'package:yesmadam/modals/sersubcat_model.dart';

abstract class SersubcategoryState extends Equatable{

}

class SersubcategoryInitialState extends SersubcategoryState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SersubcategoryLoadingState extends SersubcategoryState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SersubcategoryLoadedState extends SersubcategoryState{

  List<Data> services;

  SersubcategoryLoadedState({@required this.services});

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SersubcategoryErrorState extends SersubcategoryState{

  String msg;
  SersubcategoryErrorState({@required this.msg});

  @override
  // TODO: implement props
  List<Object> get props => [];
}