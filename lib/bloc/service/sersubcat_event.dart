import 'package:equatable/equatable.dart';

abstract class SersubcategoryEvent extends Equatable{

}

class FetchSersubcategoryEvent extends SersubcategoryEvent{
  final catid;
  final subid;
  FetchSersubcategoryEvent({this.catid,this.subid});
  @override
  // TODO: implement props
  List<Object> get props => null;
}