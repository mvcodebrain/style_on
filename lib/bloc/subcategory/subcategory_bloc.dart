
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yesmadam/bloc/subcategory/subcategory_event.dart';
import 'package:yesmadam/bloc/subcategory/subcategory_state.dart';
import 'package:yesmadam/modals/subcategory_model.dart';
import 'package:yesmadam/respos/subcategories_repo.dart';



class SubcategoryBloc extends Bloc<SubcategoryEvent,SubcategoryState>{

  SubcategoryRepos repos;
  final catid;

  SubcategoryBloc({@required this.repos,this.catid}) : super(null);

  @override
  //TODO: implement initialState
  SubcategoryState get initialSate => SubcategoryInitialState();

  @override
  Stream<SubcategoryState> mapEventToState(SubcategoryEvent event) async*{
    // TODO: implement mapEventToState
    if(event is FetchSubcategoryEvent){
      yield SubcategoryLoadingState(); 
      try{
        List<SubCategory> subcats = await repos.getSubcategory(this.catid);
        yield SubcategoryLoadedState(subcats: subcats);
      }catch (e){
        yield SubcategoryErrorState(msg : e.toString());
      }
    }
  }
}