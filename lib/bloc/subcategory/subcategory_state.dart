
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:yesmadam/modals/subcategory_model.dart';

abstract class SubcategoryState extends Equatable{

}

class SubcategoryInitialState extends SubcategoryState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SubcategoryLoadingState extends SubcategoryState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SubcategoryLoadedState extends SubcategoryState{

  List<SubCategory> subcats;

  SubcategoryLoadedState({@required this.subcats});

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SubcategoryErrorState extends SubcategoryState{

  String msg;
  SubcategoryErrorState({@required this.msg});

  @override
  // TODO: implement props
  List<Object> get props => [];
}