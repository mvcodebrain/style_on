
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yesmadam/bloc/product/product_event.dart';
import 'package:yesmadam/bloc/product/product_state.dart';
import 'package:yesmadam/modals/product_model.dart';

import 'package:yesmadam/respos/product_repos.dart';



// class ProductBloc extends Bloc<ProductEvent,ProductState>{
//
//   ProductRepos repos;
//
//   ProductBloc({@required this.repos}) : super(null);
//
//   @override
//   //TODO: implement initialState
//   ProductState get initialSate => ProductInitialState();
//
//   @override
//   Stream<ProductState> mapEventToState(ProductEvent event) async*{
//     // TODO: implement mapEventToState
//     if(event is FetchProductEvent){
//       yield ProductLoadingState();
//       try{
//         List<Productlist> productlist = await repos.getProduct();
//
//         yield ProductLoadedState(productlist:productlist);
//       }catch (e){
//         yield ProductErrorState(msg : e.toString());
//       }
//     }
//   }
//
// }