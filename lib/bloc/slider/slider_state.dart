
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:yesmadam/modals/slider_model.dart';

abstract class SliderState extends Equatable{

}

class SliderInitialState extends SliderState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SliderLoadingState extends SliderState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SliderLoadedState extends SliderState{

  List<Sliders> slider;

  SliderLoadedState({@required this.slider});

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SliderErrorState extends SliderState{

  String msg;
  SliderErrorState({@required this.msg});

  @override
  // TODO: implement props
  List<Object> get props => [];
}