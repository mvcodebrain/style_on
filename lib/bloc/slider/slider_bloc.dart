
// import 'package:flutter/cupertino.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'slider_event.dart';
// import 'slider_state.dart';
// import 'package:yesmadam/respos/slider_repo.dart';
// import 'package:yesmadam/modals/slider_model.dart';
//
//
//
//
// class SliderBloc extends Bloc<SliderEvent,SliderState>{
//
//   SliderRepos repos;
//
//   SliderBloc({@required this.repos}) : super(null);
//
//   @override
//   //TODO: implement initialState
//   SliderState get initialSate => SliderInitialState();
//
//   @override
//   Stream<SliderState> mapEventToState(SliderEvent event) async*{
//     // TODO: implement mapEventToState
//     if(event is FetchSliderEvent){
//       yield SliderLoadingState();
//       try{
//         List<Sliders> sliders = await repos.getSlider();
//         yield SliderLoadedState(slider: sliders);
//       }catch (e){
//         yield SliderErrorState(msg : e.toString());
//       }
//     }
//   }
//
// }